package bundles;


import java.util.ListResourceBundle;
import javafx.scene.image.Image;

/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CoreResources extends ListResourceBundle{
    
    private static final Image ICON_ERROR;
    private static final Image ICON_WARNING;
    private static final Image ICON_QUESTION;
    private static final Image ICON_INFO;
    
   private static final Object[][] RESOURCES;
    
    static {
        ICON_ERROR = new Image(CoreResources.class.getClassLoader().getResourceAsStream("img/icon-error.png"));
        ICON_WARNING = new Image(CoreResources.class.getClassLoader().getResourceAsStream("img/icon-warning.png"));
        ICON_QUESTION = new Image(CoreResources.class.getClassLoader().getResourceAsStream("img/icon-question.png"));
        ICON_INFO = new Image(CoreResources.class.getClassLoader().getResourceAsStream("img/icon-info.png"));
        
        RESOURCES = new Object[][]{
            {"demux.jfx.icon.error", ICON_ERROR},
            {"demux.jfx.icon.warning", ICON_WARNING},
            {"demux.jfx.icon.question", ICON_QUESTION},
            {"demux.jfx.icon.info", ICON_INFO}
        };
    }

    @Override
    protected Object[][] getContents() {
        return RESOURCES;
    }
    
}
