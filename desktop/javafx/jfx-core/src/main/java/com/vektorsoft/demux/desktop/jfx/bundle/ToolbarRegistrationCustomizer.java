/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2015 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.desktop.gui.DMXToolbarComponent;
import com.vektorsoft.demux.desktop.jfx.app.gui.PlatformThreadEventHandler;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ToolbarRegistrationCustomizer implements ServiceTrackerCustomizer<DMXToolbarComponent, DMXToolbarComponent<Node>> {

    private final BundleContext bundleContext;
    private final HBox toolbarContainer;
    private final Map<String, ToolBar> registeredToolbars;
    private final Map<String, Set<Node>> deferredNodes;
    private final NodeIndexComparator comparator;
    private final Map<String, Integer> indexMap;
    private final DMXAdapter adapter;
    private final DMXEventBus eventBus;
    private final Map<DMXToolbarComponent, PlatformThreadEventHandler> eventHandlers;

    public ToolbarRegistrationCustomizer(BundleContext ctx, HBox container, DMXAdapter adapter, DMXEventBus eventbus) {
        this.bundleContext = ctx;
        this.toolbarContainer = container;
        this.adapter = adapter;
        registeredToolbars = new HashMap<>();
        deferredNodes = new HashMap<>();
        comparator = new NodeIndexComparator();
        indexMap = new HashMap<>();
        this.eventBus = eventbus;
        this.eventHandlers = new HashMap<>();
    }

    @Override
    public DMXToolbarComponent<Node> addingService(ServiceReference<DMXToolbarComponent> sr) {
        DMXToolbarComponent<Node> component = bundleContext.getService(sr);
        String parentId = component.getParentId();
        String compId = component.getComponentId();
        if (compId == null || compId.isEmpty()) {
            throw new IllegalArgumentException("Missing ID in DMXToolbarComponent implementation. Does method getComponentId() return unique string?");
        }

        Node node = component.getComponent();
        if (node.getId() == null || node.getId().isEmpty() || !compId.equals(node.getId())) {
            throw new IllegalArgumentException("Invalid ID for component" + node + ". It must match " + compId);
        }

        indexMap.put(compId, component.preferredIndex());

        Platform.runLater(() -> {
            if (parentId == null && (node instanceof ToolBar)) {
                // check if there are deferred components waiting for this one
                if (deferredNodes.containsKey(compId)) {
                    ((ToolBar) node).getItems().addAll(deferredNodes.get(compId));
                    ((ToolBar) node).getItems().sort(comparator);
                }
                toolbarContainer.getChildren().add(node);
                toolbarContainer.getChildren().sort(comparator);
                registeredToolbars.put(compId, (ToolBar) node);

            } else {
                ToolBar parent = registeredToolbars.get(parentId);
                if (parent != null) {
                    parent.getItems().add(node);
                    parent.getItems().sort(comparator);
                } else {
                    addDeferredNode(parentId, node);
                }
            }
        });

        if (component instanceof DMXAdapterAware) {
            ((DMXAdapterAware) component).setAdapter(adapter);
        }
        if(component instanceof DMXEventHandler){
            PlatformThreadEventHandler peh = new PlatformThreadEventHandler((DMXEventHandler)component);
            eventHandlers.put(component, peh);
            eventBus.subscribe(peh);
        }

        return component;
    }

    @Override
    public void modifiedService(ServiceReference<DMXToolbarComponent> sr, DMXToolbarComponent<Node> t) {

    }

    @Override
    public void removedService(ServiceReference<DMXToolbarComponent> sr, DMXToolbarComponent<Node> t) {
        String compId = t.getComponentId();
        String parentId = t.getParentId();
        indexMap.remove(compId);

        Platform.runLater(() -> {
            if (parentId != null) {
                ToolBar tb = registeredToolbars.get(parentId);
                if (tb != null) {
                    tb.getItems().remove(t.getComponent());
                }
            } else {
                toolbarContainer.getChildren().remove(t.getComponent());
            }
        });
        if(t instanceof DMXEventHandler && eventHandlers.containsKey(t)){
            eventBus.unsubscribe(eventHandlers.get(t));
            eventHandlers.remove(t);
        }
    }

    private void addDeferredNode(String parentId, Node node) {
        if (deferredNodes.containsKey(parentId)) {
            deferredNodes.get(parentId).add(node);
        } else {
            Set<Node> items = new HashSet<>();
            items.add(node);
            deferredNodes.put(parentId, items);
        }
    }

    private class NodeIndexComparator implements Comparator<Node> {

        @Override
        public int compare(Node o1, Node o2) {
            int i1 = -1;
            if (o1.getId() != null) {
                i1 = indexMap.get(o1.getId());
            }
            if (i1 < 0) {
                i1 = Integer.MAX_VALUE;
            }
            int i2 = -1;
            if (o2.getId() != null) {
                i2 = indexMap.get(o2.getId());
            }

            if (i2 < 0) {
                i2 = Integer.MAX_VALUE;
            }
            return Integer.compare(i1, i2);
        }

    }

}
