/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class AdapterAwareTrackerCustomizer implements ServiceTrackerCustomizer<DMXAdapterAware, DMXAdapterAware>{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(AdapterAwareTrackerCustomizer.class);
    
    private final DMXAdapter adapter;
    private final BundleContext bundleContext;
    
    public AdapterAwareTrackerCustomizer(DMXAdapter adapter, BundleContext ctx){
        this.adapter = adapter;
        this.bundleContext = ctx;
    }

    @Override
    public DMXAdapterAware addingService(ServiceReference<DMXAdapterAware> sr) {
        DMXAdapterAware aware = bundleContext.getService(sr);
        aware.setAdapter(adapter);
        LOGGER.debug("Injected adapter to " + aware);
        
        return aware;
    }

    @Override
    public void modifiedService(ServiceReference<DMXAdapterAware> sr, DMXAdapterAware t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXAdapterAware> sr, DMXAdapterAware t) {
        
    }

}
