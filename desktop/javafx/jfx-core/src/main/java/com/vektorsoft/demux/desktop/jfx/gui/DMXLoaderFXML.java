/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.gui;

import com.vektorsoft.demux.core.resources.impl.DMXResourceBundle;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * <p>
 * Custom implementation of {@code FXMLLoader} for usage within DEMUX Framework. This class is
 * integrated with resources system, and provides support for dynamic locale changes and
 * GUI updating.
 * </p>
 * <p>
 *  In order to keep GUI strings in sync with locale changes, each  component which references
 * external string must also contain ID. This ID is then used to lookup a component when GUI needs
 * to be updated. Note the code snippet bellow:
 * </p>
 * 
 * @author Vladimir Djurovic
 */
public class DMXLoaderFXML extends FXMLLoader {
    
    /** Mapping of IDs to GUI properties and values. */
    private final Map<String, Map<String, String>> propertyMap;
    
    /** URL of FXML file. */
    private final URL fxmlFileUrl;
    
    /** Resource manager. */
    private final DefaultResourceHandler resourceHnadler;

   
    /**
     * Creates new instance.
     * 
     * @param fxmlFileUrl URL of FXML file to process
     * @param resourceHandler  resource handler to use
     */
    public DMXLoaderFXML(URL fxmlFileUrl, DefaultResourceHandler resourceHandler){
        this.fxmlFileUrl = fxmlFileUrl;
        propertyMap = new HashMap<>();
        this.resourceHnadler = resourceHandler;
        parseFxml();
    }

    /**
     * Loads FXML object from URL specified in constructor.
     * 
     * @return valid JavaFX UI object
     * @throws IOException if an error occurs
     */
    @Override
    public Object load() throws IOException {
        setResources(new DMXResourceBundle(resourceHnadler));
        return super.load(fxmlFileUrl.openStream());
    }

    /**
     * This implementation will ignore supplied {@code in} argument and invoke {@link #load() } method.
     * It will use URL specified in the constructor to load FXML document.
     * 
     * @param in input stream (ignored)
     * @return valid JavaFX UI object
     * @throws IOException  if an error occurs
     */
    @Override
    public Object load(InputStream in) throws IOException {
        return load();
    }
    
    /**
     * Updates node hierarchy with current values from resource bundles.
     * 
     * @param node hierarchy root node
     */
    public void updateNodeHierarchy(Node node){
        for(String id : propertyMap.keySet()){
            Node target = node.lookup("#" + id);
            if(target != null){
                updateProperties(target, id);
                // special handling for popup controls
                if(target instanceof Control){
                    Tooltip tl = ((Control)target).getTooltip();
                    if(tl != null && propertyMap.containsKey(tl.getId())){
                        updateProperties(tl, tl.getId());
                    }
                }
                
            }
        }
    }
    
    /**
     * Updates properties of a control with specified ID.
     * 
     * @param target control object
     * @param id control ID
     */
    private void updateProperties(Object target, String id) {
        Map<String, String> data = propertyMap.get(id);
        for (Map.Entry<String, String> entry : data.entrySet()) {
            String key = entry.getKey();
            // find correct method to set property value
            String methodName = "set" + Character.toUpperCase(key.charAt(0)) + key.substring(1);
            try {
                Method method = target.getClass().getMethod(methodName, String.class);
                method.invoke(target, resourceHnadler.getString(entry.getValue()));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
                System.err.println("Could not set property " + key + ", class " + target.getClass().getName());
                ex.printStackTrace(System.err);
            }

        }
    }
    
    /**
     * Parse DXML file to get mapping of component IDs to corresponding resource keys.
     */
    private void parseFxml(){
        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try{
                    SAXParserFactory factory = SAXParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    
                    SAXParser parser = factory.newSAXParser();
                    parser.parse(fxmlFileUrl.openStream(), new FXMLParseHandler());
                } catch(ParserConfigurationException | 
                        SAXException | IOException ex){
                    ex.printStackTrace(System.err);
                }
                
            }
        });
        thread.start();
    }
    
    /**
     * Content handler used to parse FXML file.
     */
    private class FXMLParseHandler extends DefaultHandler{
        
      
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            // check if there is attribute 'fx:id'
            String id = attributes.getValue("id");
            // try with namespace added
            if(id == null){
                id = attributes.getValue("http://javafx.com/fxml", "id");
            }
            if(id != null){
                 Map<String, String> curMap = new HashMap<>();
                 for(int i = 0;i < attributes.getLength();i++){
                     String val = attributes.getValue(i);
                     // check if this attribute value references resource
                     if(val != null && val.startsWith("%")){
                         curMap.put(attributes.getLocalName(i), val.substring(1));
                     }
                 }
                 if(!curMap.isEmpty()){
                     propertyMap.put(id, curMap);
                 }
            } 
        }
        
    }
    
}
