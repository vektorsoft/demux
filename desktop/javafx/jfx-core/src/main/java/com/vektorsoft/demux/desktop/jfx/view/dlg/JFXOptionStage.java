/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.view.dlg;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * <p>
 *  An implementation of {@link DMXDialog} which provides common functionality similar to 
 * Swing {@code JOptionPane}, ie. common options like <code>OK, Cancel, Yes, No</code>. Clients can 
 * display arbitrary type of content in it.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class JFXOptionStage extends Stage implements DMXDialog {
    
    /** Default spacing value. */
    private static final double SPACING = 10.0;
    
    /**
     * Number of options for Yes, No, Cancel dialog.
     */
    private static final int YES_NO_CANCEL_OPT_COUNT = 3;

    /** Scene root. */
    private VBox root;
    
    /** Dialog content. */
    private HBox content;
    
    /** Bar for dialog buttons. */
    private HBox buttonBar;
    
    /** Dialog options value. */
    private int options;
    
    /** Option buttons. */
    private Button[] buttons;
    
    /** Indicates selected option. */
    private int selOption = DMXDialog.DLG_OPTION_CLOSED;
    
    /** View to be displayed in this dialog. */
    private DMXDialogView view;
    
    /** Resource manager for I18N. */
    private DefaultResourceHandler resourceHandler;

    /**
     * Private constructor. Invoked from within this class.
     * @param parent 
     */
    private JFXOptionStage(Stage parent) {
        super();
        initModality(Modality.WINDOW_MODAL);
        initOwner(parent);
        initStyle(StageStyle.UTILITY);

        root = new VBox(SPACING);
        root.getStyleClass().add("vbox");
        content = new HBox(SPACING);
        content.setPadding(new Insets(5, 5, 5, 5));
        content.setAlignment(Pos.CENTER);
        buttonBar = new HBox(SPACING);
        buttonBar.setPadding(new Insets(3, 3, 5, 3));
        buttonBar.setAlignment(Pos.CENTER);
    }
    
    private ImageView getMessageIcon(DMXDialog.MessageType type){
        ImageView view = null;
        if(type != null && type != MessageType.MESSAGE_TYPE_PLAIN){
            switch(type){
                case MESSAGE_TYPE_ERROR:
                    view = new ImageView((Image)resourceHandler.getResource("demux.jfx.icon.error"));
                    break;
                case MESSAGE_TYPE_WARNING:
                    view = new ImageView((Image)resourceHandler.getResource("demux.jfx.icon.warning"));
                    break;
                case MESSAGE_TYPE_INFO:
                    view = new ImageView((Image)resourceHandler.getResource("demux.jfx.icon.info"));
                    break;
                case MESSAGE_TYPE_QUESTION:
                    view = new ImageView((Image)resourceHandler.getResource("demux.jfx.icon.question"));
                    break;
            }
        }
        
        return view;
    }

    /**
     * Create new instance of this dialog.
     * 
     * @param parent parent stage
     * @param options dialog options
     * @param title dialog title (optional)
     * @param content content to be displayed (either {@code DMXView} or string)
     * @param resourceHandler  resource handler to use
     * @param type message dialog type
     */
    public JFXOptionStage(Stage parent, int options, String title, Object content, DefaultResourceHandler resourceHandler, DMXDialog.MessageType type) {
        this(parent);
        this.resourceHandler = resourceHandler;
         ImageView icon = getMessageIcon(type);
        if(icon != null){
            this.content.getChildren().add(icon);
        }
        this.options = options;
        if (content instanceof DMXDialogView) {
            setTitle(((DMXDialogView) content).getDialogTitle());
            this.content.getChildren().add((Node) ((DMXDialogView) content).getViewUI());
            this.view = (DMXDialogView) content;
        } else if (content instanceof String) {
            Text txt = new Text(content.toString());
            this.content.getChildren().add(txt);
        }
        if (title != null) {
            setTitle(title);
        }
        initButtons();
        buttonBar.getChildren().addAll(buttons);

        root.getChildren().add(this.content);
        root.getChildren().add(buttonBar);

        Scene scene = new Scene(root);
        setScene(scene);
    }
    
    /**
     * Create new dialog with specified custom options.
     * 
     * @param parent dialog parent
     * @param dlgOptions array of options to display
     * @param title optional title
     * @param content dialog content
     * @param resourceHandler resource handler to use
     */
    public JFXOptionStage(Stage parent, String[] dlgOptions, String title, Object content, DefaultResourceHandler resourceHandler, DMXDialog.MessageType type){
        this(parent);
        this.resourceHandler = resourceHandler;
        ImageView icon = getMessageIcon(type);
        if(icon != null){
            this.content.getChildren().add(icon);
        }
        if (content instanceof DMXDialogView) {
            setTitle(((DMXDialogView) content).getDialogTitle());
            this.content.getChildren().add((Node) ((DMXDialogView) content).getViewUI());
            this.view = (DMXDialogView) content;
        } else if (content instanceof String) {
            this.content.getChildren().add(new Text(content.toString()));
        }
        if (title != null) {
            setTitle(title);
        }
        initButtons(dlgOptions);
        
         buttonBar.getChildren().addAll(buttons);

        root.getChildren().add(this.content);
        root.getChildren().add(buttonBar);

        Scene scene = new Scene(root);
        setScene(scene);
    }

    /**
     * Initialize buttons.
     */
    private void initButtons() {
            switch (options) {
                case DMXDialog.OPTIONS_OK:
                    buttons = new Button[1];
                    buttons[0] = new Button(resourceHandler.getString("dialog.button.ok"));
                    buttons[0].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_OK));
                    break;
                case DMXDialog.OPTIONS_OK_CANCEL:
                    buttons = new Button[2];
                    buttons[0] = new Button(resourceHandler.getString("dialog.button.ok"));
                    buttons[0].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_OK));
                    buttons[1] = new Button(resourceHandler.getString("dialog.button.cancel"));
                    buttons[1].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_CANCEL));
                    break;
                case DMXDialog.OPTIONS_YES_NO:
                    buttons = new Button[2];
                    buttons[0] = new Button(resourceHandler.getString("dialog.button.yes"));
                    buttons[0].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_YES));
                    buttons[1] = new Button(resourceHandler.getString("dialog.button.no"));
                    buttons[1].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_NO));
                    break;
                case DMXDialog.OPTIONS_YES_NO_CANCEL:
                    buttons = new Button[YES_NO_CANCEL_OPT_COUNT];
                    buttons[0] = new Button(resourceHandler.getString("dialog.button.yes"));
                    buttons[0].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_YES));
                    buttons[1] = new Button(resourceHandler.getString("dialog.button.no"));
                    buttons[1].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_NO));
                    buttons[2] = new Button(resourceHandler.getString("dialog.button.cancel"));
                    buttons[2].setOnAction(new ButtonActionHandler(DMXDialog.DLG_OPTION_CANCEL));
                    break;
                default:
                    
            }
    }
    
    /**
     * Initialize buttons with custom option strings.
     * 
     * @param dlgOptions dialog options
     */
    private void initButtons(String[] dlgOptions){
        buttons = new Button[dlgOptions.length];
        for(int i = 0;i < dlgOptions.length;i++){
            buttons[i] = new Button(resourceHandler.getString(dlgOptions[i]));
            buttons[i].setOnAction(new ButtonActionHandler(i));
        }
    }

    @Override
    public int showDialog() {
        showAndWait();
        return selOption;
    }

    @Override
    public void closeDialog() {
        hide();
    }

    @Override
    public DMXDialogView getDialogView() {
        return view;
    }


    /**
     * Action handler for dialog buttons. This will set selected option and close the dialog.
     */
    private class ButtonActionHandler implements EventHandler<ActionEvent> {

        /** Option values. */
        private final int value;

        /**
         * Creates new instance.
         * 
         * @param value option value
         */
        public ButtonActionHandler(int value) {
            this.value = value;
        }

        @Override
        public void handle(ActionEvent t) {
            selOption = value;
            closeDialog();
        }
    }
}
