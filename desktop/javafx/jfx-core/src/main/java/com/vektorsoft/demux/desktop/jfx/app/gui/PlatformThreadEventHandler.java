/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2015 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.app.gui;

import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import javafx.application.Platform;

/**
 * Event handler implementation which runs event handler code in JavaFX UI thread. This allows
 * event handler to update GUI, if needed.
 * This class uses underlying event handler to perform actual event processing.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class PlatformThreadEventHandler implements DMXEventHandler {

    private final DMXEventHandler handler;

    public PlatformThreadEventHandler(DMXEventHandler handler) {
        this.handler = handler;
    }

    @Override
    public String[] getTopics() {
        return handler.getTopics();
    }

    @Override
    public void handle(DMXEvent event) {
        Platform.runLater(() -> {
            handler.handle(event);
        });
    }

    @Override
    public DMXEventFilter getFilter(String topicName) {
        return handler.getFilter(topicName);
    }
}
