/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.ResourceManagerAware;
import java.util.Map;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * Service tracker customizer for tracking instances of {@link DMXController}. Each activated controller instance will be registered with 
 * the adapter, so it can be invoked from other components.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ControllerRegistrationCustomizer implements ServiceTrackerCustomizer<DMXController, DMXController> {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(ControllerRegistrationCustomizer.class);
    
    private final BundleContext context;
    private final DMXAdapter adapter;
    private final DMXLocaleManager resourceManager;

    public ControllerRegistrationCustomizer(BundleContext context, DMXAdapter adapter, DMXLocaleManager resourceManager) {
        this.context = context;
        this.adapter = adapter;
        this.resourceManager = resourceManager;
    }
    

    @Override
    public DMXController addingService(ServiceReference<DMXController> sr) {
        DMXController controller = context.getService(sr);
        // register controller
        adapter.registerController(controller);
        
        Map<String, Object> data = (Map<String, Object>)sr.getProperty(DMXCoreConstants.PROP_CTRL_DATA);
        if(data != null && !data.isEmpty()){
            for(Map.Entry<String, Object> entry : data.entrySet()){
                adapter.registerData(entry.getKey(), entry.getValue());
            }
        }
        // set adapter for this controller, if needed
        if(controller instanceof DMXAdapterAware){
            ((DMXAdapterAware)controller).setAdapter(adapter);
            LOGGER.debug("Injected adapter to " + controller);
        }
        if(controller instanceof ResourceManagerAware){
            ((ResourceManagerAware)controller).setResourceManager(resourceManager);
        }
        LOGGER.debug("Registered controller " + controller.getControllerId());
        
        return controller;
    }

    @Override
    public void modifiedService(ServiceReference<DMXController> sr, DMXController t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXController> sr, DMXController t) {
        adapter.removeController(t.getControllerId());
        LOGGER.debug("Removed controller " + t.getControllerId());
    }

}
