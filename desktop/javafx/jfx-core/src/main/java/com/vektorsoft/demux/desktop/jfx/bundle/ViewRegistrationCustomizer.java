/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.desktop.jfx.app.JFXViewManager;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ViewRegistrationCustomizer implements ServiceTrackerCustomizer<DMXView, DMXView>{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(ViewRegistrationCustomizer.class);
    
    private final JFXViewManager viewManager;
    private final BundleContext bundleContext;
    private final DMXAdapter adapter;

    public ViewRegistrationCustomizer(JFXViewManager viewManager, BundleContext ctx, DMXAdapter adapter) {
        this.viewManager = viewManager;
        this.bundleContext = ctx;
        this.adapter = adapter;
    }
    

    @Override
    public DMXView addingService(ServiceReference<DMXView> sr) {
        DMXView view = bundleContext.getService(sr);
        // register view
        viewManager.registerView(view);
        // set adapter if needed
        if(view instanceof DMXAdapterAware){
            ((DMXAdapterAware)view).setAdapter(adapter);
            LOGGER.debug("Injected adapter to " + view);
        }
        
        LOGGER.debug("Registered view " + view.getViewId());
        
        return view;
    }

    @Override
    public void modifiedService(ServiceReference<DMXView> sr, DMXView t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXView> sr, DMXView t) {
        viewManager.unregisterView(t.getViewId());
        LOGGER.debug("Removed view " + t.getViewId());
    }

}
