/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.gui;

import com.vektorsoft.demux.desktop.gui.DMXWindowService;
import com.vektorsoft.demux.desktop.jfx.app.gui.DMXMenuBar;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JFXWindowService implements DMXWindowService<Stage>{
    
    private final Stage primary;
    private final DMXMenuBar menuBar;
    private final HBox toolbarContainer;
    
    public JFXWindowService(Stage stage){
        this.primary = stage;
        this.menuBar = new DMXMenuBar();
        this.toolbarContainer = new HBox(3);
        init();
    }
    
     /**
     * Initializes main window. This method is executed on JavaFX platform thread.
     */
    private void init() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                primary.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream("img/main/dmx-icon-16x16.png")));
                primary.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream("img/main/dmx-icon-32x32.png")));
                primary.getIcons().add(new Image(getClass().getClassLoader().getResourceAsStream("img/main/dmx-icon-64x64.png")));
                // make containers for menu and toolbars
                VBox topContainer = new VBox();
                topContainer.getChildren().addAll(menuBar, toolbarContainer);
                BorderPane bp = (BorderPane) primary.getScene().getRoot();
                bp.setTop(topContainer);
            }
        });
    }

    @Override
    public Stage getMainWindow() {
        return primary;
    }

    public DMXMenuBar getMenuBar() {
        return menuBar;
    }

    public HBox getToolbarContainer() {
        return toolbarContainer;
    }

}
