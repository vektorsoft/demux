/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2015 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.app.gui;

import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXMenuBar extends MenuBar {

    private final Map<String, Integer> indexMap;
    private final MenuComparator comparator;
    private final Map<String, MenuItem> registeredItems;
    private final Map<String, Set<MenuItem>> deferredItems;

    public DMXMenuBar() {
        indexMap = new HashMap<>();
        comparator = new MenuComparator();
        registeredItems = new HashMap<>();
        this.deferredItems = new HashMap<>();
    }

    public void addMenuComponent(DMXMenuComponent<MenuItem> comp) {
        String parentId = comp.getParentId();
        String compId = comp.getComponentId();

        if (compId == null || compId.isEmpty()) {
            throw new IllegalArgumentException("Missing ID in DMXMenuComponent implementation. Does method getComponentId() return unique string?");
        }
        MenuItem item = comp.getComponent();
        if (item.getId() == null || item.getId().isEmpty() || !compId.equals(item.getId())) {
            throw new IllegalArgumentException("Invalid ID for component" + item + ". It must match " + compId);
        }

        registeredItems.put(compId, item);
        indexMap.put(compId, comp.preferredIndex());

        Platform.runLater(() -> {
            if (deferredItems.containsKey(compId)) {
                ((Menu) item).getItems().addAll(deferredItems.get(compId));
                ((Menu) item).getItems().sort(comparator);
                deferredItems.remove(compId);
            }
            if (parentId != null) {
                Menu parent = (Menu) registeredItems.get(parentId);
                if (parent != null) {
                    parent.getItems().add(item);
                    parent.getItems().sort(comparator);
                } else {
                    addDeferredMenu(item);
                }
            } else {
                getMenus().add(((Menu) item));
            }
            getMenus().sort(comparator);
        });

    }

    public void removeMenuComponent(DMXMenuComponent<MenuItem> comp) {
        Platform.runLater(() -> {
            MenuItem m = comp.getComponent();
            indexMap.remove(comp.getComponentId());
            String parentId = comp.getParentId();
            if (parentId != null) {
                Menu parent = (Menu) registeredItems.get(parentId);
                if (parent != null) {
                    parent.getItems().remove(m);
                }
            } else {
                getMenus().remove((Menu) m);
            }
        });

    }

    private void addDeferredMenu(MenuItem menu) {
        String id = menu.getId();
        if (deferredItems.containsKey(id)) {
            deferredItems.get(id).add(menu);
        } else {
            Set<MenuItem> items = new HashSet<>();
            items.add(menu);
            deferredItems.put(id, items);
        }
    }

    private class MenuComparator implements Comparator<MenuItem> {

        @Override
        public int compare(MenuItem o1, MenuItem o2) {
            int i1 = -1;
            if (o1.getId() != null) {
                i1 = indexMap.get(o1.getId());
            }
            if (i1 < 0) {
                i1 = Integer.MAX_VALUE;
            }
            int i2 = -1;
            if (o2.getId() != null) {
                i2 = indexMap.get(o2.getId());
            }

            if (i2 < 0) {
                i2 = Integer.MAX_VALUE;
            }
            return Integer.compare(i1, i2);
        }

    }
}
