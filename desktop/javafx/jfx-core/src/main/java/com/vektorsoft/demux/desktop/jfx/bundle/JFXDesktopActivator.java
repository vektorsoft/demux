/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.app.DMXApplication;
import com.vektorsoft.demux.core.exception.DMXExceptionHandlerService;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import com.vektorsoft.demux.desktop.gui.DMXToolbarComponent;
import com.vektorsoft.demux.desktop.jfx.app.JFXDesktopApplication;
import com.vektorsoft.demux.desktop.jfx.app.JFXViewManager;
import com.vektorsoft.demux.desktop.jfx.app.exception.JFXDefaultExceptionHandler;
import com.vektorsoft.demux.desktop.jfx.app.exception.JFXExceptionHandlerService;
import com.vektorsoft.demux.desktop.jfx.gui.JFXWindowService;
import com.vektorsoft.demux.eb.BasicEventBus;
import com.vektorsoft.demux.eb.DMXEventBus;
import java.util.Locale;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Activator for desktop bundle.
 *
 * @author Vladimir Djurovic
 */
public class JFXDesktopActivator implements BundleActivator, ServiceListener {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(JFXDesktopActivator.class);
    
    private static final String FILTER_VIEW_OBJECT = "(objectClass=" + DMXView.class.getName() + ")";
    private static final String FILTER_CTRL_OBJECT = "(objectClass=" + DMXController.class.getName() + ")";
    private static final String FILTER_LOCALE_OBJECT = "(objectClass=" + Locale.class.getName() + ")";
    private static final String FILTER_EXTENSION_CALLBACK_OBJECT = "(objectClass=" + DMXExtensionCallback.class.getName() + ")";
    
    private JFXWindowService windowService;
    private BundleContext context;
    private ServiceTracker<DMXView, DMXView> viewServiceTracker;
    private JFXViewManager viewManager;
    
    private ServiceTracker<DMXController, DMXController> controllerTracker;
    private  DMXDefaultAdapter adapter;
    
    private ServiceTracker<DMXExtensionCallback, DMXExtensionCallback> callbackTracker;
    
    private DefaultLocaleManager localeManager;
    private DefaultResourceHandler resourceHandler;
    private ServiceTracker<Locale, Locale> localeTracker;
    
    private ServiceTracker<DMXAdapterAware, DMXAdapterAware> adapterAwareTracker;
    private ServiceTracker<DMXMenuComponent, DMXMenuComponent<MenuItem>> menuTracker;
    private ServiceTracker<DMXToolbarComponent, DMXToolbarComponent<Node>> toolbarTracker;
    
    private DMXEventBus eventBus;
    
    /**
     * This method will start the bundle and register basic services.
     * 
     * @param context bundle context
     * @throws Exception if an error occurs
     */
    @Override
    public void start(BundleContext context) throws Exception {
        this.context = context;
        DMXApplication application = new JFXDesktopApplication();
        context.registerService(DMXApplication.class, application, null);
        
        eventBus = new BasicEventBus();
        adapter = new DMXDefaultAdapter();
        adapter.setEventBus(eventBus);
        localeManager = new DefaultLocaleManager();
        localeManager.setEventBus(eventBus);
        // register resources
        resourceHandler = new DefaultResourceHandler();
        resourceHandler.setCurrentLocale(localeManager.getCurrentLocale());
        resourceHandler.loadResourceBundle("bundles.messages", this.getClass().getClassLoader());
        resourceHandler.loadResourceBundle("bundles.CoreResources", this.getClass().getClassLoader());
        
        // register adapter aware tarcker
        adapterAwareTracker = new ServiceTracker<>(context,DMXAdapterAware.class, new AdapterAwareTrackerCustomizer(adapter, context));
        adapterAwareTracker.open();
        
        // start controlers tracker
        try {
            Filter ctrlFilter = context.createFilter(FILTER_CTRL_OBJECT);
            controllerTracker = new ServiceTracker<>(context, ctrlFilter, new ControllerRegistrationCustomizer(context, adapter, localeManager));
            controllerTracker.open();
        } catch(InvalidSyntaxException ex){
            LOGGER.error("Could not set up controller tracker", ex);
        }
        // start locale tracker
        try {
            Filter localeFilter = context.createFilter(FILTER_LOCALE_OBJECT);
            localeTracker = new ServiceTracker<>(context, localeFilter, new LocaleRegistrationCustomizer(localeManager, context));
            localeTracker.open();
        } catch(InvalidSyntaxException ex){
            LOGGER.error("Could not set up locale tracker");
        }
        
        boolean windowServiceFound = false;
        ServiceReference[] refs = context.getAllServiceReferences(null, null);
        for(ServiceReference sref : refs){
            if(sref.getProperty("jfx.windowservice") != null){
                windowServiceFound = true;
                windowService = new JFXWindowService((Stage)sref.getProperty("jfx.windowservice"));
                setUpTracker();
                break;
            }
        }
        if(!windowServiceFound){
            context.addServiceListener(this);
        }
        
        // register exception handler
        JFXDefaultExceptionHandler exceptionHandler = new JFXDefaultExceptionHandler(adapter, resourceHandler);
        JFXExceptionHandlerService exceptionHandlerSvc = new JFXExceptionHandlerService(exceptionHandler);
        context.registerService(DMXExceptionHandlerService.class, exceptionHandlerSvc, null);
        
    }

    @Override
    public void stop(BundleContext context) throws Exception {
        if(viewServiceTracker != null){
            viewServiceTracker.close();
        }
        if(controllerTracker != null){
            controllerTracker.close();
        }
        if(localeTracker != null){
            localeTracker.close();
        }
        if(callbackTracker != null){
            callbackTracker.close();
        }
        if(adapterAwareTracker != null){
            adapterAwareTracker.close();
        }
        if(menuTracker != null){
            menuTracker.close();
        }
        if(toolbarTracker != null){
            toolbarTracker.close();
        }
    }

    @Override
    public void serviceChanged(ServiceEvent se) {
        if (se.getType() == ServiceEvent.REGISTERED && se.getServiceReference().getProperty("jfx.windowservice") != null) {
            LOGGER.debug("DMXWindowService registered");
            windowService = (JFXWindowService) context.getService(se.getServiceReference());
            setUpTracker();
        }
    }
    
    private void setUpTracker() {
        viewManager = new JFXViewManager(windowService.getMainWindow(), resourceHandler);
        viewManager.setEventBus(eventBus);
        adapter.setViewManager(viewManager);
        try {
            Filter viewFilter = context.createFilter(FILTER_VIEW_OBJECT);
            viewServiceTracker = new ServiceTracker(context, viewFilter, new ViewRegistrationCustomizer(viewManager, context, adapter));
            viewServiceTracker.open();
        } catch (InvalidSyntaxException ex) {
            LOGGER.error("Could not register view service tracker", ex);
        }
        // start extensions tracker
        try {
            Filter extFilter = context.createFilter(FILTER_EXTENSION_CALLBACK_OBJECT);
            callbackTracker = new ServiceTracker<>(context, extFilter, new CallbackRegistrationCustomizer(context, adapter));
            callbackTracker.open();
        } catch (InvalidSyntaxException ex) {
            LOGGER.error("Could not set up callback tracker");
        }
        //start menu tracker
        menuTracker = new ServiceTracker<>(context, DMXMenuComponent.class, new MenuRegistrationCustomizer(context, windowService.getMenuBar(), adapter, eventBus));
        menuTracker.open();
        // start toolbar tracker
        toolbarTracker = new ServiceTracker<>(context, DMXToolbarComponent.class, new ToolbarRegistrationCustomizer(context, windowService.getToolbarContainer(), adapter, eventBus));
        toolbarTracker.open();
    }

}
