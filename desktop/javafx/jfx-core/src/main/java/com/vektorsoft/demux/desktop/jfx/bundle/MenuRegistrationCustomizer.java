/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import com.vektorsoft.demux.desktop.jfx.app.gui.DMXMenuBar;
import com.vektorsoft.demux.desktop.jfx.app.gui.PlatformThreadEventHandler;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.control.MenuItem;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class MenuRegistrationCustomizer implements ServiceTrackerCustomizer<DMXMenuComponent, DMXMenuComponent<MenuItem>> {
    
    private final DMXMenuBar menubar;
    private final BundleContext bundleContext;
    private final DMXAdapter adapter;
    private final DMXEventBus eventBus;
    private final Map<DMXMenuComponent, PlatformThreadEventHandler> eventHandlers;
    
    public MenuRegistrationCustomizer(BundleContext context, DMXMenuBar menubar, DMXAdapter adapter, DMXEventBus eventbus){
        this.bundleContext = context;
        this.menubar = menubar;
        this.adapter = adapter;
        this.eventBus = eventbus;
        this.eventHandlers = new HashMap<>();
    }

    @Override
    public DMXMenuComponent<MenuItem> addingService(ServiceReference<DMXMenuComponent> sr) {
        DMXMenuComponent<MenuItem> menuComp = bundleContext.getService(sr);
        menubar.addMenuComponent(menuComp);
        if(menuComp instanceof DMXAdapterAware){
           ((DMXAdapterAware)menuComp).setAdapter(adapter);
       }
         if(menuComp instanceof DMXEventHandler){
             PlatformThreadEventHandler peh = new PlatformThreadEventHandler((DMXEventHandler)menuComp);
             eventHandlers.put(menuComp, peh);
            eventBus.subscribe(new PlatformThreadEventHandler(peh));
        }
        
        return menuComp;
    }

    @Override
    public void modifiedService(ServiceReference<DMXMenuComponent> sr, DMXMenuComponent<MenuItem> t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXMenuComponent> sr, DMXMenuComponent<MenuItem> t) {
        menubar.removeMenuComponent(t);
         if(t instanceof DMXEventHandler && eventHandlers.containsKey(t)){
            eventBus.unsubscribe(eventHandlers.get(t));
            eventHandlers.remove(t);
        }
    }

}
