/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.app.gui;

import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;

/**
 * Contains utility methods used in JavaFX applications.
 *
 * @author Vladimir Djurovic
 */
public final class JavaFXUtils {
    
    /**
     * Private constructor to prevent instantiation.
     */
    private JavaFXUtils(){
        
    }

    /**
     * Finds menu item with specified ID.
     * 
     * @param menuBar menu bar
     * @param id ID of menu item
     * @return menu item
     */
    public static MenuItem findMenuItemById(MenuBar menuBar, String id){
        MenuItem item = null;
        for(Menu menu : menuBar.getMenus()){
            item = findMenuItemById(menu, id);
            if(item != null){
                break;
            }
        }
        
        return item;
    }
    
     /**
     * Returns menu item with ID {@code id} contained within {@code menu}.
     * 
     * @param menu parent menu of menu item
     * @param id menu item ID
     * @return menu item, or {@code null} if none is found
     */
    private static MenuItem findMenuItemById(Menu menu, String id){
        MenuItem result = null;
        List<MenuItem> items = menu.getItems();
        for(MenuItem item : items){
            if(item.getId() != null && item.getId().equals(id)){
                result =  item;
                break;
            } else {
                if(item instanceof Menu){
                    result = findMenuItemById((Menu)item, id);
                    if(result != null){
                        break;
                    }
                }
            }
            
        }
        return result;
    }
    
    public static Node findToolbarItemById(ToolBar toolbar, String id){
        Node out = null;
        for(Node n : toolbar.getItems()){
            if(id.equals(n.getId())){
                out = n;
                break;
            }
        }
        return out;
    }
}
