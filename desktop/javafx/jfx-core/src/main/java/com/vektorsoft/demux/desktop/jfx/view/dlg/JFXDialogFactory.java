/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.view.dlg;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.dlg.DialogCustomizer;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.mva.DMXViewManager;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.gui.DMXDesktopDialogFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * <p>
 * Dialog factory for JavaFX applications. It provides custom dialogs which can be used in DEMUX JavaFX 
 * applications. Since JavaFX currently does not have built-in dialogs, this class provides custom 
 * dialog implementation.
 * </p>
 * <p>
 *  Note that these dialogs are synchronous, meaning that they will block calling thread until they 
 * are closed.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class JFXDialogFactory implements DMXDesktopDialogFactory {

    /**
     * View-to-dialog mappings.
     */
    private final Map<String, DMXDialog> dialogs;
    
    /** View manager instance. */
    private final DMXViewManager viewManager;
    
    /**  Main stage for dialogs. */
    private final Stage parent;
    
    /** Resource manager instance. */
    private DefaultResourceHandler resourceHandler;

    /**
     * Create new instance.
     * 
     * @param manager view manager
     * @param parent  parent stage
     */
    public JFXDialogFactory(DMXViewManager manager, Stage parent) {
        this.viewManager = manager;
        this.parent = parent;
        dialogs = Collections.synchronizedMap(new HashMap<String, DMXDialog>());
    }

    /**
     * Sets resource manager.
     * 
     * @param resourceHandler resource manager
     */
    public void setResourceHandler(DefaultResourceHandler resourceHandler) {
        this.resourceHandler = resourceHandler;
    }

    
    @Override
    public JFXDialogStage createDialog(String viewId, String title) {
        JFXDialogStage dlg = null;
        DMXView view = viewManager.getView(viewId);
        if (view != null) {
            if (view instanceof DMXDialogView) {
                synchronized (dialogs) {
                    if (dialogs.containsKey(view.getViewId())) {
                        dlg = (JFXDialogStage) dialogs.get(view.getViewId());
                    } else {
                        dlg = new JFXDialogStage(parent, (DMXDialogView) view);
                        dialogs.put(viewId, dlg);
                    }
                }

                if (title != null) {
                    dlg.setTitle(title);
                }
            } else {
                throw new IllegalArgumentException("View is not instance of com.vektorsoft.demux.core.mva.DMXDialogView. View ID:" + viewId);
            }
        }
        return dlg;
    }

   
    @Override
    public JFXProgressDialog createProgressDialog(String viewId) {
        JFXProgressDialog dlg = null;
        DMXView view = viewManager.getView(viewId);
        if (view != null && view instanceof DMXDialogView) {
            synchronized (dialogs) {
                if (dialogs.containsKey(view.getViewId())) {
                    dlg = (JFXProgressDialog) dialogs.get(view.getViewId());
                } else {
                    dlg = new JFXProgressDialog(parent, (DMXDialogView) view);
                    dialogs.put(viewId, dlg);
                }
            }


        } else {
            throw new IllegalArgumentException("View is not instance of com.vektorsoft.demux.core.mva.DMXDialogView. View ID:" + viewId);
        }
        return dlg;
    }

    @Override
    public DMXDialog createOptionsDialog(String viewId, String title, int options) {
        if (viewId == null || viewId.isEmpty()) {
            throw new IllegalArgumentException("View ID is required");
        }
        JFXOptionStage dlg = null;
        DMXView view = viewManager.getView(viewId);
        if (view instanceof DMXDialogView) {
            synchronized (dialogs) {
                if (dialogs.containsKey(view.getViewId())) {
                    dlg = (JFXOptionStage) dialogs.get(view.getViewId());
                } else {
                    dlg = new JFXOptionStage(parent, options, title, view, resourceHandler, DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
                    dialogs.put(viewId, dlg);
                }
            }

        }

        return dlg;
    }

    @Override
    public DMXDialog createOptionsDialog(String viewId, String title, String[] options) {
        if (viewId == null || viewId.isEmpty()) {
            throw new IllegalArgumentException("View ID is required");
        }
        JFXOptionStage dlg = null;
        DMXView view = viewManager.getView(viewId);
        if (view instanceof DMXDialogView) {
            synchronized (dialogs) {
                if (dialogs.containsKey(view.getViewId())) {
                    dlg = (JFXOptionStage) dialogs.get(view.getViewId());
                } else {
                    dlg = new JFXOptionStage(parent, options, title, view, resourceHandler, DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
                    dialogs.put(viewId, dlg);
                }
            }

        }

        return dlg;
    }

    @Override
    public DMXDialog createMessageDialog(String title, String message, int options) {
        JFXOptionStage dlg = new JFXOptionStage(parent, options, title, message, resourceHandler, DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
        return dlg;
    }

    @Override
    public DMXDialog createMessageDialog(String title, String message, int options, DMXDialog.MessageType type) {
        JFXOptionStage dlg = new JFXOptionStage(parent, options, title, message, resourceHandler, type);
        return dlg;
    }

    @Override
    public File[] createFileChooserDialog(DialogCustomizer.FileChooseType type, DialogCustomizer customizer) {
        FileChooser chooser = new FileChooser();
        if(customizer != null){
            customizer.customize(dialogs);
        }
        
        List<File> files = new ArrayList<>();
        switch(type){
            case OPEN:
                File f = chooser.showOpenDialog(parent);
                if(f != null){
                    files.add(f);
                }
                break;
            case OPEN_MULTIPLE:
                List<File> selected = chooser.showOpenMultipleDialog(parent);
                if(selected != null && !selected.isEmpty()){
                    files.addAll(selected);
                }
                break;
            case SAVE:
                f = chooser.showSaveDialog(parent);
                if(f != null){
                    files.add(f);
                }
                break;
        }
        
        return files.toArray(new File[files.size()]);
    }

}
