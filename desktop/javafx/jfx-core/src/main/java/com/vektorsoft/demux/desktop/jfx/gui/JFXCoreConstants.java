/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.gui;

/**
 * Contains constants for values commonly used in Java FX applications.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JFXCoreConstants {

    private JFXCoreConstants(){
        // do nothing
    }
    
    /**
     * Configuration PID for JavaFX menu service.
     */
    public static final String CONFIG_MENU_SERVICE = "com.vektorsoft.demux.jfx.config.menuService";
    
    /**
     * Configuration PID for JavaFX toolbar service.
     */
    public static final String CONFIG_TOOLBAR_SERVICE = "com.vektorsoft.demux.jfx.config.toolbarService";
}
