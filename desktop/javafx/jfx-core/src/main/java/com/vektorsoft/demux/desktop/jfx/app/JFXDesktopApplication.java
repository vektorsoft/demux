/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.app;

import com.vektorsoft.demux.core.app.DMXApplication;
import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of {@code DMXApplication} which represents JavaFX desktop application.
 *
 * @author Vladimir Djurovic
 */
public  class JFXDesktopApplication implements DMXApplication {
    
    /**
     * Application name.
     */
    protected String name;
    
    /** Application version string. */
    protected String version;
    
    /** Application build number string. */
    protected String buildNumber;
    
    /** Indicates whether application has GUI or not. */
    protected boolean gui;
    
    /** Array of arguments that application was invoked with.  */
    protected String[] arguments;
    
    /**
     * Configuration parameters map.
     */
    protected Map<String, String> configuration;
    
    /**
     * Default constructor for setting default application values.
     */
    public JFXDesktopApplication(){
        name = "DEMUX Framework Application";
        version = "1.0.0";
        buildNumber = "";
        gui = true;
        arguments = new String[0];
        configuration = new HashMap<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getVersion() {
        return version;
    }

    @Override
    public String getBuildNumber() {
        return buildNumber;
    }

    @Override
    public boolean hasGUI() {
        return gui;
    }

    @Override
    public String[] getArguments() {
        String[] argCopy = new String[arguments.length];
        System.arraycopy(arguments, 0, argCopy, 0, arguments.length);
        return argCopy;
    }
    
    @Override
    public Map<String, String> getConfigurationParameters() {
        return new HashMap<>(configuration);
    }
    
    /**
     * Set application name.
     * 
     * @param name application name
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Set application version.
     * 
     * @param version application version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Set application build number.
     * 
     * @param buildNumber application build number
     */
    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    /**
     * Set whether application has GUI or not.
     * 
     * @param gui status
     */
    public void setGui(boolean gui) {
        this.gui = gui;
    }
    
    /**
     * Set application arguments.
     * 
     * @param arguments arguments to set
     */
    public void setArguments(String[] arguments) {
        this.arguments = new String[arguments.length];
        System.arraycopy(arguments, 0, this.arguments, 0, arguments.length);
    }

    /**
     * Set configuration parameters for application.
     * 
     * @param config parameters map
     */
    public void setConfiguration(Map<String, String> config) {
        if(configuration == null){
            configuration = new HashMap<>();
        } else {
            configuration.clear();
            configuration.putAll(config);
        }
    }

}
