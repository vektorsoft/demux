/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.view;

import com.vektorsoft.demux.core.mva.DMXView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * <p>
 * This class represents root view of JavaFX application. All views added to
 * application are rooted in this view.
 * </p>
 * <p>
 * This view does not have any data associated, but merely serves as a container
 * for other views. When multiple subviews are added to this views,they will be
 * stacked on top of each other, with the last view being on top. Views are stacked in 
 * Z-order, so that only last added view is visible.
 * </p>
 * <p>
 *  It is possible to control the placement of the views by specifying view placement constraint. This
 * constraint is positive integer. The higher the value, the higher view will be in Z-order. Thus, view
 * with the highest placement constraint value will be on top.
 * </p>
 */
public class JFXRootView implements DMXView {
    
    /** View ID of the root view. */
    public static final String JFX_ROOT_VIEW_ID = "com.vektorsoft.demux.desktop.jfxRootView";
    
    /** Main application stage. */
    private final Stage stage;

    /**
     * Root node of JavaFX scene graph.
     */
    private  StackPane root;
    
    /** Maps view ID to it's Z-order. */
    private final List<DMXView> zOrder;
    
    /** Comparator for views. */
    private final ViewComparator comparator;
    
    /** Indicates whether view is initialized. */
    private boolean initialized = false;
    
    /**
     * Maps view ID to nodes.
     */
    private final Map<String, Node> viewUiMap;

    /**
     * Create new instance.
     *
     * @param stage JavaFX stage
     */
    public JFXRootView(Stage stage) {
        zOrder = new ArrayList<>();
        comparator = new ViewComparator();
       this.stage = stage;
       viewUiMap = new HashMap<>();
    }
    
    /**
     * Initializes the view.
     */
    private void initView() {
        if (!initialized) {
            BorderPane bp = (BorderPane) stage.getScene().getRoot();
            root = new StackPane();
            bp.setCenter(root);
            initialized = true;
        }
    }


    @Override
    public String getViewId() {
        return JFX_ROOT_VIEW_ID;
    }

    @Override
    public String getParentViewId() {
        return null;
    }

    @Override
    public Object viewPlacementConstraint() {
        return null;
    }

    @Override
    public void addChildView(DMXView child) {
        if (!(child.getViewUI() instanceof Node)) {
            throw new IllegalArgumentException("View must be instance of javafx.scene.Node");
        }
        initView();
        Object constraint = child.viewPlacementConstraint();
        Node viewNode = (Node)child.getViewUI();
        if(constraint != null && constraint instanceof Integer){
            zOrder.add(child);
            // sort view in ascending order
            Collections.sort(zOrder, comparator);
            // reorder views in stack, only the last view should be visible
            root.getChildren().clear();
            root.getChildren().add((Node)zOrder.get(zOrder.size() - 1).getViewUI());
            
        } else {
            root.getChildren().add(viewNode);
        }
        viewUiMap.put(child.getViewId(), viewNode);
        
    }

    @Override
    public void removeChildView(String childId) {
       Node viewNode = viewUiMap.get(childId);
       if(viewNode != null){
           root.getChildren().remove(viewNode);
           viewUiMap.remove(childId);
       }
    }

    @Override
    public Object getViewUI() {
        return root;
    }

    @Override
    public void constructUI() {
        
    }

    @Override
    public void setViewActive(String id, boolean state) {
        for(DMXView view : zOrder){
            if(id.equals(view.getViewId())){
                root.getChildren().clear();
                root.getChildren().add((Node)view.getViewUI());
                break;
            }
        }
        
    }

    @Override
    public String[] getViewDataIds() {
        return new String[0];
    }

    
    /**
     * Comparator for comparing view Z-order.
     */
    private class ViewComparator implements Comparator<DMXView> {

        @Override
        public int compare(DMXView o1, DMXView o2) {
            Object c1 = o1.viewPlacementConstraint();
            Integer z1 = 0;
            if(c1 instanceof Integer){
                z1 = (Integer)c1;
            }
            Object c2 = o2.viewPlacementConstraint();
            Integer z2 = 0;
            if(c2 instanceof Integer){
                z2 = (Integer)c2;
            }
            
            return z1.compareTo(z2);
        }
        
    }
    
}
