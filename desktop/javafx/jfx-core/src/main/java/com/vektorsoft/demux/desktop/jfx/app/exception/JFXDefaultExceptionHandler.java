/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.app.exception;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.exception.DMXExceptionHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;

/**
 * An implementation of {@link DMXExceptionHandler} used for Java FX applications. This implementation will simply 
 * show message dialog to the user with exception message.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JFXDefaultExceptionHandler implements DMXExceptionHandler{
    
    /**
     * Whether this handler is enabled or not.
     */
    private boolean enabled;
    
    /**
     * Platform {@code DMXAdapter}.
     */
    private final DMXAdapter adapter;
    
    /**
     * Current resource manager.
     */
    private final DMXResourceHandler resourceHandler;
    
    /**
     * Creates new instance of this handler.
     * 
     * @param adapter adapter
     */
    public JFXDefaultExceptionHandler(DMXAdapter adapter, DMXResourceHandler handler){
        this.adapter  = adapter;
        this.resourceHandler = handler;
        enabled = true;
        
    }
    

    @Override
    public String getHandlerID() {
        return "com.vektorsoft.demux.desktop.jfx.ExceptionHandler";
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String[] overrideIds() {
        return null;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        StringBuilder sb = new StringBuilder(resourceHandler.getString("jfx.exceptionhandler.message.intro")).append("\n");
        sb.append(resourceHandler.getString("jfx.exceptionhandler.message.cause")).append(e.getClass().getName()).append("\n");
        sb.append(e.getMessage());
        adapter.getDialogFactory().createMessageDialog(resourceHandler.getString("jfx.exceptionhandler.title"), sb.toString(), DMXDialog.OPTIONS_OK, DMXDialog.MessageType.MESSAGE_TYPE_ERROR).showDialog();
    }

    @Override
    public void setOriginalSystemHandler(Thread.UncaughtExceptionHandler systemHandler) {
        // do nothing
    }
    
    
}
