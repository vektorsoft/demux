/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.app;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.AbstractViewManager;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.mva.DMXViewManager;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import com.vektorsoft.demux.desktop.jfx.app.gui.PlatformThreadEventHandler;
import com.vektorsoft.demux.desktop.jfx.view.dlg.JFXDialogFactory;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.EventBusAware;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import com.vektorsoft.demux.eb.event.ViewRegisteredEvent;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * Implementation of {@link com.vektorsoft.demux.core.mva.DMXViewManager} used for JavaFX applications.
 *
 * @author Vladimir Djurovic
 */
public class JFXViewManager extends AbstractViewManager implements EventBusAware {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(JFXViewManager.class);
    
    /** JavaFX stage. */
    private final Stage stage;
    
    /** JavaFX dialog factory. */
   private final JFXDialogFactory dlgFactory;
   
   /** Currently displayed dialog. */
   private DMXDialog curDialog;
   
   /** Filter for mouse events. */
   private final EventHandler<MouseEvent> mouseEventFilter;
   
   /** Filter for key events. */
   private final EventHandler<KeyEvent> keyEventFilter;
   
   private DMXEventBus eventBus;
    
    /**
     * Creates new instance of this view manager.
     * 
     * @param stage JavaFX stage
     * @param resourceHandler  resourceHandler to use
     */
    public JFXViewManager(Stage stage, DefaultResourceHandler resourceHandler){
        this.stage = stage;
        JFXRootView rootView = new JFXRootView(this.stage);
        registerView(rootView);
        dlgFactory = new JFXDialogFactory(this, stage);
        dlgFactory.setResourceHandler(resourceHandler);
        
        // this filter will consume mouse events when active
        mouseEventFilter = new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent t) {
                t.consume();
            }
        };
        
        // this event will consume keyboard events when active
        keyEventFilter = new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent t) {
                t.consume();
            }
        };
        
    }
    

    /**
     * <p>
     *  Set the view with specified ID as currently active. This implementation uses JavaFX-specific operations 
     * for showing the view.
     * </p>
     * <p>
     *  Parameter {@code params} controls the way view will be displayed. The following values are taken into account
     * by this implementation:
     * </p>
     * <ul>
     *  <li>key {@link DMXViewManager#PARAM_PROGRESS_DIALOG}, with values {@link DMXViewManager#VAL_DLG_SHOW} or {@link DMXViewManager#VAL_DLG_CLOSE}. 
     * If <code>VAL_DLG_SHOW</code> is specified, dialog will be displayed. Otherwise, it will be closed.</li>
     * <li>key {@link DMXViewManager#PARAM_DIALOG_TITLE} - if present, it's value will be used as dialog title </li>
     * <li>key {@link DMXViewManager#PARAM_VIEW_STATE} - if present, it's value will be use as view state (true or false).</li>
     * </ul>
     * 
     * @see DMXViewManager#selectView(java.lang.String, java.util.Map) 
     * 
     * @param viewId view ID
     * @param params parameters
     */
     @Override
    public void selectView(final String viewId, final Map<String, Object> params) {
         Platform.runLater(() -> {
             if (viewId != null) {
                 if (params != null && params.containsKey(DMXViewManager.PARAM_PROGRESS_DIALOG)) {
                     int val = (Integer) params.get(DMXViewManager.PARAM_PROGRESS_DIALOG);
                     if (val == DMXViewManager.VAL_DLG_SHOW) {
                         curDialog = dlgFactory.createProgressDialog(viewId);
                         curDialog.showDialog();
                     } else {
                         curDialog.closeDialog();
                         curDialog = null;
                     }
                 } else {
                     DMXView view = registeredViews.get(viewId);
                     if(view == null){
                         return;
                     }
                     String parentId = view.getParentViewId();
                     // check if this view is dialog view
                     boolean isdialog = true;
                     try {
                         view.getClass().getDeclaredMethod("isModal");
                     } catch(NoSuchMethodException ex){
                         isdialog = false;
                     }
                     if (isdialog) {
                         String title = null;
                         if(params != null && params.containsKey(DMXViewManager.PARAM_DIALOG_TITLE)){
                             title = (String)params.get(DMXViewManager.PARAM_DIALOG_TITLE);
                         }
                         
                         curDialog = dlgFactory.createDialog(viewId, title);
                         curDialog.showDialog();
                         
                     } else {
                         DMXView parent = registeredViews.get(parentId);
                         boolean state = (params != null && params.containsKey(DMXViewManager.PARAM_VIEW_STATE))
                                 ? (boolean)params.get(DMXViewManager.PARAM_VIEW_STATE) : true;
                         parent.setViewActive(viewId, state);
                     }
                     
                 }
             }
         });

    }
     
     
     @Override
    public JFXDialogFactory getDialogFactory() {
        return dlgFactory;
    }

    @Override
    public final void registerView(final DMXView view) {
        super.registerView(view);
       
        // add view to window
        Platform.runLater(() -> {
            view.constructUI();
            final String parentId = view.getParentViewId();
            if (parentId != null && !parentId.isEmpty()) {
                DMXView parent = getView(parentId);
                if (parent != null) {
                    parent.addChildView(view);
//                    view.render();
                }
            }
            
            afterInitialViewRegistration(view);
            if(eventBus != null){
                if(view instanceof DMXEventHandler){
                    LOGGER.debug("Register view " + view.getViewId() + " as event handler");
                    eventBus.subscribe(new PlatformThreadEventHandler((DMXEventHandler)view));
                }
                LOGGER.debug("Publising view registration event for view ID " + view.getViewId());
                ViewRegisteredEvent event = new ViewRegisteredEvent(view.getViewDataIds());
                eventBus.publish(event);
            }
        });
        
    }

    @Override
    protected void removeFromParent(String viewId) {
        Platform.runLater(()-> {
            final String parentId = registeredViews.get(viewId).getParentViewId();
            if (parentId != null && !parentId.isEmpty()) {
                DMXView parent = getView(parentId);
                if (parent != null) {
                    parent.removeChildView(viewId);
//                    parent.render();
                }
            }
        });
    }

    @Override
    public void unregisterView(String id) {
        DMXView view = getView(id);
        if(eventBus != null && (view instanceof DMXEventHandler)){
            eventBus.unsubscribe((DMXEventHandler)view);
        }
        super.unregisterView(id);
    }
    

    @Override
    protected void processDeferredViews(final String parentId, final Set<String> childrenIds) {
        if(childrenIds != null && !childrenIds.isEmpty()){
            for(final String id : childrenIds){
                Platform.runLater(() -> {
                    DMXView view = getView(id);
                    getView(parentId).addChildView(view);
//                    view.render();
                });
            }
        }
    }

    @Override
    protected void applyViewExtension(final String viewId, final DMXExtensionCallback callback) {
        Platform.runLater(() -> {
            DMXView view = getView(viewId);
            if (view != null && view instanceof DMXExtendable) {
                ((DMXExtendable) view).setExtensionCallback(callback);
                LOGGER.debug("Set extension callback for view " + viewId);
            }
        });
    }


    @Override
    public void blockGui(boolean state, GUIBlockingScope scope, String... ids) {

        final GUIBlockingScope theScope = (scope != null) ? scope : GUIBlockingScope.NONE;

        Platform.runLater(() -> {
            switch (theScope) {
                case TOP_LEVEL:
                    setStageBlocking(state);
                    break;
                case COMPONENTS:
                    for (String id : ids) {
                        Node n = stage.getScene().lookup("#" + id);
                        if (n != null) {
                            n.setDisable(state);
                        }
                    }
                    break;
                default:
            }
        });
    }
    
    /**
     * Block all input events from mouse and keyboard coming to this stage.
     * 
     * @param disable whether to enable or disable input
     */
    private void setStageBlocking(boolean disable){
        if(disable){
            stage.getScene().addEventFilter(MouseEvent.ANY, mouseEventFilter);
            stage.getScene().addEventFilter(KeyEvent.ANY, keyEventFilter);
        } else {
            stage.getScene().removeEventFilter(MouseEvent.ANY, mouseEventFilter);
            stage.getScene().removeEventFilter(KeyEvent.ANY, keyEventFilter);
        }
    }


    @Override
    public void setEventBus(DMXEventBus bus) {
        this.eventBus = bus;
    }
    
}
