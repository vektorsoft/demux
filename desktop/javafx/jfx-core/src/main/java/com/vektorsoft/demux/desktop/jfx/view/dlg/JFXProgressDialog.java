/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.jfx.view.dlg;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Dialog used to display progress information. It embeds a view which is responsible for actual 
 * progress display. This dialog will be shown automatically when appropriate task is started, and 
 * be closed when task is finished.
 *
 * @author Vladimir Djurovic
 */
public class JFXProgressDialog extends Stage implements DMXDialog{
    
    /** View for this dialog. */
    private final DMXDialogView dlgView;
    
     /** Create new instance.
     * 
     * @param parent parent stage
     * @param view view to be displayed in this dialog
     */
    public JFXProgressDialog(Stage parent, DMXDialogView view){
        super();
        this.dlgView = view;
        if(view.isModal()){
            initModality(Modality.WINDOW_MODAL);
        } else {
            initModality(Modality.NONE);
        }
        setTitle(view.getDialogTitle());
        initOwner(parent);
        initStyle(StageStyle.UTILITY);
        
        Scene scene = new Scene((Parent)view.getViewUI());
        setScene(scene);
        view.setDialog(this);
    }

    @Override
    public int showDialog() {
        show();
        return DMXDialog.DLG_OPTION_CLOSED;
    }

    @Override
    public void closeDialog() {
        hide();
    }

    @Override
    public DMXDialogView getDialogView() {
        return dlgView;
    }

}
