/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.gui;

import com.vektorsoft.demux.desktop.jfx.gui.DMXLoaderFXML;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 * Provides test cases for class {@link DMXLoaderFXML}.
 *
 * @author Vladimir Djurovic
 */
public class DMXLoaderFXMLTest {

    
    /**
     * <p>
     * Verifies that FXML document is correctly parsed to find references resources and element IDs. 
     * These will be stored and used when GUI needs to be updated on locale changes. This method uses
     * FXML file with no namespace declared.
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of DMXLoaderFXML referencing test document.</li>
     *  <li>Verify that FXML elements are correctly recognized.</li>
     * </ol>
     */
    @Test
    public void testFxmlParserNoNamespace() throws Exception{
        
        URL fxmlFile = this.getClass().getResource("/fxml_sample.xml");
        testParser(fxmlFile);
    }
    
    
    /**
     * <p>
     * Verifies that FXML document is correctly parsed to find references resources and element IDs. 
     * These will be stored and used when GUI needs to be updated on locale changes. This method uses
     * FXML file with namespace declared.
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of DMXLoaderFXML referencing test document.</li>
     *  <li>Verify that FXML elements are correctly recognized.</li>
     * </ol>
     */
    @Test
    public void testFxmlParserWithNamespace() throws Exception{
         URL fxmlFile = this.getClass().getResource("/fxml_sample_namespace.xml");
        testParser(fxmlFile);
    }
    
    /**
     * Load and verify FXML file.
     * 
     * @param url file URL
     * @throws Exception if an error occurs
     */
    private void testParser(URL url) throws Exception{
        DMXLocaleManager manager = new DefaultLocaleManager();
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.setCurrentLocale(manager.getCurrentLocale());
        DMXLoaderFXML loader = new DMXLoaderFXML(url, handler);
        
        // wait for threads to finish XML processing
        Thread.sleep(1000);
        // verify that all fields are correctly recognized
        Field field = loader.getClass().getDeclaredField("propertyMap");
        field.setAccessible(true);
        Map<String, Map<String, String>>  map = (Map<String, Map<String, String>>)field.get(loader);
        
        Assert.assertTrue("ID not contained in map", map.containsKey("topLabelId"));
        Map<String, String> data = map.get("topLabelId");
        Assert.assertTrue("Reuired property not present", data.containsKey("text"));
        Assert.assertEquals("Invalid resource value", "string.title", data.get("text"));
        Assert.assertTrue("ID not contained in map", map.containsKey("centerLabelId"));
        
        data = map.get("centerLabelId");
        Assert.assertTrue("Reuired property not present", data.containsKey("text"));
        Assert.assertEquals("Invalid resource value", "string.data", data.get("text"));
    }
}

