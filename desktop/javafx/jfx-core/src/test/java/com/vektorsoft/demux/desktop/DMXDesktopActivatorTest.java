/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop;

import com.vektorsoft.demux.desktop.jfx.bundle.JFXDesktopActivator;
import com.vektorsoft.demux.core.app.DMXApplication;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;

/**
 * Contains test cases for {@link JFXDesktopActivator} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXDesktopActivatorTest {
    
   

    /**
     * <p>
     *  Verifies that all required services are registered correctly. Activator should register 
     * {@link DMXApplication} service, and open tracker to register {@link DMXDefaultAdapter}.
     * </p>
     * <ul>
     *  <li>Create activator instance</li>
     *  <li>Create mock services and register them</li>
     *  <li>Verify that correct bundle context methods are called</li>
     * </ul>
     */
    @Test
    @Ignore
    public void testStart() throws Exception {
        JFXDesktopActivator activator = new JFXDesktopActivator();
        
        BundleContext ctx = Mockito.mock(BundleContext.class);
        Filter filter = Mockito.mock(Filter.class);
        Mockito.when(ctx.createFilter("(objectClass=com.vektorsoft.demux.desktop.jfx.main.svc.JFXWindowService)")).thenReturn(filter);
        activator.start(ctx);
        
        
    }

}