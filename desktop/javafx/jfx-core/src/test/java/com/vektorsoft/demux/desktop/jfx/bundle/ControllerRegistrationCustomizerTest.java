/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.bundle;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.ServiceReference;

import static org.mockito.Mockito.*;
import org.osgi.framework.BundleContext;

/**
 * Contains test cases for {@link ControllerRegistrationCustomizer} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ControllerRegistrationCustomizerTest {
    
    private DMXAdapter adapter;
    private ServiceReference<DMXController> mockRef;
    private BundleContext ctx;
    private DMXController controller;
    private Map<String, Object> data;
    
    @Before
    public void setup(){
        adapter = mock(DMXAdapter.class);
        mockRef = mock(ServiceReference.class);
        ctx = mock(BundleContext.class);
        controller = mock(DMXController.class);
        data = new HashMap<String, Object>();
        when(mockRef.getProperty(DMXCoreConstants.PROP_CTRL_DATA)).thenReturn(data);
        when(ctx.getService(mockRef)).thenReturn(controller);
    }
    
    @Test
    public void controlelrRegistrationTest(){
         data.put("data1", "value1");
         data.put("data2", 2);
        when(mockRef.getProperty(DMXCoreConstants.PROP_CTRL_DATA)).thenReturn(data);
        
        ControllerRegistrationCustomizer customizer = new ControllerRegistrationCustomizer(ctx, adapter, new DefaultLocaleManager());
        customizer.addingService(mockRef);
        
        verify(adapter).registerData("data1", "value1");
        verify(adapter).registerData("data2", 2);
        verify(adapter).registerController(controller);
    }
}
