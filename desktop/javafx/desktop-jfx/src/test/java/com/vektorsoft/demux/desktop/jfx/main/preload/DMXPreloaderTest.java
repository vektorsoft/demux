/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.main.preload;

import java.lang.reflect.Field;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 * Provides unit tests for {@link DMXPreloader} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXPreloaderTest {
    
    /** Default progress value. */
    private static final double DEFAULT_PROGRESS = 0.5;
    
    /** Default delta for progress comparison. */
    private static final double DEFAULT_DELTA = 0.01;
    
    /** Preloader test object. */
    private DMXPreloader preloader;
    
    /** Progress bar component. */
    private ProgressBar progressBar;
    
    /** Text component for test. */
    private Text text;
    
    /**
     * Construct object required for tests.
     * 
     * @throws Exception if an error occurs
     */
//    @Before
//    public void setUp() throws Exception{
//        preloader = new DMXPreloader();
//        progressBar = new ProgressBar();
//        Field barField = preloader.getClass().getDeclaredField("bar");
//        barField.setAccessible(true);
//        barField.set(preloader, progressBar);
//        
//        // create text component
//        text = new Text();
//        Field textField = preloader.getClass().getDeclaredField("text");
//        textField.setAccessible(true);
//        textField.set(preloader, text);
//        
//    }

    /**
     * Test of start method, of class DMXPreloader.
     */
    @Test
    public void testStart() throws Exception {
    }


    /**
     * <p>
     * Verifies correct behavior of {@link DMXPreloader#handleApplicationNotification(javafx.application.Preloader.PreloaderNotification) }
     * method when invoked for {@link LoadNotification} type argument. This should set correct progress ratio and message.
     * </p>
     * 
     * @throws Exception if an error occurs
     */
//    @Test
//    public void testHandleLoadNotification()throws Exception {
//        String bundle = "bundle-1.0.0";
//        // check with INIT operation
//        LoadNotification notification = new LoadNotification(DEFAULT_PROGRESS, LoadNotification.Operation.INIT, bundle);
//        preloader.handleApplicationNotification(notification);
//        // verify that values are set correctly
//        Field msgField = preloader.getClass().getDeclaredField("message");
//        msgField.setAccessible(true);
//        String msg = (String) msgField.get(preloader);
//        assertEquals("Invalid message values", "Initializing...", msg);
//
//        Field barField = preloader.getClass().getDeclaredField("bar");
//        barField.setAccessible(true);
//        double progress = ((ProgressBar) barField.get(preloader)).getProgress();
//        assertEquals("Invalid progress value", notification.getProgress(), progress, DEFAULT_DELTA);
//        
//        // check with INSTALL operation
//        notification = new LoadNotification(DEFAULT_PROGRESS, LoadNotification.Operation.INSTALL, bundle);
//        preloader.handleApplicationNotification(notification);
//        // verify that values are set correctly
//        msgField = preloader.getClass().getDeclaredField("message");
//        msgField.setAccessible(true);
//        msg = (String) msgField.get(preloader);
//        assertEquals("Invalid message values", "Installing: " + bundle, msg);
//        
//        // check with START operation
//        notification = new LoadNotification(DEFAULT_PROGRESS, LoadNotification.Operation.START, bundle);
//        preloader.handleApplicationNotification(notification);
//        // verify that values are set correctly
//        msgField = preloader.getClass().getDeclaredField("message");
//        msgField.setAccessible(true);
//        msg = (String) msgField.get(preloader);
//        assertEquals("Invalid message values", "Starting: " + bundle, msg);
//        
//    }
}
