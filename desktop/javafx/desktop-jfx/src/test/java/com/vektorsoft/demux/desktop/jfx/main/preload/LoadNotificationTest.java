/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.main.preload;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Provides unit tests for {@link LoadNotification} class.
 *
 * @author Vladimir Djurovic
 */
public class LoadNotificationTest {
    
    /** Test object. */
    private LoadNotification notification;
    
    /**
     * Set up test object.
     */
    @Before
    public void setUp(){
        notification = new LoadNotification(0.5, LoadNotification.Operation.INIT, "Message");
    }

    /**
     * Verifies that operation field is returned correctly.
     */
    @Test
    public void testGetOperation() {
        assertEquals("Invalid operation", LoadNotification.Operation.INIT, notification.getOperation());
    }

    /**
     * Verifies that message is returned correctly.
     */
    @Test
    public void testGetMessage() {
        assertEquals("Invalid message", "Message", notification.getMessage());
    }
}
