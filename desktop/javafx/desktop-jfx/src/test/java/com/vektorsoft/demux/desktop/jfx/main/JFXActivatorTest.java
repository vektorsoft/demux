/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.desktop.jfx.main;

import com.vektorsoft.demux.desktop.jfx.gui.JFXWindowService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;

/**
 * Provides unit tests for {@link JFXActivator} class.
 *
 * @author Vladimir Djurovic
 */
public class JFXActivatorTest {
    
    private BundleContext ctx;
    private Filter filter;
    private Filter adapterFilter;
    
    /**
     * Set up test cases.
     */
    @Before
    public void setUp() throws Exception{
        ctx = Mockito.mock(BundleContext.class);
        filter = Mockito.mock(Filter.class);
        Mockito.when(ctx.createFilter("(objectClass=com.vektorsoft.demux.core.app.DMXApplication)")).thenReturn(filter);
        adapterFilter = Mockito.mock(Filter.class);
        Mockito.when(ctx.createFilter("(objectClass=com.vektorsoft.demux.desktop.svc.DesktopAdapter)")).thenReturn(adapterFilter);
    }

    /**
     * <p>
     *  Tests correct behavior of {@link JFXActivator#start(org.osgi.framework.BundleContext) } method. This
     * method is intended to initialize the bundle and register required service. Services that will be registered 
     * are the following:
     * </p>
     * <ul>
     *  <li>{@link JFXWindowService}</li>
     * </ul>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create an instance of testing class</li>
     *  <li>Create mock bundle context</li>
     *  <li>Invoke start method and verify that service is registered</li>
     * </ul>
     * 
     * @throws Exception if an error occurs
     */
    @Test
    public void testStart() throws Exception {
//        JFXActivator activator = new JFXActivator();
//        JFXWindowService mockSvc = Mockito.mock(JFXWindowService.class);
//        activator.setWindowService(mockSvc);
//        activator.start(ctx);
//        // verify that registerService is called on bundle context
//        Mockito.verify(ctx).registerService(JFXWindowService.class, mockSvc, null);
    }

    /**
     * Behavior of this method is currently undefined.
     * 
     * @throws Exception if an error occurs
     */
    @Test
    public void testStop() throws Exception {
//        JFXActivator activator = new JFXActivator();
//        activator.start(ctx);
//        activator.stop(ctx);
    }

}
