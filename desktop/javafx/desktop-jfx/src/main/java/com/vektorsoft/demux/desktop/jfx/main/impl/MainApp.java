/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.main.impl;


import com.vektorsoft.demux.desktop.jfx.gui.JFXWindowService;
import com.vektorsoft.demux.desktop.jfx.main.JFXActivator;
import com.vektorsoft.demux.desktop.jfx.main.preload.LoadNotification;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.application.Preloader;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.util.FelixConstants;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;

/**
 * Main JavaFX application class. It is intended to start JavaFX runtime and OSGI framework. Also, it will
 * install and start bundles from predefined directory (<code>bundles</code> by default).
 *
 * @author vlada
 */
public class MainApp extends Application {
    
    /** Default directory where bundles are stored. */
    private   static final String VAL_DEFAULT_BUNDLE_DIR_NAME = "bundles";
    
    /** Property name of bundles directory. */
    private  static final String KEY_BUNDLE_DIR_NAME = "bundles.dir";
    
    /** Jar file extension. */
    private  static final String JAR_EXTENSION = ".jar";
    
    /** Amount of time to wait for framework to shut down. */
    private  static final long STOP_TIMEOUT = 2000;
    
    /** Name of external configuration file. */
    private static final String CONFIG_FILE_NAME = "config.properties";
    
    /** File containing names of packages that should be exported from system bundle. */
    private static final String EXTRA_PACKAGES_FILE_NAME = "/extra-packages";
    
    /** Default width of application window. */
    private static final double DEFAULT_STAGE_WIDTH = 600.0;
    
    /** Default height of application window. */
    private static final double DEFAULT_STAGE_HEIGHT = 400.0;
    
    /**
     * Name of the environment property to clear bundle cache.
     */
    private static final String PROP_CLEAR_CACHE = "demux.clear.cache";
    
    
    
    /**  Indicates whether application is ready to start. */
    private BooleanProperty ready = new SimpleBooleanProperty(false);
    
    /** Configuration map. */
    private  Map<String, Object> config;
    
    
    /** Framework instance. */
    private Felix framework;
    
    /** Main application window. */
    private Stage primaryStage;
    
     /**
     * Check if external configuration file exists. This file is assumed to
     * be named <code>config.properties</code>, and should be present in work directory.
     * 
     * @return empty {@code Properties} object if file does not exist, or loaded properties if it does
     * @throws IOException if an error occurs
     */
    private Properties checkForConfigOptions() throws IOException{
        Properties props = new Properties();
        File file = new File(CONFIG_FILE_NAME);
    
        if (file.exists()) {
            try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
                if (file.exists()) {
                    props.load(reader);
                }
            }
        } 
        
        return props;
    }
    
    /**
     * Loads configuration properties from external file. This file should be named 
     * <code>configMap.properties</code> and should be in working directory.
     * 
     * @param configMap configuration map
     */
    private void loadExternalConfiguration(Map<String, Object> configMap){
        try{
            Properties props = checkForConfigOptions();
            if(!props.isEmpty()){
                Enumeration<?> names = props.propertyNames();
                while(names.hasMoreElements()){
                    String name = names.nextElement().toString();
                    configMap.put(name, props.getProperty(name));
                }
            }
        } catch(IOException ex){
            System.err.println("Could not read configuration properties: " + ex.getMessage());
        }
    }
    
    /**
     * Finds a list of packages that should be exported from system bundle. These packages should
     * be specified in a file called <code>extra-packages</code> and should be located in default
     * package. File encoding must be <code>UTF-8</code>
     * 
     * @return list of comma separated packages
     * @throws IOException if an error occurs
     */
    private String findSystemBundleExtraPackages() throws IOException{
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(EXTRA_PACKAGES_FILE_NAME), "UTF-8"));){
            
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }

    @Override
    public void init() throws Exception {
        super.init(); 
        notifyPreloader(new LoadNotification(0, LoadNotification.Operation.INIT, ""));
        
        config = new HashMap<>();
        // check for external configuration properties
        loadExternalConfiguration(config);
        
        // read  packages that need to be exported in system bundle
        String list = null;
        try{
            list = findSystemBundleExtraPackages();
        } catch(IOException ex){
            System.err.println("Extra packages list file not found.");
        }
        
        if(list != null && !list.isEmpty()){
            config.put(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, list);
        }
        config.put(KEY_BUNDLE_DIR_NAME, VAL_DEFAULT_BUNDLE_DIR_NAME);
        // clear bundle cache if needed
        if("true".equals(System.getProperty(PROP_CLEAR_CACHE))){
            config.put(Constants.FRAMEWORK_STORAGE_CLEAN, Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT);
        }
        
        // set framework storage directory to be in user's home dir
        String home = System.getProperty("user.home");
        File demuxDir = new File(home, ".demux");
        if(!demuxDir.exists()){
            demuxDir.mkdir();
        }
        // cache dir will be under <home>/.demux directory
        String dir = System.getProperty("user.dir");
        // if we are running inside Mac OS X bundle
        if(dir.endsWith("/Contents/MacOS")){
            String[] parts = dir.split("/");
            for(int i = parts.length - 1;i >= 0;i--){
                if(parts[i].endsWith(".app")){
                    dir = parts[i].substring(0, parts[i].indexOf(".app"));
                    break;
                }
            }
        } else {
            dir = dir.substring(dir.lastIndexOf(System.getProperty("file.separator")) + 1);
        }
        
        
        File cacheDir = new File(demuxDir, dir + "/cache");
        config.put(Constants.FRAMEWORK_STORAGE, cacheDir.getAbsolutePath());
    }
    
    /**
     * <p>
     * Starts OSGI framework. This method will spawn new thread which will load the framework and
     * track loading progress while it is starting.
     * </p>
     * <p>
     * During startup, bundles from specified directory will be installed and started.
     * </p>
     */
    private void startFramework() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() {
                List<BundleActivator> activators = new ArrayList<>();
                JFXActivator activator = new JFXActivator();
                activator.setWindowService(new JFXWindowService(primaryStage));
                List<String> args = getParameters().getRaw();
                activator.setArguments(args.toArray(new String[args.size()]));
                activator.setConfig(config);

                activators.add(activator);
                config.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, activators);
                
                try {
                    framework = new Felix(config);
                    framework.start();

                    File bundleDir = new File(config.get(KEY_BUNDLE_DIR_NAME).toString());
                    File[] files = new File[0];
                    if (bundleDir.exists()) {
                        files = bundleDir.listFiles(new FilenameFilter() {
                            @Override
                            public boolean accept(File dir, String name) {
                                return name.endsWith(JAR_EXTENSION);
                            }
                        });
                    }

                    int total = files.length * 2;
                    double current = 0;
                    for (File f : files) {
                        framework.getBundleContext().installBundle(f.toURI().toURL().toExternalForm());
                        current++;
                        notifyPreloader(new LoadNotification(current / total, LoadNotification.Operation.INSTALL, f.getName()));
                    }
                    for (Bundle b : framework.getBundleContext().getBundles()) {
                        if (b.getState() != Bundle.ACTIVE) {
                        // if this is fragment bundle, skip it
                            // we check for Fragment-Host header
                            if (b.getHeaders().get(Constants.FRAGMENT_HOST) != null) {
                                continue;
                            }
                            b.start();
                        }
                        current++;
                        notifyPreloader(new LoadNotification(current / total, LoadNotification.Operation.START, b.getSymbolicName()));
                    }

                    ready.setValue(Boolean.TRUE);
                } catch (BundleException | MalformedURLException bex) {
                    bex.printStackTrace(System.err);
                }
                
                
                
                notifyPreloader(new Preloader.StateChangeNotification(Preloader.StateChangeNotification.Type.BEFORE_START));
                return null;
            }
        };
        new Thread(task).start();
    }

    @Override
    public void start(final Stage stage) throws Exception {
        
        primaryStage = stage;
        BorderPane root = new BorderPane();
        Scene scene = new Scene(root, DEFAULT_STAGE_WIDTH, DEFAULT_STAGE_HEIGHT);
        primaryStage.setTitle("DEMUX Framework");
        primaryStage.setScene(scene);
        
        startFramework(); 
        
        ready.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1) {
                if(Boolean.TRUE.equals(t1)){
                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            stage.show();
                        }
                    });
                }
            }
        });
         
    }

    @Override
    public void stop() throws Exception {
        Bundle[] bundles = framework.getBundleContext().getBundles();
        for(Bundle b : bundles){
            if(b.getState() == Bundle.ACTIVE){
                b.stop();
            }
        }
        framework.stop();
        framework.waitForStop(STOP_TIMEOUT);
        super.stop(); 
        System.exit(0);
    }
    
    
}
