/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.main;


import com.vektorsoft.demux.desktop.gui.DMXWindowService;
import com.vektorsoft.demux.desktop.jfx.gui.JFXWindowService;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import javafx.stage.Stage;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator for JavaFX main application window. Instead of being installed as a bundle,
 * it is being invoked from OSGI launcher embedded in main application window.
 *
 * @author Vladimir Djurovic
 */
public class JFXActivator implements BundleActivator {
    
    /** System bundle context. */
    private BundleContext bctx;
    
    /** Service for interaction with main application window. */
    private DMXWindowService windowService;
    
    
    /** Application startup arguments. */
    private String[] arguments;
    
    /** Application configuration parameters. */
    private Map<String, String> config;
    
    /**
     * Creates new instance.
     */
    public JFXActivator(){
        arguments = new String[0];
        config = new HashMap<>();
    }

    /**
     * Starts the bundle and registers basic application services. These are:
     * <ul>
     *  <li>{@link JFXWindowService} for interaction with main application window</li>
     *  <li>{@link DMXApplication} for basic application information </li>
     * </ul>
     * 
     * @param bc bundle context
     * @throws Exception if an error occurs
     */
    @Override
    public void start(BundleContext bc) throws Exception {
        this.bctx = bc;
        
        // register application window windowService
        Dictionary<String, Object> dict = new Hashtable<>();
        dict.put("jfx.windowservice", windowService.getMainWindow());
        bctx.registerService(DMXWindowService.class, windowService, dict);
       
    }


    @Override
    public void stop(BundleContext bc) throws Exception {

    }

    /**
     * Sets main application window windowService.
     * 
     * @param service windowService to set
     */
    public void setWindowService(DMXWindowService<Stage> service) {
        this.windowService = service;
    }


    /**
     * Set arguments that application was started with.
     * 
     * @param arguments application arguments
     */
    public void setArguments(String[] arguments) {
        this.arguments = new String[arguments.length];
        System.arraycopy(arguments, 0, this.arguments, 0, arguments.length);
    }

    /**
     * Set application configuration parameters.
     * 
     * @param config configuration
     */
    public void setConfig(Map<String, Object> config) {
        config.entrySet().stream().forEach((entry) -> {
            if(entry.getValue() != null){
                this.config.put(entry.getKey(), entry.getValue().toString());
            } else {
                this.config.put(entry.getKey(), "null");
            }
        });
    }
    
}
