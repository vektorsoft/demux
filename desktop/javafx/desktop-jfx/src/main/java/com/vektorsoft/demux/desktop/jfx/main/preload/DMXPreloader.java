/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.main.preload;

import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.SplashScreen;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javafx.application.Preloader;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Preloader for main application. It will display splash screen with progress indicator 
 * while main application is starting.
 *
 * @author Vladimir Djurovic
 */
public class DMXPreloader extends Preloader {
    
    /** Splash screen image path. */
    private static final String SPLAH_IMG_PATH = "/dmx-splash.png";
    
    /** Default padding value for left, right and bottom. */
    private static final double DEFAULT_VBOX_PADDING = 5.0;
    
    /** Default component spacing value. */
    private static final double DEFAULT_SPACING = 3.0;
    
    /** Space between progress bar and image edges. */
    private static final double PBAR_WIDTH_REDUCTION = 10.0;
    
    /** Top padding for progress bar. */
    private static final double VBOX_TOP_PADDING = 260.0;

    /** Progress bar for loader. */
    private ProgressBar bar;
    
    /** Preloader window. */
    private Stage preloaderStage;
    
    /** Default progress message. */
    private String message = "";
    
    /** Progress text component. */
    private Text text;
    
    /**
     * Creates new preloader scene.
     * 
     * @param imageUrl URL of image to be displayed
     * @return scene
     */
    private Scene createPreloaderScene(URL imageUrl) {
        bar = new ProgressBar();
        
        InputStream imgStream = null;
        if(imageUrl == null){
            imageUrl = this.getClass().getResource(SPLAH_IMG_PATH);
        }
        try {
            imgStream = imageUrl.openStream();
        } catch(IOException ex){
            imgStream = this.getClass().getResourceAsStream(SPLAH_IMG_PATH);
        }
        StackPane stack = new StackPane();
        Image img = new Image(imgStream);
        ImageView imgView = new ImageView(img);
        stack.getChildren().add(imgView);
        
        VBox vbox = new VBox();
        vbox.setSpacing(DEFAULT_SPACING);
        text = new Text(message);
        bar.setPrefWidth(img.getWidth() - PBAR_WIDTH_REDUCTION);
        bar.setProgress(0);
        
        vbox.setPadding(new Insets(VBOX_TOP_PADDING, DEFAULT_VBOX_PADDING, DEFAULT_VBOX_PADDING, DEFAULT_VBOX_PADDING));
        vbox.getChildren().addAll(text, bar);
        stack.getChildren().add(vbox);
        return new Scene(stack);        
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        
        this.preloaderStage = stage;
        // check if splash screen is displayed
        URL splashImgUrl = null;
        SplashScreen splash = null;
        // this is to avoid HeadlessException if AWT is not supported
        try {
            splash = SplashScreen.getSplashScreen();
        } catch(HeadlessException ex){
            System.err.println("Could not get splash screeen");
        }
        if(splash != null){
            splashImgUrl = splash.getImageURL();
            Rectangle rect = splash.getBounds();
            stage.setX(rect.getX());
            stage.setY(rect.getY());
        }
        stage.setScene(createPreloaderScene(splashImgUrl));   
        stage.initStyle(StageStyle.UNDECORATED);
        
        stage.show();
        // close splash screen after showing prelaoder
        if(splash != null){
            splash.close();
        }
    }
    

    @Override
    public void handleApplicationNotification(PreloaderNotification pn) {
        
        if(pn instanceof LoadNotification){
            double v = ((LoadNotification)pn).getProgress();
            bar.setProgress(v);
             getMessage(((LoadNotification)pn));
           text.setText(message);
        } else if(pn instanceof StateChangeNotification) {
            if(((StateChangeNotification)pn).getType().equals(StateChangeNotification.Type.BEFORE_INIT)){
                message = "Initializing...";
                text.setText(message);
            } else {
                preloaderStage.hide();
            }
            
        }
    }
    
    /**
     * Create loader message from notification.
     * 
     * @param ln notification
     */
    private void getMessage(LoadNotification ln){
        LoadNotification.Operation op = ln.getOperation();
        String msg = ln.getMessage();
        switch(op){
            case INIT:
                message = "Initializing...";
                break;
            case INSTALL:
                message = "Installing: " + msg;
                break;
            case START:
                message = "Starting: " + msg;
                break;
            default:
                // should not have got here
        }
    }
    
}
