/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.jfx.main.preload;

import javafx.application.Preloader;

/**
 * Notification about loading progress.
 *
 * @author Vladimir Djurovic
 */
public class LoadNotification extends Preloader.ProgressNotification {
    
    /**
     * Enumeration of operation types.
     */
    public static enum Operation {
        
        /** Initialization stage. */
        INIT ,
        /** Installation stage. */
        INSTALL ,
        /** Starting stage. */
        START;
        
    }
    /** Current operation. */
    private Operation operation;
    
    /** Current message. */
    private String message;
    
    /**
     * Create new instance.
     * 
     * @param progress progress value (0.0 - 1.0)
     * @param op operation
     * @param message text message
     */
    public LoadNotification(double progress, Operation op, String message){
        super(progress);
        this.operation = op;
        this.message = message;
    }

    /**
     * Returns current operation.
     * 
     * @return operation
     */
    public Operation getOperation() {
        return operation;
    }

    /**
     * Returns message.
     * 
     * @return message
     */
    public String getMessage() {
        return message;
    }
    
    
}
