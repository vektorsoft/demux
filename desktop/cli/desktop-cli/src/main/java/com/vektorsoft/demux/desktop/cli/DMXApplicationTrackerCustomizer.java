/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.cli;


import java.lang.reflect.Field;
import java.util.Map;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * <p>
 *  Implementation of OSGI {@link ServiceTrackerCustomizer} for tracking instances of {@link DMXApplication}
 * service. It is important to note that this class is used from within main application, and thus can not 
 * interact with OSGI class loaders in a usual way, so it requires some workaround.
 * </p>
 * <p>
 *  This class uses reflection to find instances tracked service, and set environment data for application. It is
 * important to keep this class in sync with changes of {@link DMXDesktopApplication}, so reflection works correctly.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class DMXApplicationTrackerCustomizer implements  ServiceTrackerCustomizer<Object, Object> {
    
    /** Name of service implementation class. */
    private static final String SERVICE_CLASS_NAME = "com.vektorsoft.demux.desktop.app.DMXDesktopApplication";
    
    /** Name of the argument field. */
    private static final String ARGS_FIELD_NAME = "arguments";
    
    /** Name of the configuration field. */
    private static final String CONFIG_FIELD_NAME = "configuration";
    
    /** Name of GUI field. */
    private static final String GUI_FIELD_NAME = "gui";
    
    
    /** Bundle context. */
    private BundleContext context;
    
    /** Arguments that application was started with. */
    private String[] arguments;
    
    /** Configuration parameters used to launch application. */
    private Map<String, String> config;
    

    /**
     * Creates new instance.
     * 
     * @param ctx bundle context
     * @param arguments application arguments
     * @param config application configuration parameters
     */
    public DMXApplicationTrackerCustomizer(BundleContext ctx, String[] arguments, Map<String, String> config){
        this.context = ctx;
        this.config = config;
        this.arguments = new String[arguments.length];
        System.arraycopy(arguments, 0, this.arguments, 0, arguments.length);
    }

    /**
     * Invoked when service is added. This method will set arguments and configuration parameters
     * so they can be accessed at runtime.
     * 
     * @param reference service reference
     * @return service object
     */
    @Override
    public Object addingService(ServiceReference<Object> reference) {
        Object obj = context.getService(reference);
        if (obj.getClass().getName().equals(SERVICE_CLASS_NAME)) {
            try {

                Field f = obj.getClass().getDeclaredField(ARGS_FIELD_NAME);
                f.setAccessible(true);
                f.set(obj, arguments);
                f = obj.getClass().getDeclaredField(CONFIG_FIELD_NAME);
                f.setAccessible(true);
                f.set(obj, config);
                f = obj.getClass().getDeclaredField(GUI_FIELD_NAME);
                f.setAccessible(true);
                f.set(obj, true);

            } catch (Exception ex) {
                System.err.println("Could not set service values: " + ex.getMessage());
            }
        }

        return obj;
    }
    
      @Override
    public void modifiedService(ServiceReference<Object> sr, Object t) {
        
    }

    @Override
    public void removedService(ServiceReference<Object> sr, Object t) {
        
    }
}
