/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.desktop.cli;

import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Activator for CLI type of application. 
 *
 * @author Vladimir Djurovic
 */
public class DMXDesktopCLIActivator implements BundleActivator {
    
    /** Bundle context. */
    private BundleContext context;
    
    /** Arguments that application was started with. */
    private String[] runtimeArgs;
    
    /** Application configuration parameters. */
    private Map<String, String> configParams;
    
     /** Tracker for {@link DMXApplication} service. */
    private ServiceTracker<Object, Object> tracker;

    @Override
    public void start(BundleContext bc) throws Exception {
        this.context = bc;
        
        // start tracker for DMXApplication service
        DMXApplicationTrackerCustomizer customizer = new DMXApplicationTrackerCustomizer(context, runtimeArgs, configParams);
        String filterString = "(objectClass=com.vektorsoft.demux.core.app.DMXApplication)";
        Filter filter = context.createFilter(filterString);
        
        tracker = new ServiceTracker<>(context, filter, customizer);
        tracker.open();
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        tracker.close();
    }

    /**
     * Set runtime arguments of application.
     * 
     * @param runtimeArgs arguments
     */
    public void setRuntimeArgs(String[] runtimeArgs) {
        this.runtimeArgs = new String[runtimeArgs.length];
        System.arraycopy(runtimeArgs, 0, this.runtimeArgs, 0, runtimeArgs.length);
    }

    /**
     * Set application configuration parameters.
     * 
     * @param configParams configuration parameters
     */
    public void setConfigParams(Map<String, String> configParams) {
        this.configParams = configParams;
    }

}
