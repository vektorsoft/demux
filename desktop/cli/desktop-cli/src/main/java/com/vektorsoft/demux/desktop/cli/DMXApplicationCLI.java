/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
 
package com.vektorsoft.demux.desktop.cli;

import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ServiceLoader;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

/**
 * Main class for launching CLI application instances.
 *
 * @author Vladimir Djurovic
 */
public class DMXApplicationCLI {

    /**
     * Default directory where bundles are stored.
     */
    private static final String VAL_DEFAULT_BUNDLE_DIR_NAME = "bundles";
    /**
     * Property name of bundles directory.
     */
    private static final String KEY_BUNDLE_DIR_NAME = "bundles.dir";
    /**
     * Jar file extension.
     */
    private static final String JAR_EXTENSION = ".jar";
    /**
     * Amount of time to wait for framework to shut down.
     */
    private static final long STOP_TIMEOUT = 2000;
    /**
     * Name of external configuration file.
     */
    private static final String CONFIG_FILE_NAME = "config.properties";
    /**
     * File containing names of packages that should be exported from system
     * bundle.
     */
    private static final String EXTRA_PACKAGES_FILE_NAME = "/extra-packages";
    
    /** Framework factory instance. */
    private FrameworkFactory factory;
    
    /** Framework instance. */
    private Framework framework;
    
    /** Configuration parameters map. */
    private Map<String, String> config;
    
    /** Activator instance. */
    private DMXDesktopCLIActivator activator;
    

    /**
     * Check if external configuration file exists. This file is assumed to be
     * named
     * <code>config.properties</code>, and should be present in work directory.
     *
     * @return empty {@code Properties} object if file does not exist, or loaded
     * properties if it does
     * @throws IOException if an error occurs
     */
    private Properties checkForConfigOptions() throws IOException {
        Properties props = new Properties();
        File file = new File(CONFIG_FILE_NAME);

        try (FileReader reader = new FileReader(file);) {
            if (file.exists()) {
                props.load(reader);
            }
        }

        return props;
    }

    /**
     * Finds the instance of {@link FrameworkFactory} on classpath.
     *
     * @return {@code FrameworkFactory} instance
     */
    private FrameworkFactory getFrameworkFactory() {
        ServiceLoader<FrameworkFactory> loader = ServiceLoader.load(FrameworkFactory.class);
        if (loader == null) {
            System.err.println("Could not find service loader. Exiting.");
            throw new RuntimeException("Service loader not found.");
        }

        return loader.iterator().next();
    }

    /**
     * Loads configuration properties from external file. This file should be
     * named
     * <code>config.properties</code> and should be in working directory.
     *
     */
    private void loadExternalConfiguration() {
        try {
            Properties props = checkForConfigOptions();
            if (!props.isEmpty()) {
                Enumeration<?> names = props.propertyNames();
                while (names.hasMoreElements()) {
                    String name = names.nextElement().toString();
                    config.put(name, props.getProperty(name));
                }
            }
        } catch (IOException ex) {
            System.err.println("Could not read configuration properties: " + ex.getMessage());
        }
    }

    /**
     * Finds a list of packages that should be exported from system bundle.
     * These packages should be specified in a file called
     * <code>extra-packages</code> and should be located in default package.
     * File encoding must be
     * <code>UTF-8</code>
     *
     * @return list of comma separated packages
     * @throws IOException if an error occurs
     */
    private String findSystemBundleExtraPackages() throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(EXTRA_PACKAGES_FILE_NAME), "UTF-8"));) {

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
        }
        return sb.toString();
    }

    /**
     * Application main method.
     * 
     * @param args runtime arguments
     */
    public static void main(String[] args) {
        System.err.println("Starting framework....");
        final DMXApplicationCLI application = new DMXApplicationCLI();
        try {
            application.runApplication(args);
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

                @Override
                public void run() {
                    System.err.println("Running shutdown hook.");
                    application.stopApplication();
                }
            }));
            Console console = System.console();
            
            while(true){
                String cmd = console.readLine("prompt>");
                console.printf("Input: %s\n", cmd);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace(new PrintStream(System.err));
        }

    }

    /**
     * Run the application in OSGI framework.
     * 
     * @param args runtime arguments
     * @throws Exception if an error occurs
     */
    public void runApplication(String[] args) throws Exception {
        factory = getFrameworkFactory();

        config = new HashMap<>();
        // check for external configuration properties
        loadExternalConfiguration();

        // read  packages that need to be exported in system bundle
        String list = null;
        try {
            list = findSystemBundleExtraPackages();
        } catch (IOException ex) {
            System.err.println("Extra packages list file not found.");
        }

        if (list != null && !list.isEmpty()) {
            config.put(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA, list);
        }
        config.put(KEY_BUNDLE_DIR_NAME, VAL_DEFAULT_BUNDLE_DIR_NAME);
        activator = new DMXDesktopCLIActivator();
        activator.setRuntimeArgs(args);
        activator.setConfigParams(config);

        // start framework
        framework = factory.newFramework(config);
        framework.init();
        activator.start(framework.getBundleContext());

        // install and start bundles
        File bundleDir = new File(config.get(KEY_BUNDLE_DIR_NAME));
        File[] files = new File[0];
        if (bundleDir.exists()) {
            files = bundleDir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(JAR_EXTENSION);
                }
            });
        }

        for (File f : files) {
            framework.getBundleContext().installBundle(f.toURI().toURL().toExternalForm());
        }
        for (Bundle b : framework.getBundleContext().getBundles()) {
            if (b.getState() != Bundle.ACTIVE) {
                // if this is fragment bundle, skip it
                // we check for Fragment-Host header
                if (b.getHeaders().get(Constants.FRAGMENT_HOST) != null) {
                    continue;
                }
                b.start();
            }
        }
    }
    
    /**
     * Stop application and shut down OSGI framework.
     */
    public void stopApplication(){
        try{
            framework.stop();
        framework.waitForStop(STOP_TIMEOUT);
        } catch(Exception ex){
            System.err.println("Error shuting down framework: " + ex.getMessage());
        }
        
    }
}
