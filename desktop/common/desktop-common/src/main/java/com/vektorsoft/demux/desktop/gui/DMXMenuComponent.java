/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.desktop.gui;

/**
 * Represents a menu component which can be added to menu bar. It could be submenu, menu item, 
 * separator or any other menu related component. This interfaces defined methods which allow to define
 * where and how this component will be placed.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DMXMenuComponent<T> {

    /**
     * Returns an ID of parent of this component. Component will be added to menu as a child of 
     * it's parent. If this parent ID is null, and this component is a menu, it will be appended
     * directly to menu bar.
     * 
     * @return ID of the parent component
     */
    String getParentId();
    
    /**
     * Returns unique ID of this menu component. This ID must match the ID of the underlying component.
     * 
     * @return  component ID
     */
    String getComponentId();
    
    /**
     * Returns the actual menu component.
     * @return 
     */
    T getComponent();
    
    /**
     * <p>
     * Returns an integer representing preferred index at which this component should be inserted, relative to 
     * it's parent component. Platform will take best effort to set this component at specified index, but it cannot 
     * be guaranteed. If not possible, component will be added to next available index.
     * </p>
     * <p>
     *  If this value is negative, component will be appended to it's parent's end of child list.
     * </p>
     * @return 
     */
    int preferredIndex();
}
