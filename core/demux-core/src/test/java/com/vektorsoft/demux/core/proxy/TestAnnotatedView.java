/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.proxy;

import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXView;

/**
 * Represents simple annotated view used for tests.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration (parentViewId = "parent", viewID = "testView")
public class TestAnnotatedView {

    @DMXViewDeclaration.ViewUI
    private Object root;
    
    private boolean rendered = false;
    private boolean constructed = false;
    
    @DMXModelData (id = "data1")
    private int data;
    
    
    
    @DMXViewDeclaration.ConstructUI
    public void construct(){
        root = new Object();
    }
    
    @DMXViewDeclaration.PlacementConstraint
    public Object constraint(){
        return "";
    }
    
    @DMXViewDeclaration.AddChildView
    public void addChildView(DMXView view){
        
    }
    
    @DMXViewDeclaration.RemoveChildView
    public void removeView(String id){
        
    }

    public Object getRoot() {
        return root;
    }

    public void setRoot(Object root) {
        this.root = root;
    }

    public boolean isRendered() {
        return rendered;
    }

    public boolean isConstructed() {
        return constructed;
    }

    public void setData(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }
    
    
}
