/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.i18n;

import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * <p>
 *  Provides test cases for {@link DefaultLocaleManager} class.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class DefaultResourceManagerTest {
    
    /**
     * <p>
     * Verifies correct behavior of {@link DMXResourceManager#setCurrentLocale(java.util.Locale) } and {@link DMXResourceManager#getCurrentLocale() } methods. It should set
     * current application locale to a given value. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create an instance of {@link DefaultLocaleManager} class.</li>
     *  <li>Set current locale to some value</li>
     *  <li>Verify that current locale is set to a given value</li>
     *  <li>Set <code>null</code> for current locale, and verify that an exception is thrown</li>
     * </ol>
     */
    @Test
    public void testLocaleChange(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        Locale mockLocale = new Locale("xx");
        
        rm.setCurrentLocale(mockLocale);
        // verify that the same locale is current
        Locale out = rm.getCurrentLocale();
        assertEquals("Invalid current locale", out, mockLocale);
        
        try{
            rm.setCurrentLocale(null);
            fail("Did not throw exception");
        } catch (IllegalArgumentException ex){
            
        }
        
    }
    
    /**
     * <p>
     * Verifies that method {@link DMXResourceManager#getCurrentLocale() } will return JVM default locale 
     * if none is explicitely set for application. Steps taken in this step:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>invoke <code>getCurrentLocale()</code> and verify that it matches JVM default locale.</li>
     * </ol>
     */
    @Test
    public void testDefaultLocale(){
        DMXLocaleManager manager = new DefaultLocaleManager();
        Locale def = Locale.getDefault();
        
        Locale out = manager.getCurrentLocale();
        assertEquals("invalid default locale", out, def);
    }
    
    /**
     * <p>
     * Verifies correct setting/getting supported locales.This method uses {@link DMXResourceManager#setSupportedLocales(java.util.Locale[]) } and
     * {@link DMXResourceManager#getSupportedLocales() } to verify this features. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>Create mock locales to be supported</li>
     *  <li>Verify that mock locales are showing as supported</li>
     *  <li>Verify that exception is thrown when <code>null</code> is set as supported locale list</li>
     * </ol>
     */
    @Test
    public void supportedLocalesTest(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        Locale loc1 = new Locale("xx");
        Locale loc2 = new Locale("yy");
        
        List<Locale> locales = new ArrayList<Locale>();
        locales.add(loc1);
        locales.add(loc2);
        rm.setSupportedLocales(locales.toArray(new Locale[2]));
        // verify
        Locale[] out = rm.getSupportedLocales();
        assertEquals("Invalid supproted locales size",2,out.length);
        
        try {
            rm.setSupportedLocales(null);
            fail();
        } catch(IllegalArgumentException ex){
            
        }
        
    }
    
    /**
     * <p>
     * Verifies correct adding of new supported locale to application. This new locale should be added to the list of 
     * supported locales. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>Create new mock locale and add it to supported locales</li>
     *  <li>Verify that it is added correctly</li>
     * </ol>
     */
    @Test
    public void addLocaleTest(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        Locale loc = new Locale("xx");
        rm.addSupportedLocale(loc);
        
        //verify
        Locale[] out = rm.getSupportedLocales();
        assertEquals("Invalid array size", 1, out.length);
        assertEquals("Invalid locale", out[0], loc);
    }
    
    /**
     * <p>
     *  Verifies that supported locales are cleared. This will remove all entries from supported 
     * locales list, and also set current locale to default. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>Set a couple of mock supported locales </li>
     *  <li>Clear locales and verify empty array is returned</li>
     * </ol>
     */
    @Test
    public void clearLocaleTest(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        rm.addSupportedLocale(Locale.CANADA);
        rm.addSupportedLocale(Locale.FRENCH);
        
        Locale[] out = rm.getSupportedLocales();
        assertEquals("Invalid locale count", 2, out.length);
        rm.clearSupportedLocales();
        // verify that empty array is returned
        out = rm.getSupportedLocales();
        assertTrue("Locale array not empty", out.length == 0);
    }
    
    
    private class TestObserver implements Observer {
        
        private boolean invoked = false;

        @Override
        public void update(Observable o, Object arg) {
            invoked = true;
        }

        public boolean isInvoked() {
            return invoked;
        }
        
    }
    
}