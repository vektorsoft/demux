/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.extension;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import java.util.HashMap;
import java.util.Map;

/**
 * Instance of {@code DMXExtensionCallback} used for testing.
 *
 * @author Vladimir Djurovic
 */
public class TestExtensionCallback implements DMXExtensionCallback {

    private Map<String, Object> dataMap;
    
    public TestExtensionCallback(){
        dataMap = new HashMap<String, Object>();
    }
    

    public String callback(DMXLocalModel model, String data){
        int val = model.get("testVal", Integer.class,3);
        val++;
        model.set("testVal", val);
        return data;
    }


    @Override
    public String[] getMappings() {
        return new String[]{"testController"};
    }

    @Override
    public String[] getCallbackData() {
       return new String[]{"testVal"};
    }



    @Override
    public DMXExtensionType getExtensionType() {
        return DMXExtensionType.CONTROLLER_EXTENSION;
    }

    @Override
    public DMXResourceHandler getResourceHandler() {
        return new DefaultResourceHandler();
    }

}
