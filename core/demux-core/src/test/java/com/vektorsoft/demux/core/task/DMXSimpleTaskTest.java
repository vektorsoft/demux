/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.task;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Vladimir Djurovic
 */
public class DMXSimpleTaskTest {
    
    public DMXSimpleTaskTest() {
    }

    /**
     * <p>
     *  Tests basic characteristics of {@link DMXBasicTask} class. This test will verify that new instance
     * is correctly created.
     * </p>
     */
    @Test
    public void basicTest(){
        DMXBasicTask task = new DMXBasicTask("controller");
        assertEquals("Invalid controller ID", "controller", task.getControllerId());
        assertEquals("Wrong blocking scope", GUIBlockingScope.NONE, task.getBlockingScope());
    }
}