/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.event.DMXEvent;
import java.util.HashMap;
import java.util.Map;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import org.mockito.Matchers;

/**
 * Contains test cases for class {@link DMXLocalModel}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXLocalModelTest {

    /**
     * Verifies correct notification of model change listeners when data is modified.
     */
    @Test
    public void testListenerNotification(){
        DMXEventBus bus = Mockito.mock(DMXEventBus.class);
        
        // no listener set, should not invoke
        DMXLocalModel.instance().set("data", "some data");
        Mockito.verifyZeroInteractions(bus);
        
        // set listener and verify it has been notified
        DMXLocalModel model = DMXLocalModel.instance();
        model.setEventBus(bus);
        model.set("simeid", "some data");
        Map<String, Object> tmp = new HashMap<String, Object>();
        tmp.put("simeid", "some data");
        DMXModelEvent event = new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, null, tmp);
        Mockito.verify(bus).publish(Matchers.any(DMXEvent.class));
    }
    
    /**
     * Verifies correct data retrieval.
     */
    @Test
    public void testGetData(){
        DMXLocalModel model = DMXLocalModel.instance().set("id1", "data1").set("id2", 2);
        
        String s = model.get("id1", String.class);
        int i = model.get("id2", Integer.class);
        assertEquals("Invalid strng value", "data1", s);
        assertEquals("Invalid int value", 2, i);
        
        // test default value
        int k = model.get("id3", Integer.class, 5);
        assertEquals("Invalid default int value", 5, k);
    }
}
