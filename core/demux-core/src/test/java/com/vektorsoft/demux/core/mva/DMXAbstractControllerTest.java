/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import org.junit.Assert;
import org.junit.Test;

/**
 * Provides test cases for {@link DMXAbstractController} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXAbstractControllerTest {

    /**
     * <p>
     *  Verifies correct behavior of {@link DMXAbstractController#getControllerId() } method. It should return 
     * name of the class that extends {@link DMXAbstractController}.
     * </p>
     */
    @Test
    public void getMappingTest(){
        TestController ctrl = new TestController("data1","data2");
        String mapping = ctrl.getControllerId();
        Assert.assertEquals("Invalid controller mapping", mapping, TestController.class.getName());
    }
    
    
    private class TestController extends DMXAbstractController {
        
        public TestController(String... args){
            super(args);
        }
        
        @Override
        public void execute(DMXLocalModel mode, Object... params) throws ExecutionException {
            
        }

        
    }
    
}
