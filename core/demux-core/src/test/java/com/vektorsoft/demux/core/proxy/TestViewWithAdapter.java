/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.proxy;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXAdapter;

/**
 * Test view with {@link DMXAdapter} as
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration(parentViewId = "parent", viewID = "testView")
public class TestViewWithAdapter {

    @DMXViewDeclaration.ViewUI
    private Object root;
    
    @DMXViewDeclaration.PlacementConstraint
    private Object placement;

    private DMXAdapter adapter;

    
    public TestViewWithAdapter(DMXAdapter adapter){
        this.adapter = adapter;
    }

    @DMXViewDeclaration.ConstructUI
    public void construct() {
        root = new Object();
    }

    public Object getRoot() {
        return root;
    }

    public void setRoot(Object root) {
        this.root = root;
    }


    public Object getPlacement() {
        return placement;
    }
    
}
