/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.proxy;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;

/**
 * Class for testing annotated dialog views.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration (parentViewId = "dlgParent", viewID = "dlgView", isDialog = true, modal = true, title = "Dialog title")
public class AnnotatedDialogView {

    @DMXViewDeclaration.ViewUI
    private Object root;
    
    private DMXDialog dialog;
    
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
        
    }

    public Object getRoot() {
        return root;
    }

    public void setRoot(Object root) {
        this.root = root;
    }

    public void setDialog(DMXDialog dialog) {
        this.dialog = dialog;
    }

    public DMXDialog getDialog() {
        return dialog;
    }
   
}
