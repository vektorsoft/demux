/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.proxy;

import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.proxy.impl.ViewInvocationHandler;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Contains test cases for class {@link ViewInvocationHandler}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ViewInvocationHandlerTest {
    
    private DMXAdapter adapter;
    
    @Before
    public void setup(){
        adapter = mock(DMXAdapter.class);
    }

    /**
     * Verifies that view invocation handler is correctly registered.
     */
    @Test
    public void testInitialization() throws Throwable {
        ViewInvocationHandler handler = new ViewInvocationHandler(new TestAnnotatedView());
        Field underlying = handler.getClass().getDeclaredField("underlying");
        underlying.setAccessible(true);
        TestAnnotatedView testView = (TestAnnotatedView)underlying.get(handler);
        
        assertNotNull("underlying oject is null", testView);
        
        // asert that view is correctly registered
        Method viewIdMethod = DMXView.class.getDeclaredMethod("getViewId");
        Method parentIdMethod = DMXView.class.getDeclaredMethod("getParentViewId");
        Method viewDataIdsMethod = DMXView.class.getDeclaredMethod("getViewDataIds");
        
        assertEquals("Invalid view ID", "testView", handler.invoke(this, viewIdMethod, null));
        assertEquals("Invalid parent ID", "parent", handler.invoke(this, parentIdMethod, null));
        String[] ids = (String[])handler.invoke(null, viewDataIdsMethod, null);
        assertEquals("Invalid IDs size", 1, ids.length);
        
    }
    
    /**
     * Verifies correct initialization of view with {@code DMXAdapter} as parameter 
     * of constructor.
     */
    @Test
    public void testViewWithAdapter() throws Throwable{
        ViewInvocationHandler handler = new ViewInvocationHandler(new TestViewWithAdapter(adapter));
        
         // asert that view is correctly registered
        Method viewIdMethod = DMXView.class.getDeclaredMethod("getViewId");
        Method parentIdMethod = DMXView.class.getDeclaredMethod("getParentViewId");
        
        assertEquals("Invalid view ID", "testView", handler.invoke(this, viewIdMethod, null));
        assertEquals("Invalid parent ID", "parent", handler.invoke(this, parentIdMethod, null));
    }
    
    /**
     * Verifies correct initialization of dialog view.
     */
    @Test
    public void testDialogViewInitialization() throws Throwable {
        ViewInvocationHandler handler = new ViewInvocationHandler(new AnnotatedDialogView());
        Field isDialogField = handler.getClass().getDeclaredField("isDialog");
        isDialogField.setAccessible(true);
        assertNotNull(isDialogField);
        assertTrue("Invalid isDialog value", isDialogField.getBoolean(handler));
        
        
         // asert that view is correctly registered
        Method viewIdMethod = DMXView.class.getDeclaredMethod("getViewId");
        Method parentIdMethod = DMXView.class.getDeclaredMethod("getParentViewId");
        Method isModalMethod = DMXDialogView.class.getDeclaredMethod("isModal");
        Method dialogTitleMethod = DMXDialogView.class.getDeclaredMethod("getDialogTitle");
        
        
        assertEquals("Invalid view ID", "dlgView", handler.invoke(this, viewIdMethod, null));
        assertEquals("Invalid parent ID", "dlgParent", handler.invoke(this, parentIdMethod, null));
        assertTrue("Invalid modality", (Boolean)(handler.invoke(null, isModalMethod, null)));
        assertEquals("Invalid dialog title", "Dialog title", handler.invoke(null, dialogTitleMethod, null));
    }
}
