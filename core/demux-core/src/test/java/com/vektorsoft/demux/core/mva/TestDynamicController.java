/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *  Dynamic controller for testing.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TestDynamicController extends DMXAbstractController{

    @Override
    public String getControllerId() {
        return "testDynamicController";
    }
    
    

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        int value = model.get(params[0].toString(), Integer.class);
        value += 5;
        model.set(params[0].toString(), value);
    }

    @Override
    public boolean isDynamic() {
        return true;
    }

    
}
