/******************************************************************************   
 *
 *  Copyright 2012 Vektor Software
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Contains unit tests for {@link AbstractViewManager} class.
 * 
 * @author Vladimir Djurovic
 */
public class AbstractViewManagerTest {

    /**
     * <p>
     * Verifies correct behavior of {@link AbstractViewManager#registerView(com.vektorsoft.demux.core.mva.DMXView) }
     * method. This method should register view with this view manager.
     * </p>
     * <p>
     *  Steps taken in this test:
     *  <ol>
     *  <li>Create dummy view manager for testing</li>
     *  <li>Create mock view</li>
     *  <li>register view by invoking tested method</li>
     *  <li>verify that view is registered with view manager</li>
     *  </ol>
     * </p>
     */
    @Test
    public void registerViewTest(){
        AbstractViewManager mngr = new DummyViewManager();
        DMXView view = Mockito.mock(DMXView.class);
        Mockito.when(view.getViewId()).thenReturn("testview");
        Mockito.when(view.getViewDataIds()).thenReturn(new String[]{"id1"});
        mngr.registerView(view);
        // verify that view is registered
        DMXView out = mngr.getView("testview");
        Assert.assertEquals("Invalid view registration", out, view);
    }
    
    
    /**
     * <p>
     *  Verifies correct operation when view is registered with the same data IDs that is
     * already registered. In this case, view ID should be added to a set of views that are
     * registered to monitor this data.
     * </p>
     * <p>
     *  Steps taken in this test:
     * <ol>
     *  <li>Create an instance of AbstractViewManager</li>
     *  <li>Create mock view with some data and register it.</li>
     *  <li>Create another view with the same data and register</li>
     *  <li>Verify that both views refer to the same data</li>
     * </ol>
     * </p>
     */
    @Test
    public void registerViewWithExistingData(){
        AbstractViewManager mngr = new DummyViewManager();
        DMXView view1 = Mockito.mock(DMXView.class);
        Mockito.when(view1.getViewId()).thenReturn("view1");
        Map<String, Object> data1 = new HashMap<String, Object>();
        data1.put("d1", "value1");
        data1.put("d2", "value2");
        
        DMXController controller1 = Mockito.mock(DMXController.class);
        Mockito.when(controller1.getControllerData()).thenReturn(data1.keySet().toArray(new String[1]));
        
        Mockito.when(view1.getViewDataIds()).thenReturn(data1.keySet().toArray(new String[1]));
        // create another mock view
        DMXView view2 = Mockito.mock(DMXView.class);
        Mockito.when(view2.getViewId()).thenReturn("view2");
        Map<String, Object> data2 = new HashMap<String, Object>();
        data2.put("d1", "value1");
        data2.put("d2", "value2");
        
        Mockito.when(view2.getViewDataIds()).thenReturn(data2.keySet().toArray(new String[2]));
        // register view
        mngr.registerView(view1);
        mngr.registerView(view2);
      
    }
    
    /**
     * <p>
     * Verifies correct behavior of "deferred" view registration. This is the case where view registration 
     * can not be completed, because parent view has not been registered yet. In this case, view being registered
     * should be stored in "pending" views data, and it's registration is deferred until parent view is registered.
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create mock parent and child views</li>
     * <li>Register child view before the parent, and verify that it has been stored in pending queue</li>
     * <li>Register parent view and verify that child view registration is completed</li>
     * </ul>
     */
    @Test
    public void testDefferedViewRegistration() throws Exception{
        DMXView parentView = Mockito.mock(DMXView.class);
        Mockito.when(parentView.getViewId()).thenReturn("parent");
        
        DMXView childView = Mockito.mock(DMXView.class);
        Mockito.when(childView.getViewId()).thenReturn("childView");
        Mockito.when(childView.getParentViewId()).thenReturn("parent");
        
        DMXView childView1 = Mockito.mock(DMXView.class);
        Mockito.when(childView1.getViewId()).thenReturn("childView1");
        Mockito.when(childView1.getParentViewId()).thenReturn("parent");
        
        AbstractViewManager manager = new DummyViewManager();
        manager.registerView(childView);
        manager.registerView(childView1);
        
        // verify that view is stored in queue
        Field f = manager.getClass().getSuperclass().getDeclaredField("deferredViews");
        f.setAccessible(true);
        Map map = (Map)f.get(manager);
        Assert.assertFalse("View map is empty", map.keySet().isEmpty());
        Assert.assertTrue("Parent view ID not in map", map.containsKey("parent"));
        Set<String> children = (Set<String>)map.get("parent");
        Assert.assertEquals("Invalid view set size", 2, children.size());
        Assert.assertTrue("View ID not found", children.contains("childView"));
        
        // register parent and verify deferred views processing
        manager.registerView(parentView);
        f = manager.getClass().getSuperclass().getDeclaredField("deferredViews");
        f.setAccessible(true);
        map = (Map)f.get(manager);
        Assert.assertFalse("View ID still in map", map.containsKey("parent"));
    }
    
    
    /**
     * Verifies that view is unregistered correctly.
     */
    @Test
    public void removeViewTest(){
        DMXView parentView = Mockito.mock(DMXView.class);
        Mockito.when(parentView.getViewId()).thenReturn("parentView");
        
        DMXView childView = Mockito.mock(DMXView.class);
        Mockito.when(childView.getViewId()).thenReturn("childView");
        Mockito.when(childView.getParentViewId()).thenReturn("parentView");
        Mockito.when(childView.getViewDataIds()).thenReturn(new String[]{"data1", "data2"});
        
        DummyViewManager manager = new DummyViewManager();
        manager.registerView(parentView);
        manager.registerView(childView);
        
        // verify that data is set correctly
        Map<String, DMXView> viewMap = manager.getRegisteredViews();
        Assert.assertTrue("View not contained", viewMap.containsKey("parentView"));
        Assert.assertTrue("View not contained", viewMap.containsKey("childView"));
        
        manager.unregisterView("childView");
        viewMap = manager.getRegisteredViews();
        Assert.assertTrue("View not contained", viewMap.containsKey("parentView"));
        Assert.assertFalse("Removed view contained", viewMap.containsKey("childView"));
    }
    
    /**
     * Dummy implementation of {@link DMXViewManager} used for testing.
     */
    private class DummyViewManager extends AbstractViewManager {

        public DummyViewManager() {
            adapter = new DMXDefaultAdapter();
        }

        @Override
        public void registerView(DMXView view) {
            super.registerView(view); 
            afterInitialViewRegistration(view);
        }

        @Override
        public void selectView(String viewId, Map dataValues) {
            
        }

        @Override
        protected void processDeferredViews(String parentId, Set<String> childrenIds) {
            
        }
        

        @Override
        public void  blockGui(boolean state, GUIBlockingScope scope, String... ids) {
            
        }

        @Override
        protected void removeFromParent(String viewId) {
            
        }

        @Override
        public <T extends DialogFactory> T getDialogFactory() {
            return null;
        }


       public Map<String, DMXView> getRegisteredViews(){
           return registeredViews;
       }

        @Override
        protected void applyViewExtension(String viewId, DMXExtensionCallback callback) {
            
        }

        
    }
}
