/******************************************************************************   
 *
 *  Copyright 2012 Vektor Software
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/

package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.model.DMXModel;
import com.vektorsoft.demux.core.annotations.AnnotatedTestController;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import com.vektorsoft.demux.core.task.DMXBasicTask;
import com.vektorsoft.demux.core.task.TestController;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.event.DMXEvent;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.Mockito;

/**
 * Test cases for {@link DMXDefaultAdapter} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXAdapterTest {
    
     private DMXAdapter adapter;
     private DMXViewManager mockViewManager;
    
    @Before
    public void init(){
        DMXLocaleManager manager = mock(DMXLocaleManager.class);
        mockViewManager = mock(DMXViewManager.class);
        adapter = new DMXDefaultAdapter();
        ((DMXDefaultAdapter)adapter).setViewManager(mockViewManager);
    }
    
    @Test
    public void testDataRegistration() throws Exception {
        adapter.registerData("testData1", 1);
        adapter.registerData("testData2", "2");
        
        // verify correct registration
        Field field = adapter.getClass().getDeclaredField("dataModel");
        field.setAccessible(true);
        DMXModel model = (DMXModel)field.get(adapter);
        
        assertEquals("Invalid testData1 value", 1, model.getDataValue("testData1"));
        assertEquals("Invalid testData2 value", "2", model.getDataValue("testData2"));
    }
    
    /**
     * <p>
     *  Verifies correct behavior of {@link DMXDefaultAdapter#registerController(com.vektorsoft.demux.core.mva.DMXController) } method.
     *  Correct behavior is to model controller to it's target and use it as necessary. 
     * </p>
     * <p>
     *  Steps taken in this test:
     * <ol>
     *  <li>Instantiate <code>DMXDefaultAdapter</code></li>
     *  <li>Create mock DMXController instance and register it with adapter</li>
     *  <li>Verify that controller is added to model</li>
     * </ol>
     * </p>
     * @throws Exception if error occurs
     */
    @Test
    public void registerControllerTest() throws Exception{
        DMXController ctrl = mock(DMXController.class);
        when(ctrl.getControllerId()).thenReturn("mockController");
        adapter.registerController(ctrl);
        
        // verify registration
        Field ctrlMap = adapter.getClass().getDeclaredField("controllerMap");
        ctrlMap.setAccessible(true);
        Map<String, DMXController> map = (Map<String, DMXController>)ctrlMap.get(adapter);
        assertTrue("Controller not in mapp", map.containsKey("mockController"));
        assertEquals("Controller instance mismatch", ctrl, map.get("mockController"));
    }
    
    /**
     * <p>
     *  Verifies correct behavior when attempting to register controller with invalid
     * data, such as mapping being <code>null</code>. Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create instance of {@link DMXDefaultAdapter}</li>
     *  <li>Create mock controller which has <code>null</code> for mapping</li>
     *  <li>Create mock controller which has empty string for mapping.</li>
     *  <li>Attempt to register controllers and verify that exceptions are thrown</li>
     *  <li>Attempt to register <code>null</code> as controller and verify that exception is thrown.</li>
     * </ul>
     */
    @Test (expected = IllegalArgumentException.class)
    public void registerControllerInvalidArgs() {
        DMXDefaultAdapter adapter = new DMXDefaultAdapter();
        
        DMXController ctrl = mock(DMXController.class);
        when(ctrl.getControllerId()).thenReturn(null);
        try {
            adapter.registerController(ctrl);
            // should not have reach this
            Assert.fail("Did not throw exception for null mapping");
        } catch(IllegalArgumentException ex){
            
        }
        
        DMXController ctrl1 = mock(DMXController.class);
        when(ctrl1.getControllerId()).thenReturn("");
        try {
            adapter.registerController(ctrl1);
            // should not have reach this
            Assert.fail("Did not throw exception for empty mapping");
        } catch(IllegalArgumentException ex){
            
        }
        adapter.registerController((DMXController)null);
        Assert.fail("Did not throw exception for null controller.");
    }
    
    
    /**
     * <p>
     *  Verifies successful invocation of controller. Invocation should return without exceptions, and controllers 
     * <code>execute()<code> method should be called. Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create new adapter instance</code>
     *  <li>Create mock controller and register it</code>
     *  <li>invoke controller and verify that <code>execute()</code> method was called</li>
     * </ul>
     */
    @Test
    public void invokeControllerSuccessTest(){
          
        
        DMXController controller = Mockito.mock(DMXController.class);
        when(controller.getControllerId()).thenReturn("ctrl");
        when(controller.getControllerData()).thenReturn(new String[0]);
        adapter.registerController(controller);
        // invoke controller
        adapter.invokeController("ctrl", "arg");
        // verify
        try {
            Mockito.verify(controller).execute(any(DMXLocalModel.class),anyString());
        } catch(ExecutionException ex){
            //should not get here
            Assert.fail();
        }
        
    }
    
    /**
     * Verifies correct invocation of dynamic controller.
     */
    @Test
    public void invokeDynamicControllerTest(){
        DMXModel model = new DMXModel();
        ((DMXDefaultAdapter)adapter).setModel(model);
        
        adapter.registerData("data1", 1);
        adapter.registerController(new TestDynamicController());
        adapter.invokeController("testDynamicController", "data1", "somearg");
        
        // verify that model data has correct value
        assertEquals("Invalid model value", 6, model.getDataValue("data1"));
    }
    
    
    /**
     * <p>
     *  Verifies that adapter executes background tasks correctly. Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create instance of adapter</li>
     *  <li>Register controller with long running execution</li>
     *  <li>execute task with specified controller</li>
     * </ul>
     */
    @Test
    public void executeTaskTest(){
        DMXDefaultAdapter adapter = new DMXDefaultAdapter();
        DMXViewManager viewManager = Mockito.mock(DMXViewManager.class);
        adapter.setViewManager(viewManager);
        
        
        
        TestController controller = new TestController("test1", "count");
        adapter.registerController(controller);
        
        DMXBasicTask task = new DMXBasicTask(controller.getControllerId());
        adapter.executeTask(task);
    }
    
    /**
     * Verifies correct {@code ModelChangeListeners} notifications when model data is changed.
     */
    @Test
    public void testListenersNotification(){
        DMXModel model = new DMXModel();
        ((DMXDefaultAdapter)adapter).setModel(model);
        
        // create mock event bus
        DMXEventBus bus = mock(DMXEventBus.class);
        ((DMXDefaultAdapter)adapter).setEventBus(bus);
        
        adapter.registerData("data1", "value1");
        assertTrue("Data not in model", model.isDataRegistered("data1"));
        assertEquals("Invalid initial data value", "value1", model.getDataValue("data1"));
        Map<String, Object> tmp = new HashMap<String, Object>();
        tmp.put("data1", "value1");
        DMXModelEvent event = new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_REGISTERED, null, tmp);
        verify(bus).publish(any(DMXEvent.class));
        
        
        // verify that view update is invoked on model data change
        model.setDataValues("data1", "value2");
        Map<String, Object> old = new HashMap<String, Object>();
        old.put("data1", "value1");
        tmp.put("data1", "value2");
        event = new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, old, tmp);
        verify(bus, times(2)).publish(any(DMXEvent.class));
        
        
        adapter.unregisterData("data1");
        event = new DMXModelEvent("data1");
        verify(bus, times(3)).publish(any(DMXEvent.class));
        
    }
    
}
