/******************************************************************************   
 *
 *  Copyright 2012 Vektor Software
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/


package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.event.DMXEvent;
import java.util.HashMap;
import java.util.Map;
import junit.framework.Assert;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;

/**
 * Unit tests for {@link DMXModel} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXModelTest {

    /**
     * <p>
     * Verifies correct behavior of {@link DMXModel#registerData(java.lang.String, java.lang.Object) } method. This 
     * method should register specified data with the model so it can be accessed by ID.
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Instantiate <code>DMxModel</code> instance.</li>
     *  <li>Create sample data for registering it with the model</li>
     *  <li>Register data and verify that it is present at the model map.</li>
     * </ul>
     */
    @Test
    public void registerDataTest(){
        DMXModel model = new DMXModel();
        // create sample data
        String s1 = "testdata";
        int i1 = 12;
        boolean b = true;
        // register data
        model.registerData("stringData", s1);
        model.registerData("intData", i1);
        model.registerData("boolData", b);
        // verify correctness
        Assert.assertNotNull("stringData is null", model.getDataValue("stringData"));
        Assert.assertNotNull("intData is null", model.getDataValue("intData"));
        Assert.assertEquals("String mismatch", s1, model.getDataValue("stringData"));
        Assert.assertEquals("Integer mismatch", i1, model.getDataValue("intData"));
        Assert.assertTrue("Boolean mismatch",  (Boolean)model.getDataValue("boolData"));
        // verify bulk data fetch
        Map<String, Object> out = model.getDataValues("stringData", "intData", "boolData");
        Assert.assertEquals("String data mismatch in map", s1, out.get("stringData"));
        Assert.assertEquals("Integer data mismatch in map", i1, out.get("intData"));
        Assert.assertTrue("Boolean data mismatch in map",  (Boolean)out.get("boolData"));
    }
    
    /**
     * <p>
     *  Verifies that model change listeners are notified about the model data change. If correct, each 
     * listener's correct method will be invoked, depending on whether data is registered, unregistered or changed.
     * </p>
     * <p>
     *  Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create new <code>DMXModel</code> instance and add mock observer.</li>
     *  <li>Register some test data and verify that listener is notified</li>
     *  <li>modify test data and verify that listener is notified </li>
     *  <li>unregister data and verify that listener is notified</li>
     * </ul>
     */
    @Test
    public void modelEVentsTest(){
        DMXModel model = new DMXModel();
        DMXEventBus bus = Mockito.mock(DMXEventBus.class);
        model.setEventBus(bus);
        
        String s = "test data";
        model.registerData("testString", s);
        Map<String, Object> tmp = new HashMap<String, Object>();
        tmp.put("testString", s);
        Mockito.verify(bus).publish(Matchers.any(DMXEvent.class));
        
        
        model.setDataValues("testString", "new string");
        Map<String, Object> changeset = new HashMap<String, Object>();
        changeset.put("testString", "new string");
        tmp.put("testString", "new string");
        Map<String, Object> old = new HashMap<String, Object>();
        old.put("testString", s);
        
        Mockito.verify(bus, times(2)).publish(Matchers.any(DMXEvent.class));
        
        model.unregisterData("testString");
        
        Mockito.verify(bus, times(3)).publish(Matchers.any(DMXEvent.class));
    }
    
    /**
     * <p>
     *  Verifies that null values are correctly registered. 
     * </p>
     */
    @Test
     public void nullValueTest(){
         DMXModel model = new DMXModel();
        // create sample data
        String s1 = null;
        // register data
        model.registerData("stringData", s1);
        // verify correctness
        Assert.assertNull("stringData is not null", model.getDataValue("stringData"));
        
        model.setDataValues("stringData", null);
        Assert.assertNull("stringData is not null", model.getDataValue("stringData"));
     }
     
     /**
      * Verifies that all model data are returned correctly.
      */
     @Test
     public void allValuesTest(){
         DMXModel model = new DMXModel();
         model.registerData("d1", "data1");
         model.registerData("d2", "data2");
         
         Map<String, Object> out = model.getAllData();
         Assert.assertEquals("Invalid data size", 2, out.size());
         Assert.assertEquals("Invalid data value", "data1", out.get("d1"));
         Assert.assertEquals("Invalid data value", "data2", out.get("d2"));
     }
}
