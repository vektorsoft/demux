/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;

/**
 * Contains unit tests for {@link DMXAbstractView} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXAbstractViewTest {

    /**
     * <p>
     *  Verifies that {@link DMXAbstractView#getViewId() } returns implementing class name. Steps taken in
     * this test:
     * </p>
     * <ul>
     *  <li>Create instance of {@link TestView} class</li>
     *  <li>Invoke {@link TestView#getViewId() } method and verify it returns class name</li>
     * </ul>
     */
    @Test
    public void getViewIdTest(){
        DMXDefaultAdapter adapter = new DMXDefaultAdapter();
        TestView view = new TestView();
        view.setAdapter(adapter);
        String id = view.getViewId();
        System.err.println("View ID: " + id);
        Assert.assertEquals("Invalid view ID", id, TestView.class.getName());
    }
    
    private class TestView extends DMXAbstractView {
        
        private int testInt;

        @Override
        public Object getViewUI() {
            return null;
        }

        @Override
        public void constructUI() {
            
        }

        @Override
        public void setViewActive(String id, boolean state) {
            // do nothing
        }

        @Override
        public void setAdapter(DMXAdapter adapter) {
            super.setAdapter(adapter);
        }
        
    }
}
