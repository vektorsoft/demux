/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.annotations;

import com.vektorsoft.demux.core.mva.DMXController;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertTrue;

/**
 * Contains test cases for testing proxy objects for controllers and views.
 *
 * @author Vladimir Djurovic
 */
public class ProxyTest {

    /**
     * <p>
     * Verifies that proxied object implements all required interfaces. Proxy should implement 
     * all interfaces of original object plus any added interfaces. Steps taken in this tes:
     * </p>
     * <ul>
     *  <li>create instance of {@code AnnotatedTestController}</li>
     *  <li>create proxy and verify that it implements all required interfaces</li>
     *  <li>create subclass of {@code AnnotatedTestController} and verify that proxy implements all required interfaces </li>
     * </ul>
     */
    @Test
    public void proxyInterfacesTest(){
        
        AnnotatedTestController ctrl = new AnnotatedTestController();
        InvocationHandler handler = Mockito.mock(InvocationHandler.class);
        
        List<Class> interfaces = new ArrayList<Class>();
        interfaces.add(DMXController.class);
        interfaces.addAll(Arrays.asList(ctrl.getClass().getInterfaces()));
        
        Object proxy = Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces.toArray(new Class[interfaces.size()]), handler);
        assertTrue("Invalid interface", (proxy instanceof DMXController));
        assertTrue("Invalid interface", (proxy instanceof TestInterface));
        
        SubController subctrl = new SubController();
        interfaces = new ArrayList<Class>();
        interfaces.add(DMXController.class);
        interfaces.addAll(Arrays.asList(subctrl.getClass().getSuperclass().getInterfaces()));
        proxy = Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces.toArray(new Class[interfaces.size()]), handler);
        assertTrue("Invalid interface", (proxy instanceof TestInterface));
    }
    
    private class SubController extends AnnotatedTestController {
        
    }
}
