/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.i18n;

import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.util.Locale;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * <p>
 *  Provides test cases for {@link DMXLocaleManager} class.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class DeafultResourceHandlerTest {

    /**
     * <p>
     * Verifies correct loading of bundles. When bundle is loaded, it's contents should be placed in
     * resource map, so they can be accessed when needed. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>Create mock bundles to load</li>
     *  <li>Lod bundles and verify that resources are available from {@code DMXResourceManager}</li>
     *  <li>Verify that event listeners are invoked.</li>
     * </ol>
     */
    @Test
    public void loadBundleTest() {
        DMXLocaleManager rm = new DefaultLocaleManager();
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.setCurrentLocale(rm.getCurrentLocale());
        
        // create mock bundles
        handler.loadResourceBundle("test", this.getClass().getClassLoader());
        handler.loadResourceBundle("com.vektorsoft.demux.core.i18n.TestBundle", this.getClass().getClassLoader());
        // verify that properties are registered
        String out = handler.getString("stringKey");
        assertEquals("Invalid property string", out, "String Value");
        Object obj = handler.getResource("key1");
        assertEquals("invalid resource", obj.toString(), "Object 1");
        obj = handler.getResource("key2");
        assertEquals("invalid resource", obj.toString(), "Object 2");
        
    }
    
    
     /**
     * <p>
     *  Verifies that message formatting works correctly. Message should be formatted correctly based on current
     * locale and supplied arguments. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>Load resources bundles</li>
     *  <li>invoke formatting message, and verify that string is formatted correctly</li>
     * </ol>
     */
    @Test
    public void formatMessageTest(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.setCurrentLocale(rm.getCurrentLocale());
        handler.loadResourceBundle("test", this.getClass().getClassLoader());
        
        String out = handler.formatMessage("formatKey", "test string", 5);
        // verify correct output
        assertEquals("invalid formatted string", out, "Format argument 0 test string as string, argument 2  5 as integer");
               
    }
    
    /**
     * <p>
     *  Verify that all bundles are reloaded on locale change. Steps taken in this test:
     * </p>
     * <ol>
     *  <li>Create new instance of {@link DefaultLocaleManager}</li>
     *  <li>load test resources for language xx and verify they are loaded correctly</li>
     *  <li>change locale to language yy, and verify that resource are changed</li>
     * </ol>
     */
    @Test
    public void bundleReloadTest(){
        DMXLocaleManager rm = new DefaultLocaleManager();
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.setCurrentLocale(rm.getCurrentLocale());
        
        
        Locale testLoc = new Locale("xx");
        rm.setCurrentLocale(testLoc);
        handler.setCurrentLocale(rm.getCurrentLocale());
        handler.loadResourceBundle("reloadtest", getClass().getClassLoader());
        // verify resouces for language xx
        String val = handler.getString("key1");
        assertEquals("Invalid resource string", val, "Resource xx");
        // change locale
        testLoc = new Locale("yy");
        rm.setCurrentLocale(testLoc);
        handler.setCurrentLocale(testLoc);
        val = handler.getString("key1");
        assertEquals("Invalid resource string", val, "Resource yy");
    }
}
