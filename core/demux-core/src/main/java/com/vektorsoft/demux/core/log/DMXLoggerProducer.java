/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.log;


/**
 * <p>
 * Defines methods for classes which produce loggers based on specific implementation. For example, clients
 * may choose to use loggers based on <code>log4j, SLF4j</code> etc. In that case, an implementation of this 
 * interface should be used to create loggers based on those platforms.
 * </p>
 * <p>
 *  A default implementation of this interface ({@link DefaultLoggerProducer} uses JDK logging.
 * </p>
 * 
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DMXLoggerProducer {
    
    /**
     * Creates logger with specified name.
     * 
     * @param name logger name
     * @return  logger
     */
    DMXLogger createLogger(String name);
    
    /**
     * Create logger with name of the specified class.
     * 
     * @param clazz class
     * @return logger
     */
    DMXLogger createLogger(Class clazz);
}
