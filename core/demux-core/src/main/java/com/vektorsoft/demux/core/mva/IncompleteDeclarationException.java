/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

/**
 * <p>
 * This exception is thrown when class declared by annotations as either view or 
 * controller can not be registered due to incomplete information.
 * </p>
 * <p>
 *  For example, class annotated with <code>@DMXControllerDeclaration</code> must contain a 
 * method annotated with <code>@DMXControllerDeclaration.Exec</code>. If it is not present, this
 * exception is thrown.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class IncompleteDeclarationException extends Exception {

    /**
     * Creates new instance of exception with a given message.
     * 
     * @param message exception message
     */
    public IncompleteDeclarationException(String message){
        super(message);
    }
}
