/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.proxy.impl;

import com.vektorsoft.demux.core.proxy.*;
import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  Invocation handler used for forwarding proxied methods for controllers.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class ControllerInvocationHandler implements InvocationHandler {
    
    /**
     * Extension callback used by this class.
     */
    protected DMXExtensionCallback callback;
    
    /** If <code>true</code>, indicates that underlying object implement
     * {@code DMXExtendable}.
     */
    protected boolean extendable = false;
    
    /** Underlying object for this handler. */
    private final Object underlying;
    
    /** Controller mapping. */
    private String mapping;
    
    /** Next view ID for this handler. */
    private final String nextViewId;
    
    
    /** Controller's {@code execute()} method. */
    private final Method execMethod;
    
    /**
     * List of variable IDs for this controller.
     */
    private final List<String> ids;
    
    /**
     * Indicates whether controller is dynamic.
     */
    private final boolean dynamic;
    
    
    /**
     * Creates new instance.
     * 
     * @param object object to register
     * @throws InstantiationException if class can not be instantiated
     * @throws IllegalAccessException if fields cannot be accessed
     * @throws InvocationTargetException if an error occurs during execution
     */
    public ControllerInvocationHandler(Object object) throws InstantiationException, IllegalAccessException, InvocationTargetException{
        underlying = object;
        Class clazz = object.getClass();
        DMXControllerDeclaration annotation = (DMXControllerDeclaration)clazz.getAnnotation(DMXControllerDeclaration.class);
        mapping = annotation.mapping();
        // if no mapping is specified, set object class name as mapping
        if(mapping.isEmpty()){
            mapping = clazz.getName();
        }
        nextViewId = annotation.nextViewId();
        ids = new ArrayList<String>();
        dynamic = annotation.dynamic();
        
       
        execMethod = ProxyUtils.getControllerExecMethod(clazz);
        if(execMethod == null){
            throw new RuntimeException("Annotated controller must contain method with @DMXControllerDeclaration.Exec");
        }
        
        
        for(Method m : clazz.getMethods()){
            if("getExtensionCallback".equals(m.getName())){
                callback = (DMXExtensionCallback)m.invoke(underlying);
                extendable = true;
            } else if(m.isAnnotationPresent(DMXControllerDeclaration.Exec.class)){
                // extract data names from parameters list
                Annotation[][] paramAnnotations = m.getParameterAnnotations();
                for(int i = 0;i < paramAnnotations.length;i++){
                    for(int j = 0;j < paramAnnotations[i].length;j++){
                        if(paramAnnotations[i][j].annotationType().equals(DMXModelData.class)){
                            String id = ((DMXModelData)paramAnnotations[i][j]).id();
                            if(!id.isEmpty()){
                                ids.add(id);
                            } 
                        }
                    }
                }
            }
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if("getControllerId".equals(method.getName())){
            return mapping;
        } else if("isDynamic".equals(method.getName())){
            return dynamic;
        } else if("getNextViewId".equals(method.getName())){
            return nextViewId;
        } else if("getControllerData".equals(method.getName())){
            return ids.toArray(new String[ids.size()]);
        }  else if("execute".equals(method.getName())){
            // set arguments for execution call
            Object[] localArgs = new Object[execMethod.getGenericParameterTypes().length];
            Object[] varArgs = (Object[])args[1];
            DMXLocalModel model = (DMXLocalModel)args[0];
            for(int i = 0;i < localArgs.length;i++){
                if(i < ids.size()){
                    localArgs[i] = model.get(ids.get(i));
                } else {
                    // skip first argument if it is DMXLocalModel
                    localArgs[i] = varArgs[(i > 0) ? i - 1: i];
                }
                
            }
            Object out = execMethod.invoke(underlying, localArgs);
            if(out != null && (out instanceof DMXLocalModel)){
                ((DMXLocalModel)args[0]).set(((DMXLocalModel)out).get());
            }
        }else if(extendable && "setExtensionCallback".equals(method.getName())){
            ((DMXExtendable)underlying).setExtensionCallback((DMXExtensionCallback)args[0]);
            callback = (DMXExtensionCallback)args[0];
        } else if(extendable && "getExtensionCallback".equals(method.getName())){
            return callback;
        }  else {
            return method.invoke(underlying, args);
        }
        return null;
    }
    
    /**
     * Returns class name of the underlying object.
     * 
     * @return class name
     */
    public String getUnderlyingClassName(){
        return underlying.getClass().getName();
    }

}
