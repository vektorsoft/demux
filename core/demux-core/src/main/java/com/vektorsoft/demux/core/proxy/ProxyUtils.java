/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.proxy;

import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Contains utility methods which are used for dealing with dynamic proxies.
 *
 * @author Vladimir Djurovic
 */
public final class ProxyUtils {
    
    /**
     * Private constructor.
     */
    private ProxyUtils(){
        
    }

    /**
     * Returns a list of all interfaces implemented by specified class. List also includes interfaces
     * implemented by super classes.
     * 
     * @param clazz class object
     * @return  list of implemented interfaces
     */
    public static List<Class> getAllImplementedInterfaces(Class clazz){
        List<Class> interfaces = new ArrayList<Class>();
        interfaces.addAll(Arrays.asList(clazz.getInterfaces()));
        
        // do the same for all super classes
        Class scl = clazz.getSuperclass();
        while(scl != null){
            interfaces.addAll(Arrays.asList(scl.getInterfaces()));
            scl = scl.getSuperclass();
        }
        
        return interfaces;
    }
    
    /**
     * Verifies that given {@code Class} object contains annotated {@code execute()} method.
     * 
     * @param clazz class to check
     * @return <code>true</code> if class contains such method, <code>false</code> otherwise
     */
    public static boolean containsExecMethod(Class clazz){
        return getControllerExecMethod(clazz) != null;
    }
    
    /**
     * Returns a {@code execute()} method for a controller represented by specified class. This is a method
     * annotated with {@link DMXControllerDeclaration.Exec} annotation.
     * 
     * @param clazz class to check
     * @return annotated method, or <code>null</code> if it does not exist
     */
    public static Method getControllerExecMethod(Class clazz){
        Method method = null;
        for(Method m : clazz.getDeclaredMethods()){
            if(m.isAnnotationPresent(DMXControllerDeclaration.Exec.class)){
                method = m;
                break;
            }
        }
        return method;
    }
    
    /**
     * Checks if specified class represents a dialog view. More specifically, this method checks if
     * class is annotated with {@link DMXViewDeclaration} and it's {@code isDialog()} value is <code>true</code>
     * 
     * @param clazz class to check
     * @return <code>true</code> if class represents a dialog view, <code>false</code> otherwise
     */
    public static boolean isDialogView(Class clazz){
        boolean result = false;
        DMXViewDeclaration decl = (DMXViewDeclaration) clazz.getAnnotation(DMXViewDeclaration.class);
        if(decl != null && decl.isDialog()){
            result = true;
        }
        return result;
    }
    
    public static boolean isEventHandler(Class clazz){
        boolean result = false;
        Method[] methods = clazz.getDeclaredMethods();
        for(Method m : methods){
            if(m.isAnnotationPresent(EventHandler.class)){
                result = true;
                break;
            }
        }
        return result;
    }
}
