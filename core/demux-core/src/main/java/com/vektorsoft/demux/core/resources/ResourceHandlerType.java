/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.resources;

/** Enumerates available types of resource handlers. There are several predefined
 * types of resource handlers which can be used to load resources, depending on underlying platform.
 *
 * @author Vladimir Djurovic
 */
public enum ResourceHandlerType {

    /** Classic JRE mechanism of storing resources in resource bundles. */
    RESOURCE_BUNDLE,
    /** Android  mechanism of storing resources in <code>res</code> directory. */
    ANDROID,
    /** Custom type of resource handling. */
    CUSTOM;
}
