/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.app;

import com.vektorsoft.demux.core.common.LocaleChangeController;
import com.vektorsoft.demux.core.common.PropertBindingController;
import com.vektorsoft.demux.core.common.ViewActivationController;

/**
 * Contains definitions of constants for Core module, which can be used across 
 * all platforms and modules.
 *
 * @author Vladimir Djurovic
 */
public final class DMXCoreConstants {
    
    /** Name of DEMUX storage directory. This is hidden directory in user home directory. */
    public static final String DEMUX_DIR_NAME = ".demux";
    
    /** Name of directory containing bundle cache. */
    public static final String DEMUX_BUNDLE_CACHE_DIR = "cache";

    /** Controller ID for view activation. */
    public static final String CTRL_VIEW_ACITVATION = ViewActivationController.class.getName();
    
    /** Controller ID for property binding. */
    public static final String CTRL_PROPERTY_BIND = PropertBindingController.class.getName();
    
    /**
     * Controller ID for locale change.
     */
    public static final String CTRL_LOCALE_CHANGE = LocaleChangeController.class.getName();
    
    /**
     * Configuration for controller data.
     */
    public static final String CONFIG_CONTROLLER_DATA_PID = "com.vektorsoft.demux.core.controllerConfigPID";
    
    /**
     * Configuration for view data.
     */
    public static final String CONFIG_VIEW_MANAGER_PID = "com.vektorsoft.demux.core.viewManagerPID";
    
    public static final String CONFIG_APPLICATION_PID = "com.vektorsoft.demux.core.applicationPID";
    
    /**
     * Name of the service registration property containing view or controller data.
     */
    public static final String PROP_CTRL_DATA = "com.vektorsoft.demux.core.ControllerDataProperty";
    
    public static final String PROP_LOCALE_CURRENT = "com.vektorsoft.demux.core.CurrentLocale";
    
    public static final String PROP_LOCALE_ADDITIONAL = "com.vektorsoft.demux.core.AdditionalLocale";
    
    /**
     * Private constructor, to disable instantiation of this class.
     */
    private DMXCoreConstants(){
        
    }
}
