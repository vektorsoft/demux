/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 * <p>
 * Controller represents one piece of application logic that performs some
 * operation. Think of it as <i>Controller</i> part of MVC pattern. 
 * </p>
 * <p>
 * In general, controller is a link between View and Model parts of MVC. In most
 * cases, controller performs one operation, but can contain arbitrary number of
 * operations. The workflow of controller should be the following:
 * </p>
 * <ol>
 * <li>Controller is invoked by some user action</li>
 * <li>Controller performs some operation. In the course of operation,it can
 * possibly modify the state of the model</li>
 * <li>After operation is complete, controller can selects the view that should be
 * displayed next</li>
 * </ol>
 */
public interface DMXController {

    /**
     * Name of the view that should be displayed after this controller
     * terminates it's operation.
     *
     * @return name of the next view to display
     */
    String getNextViewId();

    /**
     * <p>
     * Returns an ID string used to uniquely identify this controller. Each controller must have an ID, so it 
     * can be referenced from other components. This ID must be unique within application.
     * </p>
     *
     * @return a string denoting this controller's ID
     */
    String getControllerId();

    /**
     * Returns IDs of model data on which this controller operates. When controller is executed, it will retrieve 
     * values of this data from model, and use them in it's {@code execute()} method.
     *
     * @return array of data IDs
     */
    String[] getControllerData();

    /**
     * Execute action associated with this controller.
     *
     * @param model model data local to this method call
     * @param params operation parameters
     * @throws ExecutionException if an error occurs during execution
     */
    void execute(DMXLocalModel model, Object... params) throws ExecutionException;
    
    /**
     * <p>
     *  Indicates whether this controller is dynamic. Dynamic controllers can take model data IDs 
     * as arguments for {@link #execute(com.vektorsoft.demux.core.mva.model.DMXLocalModel, java.lang.Object...) } method, 
     * in addition to initialy declared data. Value of this model data will be available in {@code model} argument.
     * </p>
     * <p>
     *  When controller's {@code execute} method is invoked, {@code params} are examined for string type values. If controller 
     * is dynamic, and any of these values match ID of some model data, that data will be stored in {@code model} argument, and 
     * can be accessed from controllers {@code execute} method.
     * </p>
     * 
     * @return <code>true</code> if controller is dynamic, <code>false</code> otherwise
     */
    boolean isDynamic();

}
