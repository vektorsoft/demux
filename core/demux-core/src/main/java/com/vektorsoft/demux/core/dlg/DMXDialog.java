/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.dlg;

/**
 * <p>
 * Represents a popup dialog window that can be displayed on request. Some platforms may not necessarily support 
 * dialogs.
 * </p>
 * <p>
 *  Dialogs can be made modal or non-modal. Modal dialogs will typically block caller thread while dialog is visible, but this 
 * is implementation dependent.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public interface DMXDialog {
    
    /** Value for <code>OK</code> option. */
    int DLG_OPTION_OK = 0x100;
    
    /** Value for <code>Cancel</code> option. */
    int DLG_OPTION_CANCEL = 0x101;
    
    /** Value for <code>Yes</code> option. */
    int DLG_OPTION_YES = 0x102;
    
    /** Value for <code>No</code> option. */
    int DLG_OPTION_NO = 0x103;
    
    /** Indicates that dialog was closed on close button. */
    int DLG_OPTION_CLOSED = 0x104;
    
    /** Dialog will display only <b>OK</b> button. */
    int OPTIONS_OK = 0x200;
    
    /** Dialog will display <b>OK</b> and <b>Cancel</b> buttons. */
    int OPTIONS_OK_CANCEL = 0x201;
    
    /** Dialog will display <b>Yes</b> and <b>No</b> buttons. */
    int OPTIONS_YES_NO = 0x202;
    
    /** Dialog will display <b>Yes</b>, <b>No</b> and <b>Cancel</b> buttons. */
    int OPTIONS_YES_NO_CANCEL = 0x203;
    
    
    /**
     * Defines common message types.
     */
    enum MessageType {
        /** Plain dialog (no icon). */
        MESSAGE_TYPE_PLAIN,
         /** Information dialog (with information icon). */
        MESSAGE_TYPE_INFO,
        /** Warning dialog (with warning icon). */
        MESSAGE_TYPE_WARNING,
        /** Error dialog (with error icon). */
        MESSAGE_TYPE_ERROR,
         /** Question dialog (with question icon). */
        MESSAGE_TYPE_QUESTION;
    }
    

    /**
     * <p>
     * Show this dialog. Calling this method might block calling thread until dialog is 
     * closed. This is implementation dependent.
     * </p>
     * <p>
     * This method can optionally return an integer value 
     * that indicates some result (for example, selected option, input value etc.).
     * </p>
     * 
     * @return integer value representing a result
     */
    int showDialog();
    
    /**
     * Close this dialog. 
     * 
     */
    void closeDialog();
    
    /**
     * Returns instance of {@link DMXDialogView} displayed by this dialog.
     * 
     * @return view displayed in this dialog
     */
    DMXDialogView getDialogView();
}
