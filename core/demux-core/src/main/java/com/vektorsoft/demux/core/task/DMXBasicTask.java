/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.task;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  Basic implementation of {@link DMXTask}. By default, it simply executes given controller in background,
 * without blocking any GUI elements, or displaying progress. These features can be added by clients.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class DMXBasicTask extends Thread implements DMXTask {
    
    /** ID of controller to execute. */
    protected String controllerId;
    
    /** GUI blocking scope. */
    protected GUIBlockingScope blockScope;
    
    /** List of IDs of components that should be blocked during execution. */
    protected List<String> blockIds;
    
    /** Whether task is progress aware or not. */
    protected boolean progressAware;
    
    /** ID of view that displays progress. */
    protected String progressViewId;
    
    /** Controller to execute. */
    private DMXController controller;
    
    /** Arguments for controller execution. */
    private Object[] arguments;
    
    /** Listeners for task status. */
    private List<TaskStatusListener> listeners;
    
    /**
     * Local model for controller execution.
     */
    private DMXLocalModel local;
    
    /**
     * Creates new instance.
     * 
     * @param controllerId controller ID
     */
    public DMXBasicTask(String controllerId){
        this.controllerId = controllerId;
        this.blockScope = GUIBlockingScope.NONE;
        blockIds = new ArrayList<String>();
        progressAware = false;
        listeners = new ArrayList<TaskStatusListener>();
    }

    
    @Override
    public String getControllerId() {
        return controllerId;
    }

    
    @Override
    public GUIBlockingScope getBlockingScope() {
        return blockScope;
    }

    /**
     * Sets blocking scope for this task.
     * 
     * @param blockScope blocking scope
     */
    public void setBlockScope(GUIBlockingScope blockScope) {
        this.blockScope = blockScope;
    }
    
    

    @Override
    public String[] getIdsToBlock() {
        return blockIds.toArray(new String[blockIds.size()]);
    }

    /**
     * Set IDs of GUI components to block.
     * 
     * @param ids component IDs
     */
    public void setIdsToBlock(String... ids){
        for(String id : ids){
            blockIds.add(id);
        }
    }
    
    
    /**
     * Add IDs of GUI components that should be blocked while task is executing.
     * 
     * @param ids component IDs
     */
    public void addIdsToBlock(String... ids){
        for(String id : ids){
            blockIds.add(id);
        }
    }

    @Override
    public boolean isProgressAware() {
        return progressAware;
    }

    /**
     * Set whether this task is progress aware or not. If task can track it's progress, set
     * this to <code>true</code>. If not, or task progress is indeterminate, set to <code>false</code>.
     * 
     * @param progressAware progress awareness
     */
    public void setProgressAware(boolean progressAware) {
        this.progressAware = progressAware;
    }

    @Override
    public String getProgressViewId() {
        return progressViewId;
    }

    /**
     * Set ID of view which will display progress information.
     * 
     * @param progressViewId view ID
     */
    public void setProgressViewId(String progressViewId) {
        this.progressViewId = progressViewId;
    }
    

    @Override
    public final void run() {
        for(TaskStatusListener listener: listeners){
            listener.taskStarted(this);
        }
        try {
            controller.execute(local, arguments);
            for(TaskStatusListener listener: listeners){
            listener.taskCompleted(this, null);
        }
        } catch(ExecutionException ex){
            throw new RuntimeException(ex);
        }
        
    }

    @Override
    public void addTaskStatusListener(TaskStatusListener listener) {
        listeners.add(listener);
    }

    /**
     * Set controller which will be executed as task action.
     * 
     * @param controller controller to set
     */
    @Override
    public void setController(DMXController controller) {
        this.controller = controller;
    }

    /**
     * set arguments for controller execution.
     * 
     * @param args arguments
     */
    @Override
    public void setArguments(Object... args) {
        arguments = new Object[args.length];
        System.arraycopy(args, 0, arguments, 0, args.length);
    }

    @Override
    public void setLocalModel(DMXLocalModel model) {
        this.local = model;
    }

}
