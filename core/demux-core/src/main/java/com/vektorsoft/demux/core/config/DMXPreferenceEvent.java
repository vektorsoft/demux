/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.config;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXPreferenceEvent {

    private String nodePath;
    
    private String key;
    private Object value;
    
    public DMXPreferenceEvent(String path, String key, Object value){
        this.nodePath = path;
        this.key = key;
        this.value = value;
    }

    public String getNodePath() {
        return nodePath;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
    
}
