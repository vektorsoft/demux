/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DMXExtensionType;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import java.util.Map;

/**
 * View manager is responsible for managing all views defines for application.
 * It decides which view is to be displayed and provides all information for
 * selected view in order for it to render itself.
 */
public interface DMXViewManager {
    
    /** Name of the parameter which specifies progress dialog operations. */
    String PARAM_PROGRESS_DIALOG = "com.vektorsoft.demux.core.mva.DMXViewManager.progressDialog";
    
    /** Name of the parameter which specifies optional title for a dialog. */
    String PARAM_DIALOG_TITLE = "com.vektorsoft.demux.core.mva.DMXViewManager.dlgTitle";
    
    /** Name of the parameter which specifies view state (active or inactive). */
    String PARAM_VIEW_STATE = "com.vektorsoft.demux.core.mva.DMXViewManager.viewState";
    
    /** Indicates that progress dialog should be shown. */
    int VAL_DLG_SHOW = 0;
    
    /** Indicates that progress dialog should be closed. */
    int VAL_DLG_CLOSE = 1;

    /**
     * Registers specified view with this
     * <code>ViewManager</code>. Registering the view means that
     * <code>ViewManager</code> is responsible for managing it's state.
     *
     * @param view <code>View</code> to register
     */
      void  registerView(DMXView view);
    
    
    /**
     * Remove view with specified ID.
     * 
     * @param id view ID
     */
    void unregisterView(String id);

    /**
     * <p>
     * Sets specified view as currently active for this
     * <code>ViewManager</code>. Selecting the view will force it to be shown
     * (if not already) and render itself. Actual process of selecting the view is implementation 
     * dependent and is out of the scope of this interface definition.
     * </p>
     * <p>
     *  Parameters {@code params} is a map of optional parameters which can be used by implementations 
     * to determine conditions for selecting this view.
     * </p>
     *
     * @param viewId name of the view to be shown
     * @param params optional parameters for selecting this view
     */
    void selectView(String viewId, Map<String, Object> params);

    /**
     * Returns a view with specified ID.
     *
     * @param id view ID
     * @return view with specified ID
     */
    DMXView getView(String id);
    
    /**
     * <p>
     *  Sets extension callback of  view with specified ID. The call to callback's 
     * {@link DMXExtensionCallback#getExtensionType() } method must return
     * {@link DMXExtensionType#VIEW_EXTENSION}.
     * </p>
     * 
     * @param callback callback to register
     */
    void registerExtensionCallback(DMXExtensionCallback callback);
    
    void removeExtensionCallback(DMXExtensionCallback callback);
    
    
    /**
     * Block or unblock GUI based on specified parameters. Implementation should use underlying
     * platform features to implement this operation, if possible.
     * 
     * @param state whether to block or unblock GUI. If <code>true</code> block, otherwise unblock
     * @param scope blocking scope
     * @param ids  optional IDs of elements to block
     */
    void blockGui(boolean state, GUIBlockingScope scope, String... ids);
    
    /**
     * Retrieves dialog factory for the underlying platform. Factory can be used to create dialogs without 
     * knowing specifics of the underlying platform.
     * 
     * @param <T> type of the implementation factory
     * @return implementation of {@link DialogFactory} for the platform
     */
     <T extends DialogFactory> T getDialogFactory();
     
}
