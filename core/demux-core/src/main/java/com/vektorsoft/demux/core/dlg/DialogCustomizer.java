/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.dlg;

/**
 * <p>
 *  Callback interface for dialog customizer. Dialog customizer is intended to customize appearance of dialog 
 * (mainly system dialog) in generic manner. An example of dialog customizer would be:
 * </p>
 * <pre>
 *  <code>
 *      public class MyDlgCustomizer implements DialogCustomizer &lt;SomeDiloag&gt;{
 * 
 *          public void customize(SomeDialog dlg){
 *              dlg.setTitle("Custom title");
 *              // do other customization
 *          }
 *      }
 *  </code>
 * </pre>
 * 
 * @param <T> dialog type
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DialogCustomizer<T> {
    
    /**
     * Possible file choose types.
     */
    static enum FileChooseType{
        /** Open file dialog. */
        OPEN,
        /** Open file dialog with option to select multiple files. */
        OPEN_MULTIPLE,
        /** Save file dialog. */
        SAVE
    }
    
    /**
     * Perform dialog customization.
     * 
     * @param dialog dialog instance
     */
    void customize(T dialog);
}
