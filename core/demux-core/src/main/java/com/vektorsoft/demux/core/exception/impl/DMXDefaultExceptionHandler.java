/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.exception.impl;

import com.vektorsoft.demux.core.exception.DMXExceptionHandler;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * Default DEMUX Framework exception handler. This handler will log all uncaught exceptions using
 * which ever logging mechanism is currently configured. In addition, it will apply all other registered
 * exception handlers if they are enabled.
 * </p>
 * <p>
 *  Additional handlers can be supplied by calling {@link #addHandler(com.vektorsoft.demux.core.exception.DMXExceptionHandler) } method. Each 
 * handler added like this will have a chance for processing exception.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXDefaultExceptionHandler implements Thread.UncaughtExceptionHandler {
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(DMXDefaultExceptionHandler.class);
    
    /**
     * List of current exception handlers.
     */
    private final List<DMXExceptionHandler> handlers;
    
    /**
     * System default uncaught exception handler.
     */
    private final Thread.UncaughtExceptionHandler systemHandler;
    
    /**
     * Creates new instance. This constructor will save current system exception handler, so it can be used
     * as a fallback.
     * 
     * @param systemHandler current system handler
     */
    public DMXDefaultExceptionHandler(Thread.UncaughtExceptionHandler systemHandler){
        handlers = new ArrayList<DMXExceptionHandler>();
        this.systemHandler = systemHandler;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        LOGGER.error("Uncaught exception in thread " + t.getName(), e);
        for(DMXExceptionHandler handler : handlers){
            if(handler.isEnabled()){
                handler.uncaughtException(t, e);
            }
        }
    }

    /**
     * Add specified exception handler.
     * 
     * @param handler handler to add
     */
    public void addHandler(DMXExceptionHandler handler){
        LOGGER.debug("Adding exception handler " + handler.getHandlerID());
        handler.setOriginalSystemHandler(systemHandler);
        String[] ids = handler.overrideIds();
        // disable all overriden controllers 
        if(ids != null && ids.length > 0){
            List<String> idsList = Arrays.asList(ids);
            for(DMXExceptionHandler h : handlers){
                if(idsList.contains(h.getHandlerID())){
                    h.setEnabled(false);
                }
            }
        }
        handlers.add(handler);
    }
    
    /**
     * Removed specified exception handler.
     * 
     * @param handler handler to remove
     */
    public void removeHandler(DMXExceptionHandler handler){
         String[] ids = handler.overrideIds();
        // enable all overriden controllers 
        if(ids != null && ids.length > 0){
            List<String> idsList = Arrays.asList(ids);
            for(DMXExceptionHandler h : handlers){
                if(idsList.contains(h.getHandlerID())){
                    h.setEnabled(true);
                }
            }
        }
        handlers.remove(handler);
    }
    
}
