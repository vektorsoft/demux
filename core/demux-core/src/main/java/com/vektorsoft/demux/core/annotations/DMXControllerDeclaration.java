/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that annotated type represents a controller in MVC pattern.
 *
 * @author Vladimir Djurovic
 */
@Target (ElementType.TYPE)
@Retention (RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DMXControllerDeclaration {

    /**
     * Mapping for this controller. Default value is empty string.
     * 
     * @return controller mapping
     */
    public String mapping() default "";
    
    /**
     * ID of the view that should be activated when this controller finishes execution.
     * 
     * @return ID of next view
     */
    public String nextViewId() default "";
    
    /**
     * Indicates whether this controller is dynamic.
     * 
     * @return <code>true</code> if controller is dynamic, <code>false</code> otherwise
     */
    public boolean dynamic() default false;
    
    /**
     * Indicates that annotated method is execution method for a controller.
     * 
     */
    @Target (ElementType.METHOD)
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface Exec {
        
    }
    
}
