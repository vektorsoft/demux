/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.model.DMXModel;
import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.task.DMXTask;
import com.vektorsoft.demux.core.task.TaskStatusListener;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.EventBusAware;
import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.osgi.framework.BundleContext;

/**
 * <p> Represents the Adapter in Model-View-Adapter pattern. This class is
 * responsible for communication and synchronization of View and Model. It is
 * also used to register individual controllers and invoke them when
 * appropriate. </p>
 */
public class DMXDefaultAdapter implements DMXAdapter, EventBusAware, DMXEventHandler {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(DMXDefaultAdapter.class);
    
    /**
     * Thread executor for application.
     */
    private static final ThreadPoolExecutor EXECUTOR;
    
    /**
     * Default size of blocking queue.
     */
    private static final int DEFAULT_THREAD_QUEUE_SIZE = 10;

    /**
     * <code>DMXViewManager</code> instance that is used to manage views.
     */
    protected DMXViewManager viewManager;
    /**
     * Map of all registered controllers.
     */
    protected Map<String, DMXController> controllerMap;
    
    protected Map<String, Set<DMXExtensionCallback>> callbackMap;
    
    /** Model to be used with this adapter. */
    protected DMXModel dataModel;
    
    /** Resource manager used in this adapter. */
    protected DMXLocaleManager resourceManager;
    
    
    private BundleContext bundleContext;
    
    private DMXEventBus eventBus;
    
    static {
        int numProcessors = Runtime.getRuntime().availableProcessors();
        EXECUTOR = new ThreadPoolExecutor(numProcessors,  2 * numProcessors, 1, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(DEFAULT_THREAD_QUEUE_SIZE));
    }
    
    /**
     * Create new instance.
     * 
     */
    public DMXDefaultAdapter() {
        controllerMap = new ConcurrentSkipListMap<String, DMXController>();
        callbackMap = new ConcurrentSkipListMap<String, Set<DMXExtensionCallback>>();
        this.dataModel = new DMXModel();
    }
    
    

    /**
     * Sets the current view manager.
     * @param vm <code>DMXViewManager</code> to set
     */
    public void setViewManager(DMXViewManager vm) {
        this.viewManager = vm;

    }

    /**
     * Sets current dataModel.
     * 
     * @param model dataModel to set
     */
    public void setModel(DMXModel model) {
        this.dataModel = model;
        if(eventBus != null){
            dataModel.setEventBus(eventBus);
        }
    } 

    
    @Override
    public void registerController(DMXController controller) {
        //check parameters validity
        if (controller == null) {
            throw new IllegalArgumentException("Controller cannot be null.");
        }
        if (controller.getControllerId() == null || "".equals(controller.getControllerId())) {
            throw new IllegalArgumentException("Controller mapping cannot be null or empty string.");
        }

        if(controller instanceof DMXExtendable && callbackMap.containsKey(controller.getControllerId())){
            Set<DMXExtensionCallback> callbacks = callbackMap.get(controller.getControllerId());
            for(DMXExtensionCallback callback : callbacks){
                LOGGER.debug("Injecting callback for controller " + controller.getControllerId());
                ((DMXExtendable)controller).setExtensionCallback(callback);
            }
        }
        
        controllerMap.put(controller.getControllerId(), controller);

    }
    

    @Override
    public void registerExtensionCallback(DMXExtensionCallback callback) {
        String[] mappings = callback.getMappings();
        switch(callback.getExtensionType()){
            case CONTROLLER_EXTENSION:
                for(String map : mappings){
                    if(callbackMap.containsKey(map)){
                        callbackMap.get(map).add(callback);
                    } else {
                        Set<DMXExtensionCallback> set = new HashSet<DMXExtensionCallback>();
                        set.add(callback);
                        callbackMap.put(map, set);
                    }
                    LOGGER.debug("Registered extension callback for mapping " + map);
                    DMXController ctrl = controllerMap.get(map);
                    if(ctrl != null && (ctrl instanceof DMXExtendable)){
                        ((DMXExtendable)ctrl).setExtensionCallback(callback);
                        LOGGER.debug("Set extension callback for controller " + map);
                    }
                }
                break;
            case VIEW_EXTENSION:
                viewManager.registerExtensionCallback(callback);
                break;
        }
        
    }

    @Override
    public void removeExtensionCallback(DMXExtensionCallback callback) {
        switch(callback.getExtensionType()){
            case CONTROLLER_EXTENSION:
                for(String mapping : callback.getMappings()){
                    DMXController ctrl = controllerMap.get(mapping);
                    if(ctrl != null && ctrl instanceof DMXExtendable){
                        ((DMXExtendable)ctrl).setExtensionCallback(null);
                        LOGGER.debug("Reset extension callback for controller " + mapping);
                    }
                    callbackMap.get(mapping).remove(callback);
                    if(callbackMap.get(mapping).isEmpty()){
                        callbackMap.remove(mapping);
                    }
                }
                break;
            case VIEW_EXTENSION:
                viewManager.registerExtensionCallback(callback);
        }
        
    }
    
    
    @Override
    public <T extends DialogFactory> T getDialogFactory(){
        return viewManager.getDialogFactory();
    }
    
    /**
     * Prepares controller for execution. This method will find controller by it's ID and inject any 
     * model data required.
     * 
     * @param controller controller to invoke
     * @return controller instance
     * @throws NoSuchControllerException if no controller with specified ID is found
     */
    private DMXLocalModel prepareController(DMXController controller) throws NoSuchControllerException {
        
        String[] ids = controller.getControllerData();
        Map<String, Object> dataMap = dataModel.getDataValues(ids);
        
        // get callback data, if any
        if(controller instanceof DMXExtendable){
            DMXExtensionCallback callback = ((DMXExtendable)controller).getExtensionCallback();
            if(callback != null && callback.getCallbackData() != null){
                dataMap.putAll(dataModel.getDataValues(callback.getCallbackData()));
            }
        }
        
        return DMXLocalModel.instance().set(dataMap);
    }
    
     
    @Override
    public void invokeController(String controllerId, Object... args) {
        try {
            DMXController controller = controllerMap.get(controllerId);
            if (controller == null) {
                throw new NoSuchControllerException("Controller not found: " + controllerId);
            }
            DMXLocalModel local = prepareController(controller);
            if(controller.isDynamic()){
                for(Object argument : args){
                    if(argument instanceof String && dataModel.isDataRegistered(argument.toString())){
                        local.set(argument.toString(), dataModel.getDataValue(argument.toString()));
                    }
                }
            }

            controller.execute(local, args);
            dataModel.setDataValues(local.get());

            // set active view
            if (args.length > 1 && args[1] instanceof Map) {
                viewManager.selectView(controller.getNextViewId(), (Map<String, Object>) args[1]);
            } else {
                viewManager.selectView(controller.getNextViewId(), null);
            }

        } catch (ExecutionException ex) {
            throw new RuntimeException(ex);
        } catch (NoSuchControllerException nex) {
            throw new RuntimeException(nex);
        }
    }
    
    
    @Override
    public void executeTask(DMXTask task, Object... args) {
        try {
            DMXController controller = controllerMap.get(task.getControllerId());
            if (controller == null) {
                throw new NoSuchControllerException("Controller not found: " + task.getControllerId());
            }
            DMXLocalModel local = prepareController(controller);
           
            TaskListener taskListener = new TaskListener();
            // find progress view, if any
            String viewId = task.getProgressViewId();
            taskListener.setViewId(viewId);
            if(task.isProgressAware()){
                local.setEventBus(eventBus);
            }
            task.addTaskStatusListener(taskListener);
            task.setController(controller);
            task.setArguments(args);
            task.setLocalModel(local);
            
            EXECUTOR.execute(task);
            
        } catch (NoSuchControllerException ex) {
            throw new RuntimeException(ex);
        } 
    }


    @Override
    public void registerData(String id, Object value) {
        dataModel.registerData(id, value);
    }

    @Override
    public void unregisterData(String dataId) {
        dataModel.unregisterData(dataId);
    }


    @Override
    public void removeController(String id) {
        controllerMap.remove(id);
    }

    @Override
    public void removeView(String id) {
        viewManager.unregisterView(id);
    }


    @Override
    public void setEventBus(DMXEventBus bus) {
        this.eventBus = bus;
        eventBus.subscribe(this);
        if(dataModel != null){
            dataModel.setEventBus(eventBus);
        }
    }

    @Override
    public String[] getTopics() {
        return new String[]{DMXEventConstants.TOPIC_VIEW_REGISTERED};
    }

    @Override
    public void handle(DMXEvent event) {
        if(DMXEventConstants.TOPIC_VIEW_REGISTERED.equals(event.getTopic())){
            LOGGER.debug("Received view registration event");
            String[] ids = (String[])event.getData();
            if(ids != null && ids.length > 0){
                eventBus.publish(new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, null, dataModel.getDataValues(ids)));
            }
        }
    }

    @Override
    public DMXEventFilter getFilter(String topicName) {
        return null;
    }
    
    /**
     * Implementation of {@code TaskStatusListener}. This implementation will track changes to task
     * status and handle them.
     */
    private class TaskListener implements  TaskStatusListener {
        
        /** ID of the view used to display progress. */
        private String viewId;
        
        /** Parameters map. */
        private Map<String, Object> params = new HashMap<String, Object>();

        /**
         * Set view ID.
         * 
         * @param viewId  view ID
         */
        public void setViewId(String viewId) {
            this.viewId = viewId;
        }

        @Override
        public void taskStarted(DMXTask task) {
            viewManager.blockGui(true, task.getBlockingScope(), task.getIdsToBlock());
            if(viewId != null){
                params.put(DMXViewManager.PARAM_PROGRESS_DIALOG, DMXViewManager.VAL_DLG_SHOW);
                viewManager.selectView(viewId, params);
            }
            
        }

        @Override
        public void taskCompleted(DMXTask task, Object result) {
            viewManager.blockGui(false, task.getBlockingScope(), task.getIdsToBlock());
             if(viewId != null){
                params.put(DMXViewManager.PARAM_PROGRESS_DIALOG, DMXViewManager.VAL_DLG_CLOSE);
                viewManager.selectView(viewId, params);
            }
        }
        
    }
}
