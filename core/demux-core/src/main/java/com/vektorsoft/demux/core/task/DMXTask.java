/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.task;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 * <p>
 *  Represents a long running task that should be run in background. This interface provides method declarations
 * needed to provide common functionality for background tasks across multiple platforms. Main properties of this
 * kind of tasks should be:
 * </p>
 * <ul>
 *  <li>run long-running controller execution in separate thread</li>
 *  <li>block parts of GUI if needed during task execution</li>
 *  <li>provide visual feedback for task progress, if needed</li>
 * </ul>
 *
 * @author Vladimir Djurovic
 */
public interface DMXTask extends Runnable {

    /**
     * Returns ID of controller that should be executed with this task.
     * 
     * @return controller ID
     */
    String getControllerId();
    
    /**
     * Returns GUI block scope for this task.
     * 
     * @return GUI block scope
     */
    GUIBlockingScope getBlockingScope();
    
    /**
     * Returns an array of IDs of GUI components that should be blocked while this task is executing. These
     * can either be IDs of {@code DMXView} instances or individual GUI components (eg. buttons, labels, etc).
     * 
     * @return array of component IDs
     */
    String[] getIdsToBlock();
    
    /**
     * Indicates whether this task should track it's progress. Task which is progress aware supports 
     * publishing it's progress information, so it can be displayed by the GUI. If task does not care about
     * progress, or progress is indeterminate, returns <code>false</code>
     * 
     * @return <code>true</code> if task supports publishing progress info, <code>false</code> otherwise
     */
    boolean isProgressAware();
    
    /**
     * Returns ID of view that should display progress for this task.
     * 
     * @return view ID
     */
    String getProgressViewId();
    
    /**
     * Set controller which will perform actual task.
     * 
     * @param controller controller
     */
    void setController(DMXController controller);
    
    /**
     * Set arguments for execution.
     * 
     * @param args arguments
     */
    void setArguments(Object... args);
    
     /**
     * Add listener to track changes to task status.
     * 
     * @param listener task listener
     */
    void addTaskStatusListener(TaskStatusListener listener);
    
    /**
     * Sets local model data for controller execution.
     * 
     * @param model model data to set
     */
    void setLocalModel(DMXLocalModel model);
    
}
