/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.app;

import java.util.Map;

/**
 * <p>
 *  This interface defines common features that are defined for applications based on 
 * DEMUX Framework.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public interface DMXApplication {

    /**
     * Returns application name.
     * 
     * @return application name
     */
    String getName();
    
    /**
     * Returns application version as string. Version format can be arbitrary.
     * 
     * @return version string
     */
    String getVersion();
    
    /**
     * Returns application build number as string. Build number format is not defined.
     * 
     * @return build number as string
     */
    String getBuildNumber();
    
    /**
     * Indicates whether application has graphical user interface or not. If <code>true</code>, 
     * the application is GUI application, otherwise not.
     * 
     * @return <code>true</code> if application has GUI
     */
    boolean hasGUI();
    
    /**
     * Returns arguments list that was used to launch application.
     * 
     * @return array of application arguments
     */
    String[] getArguments();
    
    /**
     * Returns a set of configuration parameters for application. Parameters are 
     * specified as key-values pairs of strings.
     * 
     * @return map of configuration parameters
     */
    Map<String, String> getConfigurationParameters();
}
