/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.bundle;

import com.vektorsoft.demux.core.common.LocaleChangeController;
import com.vektorsoft.demux.core.common.PropertBindingController;
import com.vektorsoft.demux.core.common.ViewActivationController;
import com.vektorsoft.demux.core.exception.impl.DMXDefaultExceptionHandler;
import com.vektorsoft.demux.core.exception.DMXExceptionHandlerService;
import com.vektorsoft.demux.core.exception.impl.ExceptionServiceCustomizer;
import com.vektorsoft.demux.core.mva.DMXController;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 * ACtivator for <code>demux-core</code> bundle.
 * 
 * @author Vladimir Djurovic
 */
public class DemuxActivator implements BundleActivator {
    
    
    /** Tracker for {@code DMXAdapter} service. */
//    private ServiceTracker tracker;
    
    
    /**
     * Exception service tracker.
     */
    private ServiceTracker<DMXExceptionHandlerService, DMXExceptionHandlerService> exceptionTracker;

    /**
     * @see
     * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
     * 
     * @param context bundle context
     */
    @Override
    public void start(BundleContext context) {
        
        DMXDefaultExceptionHandler exceptionHandler = new DMXDefaultExceptionHandler(Thread.getDefaultUncaughtExceptionHandler());
        Thread.setDefaultUncaughtExceptionHandler(exceptionHandler);
        
        
        // start tracker for exception handlers
        exceptionTracker = new ServiceTracker<DMXExceptionHandlerService, DMXExceptionHandlerService>(context, 
                DMXExceptionHandlerService.class, new ExceptionServiceCustomizer(exceptionHandler, context));
        exceptionTracker.open();
        
        // register common controllers
        context.registerService(DMXController.class, new ViewActivationController(), null);
        context.registerService(DMXController.class, new PropertBindingController(), null);
        context.registerService(DMXController.class, new LocaleChangeController(), null);
    }

    /**
     * @see
     * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     * 
     * @param context bundle context
     */
    public void stop(BundleContext context) {
        exceptionTracker.close();
        
    }

}
