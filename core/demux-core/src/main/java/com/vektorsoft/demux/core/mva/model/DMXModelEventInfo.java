/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Represents a change in application data model. This can be related to model data being registered 
 * or removed, or data being updates.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXModelEventInfo {

    /**
     * Possible event types enumeration.
     */
    public static enum ModelEventType{
        /** New data is registered with mode. */
        DATA_REGISTERED,
        /** Data is removed from model. */
        DATA_REMOVED,
        /** Model data is updated. */
        DATA_UPDATED
    }
    
    /**
     * EVent type.
     */
    private final ModelEventType type;
    
    /**
     * Previous values of changed data.
     */
    private final Map<String, Object> oldValues;
    
    /**
     * New values of changed data.
     */
    private final DMXLocalModel newValues;
    
    /**
     * Creates new event of specified type, with previous and new data values.
     * 
     * @param type event type
     * @param oldValues old data values
     * @param newValues new data values
     */
    public DMXModelEventInfo(ModelEventType type, Map<String, Object> oldValues, Map<String, Object> newValues){
        this.type = type;
        this.oldValues = oldValues;
        this.newValues = DMXLocalModel.instance().set(newValues);
    }
    
    /**
     * Creates event with type {@code DATA_REMOVED}, with ID which specifies removed data.
     * 
     * @param id ID of removed data
     */
    public DMXModelEventInfo(String id){
        this.type = ModelEventType.DATA_REMOVED;
        this.oldValues = null;
        this.newValues = DMXLocalModel.instance().set(id, 1);
    }

    /**
     * Returns event type.
     * 
     * @return event type
     */
    public ModelEventType getType() {
        return type;
    }

    /**
     * Returns new data values/
     * 
     * @return map of new data values
     */
    public DMXLocalModel getNewValues() {
        return newValues;
    }

    /**
     * Returns old data values.
     * 
     * @return map of old data values
     */
    public Map<String, Object> getOldValues() {
        return oldValues;
    }
    
    /**
     * Returns IDs of changed data.
     * 
     * @return IDs of new changed data
     */
    public Set<String> getChangedIds(){
        return newValues.get().keySet();
    }
    
}
