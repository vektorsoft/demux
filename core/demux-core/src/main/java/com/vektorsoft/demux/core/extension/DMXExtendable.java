/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.extension;

/**
 * <p>
 * This interface should be implemented by views and controllers that can be extended by callbacks. For 
 * example, this might be the case for classes which are intended to be used by third parties, so they can
 * add custom functionality to extend them. 
 * </p>
 * <p>
 *  Classes which implement this interface provide a callback interface of type {@link DMXExtensionCallback}, whose
 * methods are called when custom functionality needs to be invoked.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public interface DMXExtendable {

    /**
     * Set extension callback for this interface. Implementation should provide a custom field 
     * to hold interface instance for reference.
     * 
     * @param callback callback interface
     */
    void setExtensionCallback(DMXExtensionCallback callback);
    
    /**
     * Returns the callback object set for this extendable.
     * 
     * @return callback object
     */
    DMXExtensionCallback getExtensionCallback();
      
}
