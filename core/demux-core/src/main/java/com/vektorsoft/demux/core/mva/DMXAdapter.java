/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.task.DMXTask;

/**
 * <p>
 *  Defines methods required by Adapter part of the MVA (Model-View-Adapter) pattern. Classes implementing
 * this interface should take into account specific features and requirements of the underlying platform for
 * correct implementation.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public interface DMXAdapter {

    
    /**
     * <p>
     * Registers specified controller to be used by the Adapter. Each controller
     * needs to be registered in order to be used properly. This method will do the following:
     * </p>
     * <ol>
     *  <li>Add controller to a list of registered controllers</li>
     *  <li>Register all controller data with the dataModel</li>
     * </ol>
     * <p>
     *  If specified controller also implements {@link DMXExtendable}, system will look for suitable
     * {@link DMXExtensionCallback} for it. If found, controller will be registered. Otherwise, it will
     * be put on hold until suitable extension callback is registered, or system is started up completely.
     * </p>
     * 
     * @param controller controller to register
     */
    void registerController(DMXController controller);
    
    
    /**
     * Remove controller with specified ID.
     * 
     * @param id controller ID
     */
    void removeController(String id);
    
    
    /**
     * Remove view with specified ID.
     * 
     * @param id view ID
     */
    void removeView(String id);
    
    /**
     * Registers extension callback for controller or view. This callback will be used for all
     * instances of {@link DMXExtendable} which match it.
     * 
     * @param callback extension callback
     */
    void registerExtensionCallback(DMXExtensionCallback callback);
    
    void removeExtensionCallback(DMXExtensionCallback callback);
    
    /**
     * Returns dialog factory for underlying platform.
     * 
     * @param <T> type parameter
     * @return instance of {@code DialogFactory}
     */
    <T extends DialogFactory> T getDialogFactory();
    
    
    /**
     * Invokes controller with specified ID. This method is called whenever controller needs to be executed. It will
     * call controllers <code>execute()</code> method with arguments provided.
     * 
     * @param controllerId controller ID
     * @param args execution arguments
     */
    void invokeController(String controllerId, Object... args);
    
    
    /**
     * Execute specified task in background. If task supports progress tracking, it will be displayed by the view.
     * 
     * @param task task to execute
     * @param args execution arguments
     */
    void executeTask(DMXTask task, Object... args);
    
    
    /**
     * Register data with the model. This method associates given ID with value.
     * 
     * @param id data ID
     * @param value  data value
     */
    void registerData(String id, Object value);
    
    /**
     * Remove data with specified ID from model.
     * 
     * @param dataId ID of data to remove
     */
    void unregisterData(String dataId);
    
}
