/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.exception.impl;

import com.vektorsoft.demux.core.exception.DMXExceptionHandlerService;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * Service tracker customizer for {@link DMXExceptionHandlerService}. The tracker will register exception handler for each service 
 * of this type when it is registered. When service is unregistered, exception handler will be removed.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ExceptionServiceCustomizer implements  ServiceTrackerCustomizer<DMXExceptionHandlerService, DMXExceptionHandlerService> {
    
    /** Logger for this class. */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(ExceptionServiceCustomizer.class);

    /**
     * Default platform exception handler.
     */
    private final DMXDefaultExceptionHandler handler;
    
    /**
     * Bundle context.
     */
    private final BundleContext context;
    
    /**
     * Creates new instance.
     * 
     * @param handler exception handler
     * @param ctx bundle context
     */
    public ExceptionServiceCustomizer(DMXDefaultExceptionHandler handler, BundleContext ctx){
        this.handler = handler;
        this.context = ctx;
    }
    
    @Override
    public DMXExceptionHandlerService addingService(ServiceReference<DMXExceptionHandlerService> sr) {
        DMXExceptionHandlerService h = context.getService(sr);
        LOGGER.debug("Adding service class " + h.getClass().getName());
        handler.addHandler(h.getExceptionHandler());
        return h;
    }

    @Override
    public void modifiedService(ServiceReference<DMXExceptionHandlerService> sr, DMXExceptionHandlerService t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removedService(ServiceReference<DMXExceptionHandlerService> sr, DMXExceptionHandlerService t) {
        handler.removeHandler(context.getService(sr).getExceptionHandler());
    }
    
}
