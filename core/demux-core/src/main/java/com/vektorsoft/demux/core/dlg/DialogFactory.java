/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.dlg;

import java.io.File;

/**
 * This is tagging interface which marks implementing class as being a factory 
 * for dialogs. Each supported platform will have different implementation of dialogs, 
 * but common functionality should be the same.
 *
 * @author Vladimir Djurovic
 */
public interface DialogFactory {

    /**
     * <p>
     * Creates dialog based on provided parameters. This
     * method will show view with specified ID inside a dialog window. Dialog
     * parameters (title, modality) are drawn from view definitions. Note that
     * this view should be an instance on {@link DMXDialogView}.
     * </p>
     * <p>
     * Clients can specify alternate dialog title by parameter {@code title},
     * which can be different from title defined within the view. If this
     * parameter is {@code null}, original title is used.
     * </p>
     *
     * @param viewId ID of view to display in dialog
     * @param title alternate title (different from one defined
     * @return dialog instance, or {@code null} if view with specified ID does
     * not exist
     */
    DMXDialog createDialog(String viewId, String title);
    
    /**
     * <p>
     * Creates a dialog which contains embedded view with ID {@code viewId} and options buttons (OK, Cancel, Yes, No). Buttons
     * displayed are controlled by the values:
     * </p>
     * <ul>
     *  <li>{@link DMXDialog#OPTIONS_OK} - displayed only OK button </li>
     *  <li>{@link DMXDialog#OPTIONS_YES_NO} - display Yes/No buttons<li>
     *  <li>{@link DMXDialog#OPTIONS_YES_NO_CANCEL} - display Yes/No/Cancel buttons<li>
     * </ul>
     * <p>
     *  View denoted with {@code viewId} must be an instance of {@link DMXDialogView}. If {@code title} is specified, it overrides the title 
     * defined in a view.
     * </p>
     * 
     * @param viewId ID of view to show in dialog
     * @param title dialog title (overrides one defined in view)
     * @param options option buttons
     * @return dialog window
     */
    DMXDialog createOptionsDialog(String viewId, String title, int options);
    
    /**
     * <p>
     * Creates options dialog with custom buttons. Buttons text is specified as array in {@code options}
     * parameter. Buttons are laid out in order specified, from left to right. The return value is index of 
     * selected button in {@code options} array. For example, in the following code:
     * 
     * <pre>
     *  <code>
     *      String[] options = {"Option 1", "Option 2", "Option 3"};
     *  </code>
     * </pre>
     * 
     *  If user selected {@code Option 2}, return value is 1 (option index in array).
     * </p>
     * <p>
     *  Parameter {@code viewId} is ID of the view which should be displayed inside dialog. If this parameter is null,
     * then parameter {@code content} is used as view content.
     * </p>
     * 
     * @param viewId ID of view to show in dialog
     * @param title dialog title (overrides one defined in view)
     * @param options option buttons
     * @return dialog window
     */
    DMXDialog createOptionsDialog(String viewId, String title, String[] options);
    
    /**
     * Creates simple message box to show to the user. User can select one of supplied {@code options} 
     * as response to the message. 
     * 
     * @param title dialog title
     * @param message message to show
     * @param options option buttons
     * @return dialog
     */
    DMXDialog createMessageDialog(String title, String message, int options);
    
    /**
     * Same as {@link #createMessageDialog(java.lang.String, java.lang.String, int) }, but with added support for icon 
     * to show for dialog. Icon is one of the predefined icons associated with {@code DMXDialog.MessageType}.
     * 
     * @param title dialog title
     * @param message message to display
     * @param options option buttons
     * @param type message type (determines icon to show)
     * @return dialog
     */
    DMXDialog createMessageDialog(String title, String message, int options, DMXDialog.MessageType type);
    
     /**
     * <p>
     *  Creates dialog that will be used to display task progress. Progress display is handled by a view with 
     * specified ID (parameter {@code viewId}).
     * </p>
     * <p>
     *  This kind of dialog is shown automatically on task start, and removed when task is ended.
     * </p>
     * 
     * @param viewId view ID
     * @return progress dialog
     */
    DMXDialog createProgressDialog(String viewId);
    
}
