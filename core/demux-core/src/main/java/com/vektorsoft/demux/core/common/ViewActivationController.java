/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.common;

import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;

/**
 *  Controller for selecting view to be activated or deactivated by a view manager. This controller 
 * can be used when only a view change is required, without additional logic.
 *
 * @author Vladimir Djurovic
 */
public final class ViewActivationController extends DMXAbstractController{
    
    /** Thread local storage for view to be displayed. */
    private final ThreadLocal<String> threadStorage;
    
    /**
     * Creates new instance.
     */
    public ViewActivationController(){
        threadStorage = new ThreadLocal<String>();
    }

    /**
     * Returns ID of the view that should be activated/shown. This ID is local
     * to the thread executing {@code execute()} method.
     * 
     * @return ID of the view to be activated
     */
    @Override
    public String getNextViewId() {
        String id = threadStorage.get();
        threadStorage.remove();
        return id;
    }
    

    /**
     * Sets the ID of the view to be activated. View ID is passed as first 
     * method parameter.
     * 
     * @param model local model for this method call
     * @param params view ID must be the first parameter
     * @throws ExecutionException  if an error occurs
     */
    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        if(params.length > 0){
            if(params[0] instanceof String){
                threadStorage.set(params[0].toString());
            } else {
                throw new ExecutionException("First parameter must valid view ID string");
            }
        }
        
        
    }

}
