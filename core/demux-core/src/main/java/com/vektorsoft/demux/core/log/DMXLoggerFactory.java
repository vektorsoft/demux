/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.log;

import com.vektorsoft.demux.core.log.impl.DefaultLoggerProducer;

/**
 * <p>
 * Factory for producing {@link DMXLogger} implementations at runtime. This class can be used on all platforms to 
 * produce specific implementation for the platform. By default, this factory will produce loggers based on JDK logging,
 * if no other options are specified.
 * </p>
 * <p>
 *  Particular logging implementation can be specified by invoking {@link #setLogPorducer(com.vektorsoft.demux.core.log.DMXLoggerProducer) } method
 * with appropriate {@link DMXLoggerProducer} implementation. For example, on Android, this factory will produce loggers based on Android {@code Log}
 * class.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public abstract class DMXLoggerFactory {
    
    /**
     * Producer for logs.
     */
    private static DMXLoggerProducer producer;
    
    static {
        producer = new DefaultLoggerProducer();
    }
    
    /**
     * Creates new logger with a given name.
     * 
     * @param name logger name
     * @return  logger implementation
     */
    public static DMXLogger getLogger(String name){
        return producer.createLogger(name);
    }
    
    /**
     * Creates new logger for specified class.
     * 
     * @param clazz class to create logger from
     * @return logger implementation
     */
    public static DMXLogger getLogger(Class clazz){
        return producer.createLogger(clazz);
    }
    
    /**
     * Sets an implementatio of {@link DMXLoggerProducer} to use.
     * 
     * @param prod  producer
     */
    public static void setLogPorducer(DMXLoggerProducer prod){
        producer = prod;
    }
}
