/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.task;

/**
 * <p>
 * This enumeration provides type of GUI blocking while background task is executed. Blocking 
 * GUI prevents unwanted user input during task execution. The following types of GUI blocking are supported:
 *</p>
 * <ul>
 *  <li><code>NONE</code> - do not block GUI</li>
 *  <li><code>TOP_LEVEL</code> - block entire GUI (top level window)</li>
 *  <li><code>VIEW</code> - block GUI part which is defined by a {@code DMXView}</li>
 *  <li><code>COMPONENTS</code> - block set of specified GUI components</li>
 * </ul>
 * @author Vladimir Djurovic
 */
public enum GUIBlockingScope {

    /** Do not block anything. */
    NONE,
    /** Block entire GUI (top-level window). */
    TOP_LEVEL,
    /** Block GUI of specified view. */
    VIEW,
    /** Block a set of speciieid GUI components. */
    COMPONENTS;
}
