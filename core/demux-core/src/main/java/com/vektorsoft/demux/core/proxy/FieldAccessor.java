/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.proxy;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <p>
 *  This class allows read/write access to class fields based on their access level 
 * and any access methods defined. It uses basic JavaBeans naming convention to find
 * appropriate get/set methods for fields with restricted access.
 * </p>
 * <p>
 *  This class is used with reflection mechanism to get/set field values for an object.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public class FieldAccessor {

    /**
     * Field to be accessed.
     */
    private Field field;
    
    /**
     * Method for getting filed value.
     */
    private Method accessor;
    
    /**
     * Method for setting field value.
     */
    private Method mutator;
    
    /**
     * Object containing the field.
     */
    private Object target;
    
    /**
     * Indicates whether field is accessible or not.
     */
    private boolean accessible;
    
    
    /**
     * Creates new instance of accessor for a given field inside a given object.
     * 
     * @param field field to access
     * @param target target object
     */
    public FieldAccessor(Field field, Object target){
        this.field = field;
        this.target = target;
        
        accessible = field.isAccessible();
        initializeMethods();
    }
    
    /**
     * Initializes access methods.
     */
    private void initializeMethods(){
        if(!accessible){
            
            // find getter
            String fieldName = field.getName();
            StringBuilder sb = new StringBuilder("get");
            sb.append(fieldName.substring(0, 1).toUpperCase());
            sb.append(fieldName.substring(1));
            try {
                accessor = target.getClass().getMethod(sb.toString());
            } catch(NoSuchMethodException ex){
                System.err.println("No getter method: " + sb.toString() + " for class " + target.getClass().getName());
            }
            
            // find setter
            sb.setCharAt(0, 's');
            
            try{
                mutator = target.getClass().getMethod(sb.toString(), field.getType());
            } catch(NoSuchMethodException ex){
                System.err.println("No setter method: " + sb.toString() + " for class " + target.getClass().getName());
            }
            
        }
    }
    
    /**
     * Returns current field value.
     * 
     * @return field value
     * @throws IllegalAccessException if field cannot be accessed
     * @throws InvocationTargetException if an error occurs during method invocation
     */
    public Object getFieldValue() throws IllegalAccessException, InvocationTargetException{
        if(accessible){
            return field.get(target);
        } else {
            if(accessor != null){
                return accessor.invoke(target);
            } else {
                throw new IllegalStateException("No accessor method for field " + field.getName());
            }
            
        }
        
    }
    
    /**
     * Set field value to specified value.
     * 
     * @param value value to set
     * @throws IllegalAccessException if field cannot be accessed
     * @throws InvocationTargetException if an error occurs during method execution
     */
    public void setFieldValue(Object value) throws IllegalAccessException, InvocationTargetException{
        if(accessible){
            field.set(target, value);
        } else {
            if(mutator != null){
                mutator.invoke(target, value);
            } else {
                throw new IllegalStateException("No setetr method for field " + field.getName());
            }
            
        }
        
    }
}
