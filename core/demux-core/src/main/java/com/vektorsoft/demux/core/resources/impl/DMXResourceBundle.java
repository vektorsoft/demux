/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.resources.impl;

import java.util.Collections;
import java.util.Enumeration;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Custom resource bundle implementation which pulls resources from underlying {@link DMXResourceManager}. It
 * provides consistent access to resources within the framework, while still being transparent to classes that 
 * do not rely on DEMUX Framework.
 *
 * @author Vladimir Djurovic
 */
public class DMXResourceBundle extends ResourceBundle{
    
    /**
     * Resource manager for the framework.
     */
    private final DefaultResourceHandler resourceHandler;
    
    /**
     * Creates new instance.
     * 
     * @param resourceHandler underlying resource manager
     */
    public DMXResourceBundle(DefaultResourceHandler resourceHandler){
        this.resourceHandler = resourceHandler;
    }

    @Override
    protected Object handleGetObject(String key) {
        return  resourceHandler.getResource(key);
    }

    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(resourceHandler.getResourceKeys());
    }

    @Override
    public Set<String> keySet() {
        return resourceHandler.getResourceKeys();
    }

    
}
