/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that annotated type represents view in MVC pattern.
 *
 * @author Vladimir Djurovic
 */
@Target (ElementType.TYPE)
@Retention (RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DMXViewDeclaration {

    /**
     * ID of this view.
     * 
     */
    public String viewID();
    
    /**
     * ID of the parent of this view.
     * 
     */
    public String parentViewId();
    
    /**
     * If {@code true}, indicates that this view represents dialog view. Default
     * value is {@code false}.
     * 
     */
    public boolean isDialog() default false;
    
    /**
     * If {@code isDialog()} is <code>true</code>, indicates whether dialog is modal
     * or not.
     * 
     */
    public boolean modal() default true;
    
    /**
     * If {@code isDialig()} is set to <code>true</code>, indicates dialog title. Default value is empty
     * string.
     * 
     * @return dialog title
     */
    public String title() default "";
    
    /**
     * If present, represents array of model data IDs used by this view.
     * @return 
     */
    public String[] dataIds() default "";
    
    /**
     * Indicates that annotated element represents placement constraint for adding this
     * view to it's parent view.
     */
    @Target ({ElementType.METHOD, ElementType.FIELD})
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface PlacementConstraint {
        
    }
    
    /**
     * Indicates that annotated method is used to add child view to this view. Methods annotated
     * with this annotation must take one parameter of type {@link DMXView}, which is a view 
     * to be added as child.
     */
    @Target (ElementType.METHOD)
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface AddChildView{
        
    }
    
    /**
     * Indicates that annotated method is used to add child view to this view. Methods annotated
     * with this annotation must take one parameter of type {@code String}, which is ID of view to 
     * be removed.
     */
    @Target (ElementType.METHOD)
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface RemoveChildView{
        
    }
    
    /**
     * Indicates that annotated field or method represent UI object for this view.
     */
    @Target ({ElementType.METHOD, ElementType.FIELD})
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface ViewUI {
        
    }
    
    /**
     * <p>
     * Indicates that annotated method is invoked to construct UI. All views must have 
     * method annotated with this annotation in order to construct UI. It will be invoked
     * on UI thread in order to maintain compatibility with underlying UI system.
     * </p>
     * <p>
     *  Method signature of the annotated method may differ based on underlying UI system (JavaFX,
     * Android, web etc.). Each system will define  specific method signature.
     * </p>
     */
    @Target ({ElementType.METHOD})
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface ConstructUI {
        
    }
    
    
    /**
     * <p>
     *  Marks annotated method as being responsible for activating or deactivating 
     * child views. This method is optional for the view. Annotated method must have 
     * the following parameters:
     * </p>
     * <ul>
     *  <li> String parameter which represents ID of the view to activate or deactivate</li>
     *  <li>boolean representing desired state of the view(<code>true</code>-  activate, 
     *      <code>false</code> - deactivate</li>
     * </ul>
     * <p>
     *   Annotated method performs the same task as {@link DMXView#setViewActive(java.lang.String, boolean) }.
     * </p>
     * 
     */
    @Target ({ElementType.METHOD})
    @Retention (RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface Activation {
        
    }
}
