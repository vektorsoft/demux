/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Field-level annotation which indicates that annotated field is to be registered with the model as 
 * model variable. This annotation should be used to annotate fields in views and controllers as being 
 * part of the model.
 *
 * @author Vladimir Djurovic
 */
@Target ({ElementType.PARAMETER, ElementType.FIELD})
@Retention (RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface DMXModelData {

    /**
     * Represents model data ID. Default value is empty string.
     * 
     */
    public String id();
}
