/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

/**
 * <p>
 * Contains classes and interface related to extensions management. Extension system is vital part of the 
 * framework and allows code reuse and customization of the existing components. System is centered around callback
 * methods which add additional functionality to classes intended for extension. These classes must implement {@link DMXExtendable}
 * interface, which callback classes must implement {@link DMXExtensionCallback} interface.
 * </p>
 * <p>
 *  Extension callbacks are third-party code which is packaged into separate OSGI bundles. These  bundles must be fragment bundles, and their
 * host bundle must be the bundle meant for extension. Fragment bundles contain extension callbacks and any additional resources.
 * </p>
 */
package com.vektorsoft.demux.core.extension;
