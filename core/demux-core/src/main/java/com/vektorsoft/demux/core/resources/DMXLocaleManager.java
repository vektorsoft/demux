/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.resources;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import java.util.Locale;

/**
 * Provides generic methods for supporting locale handling in applications.
 *
 * @author Vladimir Djurovic
 */
public interface DMXLocaleManager {

    /**
     * <p>
     * Sets current locale to be used in application. If supplied argument  is <code>null</code>,
     * a {@code IllegalArgumentException} will be thrown.
     * </p>
     * <p>
     *  Setting new <code>Locale</code> will cause all loaded bundles for previous <code>Locale</code> to be unloaded, 
     * and then reloaded for newly set <code>Locale</code>.
     * </p>
     * 
     * @param locale locale to set
     */
    void setCurrentLocale(Locale locale);

    /**
     * Returns application's current locale. If no {@code Locale} is specifically
     * set, this method will return JVM default locale.
     *
     * @return current locale
     */
    Locale getCurrentLocale();

    /**
     * Sets locales that applications supports. If supplied array is <code>null</code> or empty,
     * an {@code IllegalArgumentException} is thrown.
     *
     * @param locales array of supported locales
     */
    void setSupportedLocales(Locale[] locales);

    /**
     * Returns the {@code Locale}s supported by the application. If no locales are explicitly added,
     * this method returns an array with only JVM default locale.
     *
     * @return array of {@code Locale}s
     */
    Locale[] getSupportedLocales();

    /**
     * Adds new locale to a list of supported locales. If supplied argument is <code>null</code>,
     * an {@code IllegalArgumentException} is thrown.
     *
     * @param locale locale to add
     */
    void addSupportedLocale(Locale locale);

    /**
     * Removes all supported locales from this resource manager. After calling this method, implementation
     * should support only JVM default locale. This method will also set current locale to JVm default one.
     *
     */
    void clearSupportedLocales();
    
    /**
     * Returns resource handler implementation associated with this instance of resource manager.
     * 
     * @param type requested type of resource handler
     * @param <T> type of resource handler
     * @return resource handler
     */
//    <T extends DMXResourceHandler> T getResourceHandler(ResourceHandlerType type);
    
    
    /**
     * Registers resources specified in extension callback. Depending on underlying implementation,
     * these resources can come from different sources (resource bundles, Android resources etc). These
     * values take precedence over the resources originally registered. 
     * 
     * @param callback  extension callback
     */
    void registerCallbackResources(DMXExtensionCallback callback);
}
