/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.common;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.ResourceManagerAware;
import java.util.Locale;

/**
 * Performs appliaction-wide locale change.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class LocaleChangeController implements DMXController, ResourceManagerAware {
    
    private DMXLocaleManager resourceManager;

    @Override
    public void setResourceManager(DMXLocaleManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    @Override
    public String getNextViewId() {
        return null;
    }

    @Override
    public String getControllerId() {
        return this.getClass().getName();
    }

    @Override
    public String[] getControllerData() {
        return new String[0];
    }

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        Locale locale = (Locale)params[0];
        resourceManager.setCurrentLocale(locale);
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

}
