/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.event.DMXEvent;
import java.util.Map;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXModelEvent implements DMXEvent {
    
    private final DMXModelEventInfo eventInfo;
    
    public DMXModelEvent(DMXModelEventInfo.ModelEventType type,Map<String, Object> oldValues, Map<String, Object> newValues){
        eventInfo = new DMXModelEventInfo(type, oldValues, newValues);
    }
    
    public DMXModelEvent(String id){
        eventInfo = new DMXModelEventInfo(id);
    }

    @Override
    public String getTopic() {
        return DMXEventConstants.TOPIC_MODEL_CHANGE;
    }

    @Override
    public DMXModelEventInfo getData() {
        return eventInfo;
    }

}
