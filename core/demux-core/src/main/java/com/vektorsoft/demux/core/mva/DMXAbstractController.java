/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

/**
 * <p>
 *  Abstract implementation of {@link DMXController}. Clients can extends this class in order to reuse 
 * common functionality found in most controllers, and only implement business logic methods.
 * </p>
 * <p>
 *  In addition, this class implements {@link ModelChangeAware} interface, which enables it to notify listeners about changes in
 * model. This is used to track progress for long-running tasks.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public abstract  class DMXAbstractController implements DMXController{
    
    /**
     * Model data IDs.
     */
    protected String[] ids;
    
    /**
     * Creates new instance.
     * 
     * @param dataIds model data IDs
     */
    public DMXAbstractController(String... dataIds){
        if(dataIds.length > 0){
            ids = new String[dataIds.length];
            System.arraycopy(dataIds, 0, ids, 0, dataIds.length);
        } else {
            ids = new String[0];
        }
    }

    /**
     * This method assumes that no next view is defines, and thus returns an empty string.
     * 
     * @return empty string
     */
    @Override
    public String getNextViewId() {
        return "";
    }

    /**
     * Returns a mapping for this controller. By default, class name is used as mapping.
     * 
     * @return class name as controller mapping
     */
    @Override
    public String getControllerId() {
        return getClass().getName();
    }

    @Override
    public final  String[] getControllerData() {
        String[] copy = new String[ids.length];
        if(copy.length > 0){
            System.arraycopy(ids, 0, copy, 0, ids.length);
        }
        return copy;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }
    
    

}
