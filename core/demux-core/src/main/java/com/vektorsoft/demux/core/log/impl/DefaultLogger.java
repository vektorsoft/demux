/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.log.impl;

import com.vektorsoft.demux.core.log.DMXLogger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>
 * Default implementation of {@link DMXLogger} interface. This implementation is based on JDK logging, and represents a wrapper for
 * JDK {@code Logger}, with some additional functionality.
 * </p>
 * <p>
 *  For use in OSGI environments, this implementation will prepend bundle symbolic name to log message. For example, log call like  
 * <code>logger.debug("some message");</code> will produce the following logoutput: <code>[bundle-sym-name] some message</code> , 
 * where <code>bundle-sym-name</code> is symbolic name of bundle where logger is invoked.
 * </p>
 * <p>
 *  <strong>Note:</strong> Bundle name is prepended only if logger is created with class, not name
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DefaultLogger implements DMXLogger {
    
    /**
     * Underlying logger to use.
     */
    private final Logger logger;
    
    /**
     * Name of the bundle invoking the logger.
     */
    private final String bundleName;
    
    /**
     * Creates new logger based on JDK {@code Logger}.
     * 
     * @param logger logger to wrap
     */
    public DefaultLogger(Logger logger){
        this(logger, null);
    }
    
    /**
     * Creates new logger based on specified JDK {@code Logger} and bundle name.
     * 
     * @param logger logger to wrap
     * @param bundleName bundle symbolic name
     */
    public DefaultLogger(Logger logger, String bundleName){
        this.logger = logger;
        this.bundleName = bundleName;
    }
    
    /**
     * Formats log message by prepending bundle name (if it exists).
     * 
     * @param message log message
     * @return formatetd message
     */
    private String formatMessage(String message){
        if(bundleName != null){
            return "[" + bundleName + "] " + message;
        }
        return message;
    }
    
    @Override
    public void debug(String message){
        logger.log(Level.FINE, formatMessage(message));
        
    }

    @Override
    public void debug(String message, Throwable throwable) {
        logger.log(Level.FINE, formatMessage(message), throwable);
    }
    
    
    @Override
    public void info(String message){
        logger.info(formatMessage(message));
    }

    @Override
    public void info(String message, Throwable throwable) {
        logger.log(Level.INFO, formatMessage(message), throwable);
    }
    
    
    @Override
    public void warn(String message){
        logger.warning(formatMessage(message));
    }

    @Override
    public void warn(String message, Throwable throwable) {
        logger.log(Level.WARNING, formatMessage(message), throwable);
    }
    
    
    @Override
    public void error(String message){
        logger.severe(formatMessage(message));
    }

    @Override
    public void error(String message, Throwable throwable) {
        logger.log(Level.SEVERE, formatMessage(message), throwable);
    }
    
}
