/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.EventBusAware;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 * Represents a Model in Model-View-Adapter pattern. This class maintains
 * application state, meaning that all changes to application data are reflected
 * to Model.
 * </p>
 * <p>
 * Every piece of application data that is used by View and Adapter is
 * registered with the model. Each data needs to have unique name so it can be
 * identified accessed unambiguously.
 * </p>
 * <p>
 * <b>Notice:</b> All operations on this class are thread safe.
 * </p>
 */
public final class DMXModel implements EventBusAware {

    /**
     * String representing null value. This will be placed in data map instead of a <code>null</code> value.
     */
    private static final String NULL_VALUE_STRING = "com.vektorsoft.demuc.core.mva.DMXModel.NULL";
    /**
     * Holds application data. Map key is data name, and value is actual data
     * value.
     */
    private final Map<String, Object> dataMap;
    
    
    private DMXEventBus eventBus;

    /**
     * Create new instance.
     */
    public DMXModel() {
        dataMap = new ConcurrentHashMap<String, Object>();
    }

    /**
     * Registers specified data with the model. Data is registered under
     * specified dataId, which must be unique within model.
     *
     * @param dataId dataId used to register data
     * @param value data value
     */
    public void registerData(String dataId, Object value) {
        // replace original data with null string, if it is null
        Object val = (value == null) ? NULL_VALUE_STRING : value;
        dataMap.put(dataId, val);
        Map<String, Object> tmp = new HashMap<String, Object>();
        tmp.put(dataId, val);
        notifyDataChanged(new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_REGISTERED, null, tmp));
    }
    
    /**
     * Remove data with specified ID from model.
     * 
     * @param id data ID
     */
    public void unregisterData(String id){
        dataMap.remove(id);
        notifyDataChanged(new DMXModelEvent(id));
    }

    /**
     * Set value for data with specified ID.
     *
     * @param id data ID
     * @param value data value
     */
    public void setDataValues(String id, Object value) {
        Map<String, Object> data = new HashMap<String, Object>();
        // replace original data with null string, if it is null
        Object val = (value == null) ? NULL_VALUE_STRING : value;
        data.put(id, val);
        setDataValues(data);
    }

    /**
     * Sets values for data specified in a map. Map key is data ID, and value is
     * data value.
     *
     * @param map data map
     */
    public void setDataValues(Map<String, Object> map) {
        Map<String, Object> oldVals = new HashMap<String, Object>();
        for(Map.Entry<String, Object> entry : map.entrySet()){
            oldVals.put(entry.getKey(), dataMap.get(entry.getKey()));
            if(entry.getValue() != null){
                dataMap.put(entry.getKey(), entry.getValue());
            } else {
                dataMap.put(entry.getKey(), NULL_VALUE_STRING);
            }
        }
        notifyDataChanged(new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, oldVals, map));
    }

    /**
     * Returns value of model data with specified ID.
     *
     * @param dataId ID under which data is registered
     * @return data value
     */
    public Object getDataValue(String dataId) {
        Object data = dataMap.get(dataId);
        if(data.toString().equals(NULL_VALUE_STRING)){
            data = null;
        }
        return data;
    }

    /**
     * Checks is data with specified ID is registered.
     *
     * @param dataId data ID
     * @return <code>true</code> if data is registered, <code>false</code>
     * otherwise
     */
    public boolean isDataRegistered(String dataId) {
        return dataMap.containsKey(dataId);
    }

    /**
     * Returns values of model data with specified IDs. The result map contains
     * data IDs as map keys, and values as map data.
     *
     * @param dataIds IDs of data to get
     * @return data map
     */
    public Map<String, Object> getDataValues(String... dataIds) {
        Map<String, Object> result = new HashMap<String, Object>();
        for (String id : dataIds) {
            // we only want to return data if it is contained within the model
            if (dataMap.containsKey(id)) {
                result.put(id, getDataValue(id));
            } else {
                // otherwise, display error
                System.err.println("Variable name not registered: " + id);
            }

        }
        return result;
    }
    
    /**
     * Returns all data values in model.
     * 
     * @return map of registered data;
     */
    public Map<String, Object> getAllData(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.putAll(dataMap);
        
        return result;
    }


    /**
     * Notifies registered listeners about the change in data.
     *
     * @param event  change event
     */
    private void notifyDataChanged(DMXModelEvent event) {
       if(eventBus != null){
           eventBus.publish(event);
       }
    }

    @Override
    public void setEventBus(DMXEventBus bus) {
        this.eventBus = bus;
    }
    
}
