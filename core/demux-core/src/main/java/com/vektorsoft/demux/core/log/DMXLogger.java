/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.log;

/**
 * Basic logging interface for DEMUX platform. This interface defines common logging methods which can be 
 * used across all platforms and logging implementations. Clients are encouraged to use this interface for their
 * logging needs instead of specific implementation. This allows for greater flexibility of underlying logging implementation
 * at runtime, since various implementations can be used (ie. JDK Log, OSGI <code>LogService</code>, SLF4J etc.).
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DMXLogger {

    /**
     * Logs a message with DEBUG level.
     * 
     * @param message  message to log
     */
    void debug(String message);
    
    /**
     * Logs a message with DEBUG level and adds exception info.
     * 
     * @param message message to log
     * @param throwable exception to log
     */
    void debug(String message, Throwable throwable);

    /**
     * Logs message with ERROR level.
     * 
     * @param message message to log
     */
    void error(String message);
    
    /**
     * Logs a message with ERROR level and adds exception info.
     * 
     * @param message message to log
     * @param throwable exception to log
     */
    void error(String message, Throwable throwable);

    /**
     * Logs message with INFO level.
     * 
     * @param message message to log
     */
    void info(String message);
    
    /**
     * Logs a message with INFO level and adds exception info.
     * 
     * @param message message to log
     * @param throwable exception to log
     */
    void info(String message, Throwable throwable);

    /**
     * Logs message with WARNING level.
     * 
     * @param message message to log
     */
    void warn(String message);
    
    /**
     * Logs a message with WARNING level and adds exception info.
     * 
     * @param message message to log
     * @param throwable exception to log
     */
    void warn(String message, Throwable throwable);
    
}
