/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.DMXEventBus;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Represents snapshot of model data which is passed to methods in order to supply execution
 * parameters.
 * </p>
 * <p>
 *  Usually, main application model is updated when controller execution is completed. However, it is possible to 
 * update main model as soon as changes to local model are made, even before controller execution has finished. This is 
 * achieved by setting {@code ModelChangeListener} on local model. This listener will notify adapter about any data changes. It can 
 * be useful when controller runs for a long time, and progress updates must be published to the application.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class DMXLocalModel {
    
    /**
     * Local model data.
     */
    private Map<String, Object> data;
    
    private DMXEventBus eventBus;
    
     /**
     * Creates new instance.
     */
    private DMXLocalModel(){
        data = new HashMap<String, Object>();
    }
    
    /**
     * Creates new instance of this class.
     * 
     * @return class instance
     */
    public static DMXLocalModel instance(){
        return new DMXLocalModel();
    }

    public void setEventBus(DMXEventBus eventBus) {
        this.eventBus = eventBus;
    }
    
    
    /**
     * Adds data to the local model.
     * 
     * @param id data ID
     * @param value  data value
     * @return current model instance
     */
    public DMXLocalModel set(String id, Object value){
        data.put(id, value);
        // notify listener
        if(eventBus != null){
            Map<String, Object> datamap = new HashMap<String, Object>();
            datamap.put(id, value);
            eventBus.publish(new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, null, datamap));
        }
        return this;
    }
    
    /**
     * Set data map to this local model.
     * 
     * @param dataMap data map to set
     * @return {@code DMXLocalModel} instance
     */
    public DMXLocalModel set(Map<String, Object> dataMap){
        this.data = dataMap;
        // notify listener
        if(eventBus != null){
            eventBus.publish(new DMXModelEvent(DMXModelEventInfo.ModelEventType.DATA_UPDATED, null, dataMap));
        }
        return this;
    }

    /**
     * Gets all data registered with this local model.
     * 
     * @return  model data
     */
    public Map<String, Object> get() {
        return data;
    }
    
    /**
     * Returns value of data with given ID.
     * 
     * @param id data ID
     * @return data value
     */
    public Object get(String id){
        return data.get(id);
    }
    
    /**
     *  Returns value of data with given ID as type of specified class. If data with specified ID does not exist,
     * returns <code>null</code>
     * 
     * @param <T> return type
     * @param id data ID
     * @param clazz data class 
     * @return data value or <code>null</code> if does not exist in this local model
     */
    public <T> T get(String id, Class<T> clazz){
        return (T)data.get(id);
    }
    
    /**
     * Returns value of data with given ID as type of specified class.
     * 
     * @param <T> required data type
     * @param id data ID
     * @param clazz data class
     * @param defaultValue default value to return if variable with given ID does not exist
     * @return data value as specified type
     */
    public <T> T get(String id, Class<T> clazz, T defaultValue){
        if(!data.containsKey(id)){
            return defaultValue;
        }
        return (T)data.get(id);
    }
    
    public boolean containsData(String dataId){
        return data.containsKey(dataId);
    }
    
}
