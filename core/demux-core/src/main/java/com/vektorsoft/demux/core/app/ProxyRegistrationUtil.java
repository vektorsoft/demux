/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.app;

import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.proxy.ProxyUtils;
import com.vektorsoft.demux.core.proxy.impl.ControllerInvocationHandler;
import com.vektorsoft.demux.core.proxy.impl.ViewInvocationHandler;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ProxyRegistrationUtil {

    public static void registerAnnotatedController(BundleContext context, Object object, Dictionary properties){
        DMXController controller = null;
         if(object instanceof DMXController){
            controller = (DMXController)object;
        } else {
            Class clazz = object.getClass();
            // check annotations
            if (clazz.isAnnotationPresent(DMXControllerDeclaration.class)) {
                try {
                    ControllerInvocationHandler handler = new ControllerInvocationHandler(object);
                    List<Class> interfaces = new ArrayList<Class>();
                    interfaces.add(DMXController.class);
                    interfaces.addAll(ProxyUtils.getAllImplementedInterfaces(clazz));
                    controller = (DMXController)Proxy.newProxyInstance(ProxyRegistrationUtil.class.getClassLoader(), interfaces.toArray(new Class[interfaces.size()]), handler);

                } catch (Exception ex) {
                    
                    throw new RuntimeException(ex);
                }

            }
        }
         if(controller != null){
             context.registerService(DMXController.class.getName(), controller, properties);
         }
    }
    
    public static void registerAnnotatedView(BundleContext context, Object object, Dictionary properties){
        DMXView view = null;
        if (object instanceof DMXView) {
            view = (DMXView)object;
        } else {
            Class clazz = object.getClass();
            if (clazz.isAnnotationPresent(DMXViewDeclaration.class)) {
                try {
                    ViewInvocationHandler handler = new ViewInvocationHandler(object);
                    List<Class> interfaces = new ArrayList<Class>();
                    // check if this is dialog view, and set appropriate interface
                    if (ProxyUtils.isDialogView(clazz)) {
                        interfaces.add(DMXDialogView.class);
                    } else {
                        interfaces.add(DMXView.class);
                    }
                    if(ProxyUtils.isEventHandler(clazz)){
                        interfaces.add(DMXEventHandler.class);
                    }
                    interfaces.addAll(ProxyUtils.getAllImplementedInterfaces(clazz));
                    view = (DMXView) Proxy.newProxyInstance(ProxyRegistrationUtil.class.getClassLoader(), interfaces.toArray(new Class[interfaces.size()]), handler);

                } catch (Exception ex) {
                    ex.printStackTrace(System.err);
                    throw new RuntimeException(ex);
                }

            }
        }
        if(view != null){
            context.registerService(DMXView.class.getName(), view, properties);
        }
    }
}
