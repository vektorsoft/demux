/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.proxy.impl;

import com.vektorsoft.demux.core.proxy.*;
import com.vektorsoft.demux.core.annotations.DMXDialogResult;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.IncompleteDeclarationException;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import com.vektorsoft.demux.eb.annotation.Filter;
import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Invocation handler used for forwarding proxied methods for views. In order to completely conform
 * to framework operations, underlying class is required to have the following properties:
 * </p>
 * <ul>
 *  <li>must be annotated with {@link DMXViewDeclaration} annotation</li>
 *  <li>must contain a method annotated with {@code DMXViewDeclaration.Render}</li>
 *  <li>must contain field or method annotated with {@code DMXViewDeclaration.ViewUI}</li>
 *  <li>must contain field or method annotated with {@code DMXViewDeclaration.ConstructUI}</li>
 * </ul>
 * 
 * <p>
 *  Class must provide default, no-argument constructor, or a constructor with single argument of type
 * {@link DMXDefaultAdapter}. If later is provided, it will take precedence over the no-argument constructor. This
 * can be used to set {@code DMXDefaultAdapter} filed if it needs to be used.
 * </p>
 * <p>
 *  Method annotated with {@code DMXViewDeclaration.ConstructUI} will be invoked on UI thread. All UI construction 
 * operation should be performed in this method, so that UI can be constructed properly.
 * </p>
 * 
 *
 * @author Vladimir Djurovic
 */
public class ViewInvocationHandler implements InvocationHandler {

    /**
     * ID of this view.
     */
    protected String viewId;
    
    protected String[] dataIds;
    /**
     * ID of this view's parent.
     */
    protected String parentViewId;
    /**
     * Underlying object.
     */
    protected Object underlying;
    /**
     * Fields of underlying object declared as model data.
     */
    protected Map<String, FieldAccessor> modelFields;
   
    /**
     * Method for adding child view.
     */
    protected Method addChildMethod;
    
    /**
     * Method for removing child view.
     */
    protected Method removeChildMethod;
    /**
     * Field for view UI.
     */
    protected FieldAccessor viewUiField;
    /**
     * Method for getting view UI.
     */
    protected Method viewUiMethod;
    /**
     * View placement constraint field.
     */
    protected FieldAccessor constraintField;
    /**
     * Method for getting placement constraint.
     */
    protected Method constraintMethod;
    
    /** Method for initial UI construction. */
    protected Method constructUiMethod;
    
    /** Method for setting active child view. */
    protected Method viewActivationMethod;
    
    
    /** If <code>true</code>, indicates that underlying object implements
     * {@code DMXExtendable}.
     */
    protected boolean extendable = false;
    
    /** Method for getting dialog result. */
    protected Method dlgResultMethod;
    
    /** Dialog field for view. */
    protected FieldAccessor dlgField;
    
    /** Indicates whether this view is dialog view. */
    protected boolean isDialog = false;
    
    /** If this view is dialog view, this indicates modality. */
    protected boolean isModal = false;
    
    /** Title for dialog view. */
    protected String title;
    
    protected Map<String, Method> topicHandlers;
    
    protected Map<String, DMXEventFilter> topicFilters;
    
    /**
     * Default constructor used only from subclasses.
     */
    protected ViewInvocationHandler(){
        
    }

    /**
     * Create new instance.
     *
     * @param object  view object
     * @throws InstantiationException if class can not be instantiated
     * @throws IllegalAccessException if element can not be accessed
     * @throws IncompleteDeclarationException if some of the required view annotations are missing
     * @throws NoSuchMethodException if method does not exist
     * @throws InvocationTargetException if an error occurs during execution
     */
    public ViewInvocationHandler(Object object) 
                throws InstantiationException, IllegalAccessException, 
                IncompleteDeclarationException, NoSuchMethodException, InvocationTargetException {
        // check if constructor with DMXDefaultAdapter argument exists
        underlying = object;

        initialize(object.getClass());
    }

    /**
     * Initializes this handler with correct values from underlying object. This
     * method is called from constructor.
     *
     * @param clazz class of underlying object
     *
     * @throws IllegalAccessException if fields or methods cannot be accessed
     * @throws IncompleteDeclarationException if some of the required annotations are missin
     * @throws NoSuchMethodException if required method does not exist
     * @throws InvocationTargetException if an error occurs
     */
    protected final void initialize(Class clazz) 
                                            throws IllegalAccessException, IncompleteDeclarationException, NoSuchMethodException, InvocationTargetException {
        DMXViewDeclaration decl = (DMXViewDeclaration) clazz.getAnnotation(DMXViewDeclaration.class);
        viewId = decl.viewID();
        parentViewId = decl.parentViewId();
        if(ProxyUtils.isDialogView(clazz)){
            isDialog = true;
            isModal = decl.modal();
            title = decl.title();
        }
        
        dataIds = decl.dataIds();
        topicHandlers = new HashMap<String, Method>();
        topicFilters = new HashMap<String, DMXEventFilter>();

        modelFields = new HashMap<String, FieldAccessor>();
        // find model data fields and populate map
        for (Field field : clazz.getDeclaredFields()) {
            // if field is of type DMXDefaultAdapter, inject it
            if(field.getType().equals(DMXDialog.class)){
                dlgField = new FieldAccessor(field, underlying);
            }
            
           if (field.isAnnotationPresent(DMXViewDeclaration.ViewUI.class)) {
                viewUiField = new FieldAccessor(field, underlying);
            } else if (field.isAnnotationPresent(DMXViewDeclaration.PlacementConstraint.class)) {
                constraintField = new FieldAccessor(field, underlying);
            }
            
        }
        // look up all annotated methods
        for (Method m : clazz.getDeclaredMethods()) {
            if (m.isAnnotationPresent(DMXViewDeclaration.AddChildView.class)) {
                addChildMethod = m;
            } else if(m.isAnnotationPresent(DMXViewDeclaration.RemoveChildView.class)){
                removeChildMethod = m;
            }else if (m.isAnnotationPresent(DMXViewDeclaration.ViewUI.class)) {
                viewUiMethod = m;
            } else if (m.isAnnotationPresent(DMXViewDeclaration.PlacementConstraint.class)) {
                constraintMethod = m;
            } else if(m.isAnnotationPresent(DMXDialogResult.class)){
                dlgResultMethod = m;
            } else if(m.isAnnotationPresent(DMXViewDeclaration.ConstructUI.class)){
                constructUiMethod = m;
            } else if(m.isAnnotationPresent(DMXViewDeclaration.Activation.class)){
                viewActivationMethod = m;   
            }
            if(m.isAnnotationPresent(EventHandler.class)){
                topicHandlers.put(m.getAnnotation(EventHandler.class).topic(), m);
            }
            if(m.isAnnotationPresent(Filter.class)){
                DMXEventFilter filter = (DMXEventFilter)m.invoke(underlying);
                topicFilters.put(m.getAnnotation(Filter.class).topic(), filter);
            }
        }
        // validate registration
       
        if(constructUiMethod == null){
            throw new IncompleteDeclarationException("View must contain method annotated with @DMXViewDeclaration.ConstructUI");
        }
        if(viewUiField == null && viewUiMethod == null){
            throw new IncompleteDeclarationException("View must contain field or method annotated with @DMXViewDeclaration.ViewUI");
        }
        if(isDialog && dlgField == null){
             throw new IncompleteDeclarationException("Dialog view must contain field of type com.vektorsoft.demux.core.dlg.DMXDialog");
        }
        
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if ("getViewId".equals(method.getName())) {
            return viewId;
        } else if ("getParentViewId".equals(method.getName())) {
            return parentViewId;
        } else if ("getViewDataIds".equals(method.getName())) {
            return dataIds;
        } else if ("updateFromModel".equals(method.getName())) {
            DMXLocalModel data = (DMXLocalModel) args[0];
            DMXLocalModel localModel = DMXLocalModel.instance();
            for (Map.Entry<String, Object> entry : data.get().entrySet()) {
                if(modelFields.containsKey(entry.getKey())){
                    modelFields.get(entry.getKey()).setFieldValue(entry.getValue());
                } else {
                    localModel.set(entry.getKey(), entry.getValue());
                }
            }
            
        } else if ("addChildView".equals(method.getName())) {
            if (addChildMethod != null) {
                addChildMethod.invoke(underlying, args);
            }
        } else if("removeChildView".equals(method.getName())){
            if(removeChildMethod != null){
                removeChildMethod.invoke(underlying, args);
            }
        } else if ("getViewUI".equals(method.getName())) {
            if (viewUiMethod != null) {
                return viewUiMethod.invoke(underlying, args);
            } else if (viewUiField != null) {
                return viewUiField.getFieldValue();
            }
        } else if ("viewPlacementConstraint".equals(method.getName())) {
            if (constraintMethod != null) {
                return constraintMethod.invoke(underlying, args);
            } else if (constraintField != null) {
                return constraintField.getFieldValue();
            }
        } else if("setViewActive".equals(method.getName())){
            if(viewActivationMethod != null){
                viewActivationMethod.invoke(underlying, args);
            }
        } else if(isDialog && "isModal".equals(method.getName())){
            
            return isModal;
            
        } else if(isDialog && "getDialogTitle".equals(method.getName())){
            
            return title;
            
        } else if(isDialog && "getResult".equals(method.getName())){
            
            return dlgResultMethod.invoke(underlying);
            
        } else if(isDialog && "setDialog".equals(method.getName())){
           
            dlgField.setFieldValue((DMXDialog)args[0]);
        } else if(extendable && "getExtensionCallback".equals(method.getName())){
            return ((DMXExtendable)underlying).getExtensionCallback();
        } else if("constructUI".equals(method.getName())){
            return constructUiMethod.invoke(underlying, args);
            
        } else if("getTopics".equals(method.getName())) {
            return topicHandlers.keySet().toArray(new String[0]);
        } else if("handle".equals(method.getName())){
            DMXEvent event = (DMXEvent)args[0];
            return topicHandlers.get(event.getTopic()).invoke(underlying, args);
        } else if("getFilter".equals(method.getName())){
            String topic = (String)args[0];
            return topicFilters.get(topic);
        }else {
            return method.invoke(underlying, args);
        }

        return null;
    }
    
    /**
     * Returns class name of the underlying object.
     * 
     * @return class name
     */
    public String getUnderlyingClassName(){
        return underlying.getClass().getName();
    }
}
