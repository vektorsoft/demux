/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.resources;

import java.text.MessageFormat;
import java.util.Locale;

/**
 *
 * @author Vladimir Djurovic
 */
public interface DMXResourceHandler {
    
    /**
     * Set current locale for handler.
     * 
     * @param locale locale to set
     */
    void setCurrentLocale(Locale locale);

    /**
     * Gets a string specified in resource bundle with a given key. If no string exists
     * for a given key, the key value is returned.
     *
     * @param key string key
     * @return string value
     */
    String getString(String key);
    
     /**
     * <p>
     *	Formats message defined with a given key with specified arguments. Message format should conform to standard
     * Java format pattern (see {@link MessageFormat} for more information).
     * </p>
     *
     * @param key key defined for the message
     * @param args format arguments
     * @return formatted string
     */
    String formatMessage(String key, Object... args);
    
}
