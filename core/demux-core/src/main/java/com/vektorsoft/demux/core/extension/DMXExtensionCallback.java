/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.extension;

import com.vektorsoft.demux.core.resources.DMXResourceHandler;

/**
 * Defines interface for extension callbacks used to customize behavior of views and 
 * controllers.
 *
 * @author Vladimir Djurovic
 */
public interface DMXExtensionCallback {

    /**
     * Returns the mappings of views or controllers to which this
     * extension will apply. One callback can be used in multiple views 
     * or controllers.
     * 
     * @return class name
     */
    String[] getMappings();
    
    /**
     * Returns data that this callback wants to register with the model. These
     * are registered in addition to original data from view or controller, and
     * are handled in the same way as original data.
     * 
     * @return data mapping
     */
    String[] getCallbackData();
    
    DMXExtensionType getExtensionType();
   
    
    DMXResourceHandler getResourceHandler();
    
}
