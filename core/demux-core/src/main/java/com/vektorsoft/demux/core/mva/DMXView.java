/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva;

/**
 * Interface for all classes implementing View part of MVC pattern.
 *
 * @author Vladimir Djurovic
 */
public interface DMXView {

    /**
     * Returns this view ID. ID must be unique among all defined views.
     *
     * @return view ID
     */
    String getViewId();

    /**
     * Returns ID of parent view of this view.
     *
     * @return parent view ID
     */
    String getParentViewId();
    
    /**
     * <p>
     * Activates or deactivates the view which is the child of this view, based on the value of parameter 
     * {@code  state}.  When view is active, it means it is visible and has 
     * precedence over other view in the same position. For example, if views are 
     * stacked in vertical order, active view is placed on the top of the view hierarchy.
     * </p>
     * 
     * 
     * @param id ID of the child view to activate or deactivate
     * @param state if <code>true</code>, activate view. Otherwise, deactivate it
     */
    void setViewActive(String id, boolean state);

    /**
     * Returns an object that represents layout constraint which should be
     * respected when placing this view. For example, in GUI applications,
     * different layouts can specify constraints to where view can be placed.
     *
     * @return layout constraint for view
     */
    Object viewPlacementConstraint();

    /**
     * Adds specified view as a child of this view.
     *
     * @param child view to add as a child
     */
    void addChildView(DMXView child);
    
    /**
     * Remove child view with specified ID.
     * 
     * @param childId child view ID
     */
    void removeChildView(String childId);

    /**
     * Returns IDs of model data objects that are associated with this view.
     *
     * @return set of view IDs
     */
    String[] getViewDataIds();


    /**
     * Returns an object that represents actual UI presentation of this view.
     * For example, this can be Swing or JavaFX component, browser component
     * etc.
     *
     * @return UI object
     */
    Object getViewUI();
    
    /**
     * <p>
     * This method should be used for initial creation of user interface. It will be invoked 
     * from view manager to initially create UI. In order to maintain platform compatibility, it
     * will be invoked on UI thread of the underlying platform.
     * </p>
     * <p>
     *  <strong>Important:</strong> Implementing classes should use this method exclusively for
     * initial UI construction.
     * </p>
     * 
     */
    void constructUI();
}
