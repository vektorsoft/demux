/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.exception;

/**
 * <p>
 * This interface defines methods for exception handlers for all uncaught exceptions. Classes implementing
 * this interface must provide a logic for default handling of all uncaught exceptions in underlying platform. For 
 * example, this could mean show notifications, send error reports etc.
 * </p>
 * <p>
 *  Additionally, handler can <code>override</code> other handlers, which means that it will suppress execution of other 
 * exception handlers. This can be useful when multiple controllers have similar processing logic, but only one should be applied.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public interface DMXExceptionHandler extends Thread.UncaughtExceptionHandler {
    
    /**
     * Returns unique ID of this handler.
     * 
     * @return handler ID string
     */
    String getHandlerID();
    
    /**
     * Marks this handler as enabled, thus allowing it to process exceptions.
     * 
     * @param enabled status of the handler
     */
    void setEnabled(boolean enabled);
    
    /**
     * Checks if handler is enabled or not.
     * 
     * @return <code>true</code> if handler is enabled, <code>false</code> otherwise
     */
    boolean isEnabled();
    
    /**
     * Returns array of IDs of exception handlers which this handler overrides.
     * 
     * @return <code>true</code> if it is overriding, <code>false</code> otherwise.
     */
    String[] overrideIds();
    
    /**
     * Sets original default system exception hander. This can be used inside handler to maintain 
     * consistency with default system exception handling.
     * 
     * @param systemHandler system exception handler
     */
    void setOriginalSystemHandler(Thread.UncaughtExceptionHandler systemHandler);
}
