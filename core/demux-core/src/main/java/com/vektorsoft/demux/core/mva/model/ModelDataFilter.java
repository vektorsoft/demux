/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2015 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.mva.model;

import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import java.util.Set;

/**
 *  Implementation of {@link DMXEventFilter} for filtering model events. This implementation will
 * accept only model events which contain model data IDs declared for this view.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ModelDataFilter implements DMXEventFilter {

    private final String[] ids;

    public ModelDataFilter(String... ids) {
        this.ids = ids;
    }

    @Override
    public boolean match(DMXEvent event) {
        boolean match = false;
        if (event instanceof DMXModelEvent) {
            DMXModelEventInfo info = (DMXModelEventInfo) event.getData();
            Set<String> changedIds = info.getChangedIds();
            for (String id : this.ids) {
                if (changedIds.contains(id)) {
                    match = true;
                    break;
                }
            }
        }
        return match;
    }

}
