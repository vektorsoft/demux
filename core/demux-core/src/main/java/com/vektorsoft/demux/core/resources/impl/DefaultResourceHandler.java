/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.resources.impl;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 *
 * @author Vladimir Djurovic
 */
public class DefaultResourceHandler implements DMXResourceHandler{
    
     /** Map of loaded resources for application. */
    protected Map<String, Object> resourceMap;
    
    
     /** Map of loaded bundles and their class loaders. */
    private final Map<String, List<ClassLoader>> loadedBundles;
    
     /** List of registered extension callbacks. */
    private final List<DMXExtensionCallback> callbacks;
    
    /** Current locale. */
    private Locale currentLocale;
    
    /**
     * Creates new instance.
     */
    public DefaultResourceHandler(){
        resourceMap = new ConcurrentSkipListMap<String, Object>();
        loadedBundles = new ConcurrentSkipListMap<String, List<ClassLoader>>();
        callbacks = new ArrayList<DMXExtensionCallback>();
        currentLocale = Locale.getDefault();
    }

    @Override
    public void setCurrentLocale(Locale locale) {
        this.currentLocale = locale;
        reloadBundles();
    }

    @Override
    public String getString(String key) {
        String string = key;
        Object obj = resourceMap.get(key);
        if(obj != null){
            string = obj.toString();
        }
        return string;
    }
    
    
    /**
     * Returns a resource associated with given key.
     * 
     * @param key resource key
     * @return  resource
     */
    public Object getResource(String key) {
        Object obj = resourceMap.get(key);
        return obj;
    }

    @Override
    public String formatMessage(String key, Object... args) {
         StringBuffer sb = new StringBuffer();
        Object pattern = resourceMap.get(key);
        if(pattern != null){
            MessageFormat msgFormat = new MessageFormat(pattern.toString(), currentLocale);
            msgFormat.format(args, sb, null);
        }
        return sb.toString();
    }

    
    /**
     * <p>
     * Loads resource bundle with supplied name for the current locale. Contents of the bundle will be made 
     * available to callers. If this method is called multiple times, all resources will be added to local cache.
     * </p>
     *
     * @param name resource bundle name
     * @param classLoader class loader to use
     */
    public void loadResourceBundle(String name, ClassLoader classLoader) {
        ResourceBundle bundle = ResourceBundle.getBundle(name, currentLocale, classLoader);
        postLoadBundle(name, bundle, classLoader);
    }
    
    /**
     * Load resource bundle with specified name for each class loader.
     * 
     * @param name bundle name
     * @param cls  classloaders list
     */
    private void loadResourceBundle(String name, List<ClassLoader> cls){
        for(ClassLoader cl : cls){
            loadResourceBundle(name, cl);
        }
    }
    
    /**
     * Invoked after the bundle has been loaded. This method will perform actions needed to register bundle data and
     * notify listeners.
     * 
     * @param name resource bundle name
     * @param bundle loaded bundle
     * @param classLoader classloader for the bundle
     */
    protected void postLoadBundle(String name, ResourceBundle bundle, ClassLoader classLoader){
         // add resources to resource map
        for(String key : bundle.keySet()){
            resourceMap.put(key, bundle.getObject(key));
        }
        // place bundle into cache
        if(loadedBundles.containsKey(name)){
            loadedBundles.get(name).add(classLoader);
        } else {
            List<ClassLoader> list = new ArrayList<ClassLoader>();
            list.add(classLoader);
            loadedBundles.put(name, list);
        }
        
    }
    
    /**
     * Returns the keys of all registered resources.
     * 
     * @return set of resource keys
     */
    public Set<String> getResourceKeys() {
        Set<String> keyset = new HashSet<String>();
        keyset.addAll(resourceMap.keySet());
        
        return keyset;
    }
    
    /**
     * Unloads bundle with specified name from list of loaded bundles.
     * 
     * @param name bundle name
     */ 
    public void unloadBundle(String name) {
        loadedBundles.remove(name);
    }
    
    /**
     * Reload all loaded bundles on locale change.
     */
    public void reloadBundles(){
        Set<String> missing = new HashSet<String>();
        // clear all values from resource map
        resourceMap.clear();
        for(Map.Entry<String, List<ClassLoader>> entry : loadedBundles.entrySet()){
            try {
                // use list copy to avoid concurrent modification exception
                List<ClassLoader> copyList = new ArrayList<ClassLoader>();
                copyList.addAll(entry.getValue());
                loadResourceBundle(entry.getKey(), copyList);
            } catch(MissingResourceException ex){
                missing.add(entry.getKey());
            }
        }
        if(!missing.isEmpty()){
            for(String name : missing){
                unloadBundle(name);
            }
        }
       
    }
    
}
