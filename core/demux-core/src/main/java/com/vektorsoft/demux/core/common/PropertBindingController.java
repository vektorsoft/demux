/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.common;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;

/**
 * Controller used to bind properties of GUI components to model values. This controller is intended
 * to be used with property and event listeners in order to keep model in sync with states of GUI 
 * components.
 *
 * @author Vladimir Djurovic
 */
public class PropertBindingController implements DMXController{
    
    /**
     * IDs of variables for this controller.
     */
    private String[] ids = new String[0];

    @Override
    public String getNextViewId() {
        return null;
    }

    @Override
    public String getControllerId() {
        return this.getClass().getName();
    }

    @Override
    public String[] getControllerData() {
        return ids;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }



    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        model.set(params[0].toString(), params[1]);
    }

   

}
