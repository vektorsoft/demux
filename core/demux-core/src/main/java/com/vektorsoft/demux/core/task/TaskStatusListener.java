/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.task;

/**
 * Defined method hooks for monitoring task status. These methods are invoked on every changes of
 * task status.
 *
 * @author Vladimir Djurovic
 */
public interface TaskStatusListener {

    /**
     * Invoked when task is started.
     * 
     * @param task started task
     */
    void taskStarted(DMXTask task);
    
    /**
     * Invoked when task is completed. Parameter <code>result</code> is optional and
     * represents result of task execution, if any.
     * 
     * @param task completed task
     * @param result task execution result (if any)
     */
    void taskCompleted(DMXTask task, Object result);
}
