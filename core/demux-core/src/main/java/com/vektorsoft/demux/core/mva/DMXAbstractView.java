/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.mva.model.ModelDataFilter;
import com.vektorsoft.demux.core.resources.DMXLocaleEvent;
import com.vektorsoft.demux.core.resources.DMXLocaleEventInfo;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import com.vektorsoft.demux.eb.event.DMXEventHandler;

/**
 * <p>
 *  An implementation of {@link DMXView} interface which provides some common methods that can be used 
 * for view across all supported platforms. Clients should extend this class to gain access to functionality 
 * provided automatically by the framework.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public abstract  class DMXAbstractView implements DMXView, DMXAdapterAware, DMXEventHandler {
    
    /**
     * Supported event topics names.
     */
    private static final String[] TOPICS = new String[]{DMXEventConstants.TOPIC_MODEL_CHANGE, DMXEventConstants.TOPIC_LOCALE_INFO};
    
    /**
     * Event filter for model data.
     */
    private final ModelDataFilter modelDataFilter;

    /** Set of model data IDs used for this view. */
    protected String[] ids;
    
    /** Adapter instance this view is registered with. */
    protected DMXAdapter adapter;
    
    /**
     * Creates new instance.
     * 
     * @param dataIds model data IDs
     */
    public DMXAbstractView(String... dataIds){
         if(dataIds.length > 0){
            ids = new String[dataIds.length];
            System.arraycopy(dataIds, 0, ids, 0, dataIds.length);
            modelDataFilter = new ModelDataFilter(ids);
        } else {
            ids = new String[0];
            modelDataFilter = null;
        }
    }
   
    /**
     * Returns this class name as view ID.
     * 
     * @return class name
     */
    @Override
    public String getViewId() {
        return getClass().getName();
    }

    /**
     * Returns empty string as parent view ID.
     * 
     * @return empty string
     */
    @Override
    public String getParentViewId() {
        return "";
    }

    /**
     * Returns <code>null</code> as view placement constraint.
     * 
     * @return null
     */
    @Override
    public Object viewPlacementConstraint() {
        return null;
    }

    /**
     * This method does nothing. Subclasses should override it to add 
     * child view correctly.
     * 
     * @param child child view
     */
    @Override
    public void addChildView(DMXView child) {
        
    }

    /**
     * This method does nothing. Subclasses should override it to remove
     * child view correctly.
     * 
     * @param childId  child view ID
     */
    @Override
    public void removeChildView(String childId) {
        // do nothing
    }
    

    /**
     * Empty implementation. Subclasses should override this method for
     * actual logic.
     * 
     * @param id child view ID
     * @param state view state
     */
    @Override
    public void setViewActive(String id, boolean state) {
        // do nothing
    }
    
    
    /**
     * Returns set of data IDs. This method will call {@link #loadDataIds() } to get list 
     * of data IDs for this view.
     * 
     * @return set of data IDs
     */
    @Override
    public final String[] getViewDataIds() {
       String[] copy = new String[ids.length];
        if(copy.length > 0){
            System.arraycopy(ids, 0, copy, 0, ids.length);
        }
        return copy;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public String[] getTopics() {
        return TOPICS;
    }
    
    /**
     * Invoked when model change event is received.
     * 
     * @param model event local model
     */
    protected void modelDataChanged(DMXLocalModel model){
        
    }
    
    protected void localeInfoChanged(DMXLocaleEventInfo info){
        
    }

    @Override
    public final void handle(DMXEvent event) {
        String topic = event.getTopic();
        if(DMXEventConstants.TOPIC_MODEL_CHANGE.equals(topic)){
            DMXModelEventInfo info = (DMXModelEventInfo)event.getData();
            modelDataChanged(DMXLocalModel.instance().set(info.getNewValues().get()));
        } else if(DMXEventConstants.TOPIC_LOCALE_INFO.equals(topic)){
            localeInfoChanged(((DMXLocaleEvent)event).getData());
        }
    }

    @Override
    public DMXEventFilter getFilter(String topicName) {
        if(DMXEventConstants.TOPIC_MODEL_CHANGE.equals(topicName)){
            return modelDataFilter;
        }
        return null;
    }

    
}
