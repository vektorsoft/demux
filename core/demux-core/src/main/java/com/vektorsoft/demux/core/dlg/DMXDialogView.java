/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.dlg;

import com.vektorsoft.demux.core.mva.DMXView;

/**
 * <p>
 *  Represents a view that should be displayed in dialog. This view will contained inside 
 * {@link DMXDialog} window.
 * </p>
 *
 * @author Vladimir Djurovic
 */
public interface DMXDialogView extends DMXView {

    /**
     * Check if view (ie. dialog containing it) is modal or not.
     * 
     * @return <code>true</code> if dialog is modal, <code>false</code> otherwise
     */
    boolean isModal();
    
    /**
     * Returns a string representing a title for this view's dialog window.
     * 
     * @return title for dialog window
     */
    String getDialogTitle();
    
    /**
     * Optional result of user's interaction with this view (ie. dialog). This will give
     * dialog the possibility to optionally return some value, if required.
     * 
     * @return result
     */
    Object getResult();
    
    /**
     * Set dialog which will display this view. 
     * 
     * @param dialog view dialog
     */
    void setDialog(DMXDialog dialog);
}
