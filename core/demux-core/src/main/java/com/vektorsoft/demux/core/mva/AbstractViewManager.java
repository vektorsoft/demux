/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.core.mva;

import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DMXExtensionType;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Base class for specific implementations of
 * <code>ViewManager</code>. Provides some common methods implementation that
 * can be reused by subclasses.
 */
public abstract class AbstractViewManager implements DMXViewManager {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(AbstractViewManager.class);
    
    
     /** Adapter instance associated with this view manager. */
    protected DMXDefaultAdapter adapter;
    
    
     /**
     * Contains all views registered with this manager. Map key is v iew name,
     * and value is the view itself.
     */
    protected final Map<String, DMXView> registeredViews;
    
    
    /** Views pending registration until their parent view is registered. */
    private final Map<String, Set<String>> deferredViews;
    
    protected final Map<String, Set<DMXExtensionCallback>> callbackMap;
    
    
    protected final Map<String, Set<String>> userConfigViews;
    
   

    /**
     * Create new instance.
     */
    protected AbstractViewManager(){
        registeredViews = new ConcurrentSkipListMap<String, DMXView>();
        deferredViews = new ConcurrentSkipListMap<String, Set<String>>();
        callbackMap = new ConcurrentSkipListMap<String, Set<DMXExtensionCallback>>();
        userConfigViews = new HashMap<String, Set<String>>();
    }
    
    /**
     * Called to complete registration of deferred views. Subclasses should override this method
     * to complete view registration, if required.
     * 
     * @param parentId registered parent view
     * @param childrenIds ids of dependent views
     */
    protected abstract void processDeferredViews(String parentId, Set<String> childrenIds);
    
    /**
     * Registers specified view for management by this view manager.
     * 
     * @param view view to register
     */
    @Override
    public  void registerView(DMXView view) {
        registeredViews.put(view.getViewId(), view);

        // check if parent view is registered and save this view to deffered if needed
        String parentId = view.getParentViewId();
        if(parentId != null && !parentId.isEmpty()){
            boolean registered = registeredViews.containsKey(parentId);
            if(!registered){
                if(deferredViews.containsKey(parentId)){
                    deferredViews.get(parentId).add(view.getViewId());
                } else {
                    Set<String> ids = new HashSet<String>();
                    ids.add(view.getViewId());
                    deferredViews.put(parentId, ids);
                }
            }
        }
        
    }
    
    
    /**
     * This method should be called from subclasses after initial view registration in order 
     * to correctly register deferred view.
     * 
     * @param view view to register
     */
    protected void afterInitialViewRegistration(DMXView view){
        // check if this view is needed for deffered views to complete registration
        String viewId = view.getViewId();
        if(deferredViews.containsKey(viewId)){
            processDeferredViews(viewId, deferredViews.get(viewId));
            deferredViews.remove(viewId);
        }
        
    }


    @Override
    public void unregisterView(String id) {
        DMXView view = registeredViews.get(id);
        if (view != null) {
            if (view.getParentViewId() != null) {
                DMXView parentView = registeredViews.get(view.getParentViewId());
                if (parentView != null) {
                    removeFromParent(id);
                }
            }

            registeredViews.remove(id);
        }
    }
    
    /**
     * Remove view with specified ID from it's parent. This method is platform dependent, since
     * it involves actually changing the GUI, which must be implemented in platform-dependent manner.
     * 
     * @param viewId ID of view to remove
     */
    protected abstract void removeFromParent(String viewId);
    
    protected abstract void applyViewExtension(String viewId, DMXExtensionCallback callback);

    @Override
    public void registerExtensionCallback(DMXExtensionCallback callback) {
        if(callback.getExtensionType() == DMXExtensionType.VIEW_EXTENSION){
            String[] mappings = callback.getMappings();
            for(String mapping : mappings){
                 if(callbackMap.containsKey(mapping)){
                        callbackMap.get(mapping).add(callback);
                    } else {
                        Set<DMXExtensionCallback> set = new HashSet<DMXExtensionCallback>();
                        set.add(callback);
                        callbackMap.put(mapping, set);
                    }
                    LOGGER.debug("Registered extension callback for mapping " + mapping);
                    DMXView view = registeredViews.get(mapping);
                    
                    if(view != null && (view instanceof DMXExtendable)){
                        applyViewExtension(mapping, callback);
                    }
            }
        }
    }

    @Override
    public void removeExtensionCallback(DMXExtensionCallback callback) {
        if (callback.getExtensionType() == DMXExtensionType.VIEW_EXTENSION) {
            for (String mapping : callback.getMappings()) {
                DMXView view = registeredViews.get(mapping);
                if (view != null && (view instanceof DMXExtendable)) {
                    ((DMXExtendable) view).setExtensionCallback(null);
                    LOGGER.debug("Reset extension callback for view " + mapping);
                }
                callbackMap.get(mapping).remove(callback);
                if (callbackMap.get(mapping).isEmpty()) {
                    callbackMap.remove(mapping);
                }
            }
        }
    }

    
    /**
     * @see DMXViewManager#getView(java.lang.String) 
     * 
     * @param name view name
     * @return  view
     */
    @Override
    public DMXView getView(String name) {
        return registeredViews.get(name);
    }
    
}
