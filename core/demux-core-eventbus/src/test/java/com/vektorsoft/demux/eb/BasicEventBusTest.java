/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.eb;

import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * Contains test cases for {@link BasicEventBus} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class BasicEventBusTest {
    
    private static final String MOCK_TOPIC_1 = "mock.topic.1";
    private static final String MOCK_TOPIC_2 = "mock.topic.2";
    
    private DMXEventBus eventBus;
    
    @Before
    public void setup(){
        eventBus = new BasicEventBus();
    }

    /**
     * <p>
     * Verifies correct event delivery to interested subscribers. Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create mock event handlers for different topics</li>
     *  <li>publish events for topics</li>
     *  <li>verify that each handler is invoked for it's topic<li>
     * </ul>
     */
    @Test
    public void eventDeliveryTest() throws Exception{
        DMXEventHandler handler1 = mock(DMXEventHandler.class);
        when(handler1.getTopics()).thenReturn(new String[]{MOCK_TOPIC_1, MOCK_TOPIC_2});
        eventBus.subscribe(handler1);
        
        DMXEventHandler handler2 = mock(DMXEventHandler.class);
        when(handler2.getTopics()).thenReturn(new String[]{MOCK_TOPIC_2});
        eventBus.subscribe(handler2);
        
        DMXEvent event = mock(DMXEvent.class);
        when(event.getTopic()).thenReturn(MOCK_TOPIC_2);
        
        eventBus.publish(event);
        Thread.sleep(200);
        verify(handler1).handle(event);
        verify(handler2).handle(event);
        
        DMXEvent event1 = mock(DMXEvent.class);
        when(event1.getTopic()).thenReturn(MOCK_TOPIC_1);
        eventBus.publish(event1);
        Thread.sleep(200);
        verify(handler1, times(1)).handle(event1);
        verify(handler2, times(0)).handle(event1);
    }
}
