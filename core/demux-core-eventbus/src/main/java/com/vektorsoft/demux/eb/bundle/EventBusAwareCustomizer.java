/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.eb.bundle;

import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.EventBusAware;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class EventBusAwareCustomizer implements ServiceTrackerCustomizer<EventBusAware, EventBusAware> {
    
    private final BundleContext bundleContext;
    private final DMXEventBus eventBus;

    public EventBusAwareCustomizer(BundleContext ctx, DMXEventBus eventBus) {
        this.bundleContext = ctx;
        this.eventBus = eventBus;
    }
    
    
    

    @Override
    public EventBusAware addingService(ServiceReference<EventBusAware> sr) {
        EventBusAware aware = bundleContext.getService(sr);
        aware.setEventBus(eventBus);
        
        return aware;
    }

    @Override
    public void modifiedService(ServiceReference<EventBusAware> sr, EventBusAware t) {
        
    }

    @Override
    public void removedService(ServiceReference<EventBusAware> sr, EventBusAware t) {
        
    }

}
