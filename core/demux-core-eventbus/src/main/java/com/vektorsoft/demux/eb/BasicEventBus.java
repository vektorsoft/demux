package com.vektorsoft.demux.eb;

import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;


/**
 * A simple Event Bus implementation which receives events or messages from
 * various sources and distributes them to all subscribers of the event type.
 * This is highly useful for programs which are event driven. Swing applications
 * in particular can benefit from an event bus architecture, as opposed to the
 * traditional event listener architecture it employs.
 * <p>
 * The BasicEventBus class is thread safe and uses a background thread to notify
 * the subscribers of the event. The subscribers are notified in a serial
 * fashion, and only one event will be published at a time. Though, the
 * {@link #publish(Object)} method is done in a non-blocking way.
 * <p>
 * Subscribers subscribe to the EventBus using the {@link #subscribe(Object)}
 * method. A specific subscriber type is not required, but the subscriber will
 * be reflected to find all methods annotated with the {@link EventHandler}
 * annotations. These methods will be invoked as needed by the event bus based
 * on the type of the first parameter to the annotated method.
 * <p>
 * An event handler can indicate that it can veto events by setting the
 * {@link EventHandler#canVeto()} value to true. This will inform the EventBus
 * of the subscriber's desire to veto the event. A vetoed event will not be sent
 * to the regular subscribers.
 * <p>
 * During publication of an event, all veto EventHandler methods will be
 * notified first and allowed to throw a {@link VetoException} indicating that
 * the event has been vetoed and should not be published to the remaining event
 * handlers. If no vetoes have been made, the regular subscriber handlers will
 * be notified of the event.
 * <p>
 * Subscribers are stored using a {@link WeakReference} such that a memory leak
 * can be avoided if the client fails to unsubscribe at the end of the use.
 * However, calling the {@link #unsubscribe(Object)} method is highly
 * recommended none-the-less.
 *
 * @author Adam Taft
 */
public final class BasicEventBus implements DMXEventBus {

    private final Map<String, Set<DMXEventHandler>> handlers = new ConcurrentHashMap<String, Set<DMXEventHandler>>();
    private final BlockingQueue<DMXEvent> queue = new LinkedBlockingQueue<DMXEvent>();
//	private final BlockingQueue<DMXEventHandler> killQueue = new LinkedBlockingQueue<DMXEventHandler>();

    /**
     * The ExecutorService used to handle event delivery to the event handlers.
     */
    private final ExecutorService executorService;

    /**
     * Should the event bus wait for the regular handlers to finish processing
     * the event messages before continuing to the next event. Defaults to
     * 'false' which is sensible for most use cases.
     */
    private final boolean waitForHandlers;

    /**
     * Default constructor sets up the executorService property to use the
     * {@link Executors#newCachedThreadPool()} implementation. The configured
     * ExecutorService will have a custom ThreadFactory such that the threads
     * returned will be daemon threads (and thus not block the application from
     * shutting down).
     */
    public BasicEventBus() {
        this(Executors.newCachedThreadPool(new ThreadFactory() {
            private final ThreadFactory delegate = Executors.defaultThreadFactory();

            @Override
            public Thread newThread(Runnable r) {
                Thread t = delegate.newThread(r);
                t.setDaemon(true);
                return t;
            }
        }), false);
    }

    public BasicEventBus(ExecutorService executorService, boolean waitForHandlers) {
        // start the background daemon consumer thread.
        Thread eventQueueThread = new Thread(new EventQueueRunner(), "EventQueue Consumer Thread");
        eventQueueThread.setDaemon(true);
        eventQueueThread.start();

        this.executorService = executorService;
        this.waitForHandlers = waitForHandlers;
    }

    /**
     * Subscribe the specified instance as a potential event subscriber. The
     * subscriber must annotate a method (or two) with the {@link EventHandler}
     * annotation if it expects to receive notifications.
     * <p>
     * Note that the EventBus maintains a {@link WeakReference} to the
     * subscriber, but it is still adviced to call the
     * {@link #unsubscribe(Object)} method if the subscriber does not wish to
     * receive events any longer.
     *
     * @param subscriber The subscriber object which will receive notifications
     * on {@link EventHandler} annotated methods.
     */
    @Override
    public void subscribe(DMXEventHandler handler) {
        String[] topics = handler.getTopics();
        for (String topic : topics) {
            if (handlers.containsKey(topic)) {
                handlers.get(topic).add(handler);
            } else {
                Set<DMXEventHandler> hSet = new HashSet<DMXEventHandler>();
                hSet.add(handler);
                handlers.put(topic, hSet);
            }
        }

    }

    /**
     * *
     * Unsubscribe the specified subscriber from receiving future published
     * events.
     *
     * @param subscriber The object to unsubcribe from future events.
     */
    @Override
    public void unsubscribe(DMXEventHandler handler) {
        String[] topics = handler.getTopics();
        for (String topic : topics) {
            if (handlers.containsKey(topic)) {
                handlers.get(topic).remove(handler);
            }
        }

    }

    /**
     * Publish the specified event to the event bus. Based on the type of the
     * event, the EventBus will publish the event to the subscribing objects.
     *
     * @param event The event to publish on the event bus.
     */
    @Override
    public void publish(DMXEvent event) {
        try {
            queue.put(event);

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns if the event bus has pending events.
     *
     * @return Returns true if the event bus has pending events to publish.
     */
    @Override
    public boolean hasPendingEvents() {
        return queue.size() > 0;
    }

    // called on the background thread.
    private void notifySubscribers(final DMXEvent evt) {
        String topic = evt.getTopic();
        if (handlers.containsKey(topic)) {
            for (DMXEventHandler handler : handlers.get(topic)) {
                if(handler.getFilter(topic) != null){
                    if(handler.getFilter(topic).match(evt)){
                        executorService.submit(new HandlerRunnable(handler, evt));
                    }
                } else {
                    executorService.submit(new HandlerRunnable(handler, evt));
                }
                
            }
        }

    }

	// the background thread consumer, simply extracts
    // any events from the queue and publishes them.
    private class EventQueueRunner implements Runnable {

        @Override
        public void run() {
            try {
                while (true) {
                    notifySubscribers(queue.take());
                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

	
    private static class HandlerRunnable implements Runnable {

        private final DMXEventHandler handler;
        private final DMXEvent event;

        HandlerRunnable(DMXEventHandler handler, DMXEvent event) {
            this.handler = handler;
            this.event = event;
        }

        @Override
        public void run() {
            handler.handle(event);
        }

    }
	
}
