/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.eb.bundle;

import com.vektorsoft.demux.eb.BasicEventBus;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.EventBusAware;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class EventBusActivator implements BundleActivator {
    
    private ServiceTracker tracker;

    @Override
    public void start(BundleContext bc) throws Exception {
        BasicEventBus evBus = new BasicEventBus();
        bc.registerService(DMXEventBus.class, evBus, null);
        
        tracker = new ServiceTracker(bc, EventBusAware.class, new EventBusAwareCustomizer(bc, evBus));
        tracker.open();
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        if(tracker != null){
            tracker.close();
        }
    }

}
