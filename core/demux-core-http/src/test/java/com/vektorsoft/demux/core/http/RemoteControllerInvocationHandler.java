/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.HessianFactory;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

/** 
 * Mock implementation of handler for requests which invoke controller remotely.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class RemoteControllerInvocationHandler extends AbstractHandler{
    
    private HessianFactory hessianFactory;

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        hessianFactory = new HessianFactory();
        Hessian2Input hin = hessianFactory.createHessian2Input(request.getInputStream());
        hin.startMessage();
        int length = hin.readInt();
        Map<String, Object> data = (Map<String, Object>)hin.readObject(Map.class);
        String[] keys = data.keySet().toArray(new String[data.keySet().size()]);
        List<Object> argList = null;
        if(length == 2){
            argList = (List<Object>)hin.readObject(List.class);
        }
        hin.completeMessage();
        hin.close();
        
        int val = (Integer)data.get("testData");
        val = val + 1;
        data.put("testData", val);
        
        response.setContentType("application/x-hessian");
        Hessian2Output hout = hessianFactory.createHessian2Output(response.getOutputStream());
        hout.startMessage();
        hout.writeObject(data);
        hout.completeMessage();
        hout.close();
        
        response.setStatus(HttpServletResponse.SC_OK);
        
    }
    
}
