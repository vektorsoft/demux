/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

import com.vektorsoft.demux.core.http.ctrl.DMXRemoteInvocationController;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import com.vektorsoft.demux.core.mva.model.DMXModel;
import com.vektorsoft.demux.core.mva.DMXViewManager;
import com.vektorsoft.demux.core.resources.impl.DefaultLocaleManager;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jetty.server.Server;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 * Provides test cases for class {@code DMXHttpClient}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXHttpClientTest {
    
    private static Server server;
    
    @BeforeClass
    public static void initClass() throws Exception{
         DMXHttpClient.instance().init("http://localhost:8080");
        server = new Server(8080);
        server.setHandler(new RemoteControllerInvocationHandler());
        Runnable r = new Runnable() {

            @Override
            public void run() {
                try {
                    server.start();
                    server.join();
                } catch(Exception ex){
                    throw new RuntimeException(ex);
                }
                 
            }
        };
        Thread th = new Thread(r);
        th.start();
        
       
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception{
        server.stop();
    }
    
    @Test
    @Ignore
    public void remoteControllerInvocationTest() throws Exception{
         DMXControllerHttpRequest request = new DMXControllerHttpRequest("testController");
        Map<String, Object> entity = new HashMap<String, Object>();
        entity.put("testData", 10);
        request.entity(new Map[]{entity});
        
        DMXHttpResponse<Map<String, Object>> response = DMXHttpClient.instance().invokeRemoteController(request);
        
        Assert.assertEquals("Invalid response status", HttpURLConnection.HTTP_OK, response.getStatusCode());
        
        Map<String, Object> out = response.getEntity();
        Assert.assertNotNull("testData is null", out.get("testData"));
        int outVal = (Integer)out.get("testData");
        Assert.assertEquals("Invalid vaue", 11, outVal);
    }
    
}
