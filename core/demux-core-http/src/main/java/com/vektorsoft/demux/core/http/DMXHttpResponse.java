/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

/**
 * Represents HTTP response with all needed information: status code, headers, content etc.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXHttpResponse<T> {
    
    /**
     * Response entity.
     */
    private T entity;
    
    /**
     * Response status code.
     */
    private int statusCode;
    
    /**
     * Response content type.
     */
    private String contentType;
    
    /**
     * Sets response entity.
     * 
     * @param entity response entity to set
     * @return  HTTP response
     */
    public DMXHttpResponse<T> entity(T entity){
        this.entity = entity;
        return this;
    }
    
    /**
     * Sets response status code.
     * 
     * @param code status code to set
     * @return  HTTP response
     */
    public DMXHttpResponse<T> statusCode(int code){
        this.statusCode = code;
        return this;
    }
    
    /**
     * Sets content type of the response.
     * 
     * @param type content type
     * @return  HTTP response
     */
    public DMXHttpResponse contentType(String type){
        this.contentType = type;
        return this;
    }

    /**
     * Returns status code
     * 
     * @return status 
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * Returns response entity.
     * 
     * @return entity
     */
    public T getEntity() {
        return entity;
    }

    /**
     * Returns response content type.
     * 
     * @return  content type
     */
    public String getContentType() {
        return contentType;
    }
    
}
