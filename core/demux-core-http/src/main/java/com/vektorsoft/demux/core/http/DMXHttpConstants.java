/**pu   
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

/**
 * Contains constants used with HTTP client.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXHttpConstants {
    
    /**
     * Hessian MIME type.
     */
    public static final String MIME_TYPE_HESSIAN = "application/x-hessian";
    
    /**
     * Name of <code>Set-Cookie</code> header.
     */
    public static final String HEADER_SET_COOKIE = "Set-Cookie";
    
    /**
     * Name of <code>Cookie</code> header.
     */
    public static final String HEADER_COOKIE = "Cookie";
    
    /**
     * Name of <code>Content-type</code> header.
     */
    public static final String HEADER_CONTENT_TYPE = "Content-type";
    
    /**
     * Private constructor.
     */
    private DMXHttpConstants(){
        // prevent instantiation
    }
}
