/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

import java.net.URL;
import java.util.Map;

/**
 * This class represents HTTP request with all needed information: URL, headers,
 * method, content etc.
 *
 * @param <T> type of request entity
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXHttpRequest<T> {

    /**
     * HTTP method of request.
     */
    private final DMXHttpClient.HTTPMethod method;

    /**
     * Request URL.
     */
    private final URL url;

    /**
     * Request path.
     */
    private final String path;

    /**
     * Map of request headers with their respective values.
     */
    private Map<String, String> headers;

    /**
     * Request entity.
     */
    private T entity;

    /**
     * Creates new request.
     *
     * @param method request method
     * @param url absolute URL
     */
    public DMXHttpRequest(DMXHttpClient.HTTPMethod method, URL url) {
        this.method = method;
        this.url = url;
        this.path = null;
    }

    /**
     * Creates new request.
     *
     * @param method request method
     * @param path request path (relative to base URL)
     */
    public DMXHttpRequest(DMXHttpClient.HTTPMethod method, String path) {
        this.method = method;
        this.path = path;
        this.url = null;
    }

    /**
     * Sets request entity.
     *
     * @param entity entity to set
     * @return HTTP request
     */
    public DMXHttpRequest entity(T entity) {
        this.entity = entity;
        return this;
    }

    /**
     * Returns request entity.
     *
     * @return entity
     */
    public T getEntity() {
        return entity;
    }

    /**
     * Returns request URL.
     *
     * @return rquest URL
     */
    public URL getUrl() {
        return url;
    }

    /**
     * Returns request path.
     *
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * Returns request method.
     *
     * @return method
     */
    public DMXHttpClient.HTTPMethod getMethod() {
        return method;
    }

}
