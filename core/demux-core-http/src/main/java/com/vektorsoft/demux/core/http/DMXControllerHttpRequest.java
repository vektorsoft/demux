/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

import java.util.Map;

/**
 * Special subclass of {@link DMXHttpRequest} used to remotely invoke 
 * controller with specified ID. It is assumed that controller is available at
 * path <code>/controllers/{controllerId}</code>.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXControllerHttpRequest extends DMXHttpRequest<Map[]> {
    
    /**
     * Prefix  to be preprended before controller ID.
     */
    private static final String PATH_PREFIX = "/controllers/";
    
    /**
     * ID of controller.
     */
    private final String controlelrId;
    

    /**
     * Creates new request.
     * 
     * @param controllerId ID of controller to be invoked
     */
    public DMXControllerHttpRequest(String controllerId){
        super(DMXHttpClient.HTTPMethod.POST, "");
        this.controlelrId = controllerId;
    }

    /**
     * Returns servlet path of controller.
     * 
     * @return path in form <code>/controllers/{controller ID}</code>
     */
    @Override
    public String getPath() {
        return PATH_PREFIX + controlelrId;
    }
    
    
    
}
