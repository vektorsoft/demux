/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.HessianFactory;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

/**
 * <p>
 * Simple HTTP client for use with DEMUX Framework applications. This client
 * provides basic HTTP operations while maintaining minimal footprint and
 * dependencies.
 * </p>
 * <p>
 * In current state, client can only send requests which invoke DEMUX controller
 * on predefined server.
 * </p>
 * <pre>
 *  <code>
 *      DMXHttpClient.instance().init("http://www.myserver.org:8080");
 *      DMXControllerHttpRequest request = new DMXControllerHttpRequest("ctrl"); // invoke controller with ID "ctrl"
 *       DMXHttpResponse<Map<String, Object>> response = DMXHttpClient.instance().invokeRemoteController(request);
 *  </code>
 * </pre>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXHttpClient {

    /**
     * Enumeration of HTTP methods.
     */
    public enum HTTPMethod {

        /**
         * HTTP GET method
         */
        GET,
        /**
         * HTTP POST method
         */
        POST,
        /**
         * HTTP PUT method
         */
        PUT,
        /**
         * HTTP DELETE method
         */
        DELETE,
        /**
         * HTTP HEAD method
         */
        HEAD,
        /**
         * HTTP OPTIONS method
         */
        OPTIONS
    };

    /**
     * Factory for Hessian serialization.
     */
    private static final HessianFactory HESSIAN_FACTORY = new HessianFactory();

    /**
     * Singleton instance of this class.
     */
    private static final DMXHttpClient INSTANCE;

    /**
     * Static initializer.
     */
    static {
        CookieHandler.setDefault(new CookieManager());
        INSTANCE = new DMXHttpClient();
    }

    /**
     * Returns singleton instance of this class.
     *
     * @return instance
     */
    public static DMXHttpClient instance() {
        return INSTANCE;
    }

    /**
     * Base URL for request URL calculation.
     */
    private String baseUrl;

    /**
     * Private constructor to prevent instantiation.
     */
    private DMXHttpClient() {
        baseUrl = null;
    }

    /**
     * Initializes the class with base URL. For example, this can be target
     * server address.
     *
     * @param baseUrl base URL
     */
    public void init(String baseUrl) {
        if (baseUrl.endsWith("/")) {
            this.baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
        } else {
            this.baseUrl = baseUrl;
        }
    }

    /**
     * Calculates absolute request URL based on specified content and
     * {@code baseUrl} value.
     *
     * @param request HTTP request
     * @return absolute URL for the request
     * @throws MalformedURLException if URL is malformed
     */
    private URL calculateRequestURL(DMXHttpRequest request) throws MalformedURLException {
        URL url = request.getUrl();
        if (url == null) {
            if (baseUrl != null) {
                String path = request.getPath();
                StringBuilder sb = new StringBuilder(baseUrl);
                if (path.startsWith("/")) {
                    sb.append(path);
                } else {
                    sb.append("/").append(path);
                }
                url = new URL(sb.toString());
            } else {
                throw new IllegalStateException("DMXHttpClient is not initialized. Please specify base URL.");
            }
        }
        return url;
    }

    /**
     * Performs invocation of remote controller specified in request.
     *
     * @param request HTTP request
     * @return controller response
     */
    public DMXHttpResponse<Map<String, Object>> invokeRemoteController(DMXControllerHttpRequest request) {
        DMXHttpResponse<Map<String, Object>> response = new DMXHttpResponse();
        try {
            URL url = calculateRequestURL(request);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(request.getMethod().name());
            connection.setRequestProperty(DMXHttpConstants.HEADER_CONTENT_TYPE, DMXHttpConstants.MIME_TYPE_HESSIAN);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            //write entities
            Map[] objects = request.getEntity();
            Hessian2Output hout = HESSIAN_FACTORY.createHessian2Output(connection.getOutputStream());
            hout.startMessage();
            hout.writeInt(objects.length);
            for (Map m : objects) {
                hout.writeObject(m);
            }
            hout.completeMessage();
            hout.close();

            connection.connect();

            // check response code
            int status = connection.getResponseCode();
            response.statusCode(status);
            response.contentType(connection.getContentType());
            String jsessionid = null;
            if (status == HttpURLConnection.HTTP_ACCEPTED) {
                jsessionid = connection.getHeaderField(DMXHttpConstants.HEADER_SET_COOKIE);
                connection.disconnect();
                return invokeRemoteController(request);
            }

            if (status == HttpURLConnection.HTTP_OK && DMXHttpConstants.MIME_TYPE_HESSIAN.equals(connection.getContentType())) {
                Hessian2Input hin = HESSIAN_FACTORY.createHessian2Input(connection.getInputStream());
                hin.startMessage();
                Map<String, Object> out = (Map<String, Object>) hin.readObject(Map.class);
                hin.completeMessage();
                hin.close();
                response.entity(out);
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        return response;
    }

}
