/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.core.http.ctrl;

import com.vektorsoft.demux.core.http.DMXControllerHttpRequest;
import com.vektorsoft.demux.core.http.DMXHttpClient;
import com.vektorsoft.demux.core.http.DMXHttpResponse;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;
import java.util.Map;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXRemoteInvocationController implements DMXController {
    
    private static final ThreadLocal<Map<String, Object>> LOCAL_MAP = new ThreadLocal<Map<String, Object>>();
    
    private final String[] ids;
    private final String controllerId;
    
    public DMXRemoteInvocationController(String controllerId, String... ids){
        this.controllerId = controllerId;
        this.ids = ids;
        
    }

    @Override
    public String getNextViewId() {
        return null;
    }

    @Override
    public String getControllerId() {
        return this.getClass().getName();
    }

   

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        try {
            DMXControllerHttpRequest request = new DMXControllerHttpRequest(controllerId);
            request.entity(new Map[]{model.get()});
            DMXHttpResponse<Map<String, Object>> response = DMXHttpClient.instance().invokeRemoteController(request);
//            LOCAL_MAP.set(response.getEntity());
            model.set(response.getEntity());
        } catch (Exception ex) {
            throw new ExecutionException(ex.getMessage());
        }

    }

    @Override
    public String[] getControllerData() {
        return ids;
    }

    @Override
    public boolean isDynamic() {
        return true;
    }
    
    
    
}
