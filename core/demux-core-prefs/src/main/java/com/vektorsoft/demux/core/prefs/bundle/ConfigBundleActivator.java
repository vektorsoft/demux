/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.prefs.bundle;

import com.vektorsoft.demux.core.app.DMXAbstractActivator;
import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.prefs.DMXBasicPreferenceService;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.prefs.PreferencesService;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ConfigBundleActivator extends DMXAbstractActivator {
    
    private PreferencesService prefService;
    private ServiceRegistration<PreferencesService> serviceReg;

    @Override
    protected void onBundleStart(BundleContext ctx) {
        prefService = new DMXBasicPreferenceService();
        serviceReg = ctx.registerService(PreferencesService.class, prefService, null);
    }

    @Override
    protected void onBundleStop(BundleContext ctx) {
        if(serviceReg != null){
            ServiceReference<PreferencesService> ref = serviceReg.getReference();
            if(ref != null){
                ctx.ungetService(ref);
            }
        }
    }

    @Override
    public DMXAdapterTrackerHandler getHandler() {
        return new DMXAdapterTrackerHandler() {

            @Override
            public void onAdapterAdded(DMXAdapter adapter) {
                
            }

            @Override
            public void onAdapterModified(DMXAdapter adapter) {
                
            }

            @Override
            public void onAdapterRemoved(DMXAdapter adapter) {
                
            }
        };
    }

}
