/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.prefs;

import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.ModelChangeListener;
import org.osgi.service.prefs.Preferences;
import org.osgi.service.prefs.PreferencesService;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXBasicPreferenceService implements PreferencesService {
    
    @Override
    public Preferences getSystemPreferences() {
        return new DMXPreference(java.util.prefs.Preferences.systemRoot());
    }

    @Override
    public Preferences getUserPreferences(String string) {
        return new DMXPreference(java.util.prefs.Preferences.userRoot());
    }

    @Override
    public String[] getUsers() {
        return new String[]{System.getProperty("user.name")};
    }

}
