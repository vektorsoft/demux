/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.core.prefs;

import com.vektorsoft.demux.core.config.DMXPreferenceListener;
import com.vektorsoft.demux.core.config.DMXPreferenceChangeSupport;
import com.vektorsoft.demux.core.config.DMXPreferenceEvent;
import java.util.ArrayList;
import java.util.List;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXPreference implements  Preferences, DMXPreferenceChangeSupport {
    
    private final java.util.prefs.Preferences basePrefs;
    
    private final List<DMXPreferenceListener> listeners;
    
    public DMXPreference(java.util.prefs.Preferences prefs){
        basePrefs = prefs;
        this.listeners = new ArrayList<DMXPreferenceListener>();
    }

    @Override
    public void put(String key, String value) {
        basePrefs.put(key, value);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, value));
    }

    @Override
    public String get(String key, String defaultValue) {
        return basePrefs.get(key, defaultValue);
    }

    @Override
    public void remove(String key) {
        basePrefs.remove(key);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, null));
    }

    @Override
    public void clear() throws BackingStoreException {
        try {
            basePrefs.clear();
            firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), "*", null));
        } catch (Exception ex){
            throw new BackingStoreException(ex.getMessage(), ex);
        }
        
    }

    @Override
    public void putInt(String key, int i) {
        basePrefs.putInt(key, i);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, i));
    }

    @Override
    public int getInt(String key, int defaultValue) {
        return basePrefs.getInt(key, defaultValue);
    }

    @Override
    public void putLong(String key, long l) {
        basePrefs.putLong(key, l);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, l));
    }

    @Override
    public long getLong(String key, long defaultValue) {
        return basePrefs.getLong(key, defaultValue);
    }

    @Override
    public void putBoolean(String key, boolean bln) {
        basePrefs.putBoolean(key, bln);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, bln));
    }

    @Override
    public boolean getBoolean(String key, boolean dflt) {
        return basePrefs.getBoolean(key, dflt);
    }

    @Override
    public void putFloat(String key, float f) {
        basePrefs.putFloat(key, f);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, f));
    }

    @Override
    public float getFloat(String key, float dflt) {
        return basePrefs.getFloat(key, dflt);
    }

    @Override
    public void putDouble(String key, double d) {
        basePrefs.putDouble(key, d);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, d));
    }

    @Override
    public double getDouble(String key, double dflt) {
        return basePrefs.getDouble(key, dflt);
    }

    @Override
    public void putByteArray(String key, byte[] bytes) {
        basePrefs.putByteArray(key, bytes);
        firePreferenceChanged(new DMXPreferenceEvent(basePrefs.name(), key, bytes));
    }

    @Override
    public byte[] getByteArray(String key, byte[] dflt) {
        return basePrefs.getByteArray(key, dflt);
    }

    @Override
    public String[] keys() throws BackingStoreException {
        try {
            return basePrefs.keys();
        } catch(Exception ex){
            throw new BackingStoreException(ex.getMessage(), ex);
        }
        
    }

    @Override
    public String[] childrenNames() throws BackingStoreException {
         try {
            return basePrefs.childrenNames();
        } catch(Exception ex){
            throw new BackingStoreException(ex.getMessage(), ex);
        }
    }

    @Override
    public Preferences parent() {
        if (basePrefs.parent() != null) {
            return new DMXPreference(basePrefs.parent());
        }
        return null;
    }

    @Override
    public Preferences node(String path) {
        return new DMXPreference(basePrefs.node(path));
    }

    @Override
    public boolean nodeExists(String path) throws BackingStoreException {
         try {
            return basePrefs.nodeExists(path);
        } catch(Exception ex){
            throw new BackingStoreException(ex.getMessage(), ex);
        }
    }

    @Override
    public void removeNode() throws BackingStoreException {
         try {
            basePrefs.removeNode();
        } catch(Exception ex){
            throw new BackingStoreException(ex.getMessage(), ex);
        }
    }

    @Override
    public String name() {
        return basePrefs.name();
    }

    @Override
    public String absolutePath() {
        return basePrefs.absolutePath();
    }

    @Override
    public void flush() throws BackingStoreException {
        try {
            basePrefs.flush();
        } catch (Exception ex) {
            throw new BackingStoreException(ex.getMessage(), ex);
        }
    }

    @Override
    public void sync() throws BackingStoreException {
        try {
            basePrefs.sync();
        } catch (Exception ex) {
            throw new BackingStoreException(ex.getMessage(), ex);
        }
    }

    @Override
    public void firePreferenceChanged(DMXPreferenceEvent event) {
        for(DMXPreferenceListener listener : listeners){
            listener.preferenceChanged(event);
        }
    }

    @Override
    public void addPreferenceListener(DMXPreferenceListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removePreferenceListener(DMXPreferenceListener listener) {
        listeners.remove(listener);
    }

}
