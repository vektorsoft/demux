/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.caucho.hessian.demux;

import java.io.Serializable;

/**
 * Test Object for serialization testing.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TestObject1 implements Serializable{
    
    private String name;
    private int intValue;
    private float flaotValue;

    public float getFlaotValue() {
        return flaotValue;
    }

    public String getName() {
        return name;
    }

    public int getIntValue() {
        return intValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public void setFlaotValue(float flaotValue) {
        this.flaotValue = flaotValue;
    }
    
    
}
