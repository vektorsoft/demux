/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.caucho.hessian.demux;

import com.caucho.hessian.io.Deflation;
import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.HessianFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Basic tests for Hessian serialization and deserialization.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class BasicSerializationTest {
    
    private TestObject1 obj1;
    private TestObject1 obj2;
    private HessianFactory factory;
    private File file;
    
    @Before
    public void setup(){
        factory = new HessianFactory();
        
        obj1 = new TestObject1();
        obj1.setName("name1");
        obj1.setIntValue(10);
        obj1.setFlaotValue(10.4f);
        
        obj2 = new TestObject1();
        obj2.setName("name2");
        obj2.setIntValue(20);
        obj2.setFlaotValue(20.4f);
        
        file = new File("object.ser");
    }
    
    @After
    public void tearDownClass(){
        if(file.exists()){
            file.delete();
        }
    }
    
    
    @Test
    public void simpleTest() throws Exception{
        
        Hessian2Output hout = new Hessian2Output(new FileOutputStream(file));
        hout.startMessage();
        hout.writeObject(obj1);
        hout.completeMessage();
        hout.close();
        
        // verify serialization
        Hessian2Input hin = new Hessian2Input(new FileInputStream(file));
        hin.startMessage();
        TestObject1 out = (TestObject1)hin.readObject(TestObject1.class);
        hin.completeMessage();
        hin.close();
        
        assertEquals("Name field mismatch", out.getName(), obj1.getName());
        assertEquals("intValue field mismatch", out.getIntValue(), obj1.getIntValue());
        assertEquals("floatValue field mismatch", out.getFlaotValue(), obj1.getFlaotValue(),0.1f);
        
    }
    
     @Test
    public void envelopeTest() throws Exception{
        
        
         Deflation deflater = new Deflation();
        Hessian2Output hout = factory.createHessian2Output(new FileOutputStream(file));
        hout = deflater.wrap(hout);
        hout.startMessage();
        hout.writeObject(obj1);
        hout.completeMessage();
        hout.close();
        
        // verify serialization
        Hessian2Input hin = factory.createHessian2Input(new FileInputStream(file));
        hin = deflater.unwrap(hin);
        hin.startMessage();
        TestObject1 out = (TestObject1)hin.readObject(TestObject1.class);
        hin.completeMessage();
        hin.close();
        
        assertEquals("Name field mismatch", out.getName(), obj1.getName());
        assertEquals("intValue field mismatch", out.getIntValue(), obj1.getIntValue());
        assertEquals("floatValue field mismatch", out.getFlaotValue(), obj1.getFlaotValue(),0.1f);
        
    }
    
    @Test
    public void mapSerializationTest() throws Exception{
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("testobject", obj1);
        map.put("testString", "This is serialized string");
        map.put("testBoolean", Boolean.TRUE);
        
        Hessian2Output hout = factory.createHessian2Output(new FileOutputStream(file));
        hout.startMessage();
        hout.writeObject(map);
        hout.completeMessage();
        hout.close();
        
         // verify serialization
        Hessian2Input hin = factory.createHessian2Input(new FileInputStream(file));
        hin.startMessage();
        Map<String,Object> out = (Map<String, Object>)hin.readObject();
        hin.completeMessage();
        hin.close();
        
        assertEquals("Invalid map size", 3, out.size());
        TestObject1 out1 = (TestObject1)out.get("testobject");
        assertEquals("Name field mismatch", out1.getName(), obj1.getName());
        assertEquals("intValue field mismatch", out1.getIntValue(), obj1.getIntValue());
        assertEquals("floatValue field mismatch", out1.getFlaotValue(), obj1.getFlaotValue(),0.1f);
        
        assertEquals("Invalid string value", map.get("testString").toString(), out.get("testString").toString());
        assertTrue("Invalid boolean value", (Boolean)out.get("testBoolean"));
    }
    
    @Test
    public void multipleMapsSerializationTest() throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("testobject", obj1);
        map.put("testString", "This is serialized string");
        map.put("testBoolean", Boolean.TRUE);
        
        Map<String, Object> map1 = new HashMap<String, Object>();
        map1.put("testobject2", obj2);
        map1.put("testString2", "This is second string");
        
         Hessian2Output hout = factory.createHessian2Output(new FileOutputStream(file));
        hout.startMessage();
        hout.writeInt(2);
        hout.writeObject(map);
        hout.writeObject(map1);
        hout.completeMessage();
        hout.close();
        
          // verify serialization
        Hessian2Input hin = factory.createHessian2Input(new FileInputStream(file));
        hin.startMessage();
        int length = hin.readInt();
        Map<String,Object> out = (Map<String, Object>)hin.readObject();
        Map<String,Object> outmap1 = (Map<String, Object>)hin.readObject();
        hin.completeMessage();
        hin.close();
        
        assertEquals("Invalid message length", 2, length);
         assertEquals("Invalid map size", 3, out.size());
        TestObject1 out1 = (TestObject1)out.get("testobject");
        assertEquals("Name field mismatch", out1.getName(), obj1.getName());
        assertEquals("intValue field mismatch", out1.getIntValue(), obj1.getIntValue());
        assertEquals("floatValue field mismatch", out1.getFlaotValue(), obj1.getFlaotValue(),0.1f);
        assertEquals("Invalid string value", map.get("testString").toString(), out.get("testString").toString());
        assertTrue("Invalid boolean value", (Boolean)out.get("testBoolean"));
        
         assertEquals("Invalid map size", 2, outmap1.size());
        TestObject1 out2 = (TestObject1)outmap1.get("testobject2");
        assertEquals("Name field mismatch", out2.getName(), obj2.getName());
        assertEquals("intValue field mismatch", out2.getIntValue(), obj2.getIntValue());
        assertEquals("floatValue field mismatch", out2.getFlaotValue(), obj2.getFlaotValue(),0.1f);
        
        assertEquals("Invalid string value", map1.get("testString2").toString(), outmap1.get("testString2").toString());
        
    }
    
}
