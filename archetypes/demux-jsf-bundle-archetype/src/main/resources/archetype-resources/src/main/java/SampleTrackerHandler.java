#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

package ${package};

import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;

/**
 * Simple implementation of {@code ServiceTracker} used to track changes to {@code DMXAdapter} service.
 * 
 */
public class SampleTrackerHandler extends DMXAdapterTrackerHandler {

    @Override
    public void onAdapterAdded(DMXAdapter adapter) {
       // add here logic needed to run when adapter is registered
       // you can use this to register controllers, views etc. Example:
       // adapter.registerController(MyController.class);
      
    }

    @Override
    public void onAdapterModified(DMXAdapter adapter) {
        // add here logic needed to run when adapter is modfied
    }

    @Override
    public void onAdapterRemoved(DMXAdapter adapter) {
        // add here logic needed to run when adapter is removed
    }

}
