#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package ${package};

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DefaultExtensionConfig;
import ${groupId}.extend.CallbackImpl;
import java.util.Properties;

/**
 *
 * @author Vladimir Djurovic
 */
public class BundleExtensionConfig extends DefaultExtensionConfig {

    @Override
    protected DMXExtensionCallback[] getCustomCallbacks() {
        //return new DMXExtensionCallback[]{new CallbackImpl()};
    }

    @Override
    protected Properties getCustomProperties() {
         //Properties props = new Properties();
        //props.put("sample.prop", "Overriding value");
        
        return props;
    }

    
    
}
