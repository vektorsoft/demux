#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )

package $package;
/**
 * This is mock class used only to force Java compiler to create correct output 
 * directory structure (ie., target/classes).
 *
 */
public class Foo {
    
}
