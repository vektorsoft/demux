/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension.extend;

import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import com.vektorsoft.demux.samples.extension.SimpleCallback;
import java.util.Random;

/**
 *
 * @author Vladimir Djurovic
 */
public class CallbackImpl extends SimpleCallback {
    
    private int value;
    
    private Random random = new Random();
//    private Map<String, Object> dataMap = new HashMap<String, Object>();

    @Override
    public void doRandom(DMXLocalModel model) {
        value = random.nextInt(10);
        model.set("randomVal", value);
        System.out.println("Invoking callback.. value = " + value);
    }

    @Override
    public String[] getCallbackData() {
        return new String[]{"randomVal"};
//        dataMap.put("randomVal", value);
//        return dataMap;
    }

    @Override
    public DMXResourceHandler getResourceHandler() {
        return null;
    }
    
}
