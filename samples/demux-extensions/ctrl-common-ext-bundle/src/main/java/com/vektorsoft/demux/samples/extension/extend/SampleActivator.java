 

package com.vektorsoft.demux.samples.extension.extend;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        CallbackImpl callback = new CallbackImpl();
        Dictionary dict = new Hashtable();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("randomVal", 1);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, dict);
        bc.registerService(DMXExtensionCallback.class.getName(), callback, dict);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }

}
