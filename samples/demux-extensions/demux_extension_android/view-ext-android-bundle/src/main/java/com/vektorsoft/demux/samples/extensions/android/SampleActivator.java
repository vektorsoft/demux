
package com.vektorsoft.demux.samples.extensions.android;

import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.mva.DMXView;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        
        AndroidExtendableView view = new AndroidExtendableView();
        ProxyRegistrationUtil.registerAnnotatedView(bc, view, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    

    
}
