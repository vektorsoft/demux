/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extensions.android;

import android.content.Context;
import android.view.View;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;


/**
 *
 * @author Vladimir Djurovic
 */
public abstract  class SimpleViewCallback implements DMXExtensionCallback {

    
    
    public View getSubviewRoot(Context context){
        return null;
    }


    @Override
    public String[] getMappings() {
        return new String[] {"extendableView"};
    }
    
    public void render(DMXLocalModel model){
        
    }


}
