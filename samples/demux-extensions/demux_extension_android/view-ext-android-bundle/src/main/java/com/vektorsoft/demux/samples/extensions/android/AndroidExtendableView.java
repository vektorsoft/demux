/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extensions.android;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.app.AndroidContextAware;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.android.core.resources.ResourceHandlerAware;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.mva.model.ModelDataFilter;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import com.vektorsoft.demux.eb.annotation.Filter;
import com.vektorsoft.demux.eb.event.DMXEventFilter;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (parentViewId = DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW , viewID = "extendableView", dataIds = {"status"})
public class AndroidExtendableView implements DMXExtendable, DMXAdapterAware, AndroidContextAware, ResourceHandlerAware {
    
//    @DMXModelData (id = "status")
//    private boolean status;
    
    private DMXAdapter adapter;
    private Context context;
    
    @DMXViewDeclaration.ViewUI
    private LinearLayout root;
    private LinearLayout hbox;
    private TextView helloView;
    private Button button;
    
    private SimpleViewCallback callback;
    private DMXAndroidResourceHandler resourceHandler;
    
//    public AndroidExtendableView(Context context, DMXAdapter adapter){
//        this.context = context;
//        this.adapter = adapter;
////        this.resourceHandler = adapter.getResourceManager().getResourceHandler(ResourceHandlerType.ANDROID);
//    }
    
//    @DMXViewDeclaration.Render
//    public void render(){
//        if(status){
//            helloView.setTextColor(Color.GREEN);
//        } else {
//            helloView.setTextColor(Color.RED);
//        }
//        // render extension
//        if(callback != null){
//            callback.render();
//        }
//    }

    @Override
    public void setExtensionCallback(DMXExtensionCallback callback) {
        this.callback = (SimpleViewCallback)callback;
        if(this.callback.getSubviewRoot(context) != null){
            root.addView(this.callback.getSubviewRoot(context));
        }
    }

//    @Override
//    public String getCallbackClassName() {
//        return SimpleViewCallback.class.getName();
//    }

    @Override
    public DMXExtensionCallback getExtensionCallback() {
        return callback;
    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
         root = new LinearLayout(context);
        root.setOrientation(LinearLayout.VERTICAL);
        
        //first row
        hbox = new LinearLayout(context);
        hbox.setOrientation(LinearLayout.HORIZONTAL);
        
        helloView = new TextView(context);
        helloView.setText(resourceHandler.getString("base_label"));
        hbox.addView(helloView);
        
        button = new Button(context);
        button.setText("Click me");
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                adapter.invokeController("extController", "param");
            }
        });
        hbox.addView(button);
        root.addView(hbox);
    }
    
    @EventHandler (topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelEvent(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        boolean status = info.getNewValues().get("status", Boolean.class);
         if(status){
            helloView.setTextColor(Color.GREEN);
        } else {
            helloView.setTextColor(Color.RED);
        }
          if(callback != null){
            callback.render(info.getNewValues());
        }
    }
    
    @Filter (topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public DMXEventFilter getFilter(){
        return  new ModelDataFilter("status");
    }

    public LinearLayout getRoot() {
        return root;
    }

    public void setRoot(LinearLayout root) {
        this.root = root;
    }


    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void setResourceHandler(DMXAndroidResourceHandler resourceHandler) {
        this.resourceHandler = resourceHandler;
    }
    
    
    
}
