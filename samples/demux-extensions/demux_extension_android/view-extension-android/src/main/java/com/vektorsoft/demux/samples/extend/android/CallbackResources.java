/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extend.android;

import java.util.ListResourceBundle;

/**
 *
 * @author Vladimir Djurovic
 */
public class CallbackResources extends ListResourceBundle {
    
    private final Object[][] contents = {
        {"base_label", "override_text"}
                                        };

    @Override
    protected Object[][] getContents() {
        return contents;
    }

}
