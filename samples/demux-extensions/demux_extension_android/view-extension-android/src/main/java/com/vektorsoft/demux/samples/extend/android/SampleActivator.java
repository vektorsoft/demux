

package com.vektorsoft.demux.samples.extend.android;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        AndroidViewExtension viewext = new AndroidViewExtension();
        bc.registerService(DMXExtensionCallback.class.getName(), viewext, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }

}
