/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extend.android;

import android.content.Context;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.samples.extensions.android.SimpleViewCallback;
import com.vektorsoft.demux.android.core.resources.ResourceHandlerAware;
import java.util.HashMap;
import java.util.Map;
import com.vektorsoft.demux.core.extension.DMXExtensionType;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;

/**
 *
 * @author Vladimir Djurovic
 */
public class AndroidViewExtension extends SimpleViewCallback implements  ResourceHandlerAware{
    
    private final String[] BUNDLES = {CallbackResources.class.getName()};

     private TextView txt;
     private Map<String, Object> dataMap = new HashMap<String, Object>();
     
     private DMXAndroidResourceHandler resourceHandler;

    @Override
    public TextView getSubviewRoot(Context context) {
        txt = new TextView(context);
        txt.setText("Extension: random number: " + 0);
        return txt;
    }

    @Override
    public void render(DMXLocalModel model) {
         int value = model.get("randomVal", Integer.class, 0);
        txt.setText("Extension: random number: " + value);
//        txt.setText("Extension: random number: " + value);
    }
    
    @Override
    public String[] getCallbackData() {
        return new String[]{"randomVal"};
    }

    @Override
    public DMXResourceHandler getResourceHandler() {
        return resourceHandler;
    }
    
     @Override
    public void setResourceHandler(DMXAndroidResourceHandler resourceHandler) {
        this.resourceHandler = resourceHandler;
    }
    
    @Override
    public DMXExtensionType getExtensionType() {
        return DMXExtensionType.VIEW_EXTENSION;
    }
}
