/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DMXExtensionType;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;



/**
 *
 * @author Vladimir Djurovic
 */
public abstract  class SimpleCallback implements DMXExtensionCallback {

    
    
    public void doRandom(DMXLocalModel model){
        
    }


    @Override
    public String[] getMappings() {
        return new String[]{"extController"};
    }

    @Override
    public DMXExtensionType getExtensionType() {
        return DMXExtensionType.CONTROLLER_EXTENSION;
    }

    

}
