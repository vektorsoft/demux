
package com.vektorsoft.demux.samples.extension;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        
         Dictionary<String, Object> dict = new Hashtable<String, Object>();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("status", true);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        ExtendableController ctrl = new ExtendableController();
        
        ProxyRegistrationUtil.registerAnnotatedController(bc, ctrl, dict);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    


}
