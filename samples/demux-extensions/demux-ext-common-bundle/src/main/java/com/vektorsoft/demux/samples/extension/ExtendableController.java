/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension;

import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXControllerDeclaration (mapping = "extController")
public class ExtendableController implements DMXExtendable {
    
    private SimpleCallback callback;

    @DMXControllerDeclaration.Exec
    public DMXLocalModel execute(@DMXModelData(id="status") boolean status,String param){
        boolean newStatus = !status;
        DMXLocalModel local = DMXLocalModel.instance().set("status", newStatus);
        if(callback != null){
            callback.doRandom(local);
        }
        return local;
    }

    @Override
    public void setExtensionCallback(DMXExtensionCallback callback) {
        this.callback = (SimpleCallback)callback;
    }


    @Override
    public DMXExtensionCallback getExtensionCallback() {
        return callback;
    }

}
