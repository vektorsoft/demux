
package com.vektorsoft.demux.samples.extension.jfx;

import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.loadResourceBundle("strings", this.getClass().getClassLoader());
       ExtendableView view  = new ExtendableView(handler);
        ProxyRegistrationUtil.registerAnnotatedView(bc, view, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    


}
