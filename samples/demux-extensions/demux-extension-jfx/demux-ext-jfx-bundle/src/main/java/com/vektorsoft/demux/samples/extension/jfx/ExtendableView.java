/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension.jfx;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import com.vektorsoft.demux.eb.DMXEventConstants;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (parentViewId = JFXRootView.JFX_ROOT_VIEW_ID, viewID = "extendableView", dataIds = {"status"})
public class ExtendableView implements DMXExtendable, DMXAdapterAware {
    
    
    private DMXAdapter adapter;
    
    @DMXViewDeclaration.ViewUI
    private VBox root;
    private HBox hbox;
    private Text label;
    private Button button;
    
    private SimpleViewCallback callback;
    
    private DefaultResourceHandler handler;
    
    public ExtendableView(DefaultResourceHandler handler){
    this.handler = handler;
    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
       root = new VBox();
       
       hbox = new HBox();
       root.getChildren().add(hbox);
       label = new Text("");
       label.setText(handler.getString("string.hello"));
       button = new Button("Change");
       hbox.getChildren().add(label);
       hbox.getChildren().add(button);
       hbox.setPadding(new Insets(5, 10, 15, 20));
        
       button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                adapter.invokeController("extController", "param");
            }
        });
    }
    
    @com.vektorsoft.demux.eb.annotation.EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void handleEvent(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        boolean status = info.getNewValues().get("status", Boolean.class);
         if(status){
            label.setFill(Color.GREEN);
        } else {
            label.setFill(Color.RED);
        }
          if(callback != null){
            callback.render(info.getNewValues());
        }
    }

    @Override
    public void setExtensionCallback(DMXExtensionCallback callback) {
        this.callback = (SimpleViewCallback)callback;
        if(this.callback.getSubviewRoot() != null){
            root.getChildren().add(this.callback.getSubviewRoot());
        }
        DMXResourceHandler extHandler = callback.getResourceHandler();
        if(extHandler != null){
            String s = extHandler.getString("string.hello");
            if(!s.equals("string.hello")){
                label.setText(s);
            }
        }
    }


    @Override
    public DMXExtensionCallback getExtensionCallback() {
        return callback;
    }

    public VBox getRoot(){
      return root;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
}
