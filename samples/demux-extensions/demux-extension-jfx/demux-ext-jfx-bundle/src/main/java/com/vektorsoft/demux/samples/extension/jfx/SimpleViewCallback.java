/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension.jfx;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import javafx.scene.Node;

/**
 *
 * @author Vladimir Djurovic
 */
public abstract  class SimpleViewCallback implements DMXExtensionCallback {

    
    
    public Node getSubviewRoot(){
        return null;
    }


    @Override
    public String[] getMappings() {
        return new String[] {"extendableView"};
    }
    
    public void render(DMXLocalModel model){
        
    }

}
