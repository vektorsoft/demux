

package com.vektorsoft.demux.samples.extension.jfx.ext;

import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.loadResourceBundle("extension", this.getClass().getClassLoader());
        ExtensionViewCallback callback = new ExtensionViewCallback(handler);
        bc.registerService(DMXExtensionCallback.class.getName(), callback, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }

}
