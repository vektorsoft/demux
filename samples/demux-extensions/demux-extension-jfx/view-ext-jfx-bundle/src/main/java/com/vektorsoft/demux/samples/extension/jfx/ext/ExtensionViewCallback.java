/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.extension.jfx.ext;


import com.vektorsoft.demux.core.extension.DMXExtensionType;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.samples.extension.jfx.SimpleViewCallback;
import java.util.HashMap;
import java.util.Map;
import javafx.scene.Node;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic
 */
public class ExtensionViewCallback extends SimpleViewCallback {
    
    private Text txt;
    
    private DefaultResourceHandler handler;
    
//    private int value;
    private Map<String, Object> dataMap = new HashMap<String, Object>();
    
    public ExtensionViewCallback(DefaultResourceHandler handler){
        this.handler = handler;
    }

    @Override
    public Node getSubviewRoot() {
        txt = new Text("Extension: random number: " + 0);
        return txt;
    }

    @Override
    public void render(DMXLocalModel model) {
        int value = model.get("randomVal", Integer.class, 0);
        txt.setText("Extension: random number: " + value);
    }

    @Override
    public String[] getCallbackData() {
        return new String[]{"randomVal"};
    }

    


    @Override
    public DMXExtensionType getExtensionType() {
        return DMXExtensionType.VIEW_EXTENSION;
    }

    @Override
    public DMXResourceHandler getResourceHandler() {
        return handler;
    }
    
     
    

}
