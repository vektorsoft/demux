/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.jfx.annotated;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.mva.model.ModelDataFilter;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.Filter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (viewID = "testView", parentViewId = JFXRootView.JFX_ROOT_VIEW_ID, dataIds = {"testData"})
public class AnnotatedView implements DMXAdapterAware{
    
    private DMXAdapter adapter;
    
    private final ModelDataFilter filter = new ModelDataFilter("testData");
    
    @DMXViewDeclaration.ViewUI
    private HBox hbox;
    private Text text;
    private Button button;
    
    
    @DMXViewDeclaration.ConstructUI
    public void createUI(){
         hbox = new HBox();
        text = new Text("Hello, DEMUX!");
        button = new Button("Click me");
        hbox.getChildren().add(text);
        hbox.getChildren().add(button);
        hbox.setPadding(new Insets(5, 10, 15, 20));
        
        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                adapter.invokeController("com.vektorsoft.demux.samples.annotated.HelloController");
            }
        });
    }
    
    
    public HBox getHbox(){
      return hbox;
    }
    
    @com.vektorsoft.demux.eb.annotation.EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelEvent(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        boolean testData = info.getNewValues().get("testData", Boolean.class, false);
        if(testData){
            text.setFill(Color.GREEN);
        } else {
            text.setFill(Color.RED);
        }
    }

    @Filter (topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public ModelDataFilter getFilter() {
        return filter;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

}
