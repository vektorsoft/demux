
package com.vektorsoft.demux.samples.annotated;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.mva.DMXController;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;


public class SampleActivator implements BundleActivator{

    private ServiceReference<DMXController> helloCtrlRef;
    
    @Override
    public void start(BundleContext bc) throws Exception {
        HelloController controller = new HelloController();
         Dictionary<String, Object> dict = new Hashtable<String, Object>();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("testData", true);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        ProxyRegistrationUtil.registerAnnotatedController(bc, controller, dict);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
         if(helloCtrlRef != null){
            bc.ungetService(helloCtrlRef);
        }
    }
    

   
}
