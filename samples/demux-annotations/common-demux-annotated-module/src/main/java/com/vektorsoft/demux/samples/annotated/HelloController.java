/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.annotated;

import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXControllerDeclaration
public class HelloController {

    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(HelloController.class);
    
    
    @DMXControllerDeclaration.Exec
    public DMXLocalModel exec(@DMXModelData(id = "testData") boolean testData){
        LOGGER.info("Running controller...");
        testData = !testData;
        return DMXLocalModel.instance().set("testData", testData);
    }
    
}
