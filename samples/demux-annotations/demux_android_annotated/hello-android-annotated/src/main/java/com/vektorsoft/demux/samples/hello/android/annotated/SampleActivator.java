
package com.vektorsoft.demux.samples.hello.android.annotated;


import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        AndroidAnnotatedView view = new AndroidAnnotatedView();
        ProxyRegistrationUtil.registerAnnotatedView(bc, view, null);

    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    


}
