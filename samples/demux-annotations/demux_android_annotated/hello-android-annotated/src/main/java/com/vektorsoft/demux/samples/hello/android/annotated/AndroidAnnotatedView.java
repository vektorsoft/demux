/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.hello.android.annotated;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.app.AndroidContextAware;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.mva.model.ModelDataFilter;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import com.vektorsoft.demux.eb.annotation.Filter;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (viewID = "annotatedView", parentViewId = DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW, dataIds = {"testData"})
public class AndroidAnnotatedView implements DMXAdapterAware, AndroidContextAware {
    
    @DMXViewDeclaration.ViewUI
    private LinearLayout root;
    private TextView text;
    private DMXAdapter adapter;
    
    private Context context;
    
    private final ModelDataFilter filter = new ModelDataFilter("testData");

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
    
    

    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
	root = new LinearLayout(context);
        text = new TextView(context);
        text.setText("Hello, DEMUX!");
        root.addView(text);

        Button button = new Button(context);
        button.setText("Click me");
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.invokeController("com.vektorsoft.demux.samples.annotated.HelloController");
            }
        });
        root.addView(button);
    }
    
    public LinearLayout getRoot(){
      return root;
    }
    

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
    
    @EventHandler (topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onLocaleChange(DMXModelEvent event){
        DMXModelEventInfo info = (DMXModelEventInfo)event.getData();
        
        boolean testData = info.getNewValues().get("testData", Boolean.class);
         if (testData) {
            text.setTextColor(Color.GREEN);
        } else {
            text.setTextColor(Color.RED);
        }
    }

    @Filter(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public ModelDataFilter getFilter() {
        return filter;
    }
    
    
}
