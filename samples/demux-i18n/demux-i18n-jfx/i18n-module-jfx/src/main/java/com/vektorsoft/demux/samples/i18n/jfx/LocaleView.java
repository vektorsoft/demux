/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.i18n.jfx;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.resources.DMXLocaleEventInfo;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import java.util.Date;
import java.util.Locale;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 *
 * @author Vladimir Djurovic
 */
public class LocaleView extends DMXAbstractView {
    
    private VBox vbox;
    private HBox hbox;
    private ComboBox<Locale> combo;
    private Button button;
    private Text text;
    private Text label;
    
    private DefaultResourceHandler handler;
    
    public LocaleView(DefaultResourceHandler resourceHandler){
        handler = resourceHandler;
    }
    
    @Override
    public void constructUI(){
        vbox = new VBox(10.0);
         hbox = new HBox(10.0);
        text = new Text(handler.getString("choose.locale.text"));
        hbox.getChildren().add(text);
        
        combo = new ComboBox<>();
        combo.setButtonCell(new LocaleCell());
        combo.setCellFactory(new Callback<ListView<Locale>, ListCell<Locale>>() {
             @Override public ListCell<Locale> call(ListView<Locale> p) {
                return new LocaleCell();
            }
           
        });
        hbox.getChildren().add(combo);
        
        button = new Button(handler.getString("localeButton.text"));
        button.setOnAction(new EventHandler<ActionEvent>() {

             @Override
             public void handle(ActionEvent t) {
                 Locale loc = combo.getValue();
                 adapter.invokeController(DMXCoreConstants.CTRL_LOCALE_CHANGE, loc);
             }
         });
        hbox.getChildren().add(button);
        vbox.getChildren().add(hbox);
        
        
        label = new Text(handler.formatMessage("local.label", new Date(), 100));
        vbox.getChildren().add(label);
    }

    @Override
    public String getParentViewId() {
        return JFXRootView.JFX_ROOT_VIEW_ID;
    }
    

    @Override
    public Object getViewUI() {
       
        return vbox;
    }

    @Override
    public void addChildView(DMXView child) {
        vbox.getChildren().add((Node)child.getViewUI());
    }
    
    

    @Override
    protected void localeInfoChanged(DMXLocaleEventInfo info) {
        handler.setCurrentLocale(info.getCurrentLocale());
        combo.setButtonCell(new LocaleCell());
        combo.getItems().clear();
        combo.getItems().addAll(info.getSupportedLocales());
        combo.getSelectionModel().select(info.getCurrentLocale());
        text.setText(handler.getString("choose.locale.text"));
        button.setText(handler.getString("localeButton.text"));
        label.setText(handler.formatMessage("local.label", new Date(), 100));
    }
    
    private class LocaleCell extends ListCell<Locale> {

        @Override
        protected void updateItem(Locale item, boolean empty) {
            super.updateItem(item, empty);
            if (item != null) {
                setText(item.getDisplayLanguage() + "(" + item.getDisplayCountry() + ")");
            }
        }

    }

}
