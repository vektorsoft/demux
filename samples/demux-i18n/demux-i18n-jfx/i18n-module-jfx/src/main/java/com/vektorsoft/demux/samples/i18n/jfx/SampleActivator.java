
package com.vektorsoft.demux.samples.i18n.jfx;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import java.util.Hashtable;
import java.util.Locale;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;


public class SampleActivator implements BundleActivator {
    
    private ServiceRegistration sreg;

    @Override
    public void start(BundleContext bc) throws Exception {
        // register supported locales
        Hashtable<String, Object> props = new Hashtable<>();
        props.put(DMXCoreConstants.PROP_LOCALE_ADDITIONAL, new Locale[]{new Locale("en","US"),new Locale("sr","RS")});
        bc.registerService(Locale.class.getName(), new Locale("es","ES"), props);
       
        
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.loadResourceBundle("bundles.strings", this.getClass().getClassLoader());
        // register locale view
        LocaleView localeView = new LocaleView(handler);
        sreg = bc.registerService(DMXView.class.getName(), localeView,null);
        
        FXMLocaleView fxmlView = new FXMLocaleView(handler);
        sreg = bc.registerService(DMXView.class.getName(), fxmlView,null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        if(sreg != null){
            bc.ungetService(sreg.getReference());
        }
    }
    

    

}
