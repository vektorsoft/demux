/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.i18n.jfx;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.resources.DMXLocaleEventInfo;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.jfx.gui.DMXLoaderFXML;
import java.io.IOException;
import javafx.scene.layout.BorderPane;

public class FXMLocaleView extends DMXAbstractView{

    @DMXViewDeclaration.ViewUI
    private BorderPane root;
    
    private DefaultResourceHandler handler;
    private DMXLoaderFXML loader;
    
    public FXMLocaleView(DefaultResourceHandler handler){
        this.handler = handler;
    }
    

    @Override
    public Object getViewUI() {
        return root;
    }

    @Override
    public void constructUI() {
         try{
            loader = new DMXLoaderFXML(this.getClass().getClassLoader().getResource("/fxmllocaleview.xml"), handler);
            root = (BorderPane)loader.load();
        } catch(IOException ex){
            ex.printStackTrace(System.err);
        }
    }

    @Override
    public String getParentViewId() {
        return LocaleView.class.getName();
    }

    @Override
    protected void localeInfoChanged(DMXLocaleEventInfo info) {
        handler.setCurrentLocale(info.getCurrentLocale());
        loader.updateNodeHierarchy(root);
    }
    
}
