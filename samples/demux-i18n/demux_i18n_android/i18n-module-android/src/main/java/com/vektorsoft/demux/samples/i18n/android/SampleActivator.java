
package com.vektorsoft.demux.samples.i18n.android;

import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXView;
import java.util.Hashtable;
import java.util.Locale;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
         // register supported locales
        Hashtable<String, Object> props = new Hashtable<String, Object>();
        props.put(DMXCoreConstants.PROP_LOCALE_ADDITIONAL, new Locale[]{new Locale("es","ES"),new Locale("sr","RS")});
        bc.registerService(Locale.class.getName(), new Locale("en","US"), props);
        
        
        AndroidLocaleView localeView = new AndroidLocaleView();
        bc.registerService(DMXView.class.getName(), localeView, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    

   

}
