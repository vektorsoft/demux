
package com.vektorsoft.demux.samples.i18n.android;

import com.vektorsoft.demux.core.app.DMXAdapterTrackerHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import java.util.Locale;

/**
 * Simple implementation of {@code ServiceTracker} used to track changes to {@code DMXAdapter} service.
 * 
 */
public class SampleTrackerHandler extends DMXAdapterTrackerHandler {

    @Override
    public void onAdapterAdded(DMXAdapter adapter) {
       // add here logic needed to run when adapter is registered
       // you can use this to register controllers, views etc. Example:
       // adapter.registerController(MyController.class);
       
//         adapter.getResourceManager().addSupportedLocale(new Locale("en","US"));
//        adapter.getResourceManager().addSupportedLocale(new Locale("es","ES"));
//        adapter.getResourceManager().addSupportedLocale(new Locale("sr","RS"));
//        adapter.getResourceManager().setCurrentLocale(new Locale("en","US"));
        
//         AndroidLocaleView view = new AndroidLocaleView(adapter);
//        adapter.registerView(view);
    }

    @Override
    public void onAdapterModified(DMXAdapter adapter) {
        // add here logic needed to run when adapter is modfied
    }

    @Override
    public void onAdapterRemoved(DMXAdapter adapter) {
        // add here logic needed to run when adapter is removed
    }

}
