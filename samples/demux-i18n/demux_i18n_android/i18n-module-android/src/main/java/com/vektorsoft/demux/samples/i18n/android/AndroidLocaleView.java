/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.i18n.android;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.app.DMXAndroidAbstractView;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.android.core.resources.ResourceHandlerAware;
import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.resources.DMXLocaleEventInfo;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Vladimir Djurovic
 */
public class AndroidLocaleView extends DMXAndroidAbstractView implements ResourceHandlerAware {
    
    private static final String LOG_TAG = "Android_locale_view";
    
    private LinearLayout root;
    
    private TextView text;
    private Spinner spinner;
    private Button button;
    private TextView label;
    
    private DMXAndroidResourceHandler handler;

    @Override
    public String getParentViewId() {
        return DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW;
    }


//    public void render() {
//        Log.d(LOG_TAG, "New locale: " + adapter.getResourceManager().getCurrentLocale());
//        // set selected item
//        ArrayAdapter<Locale> spinnerAdapter = (ArrayAdapter<Locale>)spinner.getAdapter();
//        int index = spinnerAdapter.getPosition(adapter.getResourceManager().getCurrentLocale());
//        spinner.setSelection(index);
//    }
//
//    public void updateFromModel(DMXLocalModel local) {
//        
//    }
    
    @Override
    public void constructUI(){
	Log.d(LOG_TAG, "Creating locale view...");
        root = new LinearLayout(context);
        root.setOrientation(LinearLayout.VERTICAL);
        
        LinearLayout top = new LinearLayout(context);
        top.setOrientation(LinearLayout.HORIZONTAL);
        
        Log.d(LOG_TAG, "choose.locale.text=" + handler.getString("choose_locale_text"));
        text = new TextView(context);
        text.setId(1);
        text.setText(handler.getString("choose_locale_text"));
        top.addView(text);
        
        spinner = new Spinner(context);
        spinner.setId(2);
        top.addView(spinner);
        
        button = new Button(context);
        button.setText(handler.getString("localeButton_text"));
        button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Log.d(LOG_TAG, "Handling button click");
                Locale loc = (Locale)spinner.getSelectedItem();
                Log.d(LOG_TAG, "Selected locale: " + loc);
                 adapter.invokeController(DMXCoreConstants.CTRL_LOCALE_CHANGE, loc);
            }
        });
        top.addView(button);
        
        label = new TextView(context);
        label.setText(handler.formatMessage("locale_label", new Date(), 100));
       
        
        root.addView(top);
        root.addView(label);
    }

    @Override
    public Object getViewUI() {
        Log.d(LOG_TAG, "Getting view UI: " + root.getChildCount());
        return root;
    }
    
    

    @Override
    protected void localeInfoChanged(DMXLocaleEventInfo info) {
        handler.setCurrentLocale(info.getCurrentLocale());
        ArrayAdapter<Locale> spinnerAdapter = new ArrayAdapter<Locale>(context, android.R.layout.simple_spinner_item, info.getSupportedLocales());
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
         int index = spinnerAdapter.getPosition(info.getCurrentLocale());
        spinner.setSelection(index);
        text.setText(handler.getString("choose_locale_text"));
        button.setText(handler.getString("localeButton_text"));
        label.setText(handler.formatMessage("locale_label", new Date(), 100));
    }

    @Override
    public void setResourceHandler(DMXAndroidResourceHandler resourceHandler){
        this.handler = resourceHandler;
    }
}
