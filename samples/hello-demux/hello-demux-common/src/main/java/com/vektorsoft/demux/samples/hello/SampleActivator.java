
package com.vektorsoft.demux.samples.hello;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXController;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class SampleActivator implements BundleActivator{
    
    private ServiceReference<DMXController> helloCtrlRef;

    @Override
    public void start(BundleContext bc) throws Exception {
        
        Dictionary<String, Object> dict = new Hashtable<String, Object>();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("testData", true);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        ServiceRegistration<DMXController> reg = bc.registerService(DMXController.class, new HelloController(), dict);
        helloCtrlRef = reg.getReference();
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        if(helloCtrlRef != null){
            bc.ungetService(helloCtrlRef);
        }
    }
    

    

}
