/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.hello;

import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
public class HelloController extends DMXAbstractController {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(HelloController.class);
    

    public HelloController(){
        super("testData");
        LOGGER.debug("Creating HelloController");
    }
    

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        boolean testData = model.get("testData", Boolean.class, true);
        testData = !testData;
        model.set("testData", testData);
        LOGGER.info("Controller execution finished");
        
    }

}
