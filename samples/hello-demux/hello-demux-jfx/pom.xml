<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.vektorsoft.demux.samples</groupId>
    <artifactId>hello-demux-jfx</artifactId>
    <version>0.20.0</version>
    <packaging>pom</packaging>

    <name>DEMUX Framework project</name>
    <url>http://demux.vektorsoft.com</url>

    <properties>
        <debug.port>2810</debug.port>
        <app.jfx>true</app.jfx>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <jfx.ant>${java.home}/../lib/ant-javafx.jar</jfx.ant>
        <maven.project.finalName>${project.build.finalName}</maven.project.finalName>
        <dist.dir.top>${project.build.directory}/dist</dist.dir.top>
        <dist.dir.app>${dist.dir.top}/${maven.project.finalName}</dist.dir.app>
        <dist.bundle.dir>${dist.dir.app}/bundles</dist.bundle.dir>
        <felix.cache.dir>${dist.dir.app}/felix-cache</felix.cache.dir>
        <config.dir>${basedir}/config</config.dir>
        <config.common.dir>${config.dir}/common</config.common.dir>
        <staging.dir>${project.build.directory}/staging</staging.dir>
        <staging.dir.win32>${staging.dir}/win32</staging.dir.win32>
        <staging.dir.win64>${staging.dir}/win64</staging.dir.win64>
        <staging.dir.linux32>${staging.dir}/linux32</staging.dir.linux32>
        <staging.dir.linux64>${staging.dir}/linux64</staging.dir.linux64>
        <staging.dir.macosx>${staging.dir}/macosx</staging.dir.macosx>
        <win.deploy.dir>${project.build.directory}/win</win.deploy.dir>
        <linux.deploy.dir>${project.build.directory}/linux</linux.deploy.dir>
        <macosx.deploy.dir>${project.build.directory}/macosx</macosx.deploy.dir>
    </properties>
    
    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.felix</groupId>
                    <artifactId>maven-bundle-plugin</artifactId>
                    <version>2.5.3</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-antrun-plugin</artifactId>
                    <version>1.7</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>2.7</version>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>exec-maven-plugin</artifactId>
                    <version>1.2.1</version>
                    <inherited>false</inherited>
                    <executions>
                        <execution>
                            <goals>
                                <goal>exec</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <executable>java</executable>
                        <arguments>
                            <argument>-Djava.util.logging.config.file=${basedir}/config/logging.properties</argument>
			    <argument>-Ddemux.clear.cache=true</argument>
                            <argument>-jar</argument>
                            <argument>demux-desktop-jfx.jar</argument>
                        </arguments>
                        <workingDirectory>${dist.dir.app}</workingDirectory>
                    </configuration>
                </plugin>
                
                <!-- Configuration for Maven Deploy plugin. This will create platform-specific installation package, but will
                     not upload anything to repository. This is just a placeholder for invoking correct packaging task during
                     deploy phase.
                -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.7</version>
                    <inherited>false</inherited>
                    <configuration>
                        <skip>true</skip>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <executions>
                    <execution>
                        <id>antrun-package</id>
                        <phase>package</phase>
                        <inherited>false</inherited>
                        <configuration>
                            <target>
                                <ant antfile="antrun-build.xml" target="init-dist" />
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>antrun-basedir</id>
                        <phase>process-resources</phase>
                        <inherited>false</inherited>
                        <configuration>
                            <target>
                                <!-- escape backslashes on Windows -->
                                <path id="basedir.path">
                                    <pathelement path="${basedir}" />
                                </path>
                                <pathconvert targetos="unix" property="basedir.unix" refid="basedir.path" />
                                <echo output="config.properties">root.dir=${basedir.unix}${line.separator}root.project.name=${project.build.finalName}</echo>
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>wrap-jars</id>
                        <phase>package</phase>
                        <inherited>false</inherited>
                        <configuration>
                            <target>
                                 <ant antfile="antrun-build.xml" target="wrap-jars" />
                            </target>
                        </configuration>
                        <goals>
                            <goal>run</goal>
                        </goals>
                    </execution>
                     <execution>
                        <id>do-deployment</id>
                        <phase>deploy</phase>
                        <inherited>false</inherited>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <ant antfile="antrun-build.xml" target="create-installers" />
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            
            <!-- Required Java 8 or higher -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
                <version>1.3.1</version>
                <executions>
                    <execution>
                        <id>enforce-java</id>
                        <goals>
                            <goal>enforce</goal>
                        </goals>
                        <configuration>
                            <rules>
                                <requireJavaVersion>
                                    <version>[1.8,)</version>
                                </requireJavaVersion>
                            </rules>    
                        </configuration>
                    </execution>
                </executions>
            </plugin>
                
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-osgi-deps</id>
                        <phase>prepare-package</phase>
                        <inherited>false</inherited>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${dist.bundle.dir}</outputDirectory>
                            <includeScope>provided</includeScope>
                            <excludeArtifactIds>demux-desktop-jfx,org.apache.felix.framework</excludeArtifactIds>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    
    <profiles>
        <profile>
            <!-- Debug profile (use it to run application in debug mode) -->
            <id>debug-profile</id>
            <activation>
                <property>
                    <name>debug</name>
                </property>
            </activation>
            <build>
                <pluginManagement>
                    <plugins>
                        <plugin>
                            <groupId>org.codehaus.mojo</groupId>
                            <artifactId>exec-maven-plugin</artifactId>
                            <version>1.2.1</version>
                            <inherited>false</inherited>
                            <executions>
                                <execution>
                                    <goals>
                                        <goal>exec</goal>
                                    </goals>
                                </execution>
                            </executions>
                            <configuration>
                                <executable>java</executable>
                                <arguments>
                                    <argument>-Djava.util.logging.config.file=${basedir}/config/logging.properties</argument>
				    <argument>-Ddemux.clear.cache=true</argument>
                                    <argument>-Xdebug</argument>
                                    <argument>-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=${debug.port}</argument>
                                    <argument>-jar</argument>
                                    <argument>demux-desktop-jfx.jar</argument>
                                </arguments>
                                <workingDirectory>${dist.dir.app}</workingDirectory>
                            </configuration>
                        </plugin>
                    </plugins>
                </pluginManagement>
            </build>
        </profile>
        
         <!-- Profile for creating Linux RPMs -->
        <profile>
            <id>rpm-package</id>
            <activation>
                <property>
                    <name>deploy.linux</name>
                </property>
            </activation>
            <properties>
            </properties>
            <dependencies>
                <!-- Dependencies for Linux launcher -->
                <dependency>
                    <groupId>com.vektorsoft.demux.native</groupId>
                    <artifactId>demux-launcher</artifactId>
                    <version>1.0.3</version>
                    <type>zip</type>
                    <classifier>linux32</classifier>
                    <scope>runtime</scope>
                </dependency>
                <dependency>
                    <groupId>com.vektorsoft.demux.native</groupId>
                    <artifactId>demux-launcher</artifactId>
                    <version>1.0.3</version>
                    <type>zip</type>
                    <classifier>linux64</classifier>
                    <scope>runtime</scope>
                </dependency>
            </dependencies>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-antrun-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>prep-deploy</id>
                                <phase>install</phase>
                                <inherited>false</inherited>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                                <configuration>
                                    <target>
                                        <ant antfile="antrun-build.xml" target="do-staging-linux" />
                                    </target>
                                </configuration>
                            </execution>
                            <execution>
                                <id>post-linux-package</id>
                                <inherited>false</inherited>
                                <phase>deploy</phase>
                                <goals>
                                    <goal>run</goal>
                                </goals>
                                <configuration>
                                    <target>
                                        <ant antfile="antrun-build.xml" target="move-linux-packages" />
                                    </target>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>rpm-maven-plugin</artifactId>
                        <version>2.1-alpha-2</version>
                        <executions>
                            <execution>
                                <id>rpm32-build</id>
                                <inherited>false</inherited>
                                <phase>install</phase>
                                <goals>
                                    <goal>rpm</goal>
                                </goals>
                                <configuration>
                                    <workarea>${staging.dir.linux32}/rpm</workarea>
                                    <group>Applications</group>
                                    <copyright>Apache</copyright>
                                    <needarch>x86</needarch>
                                    <mappings>
                                         <!--Add application directory generated during install--> 
                                        <mapping>
                                            <directory>/usr/local/${project.build.finalName}</directory>
                                            <filemode>755</filemode>
                                            <sources>
                                                <source>
                                                    <location>${dist.dir.app}</location>
                                                </source>
                                                <source>
                                                     <!--Add launcher binary--> 
                                                    <location>${staging.dir.linux32}/launcher</location>
                                                </source>
                                                <source>
                                                     <!--Add icon image--> 
                                                    <location>${staging.dir.linux32}/demux.png</location>
                                                </source>
                                            </sources>
                                        </mapping>
                                        <!--Add launcher.ini and logging.properties files with correct permissions. --> 
                                        <mapping>
                                            <directory>/usr/local/${project.build.finalName}</directory>
                                            <filemode>666</filemode>
                                            <sources>
                                                <source>
                                                     <!--Add launcher .ini file--> 
                                                    <location>${staging.dir.linux32}/launcher.ini</location>
                                                </source>
                                                <source>
                                                     <!--Add logging.properties file--> 
                                                    <location>${staging.dir.linux64}/logging.properties</location>
                                                </source>
                                            </sources>
                                        </mapping>
					<mapping>
					  <directory>/usr/local/${project.build.finalName}/libs</directory>
					  <filemode>755</filemode>
					  <sources>
					    <source>
						  <!--Add launcher libraries--> 
						<location>${staging.dir.linux32}/libs/libstdc++.so.6</location>
					    </source>
					  </sources>
					</mapping>
                                        <mapping>
                                            <directory>/usr/share/applications</directory>
                                            <filemode>755</filemode>
                                            <sources>
                                                <source>
                                                     <!--Add desktop entry--> 
                                                    <location>${staging.dir.linux32}/${project.artifactId}.desktop</location>
                                                </source>
                                            </sources>
                                        </mapping>
                                    </mappings>
                                </configuration>
                            </execution>
                             <execution>
                                <id>rpm64-build</id>
                                <inherited>false</inherited>
                                <phase>install</phase>
                                <goals>
                                    <goal>rpm</goal>
                                </goals>
                                <configuration>
                                    <workarea>${staging.dir.linux64}/rpm</workarea>
                                    <group>Applications</group>
                                    <copyright>Apache</copyright>
                                    <needarch>x86_64</needarch>
                                    <mappings>
                                         <!--Add application directory generated during install--> 
                                        <mapping>
                                            <directory>/usr/local/${project.build.finalName}</directory>
                                            <filemode>755</filemode>
                                            <sources>
                                                <source>
                                                    <location>${dist.dir.app}</location>
                                                </source>
                                                <source>
                                                     <!--Add launcher binary--> 
                                                    <location>${staging.dir.linux64}/launcher</location>
                                                </source>
                                                <source>
                                                     <!--Add icon image--> 
                                                    <location>${staging.dir.linux64}/demux.png</location>
                                                </source>
                                            </sources>
                                        </mapping>
                                         <!--Add launcher.ini and logging.properties files with correct permissions. --> 
                                        <mapping>
                                            <directory>/usr/local/${project.build.finalName}</directory>
                                            <filemode>666</filemode>
                                            <sources>
                                                <source>
                                                     <!--Add launcher .ini file--> 
                                                    <location>${staging.dir.linux64}/launcher.ini</location>
                                                </source>
                                                <source>
                                                     <!--Add logging.properties file--> 
                                                    <location>${staging.dir.linux64}/logging.properties</location>
                                                </source>
                                            </sources>
                                        </mapping>
					<mapping>
					  <directory>/usr/local/${project.build.finalName}/libs</directory>
					  <filemode>755</filemode>
					  <sources>
					    <source>
						  <!--Add launcher libraries--> 
						<location>${staging.dir.linux64}/libs/libstdc++.so.6</location>
					    </source>
					  </sources>
					</mapping>
                                        <mapping>
                                            <directory>/usr/share/applications</directory>
                                            <filemode>755</filemode>
                                            <sources>
                                                <source>
                                                     <!--Add desktop entry--> 
                                                    <location>${staging.dir.linux64}/${project.artifactId}.desktop</location>
                                                </source>
                                            </sources>
                                        </mapping>
                                    </mappings>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
        <profile>
            <!-- Profile used when creating windows installers -->
            <id>win-deploy-profile</id>
            <activation>
                <property>
                    <name>deploy.win</name>
                </property>
            </activation>
            <dependencies>
                <!-- Dependency for Windows native launcher. -->
                <dependency>
                    <groupId>com.vektorsoft.demux.native</groupId>
                    <artifactId>demux-launcher</artifactId>
                    <version>1.0.3</version>
                    <type>zip</type>
                    <classifier>win32</classifier>
                    <scope>runtime</scope>
                </dependency>
                <dependency>
                    <groupId>com.vektorsoft.demux.native</groupId>
                    <artifactId>demux-launcher</artifactId>
                    <version>1.0.3</version>
                    <type>zip</type>
                    <classifier>win64</classifier>
                    <scope>runtime</scope>
                </dependency>
            </dependencies>
        </profile>
        <profile>
            <!-- Profile used when creating windows installers -->
            <id>mac-deploy-profile</id>
            <activation>
                <property>
                    <name>deploy.osx</name>
                </property>
            </activation>
            <dependencies>
                <!-- Dependency for Windows native launcher. -->
                <dependency>
                    <groupId>com.vektorsoft.demux.native</groupId>
                    <artifactId>demux-launcher</artifactId>
                    <version>1.0.3</version>
                    <type>zip</type>
                    <classifier>macosx</classifier>
                    <scope>runtime</scope>
                </dependency>
            </dependencies>
        </profile>
    </profiles>
    
    <dependencies>
        <!-- Provided scope dependencies -->
        <dependency>
            <groupId>org.apache.felix</groupId>
            <artifactId>org.apache.felix.framework</artifactId>
            <version>4.4.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.vektorsoft.demux.core</groupId>
            <artifactId>demux-core</artifactId>
            <version>0.20.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.vektorsoft.demux.desktop</groupId>
            <artifactId>demux-jfx-core</artifactId>
            <version>0.20.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.vektorsoft.demux.desktop</groupId>
            <artifactId>demux-desktop-jfx</artifactId>
            <version>0.20.0</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.vektorsoft.demux.desktop</groupId>
            <artifactId>demux-desktop-common</artifactId>
            <version>0.20.0</version>
	    <scope>provided</scope>
        </dependency>
	<dependency>
            <groupId>com.vektorsoft.demux.core</groupId>
            <artifactId>demux-core-eventbus</artifactId>
            <version>0.20.0</version>
            <scope>provided</scope>
        </dependency>
	<dependency>
            <groupId>com.vektorsoft.demux.samples</groupId>
            <artifactId>hello-demux-common</artifactId>
            <version>0.20.0</version>
            <scope>provided</scope>
        </dependency>
        
        <!-- Test scope dependencies -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.8.1</version>
            <scope>test</scope>
        </dependency>
        <!-- bnd is used for wrapping non-OSGI jars -->
        <dependency>
            <groupId>biz.aQute</groupId>
            <artifactId>bnd</artifactId>
            <version>1.50.0</version>
        </dependency>
    </dependencies>

       <modules>
    <module>hello-demux-jfx-bundle</module>
  </modules>
  
</project>
