; NSIS installer script for DEMUX Framework

;---------------------------------------
; Include Modern UI
!include "MUI2.nsh"

;------------------------------------


;---------------------------------------------------
; General data

; The name of the installer
Name "${project.name}"
OutFile "${maven.project.finalName}${arch.suffix}.exe"


; The default installation directory
InstallDir $PROGRAMFILES\${maven.project.finalName}

; The text to prompt the user to enter a directory
DirText "This will install ${project.name} on your computer. Choose a directory"

;--------------------------------



;--------------------------------
;Variables

  Var StartMenuFolder

;---------------------------------

;------------------------------------------
; Interface settings

 
 !define MUI_ABORTWARNING
 !define MUI_WELCOMEPAGE_TITLE "Welcome to DEMUX Framework installation wizard"
 !define MUI_WELCOMEPAGE_TEXT "This will install ${project.name} on your computer."

 ; Start menu page configuration
 !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" ; registry key for Start menu folder
 !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\${project.artifactId}" 
 !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${project.name}"

 ;Finish page configuration
 !define MUI_FINISHPAGE_TEXT "Installation completed"
 /* Function finishpageaction
    CreateShortcut "$Desktop\Hello.lnk" "$INSTDIR\launcher.exe" "$INSTDIR\launcher.ico"
   FunctionEnd
   !define MUI_FINISHPAGE_SHOWREADME ""
   !define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
   !define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
   !define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction
   !define MUI_FINISHPAGE_TEXT "Installation completed"*/

;------------------------------------------

;------------------------------------------
; Installer pages

 !insertmacro MUI_PAGE_WELCOME
 !insertmacro MUI_PAGE_DIRECTORY
 !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
 !insertmacro MUI_PAGE_INSTFILES
 !insertmacro MUI_PAGE_FINISH
  

 ; Uninstaller pages
 !insertmacro MUI_UNPAGE_WELCOME
 !insertmacro MUI_UNPAGE_CONFIRM
 !insertmacro MUI_UNPAGE_INSTFILES
 !insertmacro MUI_UNPAGE_FINISH

;------------------------------------------

;-----------------------------------------------------------
; Installer section
Section "Install"

; Set output path to the installation directory.
SetOutPath $INSTDIR


; Put file there 
File /r	 ${dist.dir.app}\bundles
File /r	 ${dist.dir.app}\*.jar
File     ${current.staging.dir}\launcher.exe 
File     ${current.staging.dir}\demux.ico 
File     ${current.staging.dir}\*.dll
File     ${current.staging.dir}\logging.properties
File	 ${config.common.dir}\launcher.ini

; Tell the compiler to write an uninstaller and to look for a "Uninstall" section
WriteUninstaller $INSTDIR\Uninstall.exe

; Create Start menu items
 !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${project.name}.lnk" "$INSTDIR\launcher.exe"
    CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd ; end the section

Section "DesktopShortcut"

 CreateShortcut "$DESKTOP\${project.name}.lnk" "$INSTDIR\launcher.exe"
SectionEnd

; The uninstall section
Section "Uninstall"

RMDir /r $INSTDIR\bundles
Delete $INSTDIR\osgi.jar
Delete $INSTDIR\demux-desktop-jfx.jar
Delete $INSTDIR\launcher.ini
Delete $INSTDIR\launcher.exe
Delete $INSTDIR\launcher.ico
Delete $INSTDIR\*.dll
Delete $INSTDIR\logging.properties
Delete $INSTDIR\Uninstall.exe
RMDir $INSTDIR

; Remove Start menu items
 !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
    
  Delete "$SMPROGRAMS\$StartMenuFolder\Uninstall.lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"

 ;Remove Desktop shortcut
 Delete "$DESKTOP\${project.name}.lnk"
  
  DeleteRegKey /ifempty HKCU "Software\${project.artifactId}"

SectionEnd ; end the section
