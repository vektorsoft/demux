<?xml version="1.0" encoding="UTF-8"?>

<project name="hello-demux-jfx" basedir="." default="init-dist">
    
    <!-- property definitions for NSIS installer -->
    <property name="nsis.config.dir" value="${config.dir}/nsis" />
    
    <!-- property definitions for .DEB installer -->
    <property name="deb.config.dir" value="${config.dir}/deb" />
    
    <!-- property definitions for Mac OS X installer -->
    <property name="osx.config.dir" value="${config.dir}/mac" />
    <property name="mac.bundle.dir" value="${staging.dir.macosx}/${maven.project.finalName}.app" />
    <property name="mac.contents.dir" value="${mac.bundle.dir}/Contents" />
    <property name="mac.macos.dir" value="${mac.contents.dir}/MacOS" />
    <property name="mac.res.dir" value="${mac.contents.dir}/Resources" />
    
    <filterset begintoken="${" endtoken="}" id="maven-filters">
        <filter token="project.name" value="${project.name}" />
        <filter token="project.groupId" value="${project.groupId}" />
        <filter token="project.artifactId" value="${project.artifactId}" />
        <filter token="project.version" value="${project.version}" />
        <filter token="maven.project.finalName" value="${maven.project.finalName}" />
        <filter token="dist.dir.app" value="${dist.dir.app}" />
        <filter token="config.common.dir" value="${config.common.dir}" />
    </filterset>
    
    <target name="init-dist" description="Initialize distribution layout">
        <!-- create directory structure -->
        <mkdir dir="${dist.dir.app}" />
        <mkdir dir="${dist.bundle.dir}" />
        <!-- copy top-level dependencies -->
        <copy file="${org.apache.felix:org.apache.felix.framework:jar}" tofile="${dist.dir.app}/osgi.jar"/>
        <copy file="${com.vektorsoft.demux.desktop:demux-desktop-jfx:jar}" tofile="${dist.dir.app}/demux-desktop-jfx.jar" />
    </target>
    
    <target name="staging-init" description="Create staging file structure for deployment">
        <mkdir dir="${staging.dir}" />
        <antcall target="-staging-init-win" />
        <antcall target="-staging-init-linux" />
        <antcall target="-staging-init-macosx" />
    </target>
    
    <target name="-staging-init-win"  description="Create staging directory structure for windows installer" if="deploy.win">
        <mkdir dir="${staging.dir.win32}" />
        <mkdir dir="${staging.dir.win64}" />
    </target>
    
    <target name="-staging-init-linux" description="Create staging directory structure for Linux installers" if="deploy.linux">
        <mkdir dir="${staging.dir.linux32}" />
        <mkdir dir="${staging.dir.linux64}" />
    </target>
    
    <target name="-staging-init-macosx" description="Create staging directory structure for Mac OS X bundles" if="deploy.osx">
        <mkdir dir="${staging.dir.macosx}" />
        <mkdir dir="${mac.bundle.dir}" />
        <mkdir dir="${mac.contents.dir}" />
        <mkdir dir="${mac.macos.dir}" />
        <mkdir dir="${mac.res.dir}" />
    </target>
    
    <target name="do-staging-win" depends="staging-init" description="Staging phase for Windows installer" if="deploy.win">
        <!-- stage 32-bit installer -->
        <unzip src="${com.vektorsoft.demux.native:demux-launcher:zip:win32}" dest="${staging.dir.win32}" />
        <copy todir="${staging.dir.win32}" filtering="true" overwrite="true">
            <fileset dir="${nsis.config.dir}">
                <include name="*.*" />
            </fileset>
            <fileset dir="${config.dir}">
                <include name="logging.properties" />
            </fileset>
            <filterset refid="maven-filters"/>
            <filterset begintoken="${" endtoken="}">
                <filter token="current.staging.dir" value="${staging.dir.win32}" />
                <filter token="arch.suffix" value="-win32" />
            </filterset>
        </copy>
        <!-- stage 64-bit installer -->
        <unzip src="${com.vektorsoft.demux.native:demux-launcher:zip:win64}" dest="${staging.dir.win64}" />
        <copy todir="${staging.dir.win64}" filtering="true" overwrite="true">
            <fileset dir="${nsis.config.dir}">
                <include name="*.*" />
            </fileset>
            <fileset dir="${config.dir}">
                <include name="logging.properties" />
            </fileset>
            <filterset refid="maven-filters" />
            <filterset begintoken="${" endtoken="}">
                <filter token="current.staging.dir" value="${staging.dir.win64}" />
                <filter token="arch.suffix" value="-win64" />
            </filterset>
        </copy>
    </target>
    
     <target name="do-staging-linux" depends="staging-init" description="Staging phase for Linux installers" if="deploy.linux">
        <!-- stage 32-bit installer -->
        <unzip src="${com.vektorsoft.demux.native:demux-launcher:zip:linux32}" dest="${staging.dir.linux32}" />
        <copy todir="${staging.dir.linux32}" filtering="true" overwrite="true">
            <fileset dir="${config.common.dir}">
                <include name="*.*" />
            </fileset>
            <fileset dir="${deb.config.dir}">
                <include name="*" />
            </fileset>
            <fileset dir="${config.dir}">
                <include name="logging.properties" />
            </fileset>
            <filterset refid="maven-filters"/>
            <filterset begintoken="${" endtoken="}">
                <filter token="deb.arch" value="i386" />
            </filterset>
        </copy>
        <!-- stage 64-bit installer -->
        <unzip src="${com.vektorsoft.demux.native:demux-launcher:zip:linux64}" dest="${staging.dir.linux64}" />
        <copy todir="${staging.dir.linux64}" filtering="true" overwrite="true">
            <fileset dir="${config.common.dir}">
                <include name="*.*" />
            </fileset>
            <fileset dir="${deb.config.dir}">
                <include name="*" />
            </fileset>
            <fileset dir="${config.dir}">
                <include name="logging.properties" />
            </fileset>
            <filterset refid="maven-filters" />
            <filterset begintoken="${" endtoken="}">
                <filter token="deb.arch" value="amd64" />
            </filterset>
        </copy>
    </target>
    
    <target name="do-staging-osx" depends="staging-init" description="Staging phase for OS X app bundle" if="deploy.osx">
        <unzip src="${com.vektorsoft.demux.native:demux-launcher:zip:macosx}" dest="${staging.dir.macosx}" />
        <!-- Copy and process Info.plist file -->
        <copy todir="${staging.dir.macosx}" filtering="true" overwrite="true">
            <fileset dir="${osx.config.dir}">
                <include name="*" />
            </fileset>
            <fileset dir="${config.common.dir}">
                <include name="launcher.ini" />
            </fileset>
            <fileset dir="${config.dir}">
                <include name="logging.properties" />
            </fileset>
            <filterset refid="maven-filters" />
        </copy>
    </target>
    
     <target name="create-nsis-installer" depends="do-staging-win" description="Create NSIS installers for Windows (32 and 64 bit)" if="deploy.win">
        <!-- create 32-bit installer -->
        <exec executable="makensis" dir="${staging.dir.win32}">
            <arg value="setup.nsi" />
        </exec>
        <move todir="${win.deploy.dir}">
            <fileset dir="${staging.dir.win32}">
                <include name="${maven.project.finalName}*.exe" />
            </fileset>
        </move> 
        <!-- create 64-bit installer -->
        <exec executable="makensis" dir="${staging.dir.win64}">
            <arg value="setup.nsi" />
        </exec>
        <move todir="${win.deploy.dir}">
            <fileset dir="${staging.dir.win64}">
                <include name="${maven.project.finalName}*.exe" />
            </fileset>
        </move> 
    </target>
    
     <target name="create-linux-installers" depends="do-staging-linux" description="Create RPM and DEB packages for Linux" if="deploy.linux">
        <!-- 32-bit DEB package -->
        <antcall target="create-pkg-deb32" />
        <!-- 64-bit DEB package -->
        <antcall target="create-pkg-deb64" />
    </target>
    
     <target name="create-pkg-deb32" description="Create 32-bit DEB pacakge" if="deploy.linux">
        <property name="deb.basedir" value="${staging.dir.linux32}/${maven.project.finalName}-i386" />
        <property name="current.staging.dir" value="${staging.dir.linux32}" />
        <antcall target="create-deb-pkg" />
    </target>
    
     <target name="create-pkg-deb64" description="Create 64-bit DEB pacakge" if="deploy.linux">
        <property name="deb.basedir" value="${staging.dir.linux64}/${maven.project.finalName}-amd64" />
        <property name="current.staging.dir" value="${staging.dir.linux64}" />
        <antcall target="create-deb-pkg" />
    </target>
    
    <target name="create-osx-installer" depends="do-staging-osx" description="Create application bundle for OS X" if="deploy.osx">
        <!-- copy required resources to MacOS directory -->
        <copy todir="${mac.macos.dir}">
            <fileset dir="${dist.dir.app}">
                <include name="**/*.*"/>
            </fileset>
            <fileset dir="${staging.dir.macosx}">
                 <include name="launcher" />
                 <include name="launcher.ini" />
                 <include name="logging.properties" />
            </fileset>
        </copy>
        <!-- copy shared libs -->
	<copy todir="${mac.macos.dir}/libs">
	  <fileset dir="${staging.dir.macosx}/libs">
                 <include name="*.*" />
            </fileset>
	</copy>
         <!-- copy application icon -->
        <copy todir="${mac.res.dir}">
            <fileset dir="${staging.dir.macosx}">
                <include name="demux.icns"/>
            </fileset>
        </copy>
        <!-- copy Info.plist -->
        <copy todir="${mac.contents.dir}">
            <fileset dir="${staging.dir.macosx}">
                <include name="Info.plist" />
            </fileset>
        </copy>
        <!-- Zip bundle directory and move it to output dir. -->
        <zip destfile="${macosx.deploy.dir}/${maven.project.finalName}.zip" level="5" >
            <zipfileset dir="${staging.dir.macosx}" includes="${maven.project.finalName}.app/**/*" excludes="**/launcher" />
            <zipfileset dir="${staging.dir.macosx}" includes="${maven.project.finalName}.app/**/launcher" filemode="755"  />
        </zip>
    </target>
    
     <target name="create-installers" description="Create platform-specific installer">
        <antcall target="create-nsis-installer" />
        <antcall target="create-linux-installers" />
        <antcall target="create-osx-installer" />
    </target>
    
     <!-- create directory structure for Debian packages -->
    <target name="create-deb-dirs" description="Create directory structure for Debian packages.">
      <echo message="Creating .deb package structure" />
      <mkdir dir="${deb.basedir}/DEBIAN" />
      <mkdir dir="${deb.basedir}/usr/local" />
      <mkdir dir="${deb.basedir}/usr/share/applications" />
      <echo message="Completed." />
    </target>
    
    <!-- Craetes .deb package from directory structure. Note that this required 'dpkg-deb' tool to be installed -->
    <target name="create-deb-pkg" depends="create-deb-dirs" description="Creates .deb package">
        <echo message="Copying required files..." />
        <!-- First, copy required files to relevanr directories -->
        <!-- application structure goes to /usr/local -->
        <copy todir="${deb.basedir}/usr/local/${maven.project.finalName}">
            <fileset dir="${dist.dir.app}" />
        </copy>
        <!-- copy launcher files -->
        <copy todir="${deb.basedir}/usr/local/${maven.project.finalName}">
            <fileset dir="${current.staging.dir}/">
                <include name="launcher" />
                <include name="demux.png" />
                <include name="launcher.ini" />
                <include name="logging.properties" />
            </fileset>
        </copy>
        <!-- copy launcher lib files -->
        <copy todir="${deb.basedir}/usr/local/${maven.project.finalName}/libs">
            <fileset dir="${current.staging.dir}/libs">
                <include name="*.*" />
            </fileset>
        </copy>
        <!-- copy desktop shortcut file -->
        <copy  todir="${deb.basedir}/usr/share/applications">
            <fileset dir="${current.staging.dir}/">
                <include name="*.desktop" />
            </fileset>
        </copy>
        <!-- copy control files -->
	<copy file="${current.staging.dir}/control" todir="${deb.basedir}/DEBIAN"/>
        <copy file="${current.staging.dir}/postinst" todir="${deb.basedir}/DEBIAN"/>
        <!-- set executable permissions for postinst script -->
        <exec executable="chmod">
	  <arg value="755" />
	  <arg value="${deb.basedir}/DEBIAN/postinst" />
	 </exec>
        <echo message="Completed copying files." />
        <!-- execute 'dpkg-deb' to create .deb package -->
        <echo message="Creating .deb package..." />
        <exec executable="fakeroot">
            <arg value="dpkg-deb" />
            <arg value="--build" />
            <arg value="${deb.basedir}" />
        </exec>
        <echo message="Done." />
    </target>
    
     <target name="move-linux-packages" description="Move generated Linux packages to deploy directory">
        <move todir="${linux.deploy.dir}" overwrite="true">
            <fileset dir="${staging.dir.linux32}" >
                <include name="${maven.project.finalName}*.deb" />
            </fileset>
        </move>
        <move todir="${linux.deploy.dir}" overwrite="true">
            <fileset dir="${staging.dir.linux64}" >
                <include name="${maven.project.finalName}*.deb" />
            </fileset>
        </move>
        <move todir="${linux.deploy.dir}" overwrite="true">
            <fileset dir="${staging.dir.linux32}/rpm/${project.artifactId}/RPMS/x86" >
                <include name="${maven.project.finalName}*.rpm" />
            </fileset>
        </move>
        <move todir="${linux.deploy.dir}" overwrite="true">
            <fileset dir="${staging.dir.linux64}/rpm/${project.artifactId}/RPMS/x86_64" >
                <include name="${maven.project.finalName}*.rpm" />
            </fileset>
        </move>
    </target>

     <!-- Wrapping non-OSGI dependencies using bnd tool -->
     <taskdef resource="aQute/bnd/ant/taskdef.properties" classpath="${biz.aQute:bnd:jar}" />
    
    <scriptdef name="wrap-jars-task" language="javascript">
        <![CDATA[ 
            var bundleDir = new java.io.File(project.getProperty("dist.bundle.dir"));
            var toWrap = new org.apache.tools.ant.types.FileSet();
            toWrap.setProject(project);
            toWrap.setDir(bundleDir);
            
            var jars = bundleDir.listFiles();
            var skip = true;
            for (i = 0; i < jars.length; ++i) {
                // is this .jar file
                if(jars[i].getName().endsWith(".jar")){
                    // chcek if this is OSGI bundle
                    var curJar = new java.util.jar.JarFile(jars[i]);
                    var curManifest = curJar.getManifest();
                    var attributes = curManifest.getMainAttributes();
                    var isBundle = false;
                    if(attributes != null){
                        var it = attributes.entrySet().iterator();
                        while(it.hasNext() && !isBundle){
                            var key = it.next().getKey();
                            if("Bundle-SymbolicName".equals(key.toString())){
                                isBundle = true;
                            }
                        }
                        if(!isBundle){
                            toWrap.setIncludes(jars[i].getName());
                            skip = false;
                        }
                    }
                }
            }
            
            if(!skip){
                self.log("Wrapping non-OSGI dependencies...");
                var bnd = project.createTask("bndwrap");
                bnd.setOutput(bundleDir);
                bnd.addConfiguredFileSet(toWrap);
                bnd.execute();
            } else {
                self.log("Skipping dependency wrapping, everything is up-to-date");
            }
            
            
        ]]>
    </scriptdef>
    
    <target name="wrap-jars" description="Wrap non-OSGI jars">
        <wrap-jars-task />
        <move todir="${dist.bundle.dir}">
            <fileset dir="${dist.bundle.dir}" />
            <mapper type="glob" from="*.bar" to="*.jar"/>
        </move>
    </target>
    
</project>