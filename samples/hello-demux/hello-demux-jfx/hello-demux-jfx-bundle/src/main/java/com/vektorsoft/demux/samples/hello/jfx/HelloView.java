
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.hello.jfx;

import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic
 */
public class HelloView extends DMXAbstractView {
    
    private HBox hbox;
    private Text text;
    private Button button;
//    private boolean testData;
    
    int val = 5;
    
    
    
    public HelloView(){
	 super("testData");
    }

   

//    @Override
//    public void render() {
//        if(testData){
//            text.setFill(Color.GREEN);
//        } else {
//            text.setFill(Color.RED);
//        }
//    }

    @Override
    public String getParentViewId() {
        return JFXRootView.JFX_ROOT_VIEW_ID;
    }

//    @Override
//    public void updateFromModel(DMXLocalModel model) {
//        testData = (model.get("testData", Boolean.class));
//    }

    @Override
    public Object getViewUI() {
        return hbox;
    }

   

    @Override
    public void constructUI() {
        hbox = new HBox();
        text = new Text("Hello, DEMUX!");
        button = new Button("Click me");
        button.setId("clickButton");
        button.setOnAction((ActionEvent event) -> {
            if(adapter != null){
                adapter.invokeController("com.vektorsoft.demux.samples.hello.HelloController");
                
                int x = 10/val;
                val--;
            }
        });
        hbox.getChildren().add(text);
        hbox.getChildren().add(button);
        hbox.setPadding(new Insets(5, 10, 15, 20));
        
    }

    @Override
    protected void modelDataChanged(DMXLocalModel model) {
        boolean testData = (model.get("testData", Boolean.class));
        if(testData){
            text.setFill(Color.GREEN);
        } else {
            text.setFill(Color.RED);
        }
    }


}
