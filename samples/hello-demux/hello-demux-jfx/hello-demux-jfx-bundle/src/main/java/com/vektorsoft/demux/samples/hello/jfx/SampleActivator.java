
package com.vektorsoft.demux.samples.hello.jfx;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXView;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class SampleActivator implements BundleActivator {
    
    private ServiceReference<DMXView> viewRef;

    @Override
    public void start(BundleContext bc) throws Exception {
        Dictionary<String, Object> dict = new Hashtable<>();
        Map<String, Object> datamap = new HashMap<>();
        datamap.put("testData", true);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        
        ServiceRegistration<DMXView> sreg = bc.registerService(DMXView.class, new HelloView(), dict);
        if(sreg != null){
            viewRef = sreg.getReference();
        }
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        if(viewRef != null){
            bc.ungetService(viewRef);
        }
    }
    

    

}
