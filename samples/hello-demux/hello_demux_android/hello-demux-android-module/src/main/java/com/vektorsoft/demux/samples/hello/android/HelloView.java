/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.hello.android;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.app.DMXAndroidAbstractView;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
public class HelloView extends DMXAndroidAbstractView {
    
    private LinearLayout root;
    private TextView text;
    
    public HelloView(){
      super("testData");
      
    }
    

    @Override
    public String getParentViewId() {
        return DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW;
    }

    @Override
    public Object getViewUI() {
        return root;
    }

    @Override
    public void constructUI() {
        root = new LinearLayout(context);
        text = new TextView(context);
        text.setText("Hello, DEMUX!");
        root.addView(text);
        
        Button button = new Button(context);
        button.setText("Click me");
        button.setId(100);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                adapter.invokeController("com.vektorsoft.demux.samples.hello.HelloController");
            }
        });
        root.addView(button);
        
        
        Button button1 = new Button(context);
        button1.setText("Me too!");
        button1.setId(101);
        root.addView(button1);
    }

    @Override
    protected void modelDataChanged(DMXLocalModel model) {
         boolean testData = (model.get("testData", Boolean.class));
        if(testData){
            text.setTextColor(Color.GREEN);
        } else {
            text.setTextColor(Color.RED);
        }
    }
   
}
