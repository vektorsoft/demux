/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.task.android;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.vektorsoft.demux.android.core.app.AndroidContextAware;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.task.DMXBasicTask;
import com.vektorsoft.demux.core.task.GUIBlockingScope;

/**
 *
 * @author Vladimir Djurovic
 */
public class TaskView extends DMXAbstractView implements AndroidContextAware{
    
    private static final String LOG_TAG = "ANDROID_TASK_VIEW";
    
    private LinearLayout root;
    private ProgressBar progressBar;
    private Button startButton;
    private Button dlgButton;
    private Context context;
    
    private GUIBlockingScope scope;
    
    
    public TaskView(){
        super("taskProgress", "blockScope");
    }

    @Override
    public String getParentViewId() {
        return DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW;
    }

    @Override
    public void constructUI() {
        root = new LinearLayout(context);
        root.setOrientation(LinearLayout.VERTICAL);
        
        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setIndeterminate(false);
        progressBar.setMax(0);
        progressBar.setProgress(0);
        
        root.addView(progressBar);
        
        startButton = new Button(context);
        startButton.setText("Start");
        startButton.setId(1);
        
        startButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                DMXBasicTask task = new DMXBasicTask("com.vektorsoft.demux.samples.task.LongRunningController");
                task.setBlockScope(GUIBlockingScope.COMPONENTS);
                task.setIdsToBlock("1");
                task.setProgressAware(true);
                adapter.executeTask(task);
            }
        });
        
        root.addView(startButton);
        
        dlgButton = new Button(context);
        dlgButton.setText("Dialog");
        dlgButton.setId(3);
        dlgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                DMXBasicTask task = new DMXBasicTask("com.vektorsoft.demux.samples.task.LongRunningController");
                task.setProgressAware(true);
                task.setProgressViewId("dlgProgressView");
                adapter.executeTask(task);
            }
        });
        root.addView(dlgButton);
    }

    @Override
    protected void modelDataChanged(DMXLocalModel model) {
        if(model.containsData("taskProgress")){
            progressBar.setProgress(0);
            progressBar.setMax(100);
            progressBar.setProgress((int)(model.get("taskProgress", Float.class) * 100));
        }
    }


    public Object getViewUI() {
        Log.d(LOG_TAG, "Getting view UI: " + root);
        return root;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

}
