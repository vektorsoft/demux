/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.task.android;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (viewID = "dlgProgressView", parentViewId = "", isDialog = true, title = "Long running task",dataIds = {"taskProgress"})
public class DialogProgressView {

    @DMXModelData (id = "taskProgress")
    private float progress;
    
    private DMXDialog dialog;
    
    @DMXViewDeclaration.ViewUI
    private LinearLayout root;
    private ProgressBar progressBar;
    
    private Context context;
    
    public DialogProgressView(Context context){
        this.context = context;
    }
    
//    @DMXViewDeclaration.Render
//    public void renderView(){
//        progressBar.setProgress(0);
//        progressBar.setMax(100);
////        progressBar.setMax(100);
//        progressBar.setProgress((int)(progress * 100));
//    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
      root = new LinearLayout(context);
        root.setOrientation(LinearLayout.HORIZONTAL);
        
        progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setIndeterminate(false);
        progressBar.setMax(0);
        progressBar.setProgress(0);
        
        
        root.addView(progressBar);
    }
    
    public float getProgress(){
      return progress;
    }
    
    
    public void setProgress(float progress){
      this.progress = progress;
    }
    
    
    public LinearLayout getRoot(){
      return root;
    }
    
    public void setDialog(DMXDialog dlg){
      this.dialog = dlg;
    }
    
    
}
