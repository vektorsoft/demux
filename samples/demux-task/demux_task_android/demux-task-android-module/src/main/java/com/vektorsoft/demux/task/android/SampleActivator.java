
package com.vektorsoft.demux.task.android;

import com.vektorsoft.demux.core.mva.DMXView;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        TaskView view = new TaskView();
        bc.registerService(DMXView.class.getName(), view, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    

 

}
