
package com.vektorsoft.demux.samples.task.jfx;

import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.mva.DMXView;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        ProgressView progView = new ProgressView();
        bc.registerService(DMXView.class.getName(), progView, null);
        ProxyRegistrationUtil.registerAnnotatedView(bc, new DialogProgressView(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    

}
