/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.task.jfx;

import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.core.mva.model.ModelDataFilter;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import com.vektorsoft.demux.eb.annotation.Filter;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.HBox;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (viewID = "dlgProgressView", parentViewId = "", isDialog = true, title = "Long running task", dataIds = {"taskProgress"})
public class DialogProgressView {

//    @DMXModelData (id = "taskProgress")
//    private float progress;
    
    private DMXDialog dialog;
    
    @DMXViewDeclaration.ViewUI
    private HBox root;
    private ProgressBar progressBar;
    
    private ModelDataFilter filter = new ModelDataFilter("taskProgress");
    
    public DialogProgressView(){
       
    }
    
//    @DMXViewDeclaration.Render
//    public void renderView(){
//        progressBar.setProgress(progress);
//    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
         root = new HBox(20.0);
        
        progressBar = new ProgressBar(0f);
        root.getChildren().add(progressBar);
    }

    public DMXDialog getDialog() {
        return dialog;
    }

    public void setDialog(DMXDialog dialog) {
        this.dialog = dialog;
    }

    public HBox getRoot() {
        return root;
    }

    @EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelEvent(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        DMXLocalModel model = info.getNewValues();
        if(model.containsData("taskProgress")){
            progressBar.setProgress(model.get("taskProgress", Float.class));
        }
    }

    @Filter(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public ModelDataFilter getFilter() {
        return filter;
    }
    
    
}
