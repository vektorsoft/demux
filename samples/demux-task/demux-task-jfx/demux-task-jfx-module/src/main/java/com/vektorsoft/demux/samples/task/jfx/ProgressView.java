/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.task.jfx;

import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.task.DMXBasicTask;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Vladimir Djurovic
 */
public class ProgressView extends DMXAbstractView {
    
//    private float progress;
    private GUIBlockingScope scope;
    
    private VBox root;
    private HBox topView;
    private ProgressBar progressBar;
    private Button startButton;
    private Button dlgButton;
    
    private VBox radioButtonBox;
    private ToggleGroup group;
    private RadioButton noneRadioButton;
    private RadioButton buttonsRadioButton;
    private RadioButton allRadioButton;
    
    public ProgressView(){
        super("taskProgress","blockScope");
        
    }

    @Override
    public String getParentViewId() {
        return JFXRootView.JFX_ROOT_VIEW_ID;
    }

//    @Override
//    public void render() {
//        progressBar.setProgress(progress);
//        if(scope == GUIBlockingScope.NONE){
//            noneRadioButton.setSelected(true);
//        }
//         if(scope == GUIBlockingScope.COMPONENTS){
//            buttonsRadioButton.setSelected(true);
//        }
//          if(scope == GUIBlockingScope.TOP_LEVEL){
//            allRadioButton.setSelected(true);
//        }
//    }

//    @Override
//    public void updateFromModel(DMXLocalModel local) {
//        progress = local.get("taskProgress", Float.class, 0f);
//        GUIBlockingScope newScope = local.get("blockScope", GUIBlockingScope.class, null);
//        if(newScope != null){
//            scope = newScope;
//        }
//    }

    @Override
    protected void modelDataChanged(DMXLocalModel model) {
        if(model.containsData("taskProgress")){
            progressBar.setProgress(model.get("taskProgress", Float.class));
        }
        if(model.containsData("blockScope")){
            scope = model.get("blockScope", GUIBlockingScope.class);
        }
    }
    

    @Override
    public Object getViewUI() {
        return root;
    }

    @Override
    public void constructUI() {
        root = new VBox(5.0);
        
        Label description = new Label("Select blocking scope");
        root.getChildren().add(description);
        
        radioButtonBox = new VBox(10.0);
        group = new ToggleGroup();
        noneRadioButton = new RadioButton("None");
        noneRadioButton.setUserData(GUIBlockingScope.NONE);
        noneRadioButton.setToggleGroup(group);
        
        radioButtonBox.getChildren().add(noneRadioButton);
        
        buttonsRadioButton = new RadioButton("Buttons");
        buttonsRadioButton.setUserData(GUIBlockingScope.COMPONENTS);
        buttonsRadioButton.setToggleGroup(group);
       
        radioButtonBox.getChildren().add(buttonsRadioButton);
        
        allRadioButton = new RadioButton("Entire Window");
        allRadioButton.setUserData(GUIBlockingScope.TOP_LEVEL);
        allRadioButton.setToggleGroup(group);
       
        radioButtonBox.getChildren().add(allRadioButton);
        group.selectToggle(noneRadioButton);
        
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle t, Toggle t1) {
                adapter.invokeController("choiceController", group.getSelectedToggle().getUserData());
            }
        });
        
        root.getChildren().add(radioButtonBox);
        
        topView = new HBox(10);
        
        progressBar = new ProgressBar(0f);
        topView.getChildren().add(progressBar);
        
        
        HBox v1 = new HBox(10);
        startButton = new Button("Start");
        startButton.setId("startButton");
        startButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                DMXBasicTask task = new DMXBasicTask("com.vektorsoft.demux.samples.task.LongRunningController");
                System.out.println("Current blocking scope: " + scope);
                task.setBlockScope(scope);
                task.setIdsToBlock("startButton", "dlgButton");
                task.setProgressAware(true);
                ProgressView.this.adapter.executeTask(task);
            }
        });
        v1.getChildren().add(startButton);
        
        root.getChildren().addAll(topView, v1);
        
        dlgButton = new Button("Dialog");
        dlgButton.setId("dlgButton");
        dlgButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                 DMXBasicTask task = new DMXBasicTask("com.vektorsoft.demux.samples.task.LongRunningController");
                task.setProgressAware(true);
                task.setProgressViewId("dlgProgressView");
                ProgressView.this.adapter.executeTask(task);
            }
        });
        v1.getChildren().add(dlgButton);
    }
    
    

}
