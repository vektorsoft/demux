/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.task;

import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;
import com.vektorsoft.demux.core.task.GUIBlockingScope;

/**
 *
 * @author Vladimir Djurovic
 */
public class ScopeChoiceController extends DMXAbstractController {
    

    public ScopeChoiceController(){
        super("blockScope");
    }

    @Override
    public String getControllerId() {
        return "choiceController";
    }



    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        GUIBlockingScope scope = (GUIBlockingScope)params[0];
        System.out.println("Set block scope to " + scope);
        model.set("blockScope", scope);
    }

}
