
package com.vektorsoft.demux.samples.task;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        Dictionary dict = new Hashtable();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("taskProgress", 0f);
        datamap.put("blockScope", GUIBlockingScope.NONE);
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        
        bc.registerService(DMXController.class.getName(), new LongRunningController(), dict);
        bc.registerService(DMXController.class.getName(), new ScopeChoiceController(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    


}
