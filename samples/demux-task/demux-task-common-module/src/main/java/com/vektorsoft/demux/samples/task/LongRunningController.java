/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.task;

import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
public class LongRunningController extends DMXAbstractController {
    
    private float progress = 0f;
    
    public LongRunningController(){
        super("taskProgress");
    }


    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        // run for 10 seconds,  increasing progress each second
        progress = model.get("taskProgress", Float.class, 0.0f);
        
//        notifyDataChanged(model.get());
        for(int i = 0;i < 11;i++){
            System.out.println("progress=" + progress);
            try{
                Thread.sleep(1000);
            } catch(InterruptedException ex){
                throw new RuntimeException(ex);
            }
            progress += 0.1f;
            model.set("taskProgress", progress);
//            notifyDataChanged(model.get());
        }
    }

}
