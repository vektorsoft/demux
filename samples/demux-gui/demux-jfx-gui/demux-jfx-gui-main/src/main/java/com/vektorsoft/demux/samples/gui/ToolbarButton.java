

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.desktop.gui.DMXToolbarComponent;
import javafx.scene.control.Button;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class ToolbarButton implements DMXToolbarComponent<Button>{
    
    private Button button;
    
    public ToolbarButton(){
        button = new Button("B3");
        button.setId("newbutton");
    }

    @Override
    public String getParentId() {
        return "sampleToolbar";
    }

    @Override
    public String getComponentId() {
        return button.getId();
    }

    @Override
    public int preferredIndex() {
        return 0;
    }

    @Override
    public Button getComponent() {
        return button;
    }

}
