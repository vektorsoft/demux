

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.desktop.gui.DMXToolbarComponent;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.ToolBar;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleToolbar implements DMXToolbarComponent<ToolBar>{
    
    private ToolBar toolbar;
    
    public SampleToolbar(){
        this.toolbar = new ToolBar();
        toolbar.setId("sampleToolbar");
        
        Button b1 = new Button("B1");
        Separator sep = new Separator(Orientation.VERTICAL);
        Button b2 = new Button("B2");
        
        toolbar.getItems().addAll(b1, sep, b2);
    }

    @Override
    public String getParentId() {
        return null;
    }

    @Override
    public String getComponentId() {
        return toolbar.getId();
    }

    @Override
    public int preferredIndex() {
        return -1;
    }

    @Override
    public ToolBar getComponent() {
        return toolbar;
    }

}
