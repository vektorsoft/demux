

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleFileMenu implements DMXMenuComponent<Menu>, DMXAdapterAware{
    
    private Menu menu;
    private DMXAdapter adapter;
    
    public SampleFileMenu(){
        menu = new Menu("File");
        menu.setId("FileMenu");
        
        MenuItem newMenuItem = new MenuItem("New...");
        newMenuItem.setOnAction((ActionEvent event) -> {
            adapter.invokeController("simpleController");
        });
        menu.getItems().add(newMenuItem);
        
        MenuItem closeItem = new MenuItem("Close");
        menu.getItems().add(closeItem);
        
        MenuItem exitItem = new MenuItem("Exit");
        menu.getItems().add(exitItem);
    }

    @Override
    public String getParentId() {
        return null;
    }

    @Override
    public String getComponentId() {
        return "FileMenu";
    }

    @Override
    public Menu getComponent() {
        return menu;
    }

    @Override
    public int preferredIndex() {
        return 0;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

}
