

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleToolsMenu implements DMXMenuComponent<Menu>{
    
    private Menu menu;
    
    public SampleToolsMenu(){
        menu = new Menu("Tools");
        menu.setId("toolsMenu");
        
        MenuItem zoom = new MenuItem("Zoom");
        menu.getItems().add(zoom);
    }

    @Override
    public String getParentId() {
        return null;
    }

    @Override
    public String getComponentId() {
        return menu.getId();
    }

    @Override
    public Menu getComponent() {
        return menu;
    }

    @Override
    public int preferredIndex() {
        return 3;
    }

}
