

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleCutItem implements DMXMenuComponent<MenuItem>, DMXAdapterAware{
    
    private MenuItem item;
    private DMXAdapter adapter;
    
    public SampleCutItem(){
        item = new MenuItem("Cut");
        item.setId("cutItem");
        item.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                adapter.invokeController("simpleController");
            }
        });
    }

    @Override
    public String getParentId() {
        return "editMenu";
    }

    @Override
    public String getComponentId() {
        return item.getId();
    }

    @Override
    public MenuItem getComponent() {
        return item;
    }

    @Override
    public int preferredIndex() {
        return -1;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

}
