
package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import com.vektorsoft.demux.desktop.gui.DMXToolbarComponent;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleActivator;


public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        // register fxml controller
        FXMLController fxmlctrl = new FXMLController();
        bc.registerService(DMXAdapterAware.class, fxmlctrl, null);
        
        DefaultResourceHandler handler = new DefaultResourceHandler();
        handler.loadResourceBundle("bundles.strings", this.getClass().getClassLoader());
        FXMLView view = new FXMLView(handler, fxmlctrl);
        ProxyRegistrationUtil.registerAnnotatedView(bc, view, null);
        
        SimpleController controller = new SimpleController();
        bc.registerService(DMXController.class, controller, null);
        
        bc.registerService(DMXMenuComponent.class, new SampleViewSubmenu(), null);
        
        SampleFileMenu fileMenu = new SampleFileMenu();
        bc.registerService(DMXMenuComponent.class, fileMenu, null);
        
        SampleToolsMenu toolsMenu = new SampleToolsMenu();
        bc.registerService(DMXMenuComponent.class, toolsMenu, null);
        
        bc.registerService(DMXMenuComponent.class, new SampleEditMenu(), null);
        
        bc.registerService(DMXMenuComponent.class, new SampleCutItem(), null);
        
        bc.registerService(DMXToolbarComponent.class, new ToolbarButton(), null);
        bc.registerService(DMXToolbarComponent.class, new SampleToolbar(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    
    
}
