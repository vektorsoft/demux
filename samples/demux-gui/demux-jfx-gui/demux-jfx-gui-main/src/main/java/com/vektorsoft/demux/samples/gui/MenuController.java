/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
public class MenuController extends DMXAbstractController {

   
    @Override
    public String getControllerId() {
         return "menuController";
    }
    
    

    @Override
    public String getNextViewId() {
        return "dialogView";
    }

   

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        System.out.println("Executing menu controller");
    }

}
