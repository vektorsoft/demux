

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class FXMLController implements DMXAdapterAware {
    
    private DMXAdapter adapter;

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
    
    @FXML
    public void handleAction(ActionEvent event){
        adapter.invokeController("simpleController");
    }

}
