

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleViewSubmenu implements DMXMenuComponent<Menu>{
    
    private Menu menu;
    
    public SampleViewSubmenu(){
        menu = new Menu("View");
        menu.setId("viewSubmenu");
        
        MenuItem zoomIn = new MenuItem("Zoom in");
        menu.getItems().add(zoomIn);
        
        MenuItem zoomOut = new MenuItem("Zoom out");
        menu.getItems().add(zoomOut);
    }

    @Override
    public String getParentId() {
        return "toolsMenu";
    }

    @Override
    public String getComponentId() {
        return menu.getId();
    }

    @Override
    public Menu getComponent() {
        return menu;
    }

    @Override
    public int preferredIndex() {
        return 0;
    }

}
