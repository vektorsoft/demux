

package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.desktop.gui.DMXMenuComponent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class SampleEditMenu implements DMXMenuComponent<Menu>{
    
    private Menu menu;
    
    public SampleEditMenu(){
        menu = new Menu("Edit");
        menu.setId("editMenu");
        
        MenuItem undoItem = new MenuItem("Undo");
        menu.getItems().add(undoItem);
        
        MenuItem redoItem = new MenuItem("Redo");
        menu.getItems().add(redoItem);
    }

    @Override
    public String getParentId() {
        return null;
    }

    @Override
    public String getComponentId() {
        return menu.getId();
    }

    @Override
    public Menu getComponent() {
            return menu;
    }

    @Override
    public int preferredIndex() {
        return 1;
    }

}
