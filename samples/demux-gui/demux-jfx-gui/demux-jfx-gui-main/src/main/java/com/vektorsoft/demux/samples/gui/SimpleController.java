/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic
 */
public class SimpleController extends DMXAbstractController{

   
    @Override
    public String getControllerId() {
        return "simpleController";
    }
    
    

   

    @Override
    public void execute(DMXLocalModel local, Object... params) throws ExecutionException {
        System.out.println("Invoked simple controller");
    }

}
