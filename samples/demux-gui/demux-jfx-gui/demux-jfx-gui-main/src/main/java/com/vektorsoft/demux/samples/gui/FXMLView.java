/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.resources.impl.DMXResourceBundle;
import com.vektorsoft.demux.core.resources.impl.DefaultResourceHandler;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

/**
 * View which constructs it's UI from FXML file.
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (parentViewId = JFXRootView.JFX_ROOT_VIEW_ID, viewID = "fxmlView")
public class FXMLView {

    private DefaultResourceHandler handler;
    private FXMLController ctrl;
    
    @DMXViewDeclaration.ViewUI
    private BorderPane pane;
    
    public FXMLView(DefaultResourceHandler handler, FXMLController ctrl){
        this.handler = handler;
        this.ctrl = ctrl;
    }
    
    @DMXViewDeclaration.ConstructUI
    public void createUI(){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setController(ctrl);
            loader.setResources(new DMXResourceBundle(handler));
            pane = (BorderPane)loader.load(this.getClass().getClassLoader().getResourceAsStream("/fxmlview.xml"));
        } catch(IOException ex){
            ex.printStackTrace(System.err);
        }
        
    }
    

    public BorderPane getPane() {
        return pane;
    }
    
}
