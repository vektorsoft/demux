/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.vektorsoft.demux.samples.gui;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic
 */
@DMXViewDeclaration (isDialog = true, parentViewId = "", title = "Menu Dialog", viewID = "dialogView")
public class MenuDialogView {

    @DMXViewDeclaration.ViewUI
    private HBox root;
    
    private DMXDialog dialog;
    
    @DMXViewDeclaration.ConstructUI
    public void createUI(){
        root = new HBox();
        Text text = new Text("Menu dialog");
        root.getChildren().add(text);
    }
    
  

    public HBox getRoot() {
        return root;
    }
    
    public void setDialog(DMXDialog dialog){
      this.dialog = dialog;
    }
    
    public DMXDialog getDialog(){
      return dialog;
    }
    
}
