/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.dlg.jfx;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.desktop.jfx.gui.JFXEventHandler;
import javafx.event.ActionEvent;
import javafx.event.EventType;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CustomDialogHandler extends JFXEventHandler<ActionEvent> {
    
    public CustomDialogHandler(DMXAdapter adapter, EventType<ActionEvent> type){
        super(adapter, type);
        setComponentIds("customDlgButton");
    }

    @Override
    public void handle(ActionEvent event) {
        adapter.invokeController("customDialogController");
    }
    
}
