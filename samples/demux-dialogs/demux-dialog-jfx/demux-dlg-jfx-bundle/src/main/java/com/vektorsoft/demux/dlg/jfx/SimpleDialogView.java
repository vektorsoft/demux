/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.dlg.jfx;

import com.vektorsoft.demux.core.annotations.DMXDialogResult;
import com.vektorsoft.demux.core.annotations.DMXModelData;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.desktop.jfx.view.dlg.JFXDialogFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration (viewID = "simpleDialog", parentViewId = "", isDialog = true, title = "My Dialog", dataIds = {"username"})
public class SimpleDialogView implements DMXAdapterAware{
    
    @DMXViewDeclaration.ViewUI
    private HBox hbox;
    private TextField field;
    private Button button;
    
 
    private DMXDialog dialog;
    private DMXAdapter adapter;
    

    @DMXDialogResult
    public Object getResult() {
        return field.getText();
    }


    
    public void onModelChange(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        field.setText(info.getNewValues().get("username", String.class, ""));
    }
    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
      
       hbox = new HBox(10.0);
        
        hbox.getChildren().add(new Text("Enter your name: "));
        
        field = new TextField();
        hbox.getChildren().add(field);
        
        button = new Button("Submit");
        button.setId("customDlgButton");
        button.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                if(field.getText() == null || field.getText().isEmpty()){
                    JFXDialogFactory factory = adapter.getDialogFactory();
                    factory.createMessageDialog( "Error", "You must enter a name.", DMXDialog.OPTIONS_OK, DMXDialog.MessageType.MESSAGE_TYPE_ERROR).showDialog();
                    return;
                }
                dialog.closeDialog();
            }
        });
        hbox.getChildren().add(button);
    }
    
    public HBox getHbox(){
      return hbox;
    }

    
     public void setDialog(DMXDialog dlg){
      this.dialog = dlg;
    }
    
    public DMXDialog getDialog(){
      return dialog;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
    

}

