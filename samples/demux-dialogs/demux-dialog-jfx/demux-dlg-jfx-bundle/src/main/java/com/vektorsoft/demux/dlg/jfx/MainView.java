package com.vektorsoft.demux.dlg.jfx;

import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import static com.vektorsoft.demux.core.dlg.DMXDialog.MessageType.MESSAGE_TYPE_ERROR;
import static com.vektorsoft.demux.core.dlg.DMXDialog.MessageType.MESSAGE_TYPE_INFO;
import static com.vektorsoft.demux.core.dlg.DMXDialog.MessageType.MESSAGE_TYPE_PLAIN;
import static com.vektorsoft.demux.core.dlg.DMXDialog.MessageType.MESSAGE_TYPE_QUESTION;
import static com.vektorsoft.demux.core.dlg.DMXDialog.MessageType.MESSAGE_TYPE_WARNING;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.desktop.jfx.view.JFXRootView;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration(viewID = "mainView", parentViewId = JFXRootView.JFX_ROOT_VIEW_ID, dataIds = {"msgType", "username"})
public class MainView implements DMXAdapterAware{


    @DMXViewDeclaration.ViewUI
    private VBox vbox;
    private HBox level1;
    private HBox level2;
    private Button msgButton;

    private RadioButton rbPlain;
    private RadioButton rbInfo;
    private RadioButton rbWarning;
    private RadioButton rbError;
    private RadioButton rbQuestion;

    private Text usernameText;
    
    private DMXAdapter adapter;

    @DMXViewDeclaration.ConstructUI
    public void makeUI() {
        vbox = new VBox(5);

        level1 = new HBox(3);
        level1.setPadding(new Insets(2, 2, 2, 2));
        // create radio buttons
        VBox selection = new VBox();
        selection.getChildren().add(new Text("Select type of message to display"));
        ToggleGroup group = new ToggleGroup();

        rbPlain = new RadioButton("Plain");
        rbPlain.setUserData(DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
        rbPlain.setToggleGroup(group);
        rbPlain.setId("plainMsgRB");
        selection.getChildren().add(rbPlain);

        rbInfo = new RadioButton("Info");
        rbInfo.setUserData(DMXDialog.MessageType.MESSAGE_TYPE_INFO);
        rbInfo.setToggleGroup(group);
        rbInfo.setId("infoMsgRB");
        selection.getChildren().add(rbInfo);

        rbWarning = new RadioButton("Warning");
        rbWarning.setUserData(DMXDialog.MessageType.MESSAGE_TYPE_WARNING);
        rbWarning.setToggleGroup(group);
        rbWarning.setId("warningMsgRB");
        selection.getChildren().add(rbWarning);

        rbError = new RadioButton("Error");
        rbError.setUserData(DMXDialog.MessageType.MESSAGE_TYPE_ERROR);
        rbError.setToggleGroup(group);
        rbError.setId("errorMsgRB");
        selection.getChildren().add(rbError);

        rbQuestion = new RadioButton("Question");
        rbQuestion.setUserData(DMXDialog.MessageType.MESSAGE_TYPE_QUESTION);
        rbQuestion.setToggleGroup(group);
        rbQuestion.setId("questionMsgRB");
        selection.getChildren().add(rbQuestion);
        level1.getChildren().add(selection);

        msgButton = new Button("Show message...");
        msgButton.setId("messageButton");
        msgButton.setOnAction((ActionEvent event) -> {
            adapter.invokeController("com.vektorsoft.demux.samples.dlg.common.MessageController");
        });
        level1.getChildren().add(msgButton);
        
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                DMXDialog.MessageType type = (DMXDialog.MessageType)newValue.getUserData();
                adapter.invokeController(DMXCoreConstants.CTRL_PROPERTY_BIND, "msgType", type);
            }
        });

        level2 = new HBox(10);
        usernameText = new Text("Username: none");
        level2.getChildren().add(usernameText);
        Button customDlg = new Button("Set name");
        customDlg.setId("customDlgButton");
        customDlg.setOnAction((ActionEvent event) -> {
            adapter.invokeController("customDialogController");
        });
        level2.getChildren().add(customDlg);

        vbox.getChildren().add(level1);
        vbox.getChildren().add(level2);
    }


    public VBox getVbox() {
        return vbox;
    }

    @EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelChange(DMXModelEvent event) {
        DMXModelEventInfo info = event.getData();
        DMXDialog.MessageType type = info.getNewValues().get("msgType", DMXDialog.MessageType.class);
        if (type != null) {
            switch (type) {
                case MESSAGE_TYPE_PLAIN:
                    rbPlain.setSelected(true);
                    break;
                case MESSAGE_TYPE_ERROR:
                    rbError.setSelected(true);
                    break;
                case MESSAGE_TYPE_INFO:
                    rbInfo.setSelected(true);
                    break;
                case MESSAGE_TYPE_QUESTION:
                    rbQuestion.setSelected(true);
                    break;
                case MESSAGE_TYPE_WARNING:
                    rbWarning.setSelected(true);
                    break;
            }
        }
        String username = info.getNewValues().get("username", String.class, "");
        usernameText.setText("Username: " + username);
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

}
