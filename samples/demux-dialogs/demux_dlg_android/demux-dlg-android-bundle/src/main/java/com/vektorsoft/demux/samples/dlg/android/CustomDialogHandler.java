/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.samples.dlg.android;

import android.view.View;
import com.vektorsoft.demux.android.core.app.AndroidEventHandler;
import com.vektorsoft.demux.core.mva.DMXAdapter;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CustomDialogHandler extends AndroidEventHandler<View.OnClickListener> implements View.OnClickListener {
    
    public CustomDialogHandler(DMXAdapter adapter){
        super(adapter);
        addComponentId(300);
    }

    @Override
    public View.OnClickListener getEventHandler() {
        return this;
    }

    @Override
    public void onClick(View view) {
        adapter.invokeController("customDialogController");
    }
    
}
