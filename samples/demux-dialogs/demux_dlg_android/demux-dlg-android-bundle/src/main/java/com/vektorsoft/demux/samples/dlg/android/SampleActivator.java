
package com.vektorsoft.demux.samples.dlg.android;

import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;


public class SampleActivator implements BundleActivator {

    @Override
    public void start(BundleContext bc) throws Exception {
        ProxyRegistrationUtil.registerAnnotatedView(bc, new MainView(), null);
        ProxyRegistrationUtil.registerAnnotatedView(bc, new SimpleDialogView(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }

}
