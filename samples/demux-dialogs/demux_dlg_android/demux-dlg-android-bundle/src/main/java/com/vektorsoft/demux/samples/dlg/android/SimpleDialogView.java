/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.samples.dlg.android;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.vektorsoft.demux.core.annotations.DMXDialogResult;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;
import com.vektorsoft.demux.android.core.app.AndroidContextAware;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration (viewID = "simpleDialog", parentViewId = "", isDialog = true, title = "My Dialog", dataIds = {"username"})
public class SimpleDialogView implements DMXAdapterAware, AndroidContextAware{

   
     
     @DMXViewDeclaration.ViewUI
     private LinearLayout root;
     private EditText field;
     private Button submit;
     
     
     private Context context;
     private DMXDialog dialog;
     private DMXAdapter adapter;
     
    
   

    @DMXDialogResult
    public Object getResult() {
        return field.getText().toString();
    }

    
    @DMXViewDeclaration.ConstructUI
    public void makeUI(){
	 root = new LinearLayout(context);
       root.setOrientation(LinearLayout.HORIZONTAL);
       
        TextView label = new TextView(context);
        label.setText("Enter name:");
        root.addView(label);
        
        field = new EditText(context);
        root.addView(field);
        
        submit = new Button(context);
        submit.setText("Submit");
        submit.setOnClickListener(new View.OnClickListener() {

           @Override
           public void onClick(View view) {
               dialog.closeDialog();
           }
       });
        root.addView(submit);
    }
    
    @EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelEvent(DMXModelEvent event){
        DMXModelEventInfo info = event.getData();
        field.setText(info.getNewValues().get("username", String.class, ""));
    }
    
    public LinearLayout getRoot(){
      return root;
    }
    
    
    public void setDialog(DMXDialog dlg){
      this.dialog = dlg;
    }
    
    public DMXDialog getDialog(){
      return dialog;
    }
    
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }
}

