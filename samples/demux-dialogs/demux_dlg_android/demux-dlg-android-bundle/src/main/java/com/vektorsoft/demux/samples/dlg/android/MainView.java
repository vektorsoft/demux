/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.samples.dlg.android;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.app.AndroidContextAware;
import com.vektorsoft.demux.core.annotations.DMXViewDeclaration;
import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXModelEvent;
import com.vektorsoft.demux.core.mva.model.DMXModelEventInfo;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.annotation.EventHandler;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXViewDeclaration(viewID = "mainView", parentViewId = DMXAndroidViewManager.DMX_ANDROID_ROOT_VIEW, dataIds = {"username", "msgType"})
public class MainView implements DMXAdapterAware, AndroidContextAware {

    @DMXViewDeclaration.ViewUI
    private LinearLayout root;
    private LinearLayout level1;
    private LinearLayout level2;
    private LinearLayout selection;

    private Context context;
    private DMXAdapter adapter;

    private RadioButton rbPlain;
    private RadioButton rbInfo;
    private RadioButton rbWarning;
    private RadioButton rbError;
    private RadioButton rbQuestion;

    private Button msgButton;
    private TextView usernameText;
    private Button customDlgButton;

/
    @EventHandler(topic = DMXEventConstants.TOPIC_MODEL_CHANGE)
    public void onModelEvent(DMXModelEvent event) {
        DMXModelEventInfo info = event.getData();
        DMXDialog.MessageType type = info.getNewValues().get("msgType", DMXDialog.MessageType.class);
        if (type != null) {
            switch (type) {
                case MESSAGE_TYPE_PLAIN:
                    rbPlain.setSelected(true);
                    break;
                case MESSAGE_TYPE_ERROR:
                    rbError.setSelected(true);
                    break;
                case MESSAGE_TYPE_INFO:
                    rbInfo.setSelected(true);
                    break;
                case MESSAGE_TYPE_QUESTION:
                    rbQuestion.setSelected(true);
                    break;
                case MESSAGE_TYPE_WARNING:
                    rbWarning.setSelected(true);
                    break;
            }
        }
        String username = info.getNewValues().get("username", String.class, "");
        usernameText.setText("Username: " + username);
    }

    @DMXViewDeclaration.ConstructUI
    public void makeUI() {
        root = new LinearLayout(context);
        root.setOrientation(LinearLayout.VERTICAL);

        level1 = new LinearLayout(context);
        level1.setOrientation(LinearLayout.HORIZONTAL);

        selection = new LinearLayout(context);
        selection.setOrientation(LinearLayout.VERTICAL);

        TextView txt = new TextView(context);
        txt.setText("Select message type");
        selection.addView(txt);

        RadioGroup rgroup = new RadioGroup(context);
        rgroup.setOrientation(LinearLayout.VERTICAL);

        rbPlain = new RadioButton(context);
        rbPlain.setText("Plain");
        rbPlain.setId(100);
        rgroup.addView(rbPlain);

        rbInfo = new RadioButton(context);
        rbInfo.setText("Info");
        rbInfo.setId(101);
        rgroup.addView(rbInfo);

        rbWarning = new RadioButton(context);
        rbWarning.setText("Warning");
        rbWarning.setId(102);
        rgroup.addView(rbWarning);

        rbError = new RadioButton(context);
        rbError.setText("Error");
        rbError.setId(103);
        rgroup.addView(rbError);

        rbQuestion = new RadioButton(context);
        rbQuestion.setText("Question");
        rbQuestion.setId(104);
        rgroup.addView(rbQuestion);

        rgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup rg, int i) {
                DMXDialog.MessageType type = DMXDialog.MessageType.MESSAGE_TYPE_PLAIN;
                switch (i) {
                    case 101:
                        type = DMXDialog.MessageType.MESSAGE_TYPE_INFO;
                        break;
                    case 102:
                        type = DMXDialog.MessageType.MESSAGE_TYPE_WARNING;
                        break;
                    case 103:
                        type = DMXDialog.MessageType.MESSAGE_TYPE_ERROR;
                        break;
                    case 104:
                        type = DMXDialog.MessageType.MESSAGE_TYPE_QUESTION;
                        break;
                }
//        LOGGER.debug("Message type: " + type);
                adapter.invokeController(DMXCoreConstants.CTRL_PROPERTY_BIND, "msgType", type);
            }
        });

        selection.addView(rgroup);
        level1.addView(selection);

        msgButton = new Button(context);
        msgButton.setText("Show Message...");
        msgButton.setId(110);
        msgButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                adapter.invokeController("com.vektorsoft.demux.samples.dlg.common.MessageController");
            }
        });
        level1.addView(msgButton);

        level2 = new LinearLayout(context);
        level2.setOrientation(LinearLayout.HORIZONTAL);

        usernameText = new TextView(context);
        usernameText.setText("Username: none");
        level2.addView(usernameText);

        customDlgButton = new Button(context);
        customDlgButton.setText("Show dialog");
        customDlgButton.setId(300);
        level2.addView(customDlgButton);

        root.addView(level1);
        root.addView(level2);

    }

    public LinearLayout getRoot() {
        return root;
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

}
