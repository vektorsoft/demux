
package com.vektorsoft.demux.samples.dlg.common;

import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXAbstractController;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class MessageController extends DMXAbstractController implements DMXAdapterAware{
    
    private DMXAdapter adapter;
    
    public MessageController(){
        super("msgType");
    }

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        DMXDialog.MessageType type = model.get("msgType", DMXDialog.MessageType.class, DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
        DMXDialog dlg = null;
        switch(type){
            case MESSAGE_TYPE_PLAIN:
                dlg = adapter.getDialogFactory().createMessageDialog("Plain message", "Plain dalog message", DMXDialog.OPTIONS_OK);
                break;
            case MESSAGE_TYPE_ERROR:
                dlg = adapter.getDialogFactory().createMessageDialog("Error message", "Error dialog message", DMXDialog.OPTIONS_YES_NO, DMXDialog.MessageType.MESSAGE_TYPE_ERROR);
                break;
            case MESSAGE_TYPE_INFO:
                dlg = adapter.getDialogFactory().createMessageDialog("Information message", "Inforation dialog message", DMXDialog.OPTIONS_YES_NO_CANCEL, DMXDialog.MessageType.MESSAGE_TYPE_INFO);
                break;
            case MESSAGE_TYPE_QUESTION:
                dlg = adapter.getDialogFactory().createMessageDialog("Question message", "Question dialog message", DMXDialog.OPTIONS_OK_CANCEL, DMXDialog.MessageType.MESSAGE_TYPE_QUESTION);
                break;
            case MESSAGE_TYPE_WARNING:
                dlg = adapter.getDialogFactory().createMessageDialog("Warning message", "Warning dialog message", DMXDialog.OPTIONS_YES_NO, DMXDialog.MessageType.MESSAGE_TYPE_WARNING);
                break;
        }

        int result = dlg.showDialog();
        System.out.println("Dialog result: " + result);
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
    
}
