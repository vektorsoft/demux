/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vektorsoft.demux.samples.dlg.common;

import com.vektorsoft.demux.core.annotations.DMXControllerDeclaration;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXAdapterAware;
import com.vektorsoft.demux.core.mva.model.DMXLocalModel;


/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@DMXControllerDeclaration (mapping = "customDialogController")
public class CustomDialogController implements DMXAdapterAware{
    
    private DMXAdapter adapter;
    
    
    
    
    @DMXControllerDeclaration.Exec
    public DMXLocalModel execute(){
        DMXDialog dlg = adapter.getDialogFactory().createDialog("simpleDialog", "Custom title");
        dlg.showDialog();
        String username = (String)dlg.getDialogView().getResult();
        System.out.println("username: " + username);
        
        return DMXLocalModel.instance().set("username", username);
    }

    @Override
    public void setAdapter(DMXAdapter adapter) {
        this.adapter = adapter;
    }
    
}
