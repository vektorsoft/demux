
package com.vektorsoft.demux.samples.dlg.common;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.app.ProxyRegistrationUtil;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.DMXController;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 *
 * An implementation of {@code com.vektorsoft.demux.core.app.DMXAbstractActivator} class. This class contains logic needed
 * for supporting extension callbacks. Use this class as a starting point for you bundle activators.
 *
 * This class uses {@code SampleTrackerHandler} to track registration of {@code DMXAdapter} service.
 * 
 */
public class SampleActivator implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        Dictionary dict = new Hashtable();
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("msgType", DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
        datamap.put("username", "");
        dict.put(DMXCoreConstants.PROP_CTRL_DATA, datamap);
        bc.registerService(DMXController.class.getName(), new MessageController(), dict);
        ProxyRegistrationUtil.registerAnnotatedController(bc, new CustomDialogController(), null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }
    

   

}
