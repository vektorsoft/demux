/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.web.jsf.core.internal.ViewResourceUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.junit.AfterClass;
import org.junit.Test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import static org.mockito.Mockito.*;

/**
 * Test cases for class {@link JSFViewManager}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JSFViewManagerTest {
    
    private static final DocumentBuilderFactory DOC_FACTORY = DocumentBuilderFactory.newInstance();
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
    
    private static File dmxDir;
    
    private JSFViewManager viewManager;
    private DMXView mainView;
    
    
    @BeforeClass
    public static void setupClass(){
        dmxDir = new File("dmx-views");
        dmxDir.mkdir();
    }
    
    @AfterClass
    public static void teardownClass(){
        File[] files = dmxDir.listFiles();
        for(File f : files){
            f.delete();
        }
        dmxDir.delete();
    }
    
    
    @Before
    public void setup() throws Exception{
        viewManager = new JSFViewManager();
        
        ServletContext sc = mock(ServletContext.class);
        when(sc.getRealPath(JSFConstants.DMX_VIEW_PATH)).thenReturn(dmxDir.getAbsolutePath());
        viewManager.setContext(sc);
        
        mainView = new MockJSFView("mainView", "/pages/main.xhtml");
    }
    
    /**
     * Verifies correct registration of view without parent.
     */
    @Test
    public void registerViewNoParent(){
         viewManager.registerView(mainView);
         
         URL out = ViewResourceUtils.getViewResourceURL(mainView.getViewId());
         assertNotNull("Invalid resource contents", out);
         assertTrue("Invalid URL patter", out.toString().endsWith("/dmx-views/" + mainView.getViewId() + JSFConstants.DMX_VIEW_EXTENSION));
    }
    
    /**
     * <p>
     *  Verifies correct registration of JSF view. If view has a parent, parent's XHTML should be 
     * changed to include child view at defined point.
     * </p>
     * 
     */
    @Test
    public void testViewRegistrationWithParent() throws Exception{
        DMXView childView = new MockChildView("childView", "/pages/child.xhtml");
         
//         JSFViewManager manager = new JSFViewManager();
         viewManager.registerView(mainView);
         URL out = ViewResourceUtils.getViewResourceURL("/dmx/" + mainView.getViewId());
         assertNotNull("Invalid resource contents", out);
         assertTrue("Invalid URL patter", out.toString().endsWith("/dmx-views/" + mainView.getViewId() + JSFConstants.DMX_VIEW_EXTENSION));
         
         //read original parent contents
         BufferedReader reader = new BufferedReader(new InputStreamReader(out.openStream()));
         StringBuilder sb = new StringBuilder();
         String line = null;
         while((line = reader.readLine()) != null){
             sb.append(line).append("\n");
         }
         reader.close();
         
         viewManager.registerView(childView);
         
         out = ViewResourceUtils.getViewResourceURL("/dmx/" + mainView.getViewId());
         assertNotNull("Expected URL is null", out);
         assertTrue("Invalid parent view URL pattern", out.toString().startsWith("file:") && out.toString().contains("dmx-"));
         // check contents
         reader = new BufferedReader(new InputStreamReader(out.openStream()));
         StringBuilder sb1 = new StringBuilder();
         line = null;
         while((line = reader.readLine()) != null){
             sb1.append(line).append("\n");
         }
         reader.close();
         
         assertFalse("outputs are equal", sb.toString().equals(sb1.toString()));
         //verify that paarent contains child view node
         Document parentDoc = DOC_FACTORY.newDocumentBuilder().parse(new InputSource(new StringReader(sb1.toString())));
         XPath xpath = XPATH_FACTORY.newXPath();
         Node element = (Node) xpath.evaluate("//*[@viewId='childView']", parentDoc, XPathConstants.NODE);
         assertNotNull("XPath element is null", element);
         assertEquals("Invalid XML element", "dmx:view", element.getNodeName());
         
    }
    
    /**
     * Verifies correct view verification when child view is registered before parent.
     */
    @Test
    public void registerChildViewBeforeParent() throws Exception{
        DMXView childView = new MockChildView("childView", "/pages/child.xhtml");
//        JSFViewManager manager = new JSFViewManager();
        viewManager.registerView(childView);
        
         
         viewManager.registerView(mainView);
         //verify that child view was added
         URL out = ViewResourceUtils.getViewResourceURL("/dmx/" + mainView.getViewId());
         assertNotNull("Expected URL is null", out);
         assertTrue("Invalid parent view URL pattern", out.toString().startsWith("file:") && out.toString().contains("dmx-"));
         StringBuilder sb;
        try ( // check contents
                BufferedReader reader = new BufferedReader(new InputStreamReader(out.openStream()))) {
            sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null){
                sb.append(line).append("\n");
            }
        }
         

         //verify that paarent contains child view node
         Document parentDoc = DOC_FACTORY.newDocumentBuilder().parse(new InputSource(new StringReader(sb.toString())));
         XPath xpath = XPATH_FACTORY.newXPath();
         Node element = (Node) xpath.evaluate("//*[@viewId='childView']", parentDoc, XPathConstants.NODE);
         assertNotNull("XPath element is null", element);
         assertEquals("Invalid XML element", "dmx:view", element.getNodeName());
    }
}
