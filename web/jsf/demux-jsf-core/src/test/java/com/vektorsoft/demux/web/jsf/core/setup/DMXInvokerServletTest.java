/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.web.jsf.core.setup;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.HessianFactory;
import com.vektorsoft.demux.core.mva.DMXModel;
import com.vektorsoft.demux.web.jsf.core.DMXAdapterJSF;
import com.vektorsoft.demux.web.jsf.core.DMXWebModel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 * Contains test cases for {@code DMXControllerInvokerServlet} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXInvokerServletTest {
    
    private static DMXControllerInvokerServlet servlet;
    private static DMXAdapterJSF adapter;
    
    private HttpServletRequest request;
    private Map<String, Object> data;
    
    @BeforeClass
    public static void initClass() throws ServletException{
        adapter = mock(DMXAdapterJSF.class);
        when(adapter.getModel()).thenReturn(new DMXWebModel(new DMXModel()));
        
        servlet = new DMXControllerInvokerServlet(adapter);
        servlet.init();
    }
    
    @Before
    public void setup(){
        request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/controllers/testController");
        when(request.getPathInfo()).thenReturn("/controller");
        
        data = new HashMap<>();
    }
    
    @Test
    @Ignore
    public void basicControllerInvocation() throws Exception {
        TestServletResponse response = new TestServletResponse();
        
        data.put("data1", "data1 value");
        data.put("data2", 10);
        
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        HessianFactory factory = new HessianFactory();
        Hessian2Output hout = factory.createHessian2Output(bout);
        hout.startMessage();
        hout.writeObject(data);
        hout.completeMessage();
        hout.close();
        
        ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
        when(request.getInputStream()).thenReturn(new TestServletInputStream(bin));
        
        servlet.doPost(request, response);
        // verify correct response code
        assertEquals("Invalid repsponse status", HttpServletResponse.SC_OK, response.getStatus());
        // verify correct response entity
        byte[] bytes = response.getResponseData();
        assertNotNull("Reponse data is null", bytes);
        assertTrue("Invalid data size", bytes.length > 0);
        
        // check response data
        ByteArrayInputStream bin1 = new ByteArrayInputStream(bytes);
        Hessian2Input hin = factory.createHessian2Input(bin1);
        hin.startMessage();
        Map<String, Object> respData = (Map<String, Object>)hin.readObject(Map.class);
        hin.completeMessage();
        hin.close();
        
        assertNotNull("Response data is null", respData);
        assertTrue("Response data invalid size", respData.size() > 0);
    }
}
