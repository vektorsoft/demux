/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.internal;

import com.vektorsoft.demux.web.jsf.core.setup.DMXServletUtils;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.ServletContext;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;

/**
 * Provides test cases for {@link JSFBundleResourceListener} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class BundleResourceListenerTest {
    
    private BundleEvent event;
    private Bundle bundle;
    private ServletContext context;
    

    private static File resourcesDir;
    private static File templatesDir;
    
    @BeforeClass
    public static void setupClass(){
        File topDir = new File(System.getProperty("user.dir"));
        topDir.mkdir();
        
        resourcesDir = new File(topDir, "resources");
        resourcesDir.mkdir();
        
        templatesDir = new File(topDir, "WEB-INF/templates");
        templatesDir.mkdirs();
    }
    
    @AfterClass
    public static void teardownClass(){
        deleteDirectory(resourcesDir);
        deleteDirectory(templatesDir);
        File f = new File("index.xhtml");
        if(f.exists()){
            f.delete();
        }
    }
    
    private static void deleteDirectory(File dir){
        if(dir.exists()){
            File[] files = dir.listFiles();
            for(File f : files){
                if(f.isDirectory()){
                    deleteDirectory(f);
                } else {
                    f.delete();
                }
            }
            dir.delete();
        }
    }
    
    @Before
    public void setUp(){
        
        
        bundle = mock(Bundle.class);
        List<URL> entries = new ArrayList<>();
        entries.add(getClass().getResource("/assets/images/image.txt"));
        entries.add(getClass().getResource("/assets/css/style.css"));
        entries.add(getClass().getResource("/assets/css/images/image.txt"));
        entries.add(getClass().getResource("/assets/templates/testTemplate.xhtml"));
        entries.add(getClass().getResource("/assets/root/index.xhtml"));
        when(bundle.findEntries("/assets", "*.*", true)).thenReturn(Collections.enumeration(entries));
        
        when(bundle.getSymbolicName()).thenReturn("org.bundle.symbolic-name");
        
        context = mock(ServletContext.class);
        when(context.getRealPath("/resources")).thenReturn(resourcesDir.getAbsolutePath());
        when(context.getRealPath("/WEB-INF/templates")).thenReturn(templatesDir.getAbsolutePath());
        when(context.getRealPath("/")).thenReturn(System.getProperty("user.dir"));
        DMXServletUtils.initialize(context);
        
        event = mock(BundleEvent.class);
        when(event.getType()).thenReturn(BundleEvent.RESOLVED);
        when(event.getBundle()).thenReturn(bundle);
    }
    
    @Test
    public void testResourceCopy() throws Exception{
        JSFBundleResourceListener listener = new JSFBundleResourceListener();
        listener.bundleChanged(event);
        // wait till threads finish
        Thread.sleep(1000);
        File result = new File(resourcesDir, bundle.getSymbolicName() + "/images/image.txt");
        assertTrue("Image file does not exist", result.exists());
        
        result = new File(resourcesDir, bundle.getSymbolicName() + "/css/style.css");
        assertTrue("CSS file does not exist", result.exists());
        
        result = new File(resourcesDir, bundle.getSymbolicName() + "/css/images/image.txt");
        assertTrue("CSS image file does not exist", result.exists());
        
        result = new File(templatesDir, bundle.getSymbolicName() + "/testTemplate.xhtml");
        assertTrue("Template does not exist", result.exists());
        
        result = new File("index.xhtml");
        assertTrue("Public page does not exist", result.exists());
    }
}
