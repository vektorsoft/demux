/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.web.jsf.core.setup;

import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TestServletInputStream extends ServletInputStream {
    
    private InputStream source;
    
    public TestServletInputStream(InputStream in){
        this.source = in;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return source.read(b);
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return source.read(b, off, len); //To change body of generated methods, choose Tools | Templates.
    }
    

    @Override
    public boolean isFinished() {
        try {
            return (source.available() == 0);
        } catch(IOException ex){
            throw new RuntimeException(ex);
        }
        
    }

    @Override
    public boolean isReady() {
        try {
            return (source.available() > 0);
        } catch(IOException ex){
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void setReadListener(ReadListener rl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int read() throws IOException {
        return source.read();
    }
    
}
