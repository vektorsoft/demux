/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXModel;
import com.vektorsoft.demux.core.mva.DMXView;
import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.AfterClass;
import org.junit.Test;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * Test cases for class {@link DMXAdapterJSF}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXAdapterJSFTest {
    
    int[] hcs = new int[2];
    private static File dmxDir;
    
    
     @BeforeClass
    public static void setupClass(){
        dmxDir = new File("dmx-views");
        dmxDir.mkdir();
    }
    
    @AfterClass
    public static void teardownClass(){
        File[] files = dmxDir.listFiles();
        for(File f : files){
            f.delete();
        }
        dmxDir.delete();
    }
    
    /**
     * <p>
     *  Verifies correct behavior of {@link DMXAdapterJSF#getModel() } method. This
     * should return the same instance of {@code DMXWebModel} for same session,
     * but different for different sessions.
     * </p>
     */
    @Test
    public void getModelTest(){
        HttpSession s1 = new DummySession("session1");
        HttpSession s2 = new DummySession("session2");
        
        final DMXAdapterJSF adapter = new DMXAdapterJSF();
        adapter.initSession(s1);
        adapter.initSession(s2);
        
        final HttpServletRequest req1 = mock(HttpServletRequest.class);
        when(req1.getSession(false)).thenReturn(s1);
       final  HttpServletRequest req2 = mock(HttpServletRequest.class);
        when(req2.getSession(false)).thenReturn(s2);
        
        
        
        Thread th1 = new Thread(new Runnable() {

            @Override
            public void run() {
                adapter.setRequest(req1);
                hcs[0] = adapter.getModel().hashCode();
            }
        });
        
         Thread th2 = new Thread(new Runnable() {

            @Override
            public void run() {
                adapter.setRequest(req2);
                hcs[1] = adapter.getModel().hashCode();
            }
        });
         th1.start();
         th2.start();
         
         while(hcs[0] == 0 && hcs[1] == 0){
             ;
         }
         
         assertTrue("invalid hash code values", hcs[0] != hcs[1]);
    }
    
    /**
     * <p>
     *  Verifies correct behavior of {@link DMXAdapterJSF#registerController(com.vektorsoft.demux.core.mva.DMXController) } method. When 
     * invoked, it will register controller with specified ID and model data.
     * </p>
     */
    @Test
    public void registerControllerTest() throws Exception{
        DMXAdapterJSF adapter = new DMXAdapterJSF();
        
        DMXController ctrl = mock(DMXController.class);
        when(ctrl.getMapping()).thenReturn("ctrl1");
        
        Map<String, Object> data = new HashMap<>();
        data.put("d1", 1);
        String[] ids = new String[]{"d1"};
        when(ctrl.getControllerData()).thenReturn(ids);
        
        adapter.registerController(ctrl);
        
        Field field = adapter.getClass().getDeclaredField("DATA_MODEL");
        field.setAccessible(true);
        DMXModel model = (DMXModel)field.get(adapter);
        
       // assertTrue("Data name not contained", model.getAllData().containsKey("d1"));
        
        field = adapter.getClass().getDeclaredField("CONTROLLER_MAP");
        field.setAccessible(true);
        Map<String, DMXController> map = (Map<String, DMXController>)field.get(adapter);
        assertTrue("Invalid controller map", map.containsKey("ctrl1"));
    }
    
    @Test
    public void registerViewTest() throws Exception{
        DMXAdapterJSF adapter = new DMXAdapterJSF();
        ServletContext sc = mock(ServletContext.class);
        when(sc.getRealPath(JSFConstants.DMX_VIEW_PATH)).thenReturn(dmxDir.getAbsolutePath());
        adapter.setServletContext(sc);
        DMXView view = new MockJSFView("mainView", "/pages/main.xhtml");
        
        adapter.registerView(view);
        
        Field field = adapter.getClass().getDeclaredField("viewManager");
        assertNotNull("View manager is null", field);
        field.setAccessible(true);
        JSFViewManager jsfManager = (JSFViewManager)field.get(adapter);
        assertNotNull("Registered view is null", jsfManager.getView("mainView"));
    }
}
