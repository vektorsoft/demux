/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.web.jsf.core.setup;

import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXLocalModel;
import com.vektorsoft.demux.core.mva.ExecutionException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class TestController implements DMXController{
    
//    private int intValue;
//    private String stringValue;

    @Override
    public String getNextViewId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getMapping() {
        return "tester";
    }

//    @Override
//    public void updateFromModel(Map<String, Object> modelData) {
//        intValue = (Integer)modelData.get("data2");
//        stringValue = (String)modelData.get("data1");
//    }
//
//    @Override
//    public Map<String, Object> updateModel() {
//        Map<String, Object> map = new HashMap<>();
//        map.put("data1", stringValue);
//        map.put("data2", intValue);
//        
//        return map;
//    }
//
//    @Override
//    public Map<String, Object> getControllerDataMap() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Set<String> getControllerDataNames() {
//        Set<String> names = new HashSet<>();
//        names.add("data1");
//        names.add("data2");
//        
//        return names;
//    }

    @Override
    public void execute(DMXLocalModel model, Object... params) throws ExecutionException {
        int intVal = model.get("data2", Integer.class, 0);
        intVal += 5;
        String stringVal = model.get("data1", String.class, "");
        stringVal = stringVal + intVal;
        
        model.set("data1", stringVal).set("data2", intVal);
//        intValue = intValue + 5;
//        stringValue = stringValue + intValue;
    }

    @Override
    public String[] getControllerData() {
        return new String[]{"data1","data2"};
    }
    
}
