/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.internal;

import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.web.jsf.core.JSFConstants;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Helper class which maps view IDs constructed from <code>dmx:view</code> tag to actual .xhtml resources.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class ViewResourceUtils {
    
    /** Mapping map. */
    private static final Map<String, URL> MAPPINGS;
    
    // static initializer
    static {
        MAPPINGS = new ConcurrentHashMap<>();
    }
    
    /**
     * Private constructor to prevent instantiation.
     */
    private ViewResourceUtils(){
        
    }
    
    /**
     * Maps specified view ID to specified view. This method will prepend <code>/dmx</code> before
     * view ID.
     * 
     * @param viewId view ID
     * @param view view object
     */
    public static final void addMapping(String viewId, DMXView view){
        URL viewUrl = view.getClass().getClassLoader().getResource(view.getViewUI().toString());
        if(viewUrl == null){
           viewUrl =  view.getClass().getResource(view.getViewUI().toString());
        }
        MAPPINGS.put(JSFConstants.DMX_VIEW_URL_PREFIX + viewId, viewUrl);
    }
    
    /**
     * Maps specified view ID to specified URL.
     * 
     * @param viewId view ID
     * @param mapping target URL
     */
    public static final void addMapping(String viewId, URL mapping){
        MAPPINGS.put(JSFConstants.DMX_VIEW_URL_PREFIX + viewId, mapping);
    }
    
    /**
     * Retrieves URL of .xhtml file for specified view.
     * 
     * @param mapping view mapping
     * @return view .xhtml URL.
     */
    public static final URL getViewResourceURL(String mapping){
        URL url = MAPPINGS.get(mapping);
        if(url == null){
            url = MAPPINGS.get(JSFConstants.DMX_VIEW_URL_PREFIX + mapping);
        }
        return url;
    }
    
    
}
