/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DMXViewCallback;
import com.vektorsoft.demux.core.mva.AbstractViewManager;
import com.vektorsoft.demux.core.mva.DMXEventRegistrationHandler;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import com.vektorsoft.demux.web.jsf.core.internal.JSFBundleResourceListener;
import com.vektorsoft.demux.web.jsf.core.internal.ViewResourceUtils;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import javax.servlet.ServletContext;

/**
 * Implementation of {@link DMXViewManager} for use in JSF applications. This implementation provides support
 * for handling JSF-specific views.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JSFViewManager extends AbstractViewManager{
    
    /** Servlet context for application. */
    private ServletContext context;
    
    /** Indicates if view manager is initialized. */
    private boolean initialized = false;
    
    /** Directory containing view files. */
    private File viewDirectory;
    
    /**
     * Set application servlet context.
     * 
     * @param context context to set
     */
    public void setContext(ServletContext context) {
        this.context = context;
        String path = this.context.getRealPath(JSFConstants.DMX_VIEW_PATH);
        if(path == null){
            path = this.context.getRealPath("/WEB-INF");
            viewDirectory = new File(path, "dmx");
        } else {
            viewDirectory = new File(path);
        }
        
        if(!viewDirectory.exists()){
            viewDirectory.mkdirs();
        }
        
        initialized = true;
    }

    /**
     * Registers specified view with this view manager. This method will perform all required actions to process
     * view .xhtml file and add any child views if necesserry.
     * @param view 
     */
    @Override
    public void registerView(DMXView view) {
        if(!initialized){
            throw new IllegalStateException("View manager not initialized. Call setContext() first.");
        }
        super.registerView(view);
        
        // extract view URL to /WEB-INF/dmx
        ViewResourceUtils.addMapping(view.getViewId(), view);
        File target = new File(viewDirectory, view.getViewId() + JSFConstants.DMX_VIEW_EXTENSION);
        try {
            //copyFiles(ViewResourceUtils.getViewResourceURL(view.getViewId()), target);
            JSFBundleResourceListener.copyFiles(ViewResourceUtils.getViewResourceURL(view.getViewId()), target);
            ViewResourceUtils.addMapping(view.getViewId(), target.toURI().toURL());
        } catch(IOException ex){
            ex.printStackTrace(System.err);
        }
        
        String parentId = view.getParentViewId();
        if (parentId != null && !parentId.isEmpty()) {
            if(view.viewPlacementConstraint() == null){
                throw new IllegalArgumentException("View placement constraint must be specified.");
            }
           
            // create document for parent view
            DMXView parentView = getView(parentId);
            if(parentView != null){
                parentView.addChildView(view);
            }

        }
        afterInitialViewRegistration(view);
    }
    

    @Override
    protected void processDeferredViews(String parentId, Set<String> childrenIds) {
        if(childrenIds != null && !childrenIds.isEmpty()){
            for(String id : childrenIds){
                DMXView child = getView(id); // should never be null
                getView(parentId).addChildView(child);
            }
        }
    }

    @Override
    public void selectView(String viewId, Map<String, Object> params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateView(String viewId, Map<String, Object> dataValues) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateViewData(Map<String, Object> dataMap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setViewExtensionCallback(String viewId, DMXViewCallback callback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void handleLocaleChange() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void blockGui(boolean state, GUIBlockingScope scope, String... ids) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T extends DialogFactory> T getDialogFactory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerEventHandler(DMXEventRegistrationHandler handler) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
