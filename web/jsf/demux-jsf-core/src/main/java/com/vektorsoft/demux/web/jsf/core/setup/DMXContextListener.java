/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.setup;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.web.jsf.core.DMXActivatorJSF;
import com.vektorsoft.demux.web.jsf.core.DMXAdapterJSF;
import java.util.EnumSet;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * Context listener for DEMUX Framework JSF application. This class will register beans, filters and handlers required
 * for correct operation of application.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXContextListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        DMXAdapterJSF adapter = null;
        
        // check if adapter is already registered as a service
        Bundle bundle = FrameworkUtil.getBundle(DMXActivatorJSF.class);
        ServiceReference<DMXAdapter> sref = bundle.getBundleContext().getServiceReference(DMXAdapter.class);
        adapter = (DMXAdapterJSF)bundle.getBundleContext().getService(sref);
       
        // check if adapter is already registered
        ServletContext context = sce.getServletContext();
        adapter.setServletContext(context);
        
        
        if(context.getAttribute(DMXAdapterJSF.ADAPTER_BEAN_NAME) == null){
            
            sce.getServletContext().setAttribute(DMXAdapterJSF.ADAPTER_BEAN_NAME, adapter);
            sce.getServletContext().log("Registered attribute " + DMXAdapterJSF.ADAPTER_BEAN_NAME);
        }
        
        // register session listener
        sce.getServletContext().addListener(new DMXSessionListener(adapter));
        // register filter
        FilterRegistration reg = sce.getServletContext().addFilter("DMXFilter", DMXFilter.class);
        // filter mapping for controller invocation servlet
        reg.addMappingForUrlPatterns(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST), true, "/controllers/*");
        // map filter to Faces Servlet pattern
        ServletRegistration sreg = sce.getServletContext().getServletRegistration("Faces Servlet");
        if(sreg != null){
             reg.addMappingForUrlPatterns(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST), true, sreg.getMappings().toArray(new String[1]));
             
        } else {
            reg.addMappingForUrlPatterns(EnumSet.of(DispatcherType.FORWARD, DispatcherType.REQUEST), true, "*.xhtml");
        }
        // register controller invoker servlet
        ServletRegistration controllerReg = sce.getServletContext().addServlet("controllerInvokerServlet", new DMXControllerInvokerServlet(adapter));
        controllerReg.addMapping("/controllers/*");
        
        // initialize DMXServletUtils
        DMXServletUtils.initialize(context);
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(DMXAdapterJSF.ADAPTER_BEAN_NAME);
    }
    
}
