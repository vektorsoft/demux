/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.web.jsf.core.internal.JSFBundleResourceListener;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Custom activator for JSF core bundle. This class takes care of setting up {@code DMXAdapter} and
 * bean registration with JSF runtime.
 *
 * @author Vladimir Djurovic
 */
public class DMXActivatorJSF implements BundleActivator{

    @Override
    public void start(BundleContext bc) throws Exception {
        // register adapter as a service
        DMXAdapterJSF adapter = new DMXAdapterJSF();
        bc.registerService(DMXAdapter.class, adapter, null);
        
        // register bundle listener
        bc.addBundleListener(new JSFBundleResourceListener());
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        // do nothing
    }
    
}
