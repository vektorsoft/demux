/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.internal;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;

/**
 * Bundle listener which extracts resources from bundles into specific web app location.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class JSFBundleResourceListener implements BundleListener{
    
    /** Executor for bundle extraction. */
    private static final ThreadPoolExecutor EXECUTOR;
    
    /** Multiplier for core number of threads in executor. It multiplies number of processors. */
    private static final int CORE_THREAD_MULTIPLIER = 2;
    
    /** Multiplier for maximum number of threads in executor. It multiplies number of processors. */
    private static final int MAX_THREAD_MULTIPLIER = 4;
    
    /** Value for thread keep alive time. */
    private static final int THREAD_ALIVE_TIME = 500;
    
    /** Initial size for executor thread queue. */
    private static final int QUEUE_SIZE = 10;
    
    static {
        int procCount = Runtime.getRuntime().availableProcessors();
        EXECUTOR = new ThreadPoolExecutor(CORE_THREAD_MULTIPLIER * procCount, MAX_THREAD_MULTIPLIER * procCount, 
                                    THREAD_ALIVE_TIME, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(QUEUE_SIZE));
    }

    @Override
    public void bundleChanged(BundleEvent be) {
        if(be.getType() == BundleEvent.RESOLVED){
            Bundle bundle = be.getBundle();
            // check if bundle contains assets
            Enumeration<URL> entries = bundle.findEntries("/assets", "*.*", true);
            if(entries != null && entries.hasMoreElements()){
                BundleResourceExtractor extractor = new BundleResourceExtractor(entries, bundle.getSymbolicName());
                EXECUTOR.submit(extractor);
            }
        }
    }
    
    /**
     * Copy file from specified source URL to target file.
     * 
     * @param source source URL
     * @param dest destination file
     * @throws IOException if an error occurs
     */
    public static void copyFiles(URL source, File dest) throws IOException{
        final ByteBuffer buffer = ByteBuffer.allocateDirect(8 * 1024);
        InputStream is = source.openStream();
        OutputStream os = new FileOutputStream(dest);
        // get NIO channels from stream
        ReadableByteChannel rbc = Channels.newChannel(is);
        WritableByteChannel wbc = Channels.newChannel(os);
        // do the copy
        while(rbc.read(buffer) != -1){
            buffer.flip();
            wbc.write(buffer);
            buffer.compact();
        }
        // drain buffer after EOF
        buffer.flip();
        while(buffer.hasRemaining()){
            wbc.write(buffer);
        }
        // close channels
        wbc.close();
        rbc.close();
    }
    
}
