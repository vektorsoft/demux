/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.web.jsf.core.internal.ViewResourceUtils;
import java.net.URL;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.application.ViewResource;
import javax.faces.context.FacesContext;

/**
 * JSF resource handler implementation for use with DEMUX Framework applications. 
 * <p>
 *  <strong>Important:</strong> This is <b>NOT</b> an implementation of {@code DMXResourceHandler}.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXResourceHandlerJSF extends ResourceHandlerWrapper{
    
    /** Wrapped resource handler. */
    private ResourceHandler wrapped;
    
    /**
     * Creates new instance.
     * 
     * @param handler underlying resource handler
     */
    public DMXResourceHandlerJSF(ResourceHandler handler){
        this.wrapped = handler;
    }

    @Override
    public ResourceHandler getWrapped() {
        return wrapped;
    }

    @Override
    public ViewResource createViewResource(FacesContext context, final String resourceName) {
        // try to find mapped resource
        final URL url = ViewResourceUtils.getViewResourceURL(resourceName);
        if(url != null){
            return new DMXViewResource(url);
        }
        
        ViewResource out = super.createViewResource(context, resourceName);
        
        return out;
    }
    
    /**
     * Custom implementation of {@code ViewResource} to be used as returned object
     * from method invocations.
     */
    private static class DMXViewResource extends ViewResource {
        
        /** Resource URL. */
        private final URL url;

        /**
         * Creates new instance.
         * 
         * @param url resource URL
         */
        public DMXViewResource(URL url) {
            this.url = url;
        }
        

        @Override
        public URL getURL() {
            return url;
        }
        
    }
    
}
