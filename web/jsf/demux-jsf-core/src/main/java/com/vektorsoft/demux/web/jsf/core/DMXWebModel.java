/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.mva.DMXModel;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Data model used in JSF applications. Unlike standard plain {@link DMXModel}, this class must be
 * safe for use within session. No data is allowed to be shared among sessions.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXWebModel implements Serializable{
    
    
     /** String representing null value. This will be placed in data map instead of a <code>null</code> value.
     */
    private static final String NULL_VALUE_STRING = "com.vektorsoft.demuc.core.mva.DMXModel.NULL";
    
    /** Data map. */
    private final Map<String, Object> dataMap;
    
    /**
     * Creates new instance.
     * 
     * @param model source data model
     */
    public DMXWebModel(DMXModel model){
        dataMap = new ConcurrentHashMap<>();
        dataMap.putAll(model.getAllData());
        
    }
    
    /**
     * Returns variable with specified name/ID.
     * 
     * @param name variable name/ID
     * @return variable value
     */
    public Object get(String name){
        return dataMap.get(name);
    }
    
    /**
     * Sets the value of the variable with specified ID.
     * 
     * @param id variable ID
     * @param value variable value
     */
    public void setValue(String id, Object value){
        dataMap.put(id, value);
    }
    
    /**
     * Returns values of model data with specified IDs. The result map contains
     * data IDs as map keys, and values as map data.
     *
     * @param dataIds IDs of data to get
     * @return data map
     */
    public Map<String, Object> getDataValues(String... dataIds) {
        Map<String, Object> result = new HashMap<>();
        for (String id : dataIds) {
            // we only want to return data if it is contained within the model
            if (dataMap.containsKey(id)) {
                result.put(id, getDataValue(id));
            } else {
                // otherwise, display error
                System.err.println("Variable name not registered: " + id);
            }

        }
        return result;
    }
    
    /**
     * Returns value of model data with specified ID.
     *
     * @param dataId ID under which data is registered
     * @return data value
     */
    public Object getDataValue(String dataId) {
        Object data = dataMap.get(dataId);
        if(data != null && data.toString().equals(NULL_VALUE_STRING)){
            data = null;
        }
        return data;
    }
    
    /**
     * Sets values for data specified in a map. Map key is data ID, and value is
     * data value.
     *
     * @param map data map
     */
    public void setDataValues(Map<String, Object> map) {
        for(Map.Entry<String, Object> entry : map.entrySet()){
            if(entry.getValue() != null){
                dataMap.put(entry.getKey(), entry.getValue());
            } else {
                dataMap.put(entry.getKey(), NULL_VALUE_STRING);
            }
        }
    }
    
}
