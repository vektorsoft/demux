/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.web.jsf.core.setup;

import com.caucho.hessian.io.Hessian2Input;
import com.caucho.hessian.io.Hessian2Output;
import com.caucho.hessian.io.HessianFactory;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.NoSuchControllerException;
import com.vektorsoft.demux.web.jsf.core.DMXAdapterJSF;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet for invoking DEMUX controllers.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXControllerInvokerServlet extends HttpServlet {
    
    /**
     * Hessian content type.
     */
    private static final String MIME_TYPE_HESSIAN = "application/x-hessian";
    
    /**
     * Adapter instance
     */
    private DMXAdapterJSF adapter;
    
    /**
     * Hessian factory.
     */
    private HessianFactory hessianFactory;


    /**
     * Creates new instance of this servlet.
     * 
     * @param adapter adapter instance
     */
    public DMXControllerInvokerServlet(DMXAdapter adapter){
        if(!(adapter instanceof DMXAdapterJSF)){
            throw new IllegalArgumentException("Argument must be of type com.vektorsoft.demux.jsf.core.DMXAdapterJSF");
        }
        this.adapter = (DMXAdapterJSF)adapter;
        hessianFactory = new HessianFactory();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session == null){
             session = req.getSession(true);
             resp.setStatus(HttpServletResponse.SC_ACCEPTED);
             return;
        }
        // check for correct content type
        String contentType = req.getContentType();
        if(!MIME_TYPE_HESSIAN.equals(contentType)){
            resp.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
            resp.setContentType("text/plain");
            resp.getWriter().write("Conent type must be application/x-hessian");
            return;
        }
        //find controller ID
        String controllerId = req.getPathInfo().substring(1);
        
        Hessian2Input hin = hessianFactory.createHessian2Input(req.getInputStream());
        hin.startMessage();
        int length = hin.readInt();
        Map<String, Object> data = (Map<String, Object>)hin.readObject(Map.class);
        String[] keys = data.keySet().toArray(new String[data.keySet().size()]);
        List<Object> argList = null;
        if(length == 2){
            argList = (List<Object>)hin.readObject(List.class);
        }
        hin.completeMessage();
        hin.close();
        
        adapter.getModel().setDataValues(data);
        Object[] args = new Object[]{};
        if(argList != null){
            args = argList.toArray();
        }
        try{
            adapter.invokeController(controllerId, args);
        } catch(Exception ex){
            resp.setContentType("text/plain");
            if(ex.getCause() instanceof NoSuchControllerException){
                resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
                resp.getWriter().write("Controller ID not found: " + controllerId);
            } else {
                resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                if(ex.getMessage() != null){
                    resp.getWriter().write(ex.getMessage());
                }
            }
            return;
        }
        
        resp.setContentType("application/x-hessian");
        Hessian2Output hout = hessianFactory.createHessian2Output(resp.getOutputStream());
        hout.startMessage();
        hout.writeObject(adapter.getModel().getDataValues(keys));
        hout.completeMessage();
        hout.close();
        
        resp.setStatus(HttpServletResponse.SC_OK);
        
    }
    
    
}
