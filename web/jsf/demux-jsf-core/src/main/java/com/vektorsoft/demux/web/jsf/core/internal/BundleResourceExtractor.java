/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.internal;

import com.vektorsoft.demux.web.jsf.core.setup.DMXServletUtils;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

/** A {@code Runnable} implementation which is used to extract resources from a bundle into 
 * specific server location. Resources can be images, CSS files, JavaScript files etc.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class BundleResourceExtractor  implements Runnable{
    
    /** Root location for assets. */
    private static final String ASSETS = "/assets/";
    
    /** Location for images. */
    private static final String ASSETS_IMG = ASSETS + "images/";
    
    /**
     * Location for CSS files.
     */
    private static final String ASSETS_CSS = ASSETS + "css/";
    
    /**
     * Location for JavaScript files.
     */
    private static final String ASSETS_JS = ASSETS + "js/";
    
    /**
     * Location for template files.
     */
    private static final String ASSETS_TEMPLATES = ASSETS + "templates/";
    
    /**
     * Location for files to be extracted into web application root directory.
     */
    private static final String ASSETS_PUBLIC = ASSETS + "root/";
    
    /**
     * All resource entries.
     */
    private final Enumeration<URL> entries;
    
    /**
     * Actual resources directory on server.
     */
    private final File resourcesDir;
    
    /**
     * Actual templates directory on server.
     */
    private final File templatesDir;
    
    /**
     * Actual web application public directory on server.
     */
    private final File publicDir;
    
    /**
     * Creates new instance.
     * 
     * @param entries resource entries
     * @param symbolicName  bundle symbolic name
     */
    public BundleResourceExtractor(Enumeration<URL> entries, String symbolicName){
        this.entries = entries;
        resourcesDir = new File(DMXServletUtils.getInstance().getResourcesDirectory(), symbolicName);
        if(!resourcesDir.exists()){
            resourcesDir.mkdirs();
        }
        templatesDir = new File(DMXServletUtils.getInstance().getTemplatesDirectory(), symbolicName);
        if(!templatesDir.exists()){
            templatesDir.mkdir();
        }
        publicDir = DMXServletUtils.getInstance().getPublicDir();
    }

    @Override
    public void run() {
        while (entries.hasMoreElements()) {
            URL entry = entries.nextElement();
            File target = null;
            // check if this is image
            if (entry.toString().split(ASSETS_IMG).length > 1) {
                String path = entry.toString().split(ASSETS)[1];
                target = getTargetFile(resourcesDir, path);

            } else if (entry.toString().split(ASSETS_CSS).length > 1) {
                String path = entry.toString().split(ASSETS)[1];
                target = getTargetFile(resourcesDir, path);
                
            } else if (entry.toString().split(ASSETS_JS).length > 1) {
                String path = entry.toString().split(ASSETS)[1];
                target = getTargetFile(resourcesDir, path);
                
            } else if(entry.toString().split(ASSETS_TEMPLATES).length > 1){
                String path = entry.toString().split(ASSETS_TEMPLATES)[1];
                target = getTargetFile(templatesDir, path);
            } else if(entry.toString().split(ASSETS_PUBLIC).length > 1){
                String path = entry.toString().split(ASSETS_PUBLIC)[1];
                target = getTargetFile(publicDir, path);
            } else {
                return;
            }

            try {
                JSFBundleResourceListener.copyFiles(entry, target);
            } catch (IOException ex) {
                ex.printStackTrace(System.err);
            }

        }
    }
    
    /**
     * Creates file from give path, including all missing subdirectories.
     * 
     * @param dir parent directory of the file
     * @param path path of the file
     * @return created file
     */
    private File getTargetFile(File dir, String path) {
        File target = new File(dir, path);
        // check if parent directory exists, and create if needed
        if (!target.getParentFile().exists()) {
            target.getParentFile().mkdirs();
        }
        return target;
    }
    
}
