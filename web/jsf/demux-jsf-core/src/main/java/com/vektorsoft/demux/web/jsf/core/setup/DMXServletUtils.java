/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.setup;

import java.io.File;
import javax.servlet.ServletContext;

/** 
 * Utility class which contains  methods used in other classes related to 
 * servlet functionality.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public final class DMXServletUtils {
    
    /**
     * Singleton instance of this class.
     */
    private static  DMXServletUtils instance;  
    
    /**
     * Servlet context to use for operations.
     */
    private final ServletContext context;
    
    /**
     * Actual server directory for resources.
     */
    private File resourcesDir;
    
    /**
     * Actual server directory for JSF templates.
     */
    private File templatesDir;
    
    /**
     * Actual server directory of web application root.
     */
    private File publicDir;
    
    /**
     * Private constructor to prevent instantiation.
     */
    private DMXServletUtils(){
        this(null);
    }
    
    /**
     * Private constructor to prevent instantiation.
     * 
     * @param context servlet context of the application.
     */
    private DMXServletUtils(ServletContext context){
        if(context == null){
            throw new IllegalArgumentException("ServletContext can not be null");
        }
        this.context = context;
    }
    
    /**
     * Initializes the class. This method should be called once, before the class is first used.
     * 
     * @param context application servlet context
     */
    public static void initialize(ServletContext context){
        if(instance == null){
            instance = new DMXServletUtils(context);
        }
    }
    
    /**
     * Returns singleton instance of the class.
     * 
     * @return class instance
     */
    public static DMXServletUtils getInstance(){
        if(instance == null){
            throw new IllegalStateException("DMXServletUtils is not initialized. Call initialize(ServletContext) first.");
        }
        return instance;
    }
    
    /**
     * Returns application servlet context.
     * 
     * @return servlet context
     */
    public  ServletContext getServletContext(){
        return context;
    }
    
    /**
     * Returns resources directory on the server ({app_root_dir}/resources).
     * 
     * @return resources directory
     */
    public File getResourcesDirectory(){
        if(resourcesDir == null){
            String path = context.getRealPath("/resources");
            if(path != null){
                resourcesDir = new File(path);
            } else {
                path = context.getRealPath("/");
                resourcesDir = new File(path, "resources");
            }
            
        }
        return resourcesDir;
    }
    
    /**
     * Returns JSF templates directory on the server ({app_root_dir}/templates).
     * 
     * @return templates directory
     */
    public File getTemplatesDirectory(){
        if(templatesDir == null){
            String path = context.getRealPath("/WEB-INF/templates");
            if(path != null){
                templatesDir = new File(path);
            } else {
                path = context.getRealPath("/WEB-INF");
                templatesDir = new File(path, "templates");
            }
            
        }
        return templatesDir;
    }

    /**
     * Returns web app public directory on the server.
     * 
     * @return web app directory
     */
    public File getPublicDir() {
        if(publicDir == null){
            publicDir = new File(context.getRealPath("/"));
        }
        return publicDir;
    }
    
    
    
}
