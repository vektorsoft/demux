/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core.setup;

import com.vektorsoft.demux.web.jsf.core.DMXAdapterJSF;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

/**
 * Filter for JSF requests targeted to DEMUX Framework application. This filter will intercept each JSF request and set request 
 * info required for correct operation of registered {@link DMXAdapterJSF}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXFilter implements Filter{
    
    /** Adapter instance. */
    private DMXAdapterJSF adapter;

    @Override
    public void init(FilterConfig fc) throws ServletException {
        adapter = (DMXAdapterJSF)fc.getServletContext().getAttribute(DMXAdapterJSF.ADAPTER_BEAN_NAME);
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        adapter.setRequest((HttpServletRequest)sr);
        fc.doFilter(sr, sr1);
    }

    @Override
    public void destroy() {
        
    }
    
}
