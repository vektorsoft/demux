/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.extension.DMXExtensionManager;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXEventRegistrationHandler;
import com.vektorsoft.demux.core.mva.DMXLocalModel;
import com.vektorsoft.demux.core.mva.DMXModel;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.mva.ExecutionException;
import com.vektorsoft.demux.core.mva.NoSuchControllerException;
import com.vektorsoft.demux.core.resources.DMXResourceManager;
import com.vektorsoft.demux.core.task.DMXTask;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * An implementation of {@link DMXAdapter} used in JSF environment. This class is registered as JSF managed bean and 
 * is available within application scope.
 * 
 * @see DMXAdapter
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
@ManagedBean (name = DMXAdapterJSF.ADAPTER_BEAN_NAME, eager = true)
@ApplicationScoped
public class DMXAdapterJSF  implements DMXAdapter, Serializable{
    
    /** Name of this bean within JSF runtime. */
    public static final String ADAPTER_BEAN_NAME = "dmxAdapter";
    
    /** Stores the current HTTP request. */
    private static final ThreadLocal<HttpServletRequest> REQUEST;
    
    /** Data model used with this adapter. */
    private static final DMXModel DATA_MODEL;
    
    /** Map of controllers registered with the adapter. */
    private static final Map<String, DMXController> CONTROLLER_MAP;
    
    /** View manager used with adapter. */
    private final JSFViewManager viewManager;
    
    // static initializer
    static {
        REQUEST = new ThreadLocal<>();
         DATA_MODEL = new DMXModel();
         CONTROLLER_MAP = new HashMap<>();
    }
    
    /**
     * Creates new instance.
     */
    public DMXAdapterJSF(){
        viewManager = new JSFViewManager();
    }

    /**
     * Sets servlet context for this adapter.
     * @param servletContext 
     */
    public void setServletContext(ServletContext servletContext) {
        viewManager.setContext(servletContext);
    }

    /**
     * Returns data model associated with current session.
     * 
     * @return  data model
     */
    public DMXWebModel getModel() {
        DMXWebModel model = null;
        HttpSession session = REQUEST.get().getSession(false);
        if(session != null){
            model = (DMXWebModel)session.getAttribute(session.getId());
        }
        
        return model;
    }
    
    /**
     * Initializes current session.
     * 
     * @param session HTTP session
     */
    public void initSession(HttpSession session){
        DMXWebModel wmodel = new DMXWebModel(DATA_MODEL);
        session.setAttribute(session.getId(), wmodel);
    }
    
    /**
     * Set current HTTP request.
     * 
     * @param request HTTP request
     */
    public void setRequest(HttpServletRequest request){
        REQUEST.set(request);
        
    }
    

    @Override
    public DMXResourceManager getResourceManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DMXExtensionManager getExtensionManager() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerController(DMXController controller) {
        //check parameters validity
        if (controller == null) {
            throw new IllegalArgumentException("Controller cannot be null.");
        }
        if (controller.getMapping() == null || "".equals(controller.getMapping())) {
            throw new IllegalArgumentException("Controller mapping cannot be null or empty string.");
        }
        
        CONTROLLER_MAP.put(controller.getMapping(), controller);
        
    }

    @Override
    public void registerController(Class clazz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerView(DMXView view) {
        viewManager.registerView(view);
    }

    @Override
    public void registerView(Class clazz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerExtensionCallback(DMXExtensionCallback callback) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <T extends DialogFactory> T getDialogFactory() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Prepares controller for execution. This method will find controller by it's ID and inject any 
     * model data required.
     * 
     * @param controllerId controller ID
     * @return controller instance
     * @throws NoSuchControllerException if no controller with specified ID is found
     */
    private DMXLocalModel prepareController(DMXController controller) throws NoSuchControllerException {
        
        String[] ids = controller.getControllerData();
        Map<String, Object> dataMap = getModel().getDataValues(ids);
        

        
        return DMXLocalModel.instance().set(dataMap);
    }

    /**
     * This method can not be called directly from JSF pages, since it is a {@code vararg} method, and Expression Language
     * does not support varargs. Instead, call one of <code>invoke(...)methods</code>
     * 
     * @param controllerId controller ID
     * @param args arguments
     */
    @Override
    public void invokeController(String controllerId, Object... args) {
        try {
           DMXController controller = CONTROLLER_MAP.get(controllerId);
            if (controller == null) {
                throw new NoSuchControllerException("Controller not found: " + controllerId);
            }
            DMXLocalModel local = prepareController(controller);
             controller.execute(local,args);
            getModel().setDataValues(local.get());
        } catch(NoSuchControllerException | ExecutionException ex){
            throw new RuntimeException(ex);
        }
        
    }

    @Override
    public void executeTask(DMXTask task, Object... args) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void registerEventHandler(DMXEventRegistrationHandler handler) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    // the following methods are used as shortcut for invokeController method with fixed number of params
    // because Expression language does not support vararg methods
    
    /**
     * Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     */
    public void invoke(String controllerId){
        invokeController(controllerId);
    }
    
    /**
     * Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id, arg1);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     * @param arg1 argument 1
     */
    public final  void invoke(String controllerId, Object arg1){
        invokeController(controllerId, arg1);
    }
    
    /**
     * Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id, arg1, arg2);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     * @param arg1 argument 1
     * @param arg2 argument 2
     */
    public final  void invoke(String controllerId, Object arg1, Object arg2){
        invokeController(controllerId, arg1, arg2);
    }
   
     /**Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id, arg1, arg2, arg3);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     * @param arg1 argument 1
     * @param arg2 argument 2
     * @param arg3 argument 3
     */
    public final  void invoke(String controllerId, Object arg1, Object arg2, Object arg3){
        invokeController(controllerId, arg1, arg2, arg3);
    }
    
    /**Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id, arg1, arg2, arg3, arg4);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     * @param arg1 argument 1
     * @param arg2 argument 2
     * @param arg3 argument 3
     * @param arg4 argument 4
     */
    public final  void invoke(String controllerId, Object arg1, Object arg2, Object arg3, Object arg4){
        invokeController(controllerId, arg1, arg2, arg3, arg4);
    }
    
    /**Invokes controller with specified ID. Calling this method is the equivalent of calling:
     * <pre>
     *  <code>
     *      invokeController(id, arg1, arg2, arg3, arg4, arg5);
     *  </code>
     * </pre>
     * 
     * @param controllerId ID of controller to invoke
     * @param arg1 argument 1
     * @param arg2 argument 2
     * @param arg3 argument 3
     * @param arg4 argument 4
     * @param arg5 argument 5
     */
    public final  void invoke(String controllerId, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5){
        invokeController(controllerId, arg1, arg2, arg3, arg4, arg5);
    }

    @Override
    public void registerData(String id, Object value) {
        DATA_MODEL.registerData(id, value);
    }
}
