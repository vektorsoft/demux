/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.web.jsf.core;

import com.vektorsoft.demux.core.mva.DMXLocalModel;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.web.jsf.core.internal.ViewResourceUtils;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * <p>
 * Base class for implementing views based on JSF technology. This class provides basic functionality to
 * use view structure provided in .xhtml files, as with ordinary JSF applications. JSF views are used consistently 
 * as any other view implementation within DEMUX Framework.
 * </p>
 * <p>
 *  Basic assumption of this class is that each {@code DMXView} has an associated .xhtml file which contains actual view
 * UI definition. Further, each view can contain an HTML element which can be used as a container to insert child view at
 * that point.
 * </p>
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public abstract class AbstractJSFView implements DMXView{
    
    /** XML document builder factory.*/
    private static final DocumentBuilderFactory DOC_FACTORY = DocumentBuilderFactory.newInstance();
    
    /** XPATH factory instance. */
    private static final XPathFactory XPATH_FACTORY = XPathFactory.newInstance();
    
    /** XPath query to extract HTML element based on ID. */
    private static final String XPATH_ID_QUERY = "//*[@id='%s']";
    
    /** Custom JSF Facelet tag for DEMUX view definition. */
     private static final String DMX_VIEW_ELEMENT_NAME = "dmx:view";
     
     /** Name of viewID attribute of <code>dmx:view</code> tag. */
    private static final String DMX_VIEW_ID_ATTR = "viewId";
    
    /** Current view ID. Must be unique within application. */
    protected String viewId;
    
    /** URL of view XHTML file. Must be absolute URL with bundle .jar file. */
    protected String viewUrl;
    
    private String[] viewDataIds;
    
    /**
     * Creates new instance of the view. Constructor arguments must obey the following constraints:
     * <ul>
     *  <li>{@code viewId} must be unique within application</li>
     *  <li>{@code viewUrl} must be valid URL for a resource inside jar (ie., in form <code>/foo/bar.xhtml</code></li>
     * </ul>
     * 
     * @param viewId view ID
     * @param viewUrl  view URL
     */
    protected AbstractJSFView(String viewId, String viewUrl){
        this.viewId = viewId;
        this.viewUrl = viewUrl;
        viewDataIds = new String[0];
    }

    /**
     * Adds child view to this view. Child view is inserted into HTML element whose ID is obtained 
     * by the call to {@link #viewPlacementConstraint() } method.
     * 
     * @see DMXView#addChildView(com.vektorsoft.demux.core.mva.DMXView) 
     * 
     * @param child child view to add
     */
    @Override
    public final void addChildView(DMXView child) {
        try {
            URL parentUrl = ViewResourceUtils.getViewResourceURL(viewId);
            XPath xpath = XPATH_FACTORY.newXPath();
            DocumentBuilder builder = DOC_FACTORY.newDocumentBuilder();
            Document doc = builder.parse(parentUrl.openStream());
            String query = String.format(XPATH_ID_QUERY, child.viewPlacementConstraint().toString());
            Node element = (Node) xpath.evaluate(query, doc, XPathConstants.NODE);
            // create node for child view
            Element childNode = doc.createElement(DMX_VIEW_ELEMENT_NAME);
            Attr attribute = doc.createAttribute(DMX_VIEW_ID_ATTR);
            attribute.setValue(child.getViewId());
            childNode.setAttributeNode(attribute);
            // append child node to parent
            element.appendChild(childNode);
            // save file
            writeModifiedFile(doc, new File(parentUrl.toURI()));
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException | TransformerException | URISyntaxException ex) {
            ex.printStackTrace(System.err);
        }
    }
    
    /**
     * Writes modified XHTML file to correct location. This file contains child view elements.
     * 
     * @param doc XML document
     * @param target target file
     * @throws TransformerException  if an error occurs during writing
     */
    private void writeModifiedFile(Document doc, File target) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(target);
        transformer.transform(source, result);
    }


    /**
     * This implementation does nothing, since JSF runtime takes care of view rendering.
     * 
     * @see DMXView#updateFromModel(java.util.Map) 
     * @param data local model data
     */
    @Override
    public final void updateFromModel(DMXLocalModel data) {
        // do nothing
    }

    /**
     * Return URL string for this view's XHTML file.
     * 
     * @return URL as a string (in form <code>/foo/bar.xhtml</code>)
     */
    @Override
    public final Object getViewUI() {
        return viewUrl;
    }

    @Override
    public final String getViewId() {
        return viewId;
    }

    @Override
    public String[] getViewDataIds() {
        return viewDataIds;
    }
    
    
    
    
}
