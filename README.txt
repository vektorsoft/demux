
                          DEMUX Framework

  What is it?
  -----------

  DEMUX Framework is Java framework for building modular applications that run on desktop, web 
  and mobile and embedded devices. Modular architecture is based on OSGI specification. By using 
  DEMUX Framework, developers can use the same source code to run applications on any platform.

  Documentation
  -------------

  The most up-to-date documentation can be found on community wiki at http://demux.vektorsoft.com/wiki


  System Requirements
  -------------------

  JDK:
    1.7 or higher with JavaFX 2.2 or higher. JavaFX is required for building desktop GUI applications.
  Android SDK:
    For developing DEMUX Android applications, Android SDK r20.0.3 or later  is required
  Memory:
    No minimum requirement.
  Operating System:
    Any operating system that supports JavaFX.

  Installing DEMUX Framework
  ----------------

 1) All DEMUX Framework artifacts are available from Maven Central repository, so no manual installation is required.
      
 2) If you want to install framework on your machine, simply download and unzip binary distribution archive. All versions
    are available at
    
      http://demux.vektorsoft.com/download

  Licensing
  ---------

  Please see the file called LICENSE

  DEMUX Framework URLS
  ----------

  Home Page:          http://demux.vektorsoft.com
  Downloads:          https://bitbucket.org/vektorsoft/demux/downloads
  Mailing Lists:      http://www.freelists.org/list/demux
  Source Code:        https://bitbucket.org/vektorsoft/demux/src
  Issue Tracking:     https://demux-jira.atlassian.net
  Wiki:               http://demux.vektorsoft.com/wiki
  Forum:	      http://demux.vektorsoft.com/forum
