/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#include <wx/filename.h>
#include <wx/dir.h>
#include "main.h"
#include "gui.h"
#include "parser.h"
#include "launcher.h"

using namespace std;

IMPLEMENT_APP_NO_MAIN(MyApp);
IMPLEMENT_WX_THEME_SUPPORT;

int main(int argc, char *argv[])
{
    // process exit code
    long result = 0;
    wxEntryStart( argc, argv );
    wxStandardPaths stdpaths =   wxStandardPaths::Get();
   
   wxString exePath = stdpaths.GetExecutablePath();
   // set working directory to current dir
   wxString dirPath = wxPathOnly(exePath);
   wxSetWorkingDirectory(dirPath);
   wxString* iniPath;
   Launcher launcher;
  
   // on Windows, replace .exe with .ini
   int count = 0;
   count = exePath.Replace(".exe", ".ini");
   
   if(count == 0){
       // if no replacement, we're not on Windows. Just append .ini
       iniPath = new wxString(exePath.Append(wxT(".ini")));
   } else {
       iniPath = new wxString(exePath);
   }
   
   if(!wxFileName::FileExists(*iniPath)){
       wxMessageDialog *dial = new wxMessageDialog(NULL, 
      wxT("Could not find launcher .ini file!"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return -1;
   }
   Parser* parser = new Parser(iniPath);
   
   wxString javaHome = parser->getJavaHome();
   // check if DEMUX home directory exists
   wxString path = wxGetHomeDir() + wxT(FILE_SEPARATOR) + wxT(DEMUX_HOME) + wxT(FILE_SEPARATOR) + "jre";
   if(wxDir::Exists(path)){
       parser->setJavaHome(path);
   }
   javaHome = parser->getJavaHome();
   if(javaHome.empty()){
       // try to run from cmd line
       result = launcher.launch(parser);
       // exit on success
       if(result == 0){
           return 0;
       }
   }
   if(javaHome.empty() && result < 0){
       wxTheApp->SetClientData(parser);
       wxTheApp->CallOnInit();
       wxTheApp->OnRun();
   }
   // try to launch again, assume java home is set
   parser = new Parser(iniPath);
   launcher.launch(parser);
   
    return 0;
}

bool MyApp::OnInit()
{
    LauncherGUI *simple = new LauncherGUI(wxT("DEMUX Framework Launcher"));
    simple->SetParser((Parser*)GetClientData());
    simple->Show(true);

    return true;
}
