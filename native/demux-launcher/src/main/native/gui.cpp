/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#include <wx/url.h>
#include <wx/stdpaths.h>
#include <wx/wfstream.h>
#include <wx/zipstrm.h>
#include <wx/progdlg.h>
#include <wx/platinfo.h>
#include <wx/filename.h>
#include <wx/utils.h> 
#include <wx/dir.h>
#include <wx/dirdlg.h>
#include <memory>

#include "gui.h"

#define JRE_DEFAULT_DIR "jre"
#define TMP_JRE_FILE "tempjre.zip"

using namespace std;

LauncherGUI::LauncherGUI(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition)
{
    
    wxPanel* panel =  new wxPanel(this, wxID_ANY);
    wxBoxSizer* vbox = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *hbox1 = new wxBoxSizer(wxHORIZONTAL);
  wxBoxSizer *hbox2 = new wxBoxSizer(wxHORIZONTAL);
    
    wxString txt = wxT("This program requires Java Runtime Environment (JRE) version 8 or higher to run, "
            "but it was not found on your computer. You can do one of the follwing:");
    
    wxStaticText* st = new wxStaticText(panel, wxID_ANY,txt);
    st->Wrap(400);
    vbox->Add(st,0,wxALIGN_LEFT | wxEXPAND);
    
    
    const wxString options[] = {wxT("Download and install JRE automatically (recommended)"),wxT("Specify JRE location mannually"), wxT("Do nothing and quit")};
    rbox = new wxRadioBox(panel, wxID_ANY, "Select Action",wxDefaultPosition,wxDefaultSize,3,options,0, wxRA_SPECIFY_ROWS);
    vbox->Add(rbox,0,wxALIGN_LEFT | wxEXPAND,40);
    
     wxButton *contButton = new wxButton(panel, BUTTON_ID, wxT("Continue"));
     contButton->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(LauncherGUI::OnContinueAction),NULL,this);
     
     hbox1->Add(new wxPanel(panel, wxID_ANY));
     vbox->Add(hbox1, 10, wxEXPAND);
     hbox2->Add(contButton);
     
     vbox->Add(hbox2, 0, wxALIGN_RIGHT | wxRIGHT | wxBOTTOM, 10);
    
    panel->SetSizer(vbox);
    
    
  Centre();
  
}

void LauncherGUI::SetParser(Parser* p){
    parser = p;
}

void LauncherGUI::OnContinueAction(wxCommandEvent& WXUNUSED(event)){
    wxDirDialog * dirDialog;
    wxString path;
    
    int selOption = rbox->GetSelection();
    int ok = -1;
    
    switch(selOption){
        case 0:
            ok = DownloadJRE();
            if(ok == 0){
                unpackJre();
            }
            Close(true);
            break;
        case 1:
            dirDialog = new wxDirDialog(this);
            dirDialog->ShowModal();
            path = dirDialog->GetPath();
            parser->setJavaHome(path);
            delete dirDialog;
            Close(true);
            break;
        case 2:
            Close(true);
            break;
    }
    
    
}

int LauncherGUI::DownloadJRE(){
    int result = 0;
    wxStandardPaths stdpaths =   wxStandardPaths::Get();
    wxPlatformInfo info = wxPlatformInfo::Get();
    
    wxString path = parser->getDownloadLink();
    if(!parser->getDownloadLink().EndsWith(wxT("/"))){
        path += wxT("/jre-");
    }
    
    wxOperatingSystemId osId = info.GetOperatingSystemId();
    wxArchitecture archId = info.GetArchitecture();
    
    switch(osId){
        case wxOS_UNIX:
        case wxOS_UNIX_LINUX:
            path += wxT("linux");
            if(archId == wxARCH_64){
                path += wxT("-x64");
            }
            break;
        case wxOS_WINDOWS:
        case wxOS_WINDOWS_9X:
        case wxOS_WINDOWS_NT:
            path += wxT("win");
            if(archId == wxARCH_64){
                path += wxT("-x64");
            }
            break;
        case wxOS_MAC:
        case wxOS_MAC_OS:
        case wxOS_MAC_OSX_DARWIN:
            path += wxT("mac");
            break;
    }
    path += wxT(".zip");
    wxURL url(path);
    
    // create temp file name
    tmpFileName = stdpaths.GetTempDir() + + wxT(FILE_SEPARATOR) + wxT(TMP_JRE_FILE);
    wxFileOutputStream* os = new wxFileOutputStream(tmpFileName);

    if(url.GetError() == wxURL_NOERR){
        wxInputStream *in = url.GetInputStream();
        
        if(in && in->IsOk()){
            unsigned char tempbuf[BUFSIZE];
             size_t total_len = in->GetSize();
            size_t data_loaded = 0;
            bool abort = false;
            float totalMB = (float)total_len/(float)1000000;
            
             wxProgressDialog progress( wxT("Downloading JRE..."), wxT("Download in progress"), total_len, this, wxPD_CAN_ABORT|wxPD_AUTO_HIDE); 
             while(in->CanRead() && !in->Eof() && !abort){
                 in->Read(tempbuf, BUFSIZE);
                 size_t readlen = in->LastRead();
                if( readlen>0 )   {
                    os->Write(tempbuf, readlen);
                   data_loaded += readlen;
                }
                 
                 if(total_len > 0){
                     // if we know content length
                     wxString msg;
                     msg.Printf( wxT("Downloaded %.2f of %.2f MB"), (float)((float)data_loaded/(float)1000000), totalMB);
                     abort = !progress.Update( data_loaded, msg );
                 } else {
                     // if we don't know the length of the file, just Pulse
                    abort = !progress.Pulse();
                 }
                 
                 if( abort ) {
                   wxLogMessage( wxT("Download was cancelled.") );
                } 
             }
             // HACK!!! - this will close progress dialog
             progress.Update(data_loaded,  wxT("Please wait... Installing"));
             
        }
        delete in;
    } else {
        result = -1;
    }
    os->Close();
    delete os;
    return result;
}

void LauncherGUI::unpackJre() {
    
    // determine target directory
    wxString homeDir = wxGetHomeDir();
    wxString targetDir = homeDir + wxT(FILE_SEPARATOR) + wxT(DEMUX_HOME) + wxT(FILE_SEPARATOR);
    wxString topdir = wxT("");

    // create target dir if it does not exist
    if (!wxDir::Exists(targetDir)) {
        wxDir::Make(targetDir);
    }
    

    // go through zip file entries
    auto_ptr<wxZipEntry> entry;
    
    wxFFileInputStream in(tmpFileName);
    wxZipInputStream zip(in);
    int total = zip.GetTotalEntries();
    wxProgressDialog progress( wxT("Installing JRE..."), wxT("Installation in progress"), total, this); 
    int entryCount = 0;
    while (entry.reset(zip.GetNextEntry()), entry.get() != NULL) {
        // access meta-data
        wxString name = entry->GetName();
        // first, create all needed directories
        if(entry->IsDir()){
            wxDir::Make(targetDir + name, wxS_DIR_DEFAULT, wxPATH_MKDIR_FULL);
             entryCount++;
            if(entryCount > total){
                entryCount = total;
            }
            progress.Update(entryCount,"Installation in progress");
        } 
       
    }

    wxFFileInputStream in1(tmpFileName);
    wxZipInputStream zip1(in1);
    while (entry.reset(zip1.GetNextEntry()), entry.get() != NULL) {
        // access meta-data
        wxString name = entry->GetName();
        // check if entry is file
        if(!(entry->IsDir())){
            
            zip1.OpenEntry(*entry.get());
            wxFileName fname(targetDir + name);
            wxFileOutputStream file(targetDir + name);
            zip1.Read(file);
            fname.SetPermissions(entry->GetMode());
            entryCount++;
            if(entryCount > total){
                entryCount = total;
            }
            progress.Update(entryCount,"Installation in progress");
        } 
        
    }
    // save JRE location
    parser->setJavaHome(targetDir + JRE_DEFAULT_DIR);

}


