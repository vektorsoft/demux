/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#include <wx/process.h>
#include <wx/utils.h> 
#include "launcher.h"

// substring to recognize java 8 version
#define JAVA8_SUBSTRING "1.8"

long Launcher::launch(Parser* parser){
    
    long status = 0;
    wxString cmdLine = parser->getCmdLine();
    wxString javaHome = parser->getJavaHome();
    
    if(checkVersion(parser) < 0){
        return -1;
    }
    
    if(javaHome.empty()){
        status = wxExecute(cmdLine, wxEXEC_SYNC);
    } else {
        status = wxExecute(javaHome + FILE_SEPARATOR + JRE_BIN_DIR + FILE_SEPARATOR  + cmdLine, wxEXEC_SYNC);
    }
    
    return status;
    
}

int Launcher::checkVersion(Parser* parser){
    wxString cmd;
    if(parser->getJavaHome().empty()){
        cmd = wxT("java -version");
    } else {
        cmd = parser->getJavaHome() + FILE_SEPARATOR + JRE_BIN_DIR + FILE_SEPARATOR  + wxT("java -version");
    }
    

    wxArrayString out;
    wxArrayString err;
    wxExecute(cmd, out, err);
    
    // examine  output, try to find string '1.8. for Java 8
     wxString line;
    if(err.GetCount() > 0){
      line = err.Item(0);
    } else if (out.GetCount() > 0){
      line = out.Item(0);
    } else {
      return -1;
    }
    line = err.Item(0);
    if(line.Find(wxT(JAVA8_SUBSTRING)) > 0){
        return 1;
    }

    return -1;
}
