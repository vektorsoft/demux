/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#ifndef PARSER_H
#define	PARSER_H

#include <wx/string.h>
#include <string>

#include "SimpleIni.h"

#define GROUP_GLOBAL "global"
#define INI_CMD_LINE "cmd_line"
#define INI_JAVA_HOME "java_home"
#define INI_DOWNLOAD_LINK "jre_download_link"

#ifdef OS_LINUX
#define GROUP_LOCAL "linux"
#elif OS_WINDOWS
#define GROUP_LOCAL "win"
#elif OS_MAC
#define GROUP_LOCAL "osx"
#endif

#define FILE_SEPARATOR "/"
#ifdef OS_WINDOWS
#undef FILE_SEPARATOR
#define FILE_SEPARATOR "\\"
#endif

#define JRE_BIN_DIR "bin"
#ifdef OS_MAC
#undef JRE_BIN_DIR
#define JRE_BIN_DIR "Contents/Home/bin"
#endif

using namespace std;

class Parser {
    
private:
    /** INI parser object. **/
    CSimpleIniA simpleIni;
    wxString* filePath;
    
    
     /**
     * Returns value for given key. This function will first look up key value in <code>global</code> group, 
     * and then in local group. Value in local group, if present, takes precedence.
     * 
     * @param key name
     * @return key value as a string
     */
    string getActualValue(string);
    
public:
    
    Parser(wxString*);
    
    /**
     * Gets command line for starting Java program.
     * 
     * @return command line string
     */
    wxString getCmdLine();
    
    /**
     * Returns path to Java installation directory.
     * 
     * @return path to Java installation dir, if specified
     */
    wxString getJavaHome();
    
    /**
     * Sets the path to Java installation directory
     * 
     * @param path to Java installation 
     */
    void setJavaHome(wxString);
    
    /**
     * Gets the URL from where to download JRE.
     * 
     * @return download URL
     */
    wxString getDownloadLink();
    
};



#endif	/* PARSER_H */

