/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#include <wx/dir.h>
#include "parser.h"

using namespace std;

Parser::Parser(wxString* path){
    filePath = path;
    // initialize parser
    simpleIni.SetUnicode();
    simpleIni.LoadFile(path->c_str().AsChar());
    
}

wxString Parser::getCmdLine(){
   return wxString(getActualValue(INI_CMD_LINE));
}

wxString Parser::getJavaHome(){
    wxString val = wxString(getActualValue(INI_JAVA_HOME));
    if(!wxDir::Exists(val)){
        val = wxString("");
    }
    return val;
}


void Parser::setJavaHome(wxString path) {
    int ret = simpleIni.SetValue(GROUP_GLOBAL, INI_JAVA_HOME, path.c_str().AsChar());
    if(ret >= 0){
        ret = simpleIni.SaveFile(filePath->c_str().AsChar(), false);
    }
}

wxString Parser::getDownloadLink(){
    return wxString(getActualValue(INI_DOWNLOAD_LINK));
}

string Parser::getActualValue(string key){
    
     // first, try to get key value from local group
    const char* val = simpleIni.GetValue(GROUP_LOCAL, key.c_str(), NULL);
    // if nothing there, try global group
    if(val == NULL){
        val = simpleIni.GetValue(GROUP_GLOBAL, key.c_str(), NULL);
    }
    string sval("");
    if(val != NULL){
        sval.append(val);
    }

    return sval;
}