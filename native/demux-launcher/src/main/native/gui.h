/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */

#include <wx/wx.h>

#include "parser.h"


#define BUFSIZE 8192
#define DEMUX_HOME ".demux"



/**
 * Main class for launcher GUI.
 */
class LauncherGUI : public wxFrame
{
    private:
        static const int BUTTON_ID = 456;
        wxRadioBox* rbox;
        wxString tmpFileName;
        Parser* parser;
        
public:
    /**
     * Creates new instance.
     * 
     * @param title window title
     */
    LauncherGUI(const wxString& title);
    
    /**
     * EVent handler for "Continue" button.
     * 
     * @param event
     */
    void OnContinueAction(wxCommandEvent&);
    
    /**
     * Download Java runtime from the URL specified in launcher.ini file.
     * 
     * @return 0 if operation was successful, -1 otherwise
     */
    int DownloadJRE();
    
    /**
     * Unpacks downloaded JRE into <code>.demux</code> folder inside user's home directory.
     */
    void unpackJre();
    
    /**
     * Set parser for this window.
     * 
     * @param parser
     */
    void SetParser(Parser*);

};


