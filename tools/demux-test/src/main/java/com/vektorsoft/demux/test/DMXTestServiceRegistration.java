/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.test;

import java.util.Dictionary;
import java.util.Properties;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Simple implementation of {@link ServiceRegistration} to be used for testing.
 *
 * @param <S> service type
 * @author Vladimir Djurovic
 */
public class DMXTestServiceRegistration<S> implements ServiceRegistration<S> {
    
    /** Service reference. */
    private ServiceReference<S> reference;
    
    /** Registration properties. */
    private Properties props;
    
    /**
     * Creates new instance.
     * 
     * @param ref service reference
     */
    public DMXTestServiceRegistration(ServiceReference<S> ref){
        this.reference = ref;
        props = new Properties();
    }

    @Override
    public ServiceReference<S> getReference() {
        return reference;
    }

    @Override
    public void setProperties(Dictionary<String, ?> dctnr) {
        while(dctnr.keys().hasMoreElements()){
            String key = dctnr.keys().nextElement();
            props.put(key, dctnr.get(key));
        }
    }

    @Override
    public void unregister() {
        
    }

}
