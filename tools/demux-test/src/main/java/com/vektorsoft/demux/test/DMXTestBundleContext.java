/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.test;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

/**
 * Simple implementation of {@code BundleContext} for use in unit tests. This class implements 
 * only partially methods from {@code BundleContext}.
 *
 * @author Vladimir Djurovic
 */
public class DMXTestBundleContext implements BundleContext {
    
    /** Map of registered services. */
    private Map<String, Object> serviceMap;
    
    /** Map of registered service references. */
    private Map<String, ServiceReference> serviceRefMap;
    
    /**
     * Creates new instance.
     */
    public DMXTestBundleContext(){
        serviceMap = new HashMap<String, Object>();
        serviceRefMap = new HashMap<String, ServiceReference>();
    }

    @Override
    public String getProperty(String string) {
        return null;
    }

    @Override
    public Bundle getBundle() {
        return null;
    }

    @Override
    public Bundle installBundle(String string, InputStream in) throws BundleException {
        return null;
    }

    @Override
    public Bundle installBundle(String string) throws BundleException {
        return null;
    }

    @Override
    public Bundle getBundle(long l) {
        return null;
    }

    @Override
    public Bundle[] getBundles() {
        return null;
    }

    @Override
    public void addServiceListener(ServiceListener sl, String string) throws InvalidSyntaxException {
        
    }

    @Override
    public void addServiceListener(ServiceListener sl) {
        
    }

    @Override
    public void removeServiceListener(ServiceListener sl) {
        
    }

    @Override
    public void addBundleListener(BundleListener bl) {
        
    }

    @Override
    public void removeBundleListener(BundleListener bl) {
        
    }

    @Override
    public void addFrameworkListener(FrameworkListener fl) {
        
    }

    @Override
    public void removeFrameworkListener(FrameworkListener fl) {
        
    }

    @Override
    public ServiceRegistration<?> registerService(String[] strings, Object o, Dictionary<String, ?> dctnr) {
        for(String name : strings){
            serviceMap.put(name, o);
            DMXTestServiceReference ref = new DMXTestServiceReference(dctnr);
            serviceRefMap.put(name, ref);
        }
        return null;
    }

    @Override
    public ServiceRegistration<?> registerService(String string, Object o, Dictionary<String, ?> dctnr) {
        return registerService(new String[]{string}, o, dctnr);
    }

    @Override
    public <S> ServiceRegistration<S> registerService(Class<S> type, S s, Dictionary<String, ?> dctnr) {
        serviceMap.put(type.getName(), s);
        DMXTestServiceReference ref = new DMXTestServiceReference(dctnr);
        serviceRefMap.put(type.getName(), ref);
        return null;
    }

    @Override
    public ServiceReference<?>[] getServiceReferences(String string, String string1) throws InvalidSyntaxException {
        return null;
    }

    @Override
    public ServiceReference<?>[] getAllServiceReferences(String string, String string1) throws InvalidSyntaxException {
        return null;
    }

    @Override
    public ServiceReference<?> getServiceReference(String name) {
        return serviceRefMap.get(name);
    }

    @Override
    public <S> ServiceReference<S> getServiceReference(Class<S> type) {
        String name = type.getName();
        return serviceRefMap.get(name);
    }

    @Override
    public <S> Collection<ServiceReference<S>> getServiceReferences(Class<S> type, String string) throws InvalidSyntaxException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public <S> S getService(ServiceReference<S> sr) {
        for(Map.Entry<String, ServiceReference> entry : serviceRefMap.entrySet()){
            if(sr.equals(entry.getValue())){
                return (S)serviceMap.get(entry.getKey());
            }
        }
        return null;
    }

    @Override
    public boolean ungetService(ServiceReference<?> sr) {
        return false;
    }

    @Override
    public File getDataFile(String string) {
        return null;
    }

    @Override
    public Filter createFilter(String string) throws InvalidSyntaxException {
        return null;
    }

    @Override
    public Bundle getBundle(String string) {
        return null;
    }

}
