/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.test;

import org.junit.Test;
import static org.junit.Assert.*;
import org.osgi.framework.ServiceReference;

/**
 * Contains unit tests for {@link DMXTestBundleContext} class.
 *
 * @author Vladimir Djurovic
 */
public class DMXTestBundleContextTest {
    
   

    /**
     * Verifies that service is correctly registered.
     */
    @Test
    public void testRegisterService(){
        
        DMXTestBundleContext ctx = new DMXTestBundleContext();
        TestService svc = new TestService();
        ctx.registerService("my.service", svc, null);
        
        // verify that service is registered
        ServiceReference ref = ctx.getServiceReference("my.service");
        TestService out = (TestService)ctx.getService(ref);
        
        assertNotNull("Out service is null", out);
        assertEquals("Service objects mismatch", svc, out);
    }
    
    /**
     * Simple service to be used for testing.
     */
    private class TestService {
        
        /**
         * Trivial test method.
         * 
         * @return hello
         */
        public String hello(){
            return "hello";
        }
    }
}