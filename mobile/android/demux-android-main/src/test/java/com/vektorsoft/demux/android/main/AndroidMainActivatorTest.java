/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2013 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.android.main;

import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;

/**
 * Provides unit tests for {@link AndroidMainActivator} class.
 *
 * @author Vladimir Djurovic
 */
public class AndroidMainActivatorTest {
    
    public AndroidMainActivatorTest() {
    }

    /**
     * <p>
     *  Verifies correct functioning of {@link AndroidMainActivator#start(org.osgi.framework.BundleContext) } method. This method
     * should register {@link DMXMainActivityService}. Steps taken in this test:
     * </p>
     * <ul>
     *  <li>Create an instance of {@link AndroidMainActivator}</li>
     *  <li>create mock objects for {@code BundleContext} and {@code DMXMainActivityService}
     * <li>call start method and verify that {@code registerService} method is called for {@code BundleContext}
     * </ul>
     */
    @Test
    @Ignore
    public void testStart() throws Exception {
        DMXMainActivityService svc = Mockito.mock(DMXMainActivityService.class);
        BundleContext ctx = Mockito.mock(BundleContext.class);
        AndroidMainActivator activator = new AndroidMainActivator(svc);
        activator.start(ctx);
        
        Mockito.verify(ctx).registerService(DMXMainActivityService.class, svc, null);
    }

   
}