/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.main.svc;

import android.support.v4.app.FragmentActivity;
import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import com.vektorsoft.demux.android.main.DMXMainActivity;

/**
 * Default implementation pf {@link DMXMainActivityService}.
 * 
 * Module: demux-android-main
 *
 * @author Vladimir Djurovic
 */
public class DMXMainActivityServiceImpl implements DMXMainActivityService {
    
    /** Main activity for application. */
    private final DMXMainActivity activity;

    /**
     * Creates new instance.
     * @param activity 
     */
    public DMXMainActivityServiceImpl(DMXMainActivity activity){
        this.activity = activity;
    }

    @Override
    public FragmentActivity getMainActivity() {
        return activity;
    }
}
