/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.android.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import com.vektorsoft.demux.android.core.log.AndroidLogProducer;
import com.vektorsoft.demux.android.main.svc.DMXMainActivityServiceImpl;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.felix.framework.Felix;
import org.apache.felix.framework.util.FelixConstants;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleException;
import org.osgi.framework.Constants;
import org.osgi.framework.launch.Framework;

/**
 * Main {@link Activity} class for DEMUX Framework Android applications. This is
 * used as main application window and parent of all sub-views.
 *
 * @author Vladimir Djurovic
 */
public class DMXMainActivity extends FragmentActivity {
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(DMXMainActivity.LOG_TAG);

    /**
     * Logging tag.
     */
    private static final String LOG_TAG = "DMXMainActivity";
    /**
     * Name of bundle cache directory.
     */
    private static final String BUNDLE_CACHE_DIR = "bundle-cache";

    /**
     * Name of the file which contains list of bundle activators to invoke.
     */
    private static final String ACTIVATORS_LIST_FILE_NAME = "activators-list";

    /**
     * Amount of time to wait for OSGi framework shutdown (in ms).
     */
    private static final int WAIT_TIMEOUT = 100;

    /**
     * Instance of running OSGI framework.
     */
    private Framework framework;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set up logging. use reflection so we don't introduce dependencies to demux-android-core
        DMXLoggerFactory.setLogPorducer(new AndroidLogProducer());
        
        
        // find current file system location
        String location = getFilesDir().getAbsolutePath();
        LOGGER.debug("Current application path: " + location);

        // create bundle cache directory
        File cacheDir = new File(location, BUNDLE_CACHE_DIR);
        if (!cacheDir.exists()) {
            boolean created = cacheDir.mkdir();
            if (!created) {
                throw new IllegalStateException("Could not create cache directory");
            }
        }

        // create OSGI runtime configuration parameters
        Map<String, Object> config = new HashMap<String, Object>();
        config.put(Constants.FRAMEWORK_STORAGE, cacheDir.getAbsolutePath());

        config.put(Constants.FRAMEWORK_BUNDLE_PARENT, Constants.FRAMEWORK_BUNDLE_PARENT_FRAMEWORK);
        config.put(FelixConstants.IMPLICIT_BOOT_DELEGATION_PROP, "false");
        LOGGER.debug("Configuration: " + config.toString());

        // create activator for this application
        List<BundleActivator> activators = new ArrayList<BundleActivator>();

        DMXMainActivityService activityService = new DMXMainActivityServiceImpl(DMXMainActivity.this);
        AndroidMainActivator activator = new AndroidMainActivator(activityService);
        activators.add(activator);

        // get the list of bundle activators from file
        try {
            InputStream is = getAssets().open(ACTIVATORS_LIST_FILE_NAME);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ((line = reader.readLine()) != null) {
                try {
                    Class clazz = Class.forName(line);
                    BundleActivator act = (BundleActivator) clazz.newInstance();
                    activators.add(act);
                    LOGGER.debug("Found activator " + line);
                    
                } catch (Exception ex) {
                    Log.e("Error loading activator class " + line, LOG_TAG, ex);
                }

            }
            reader.close();
        } catch (IOException ex) {
            LOGGER.error("Error reading activators list", ex);
        }

        config.put(FelixConstants.SYSTEMBUNDLE_ACTIVATORS_PROP, activators);

        //create and start framework
        framework = new Felix(config);

        try {
            framework.start();
        } catch (BundleException ex) {
            throw new IllegalStateException("Could not start framework: " + ex.getMessage());
        }
    }

    @Override
    protected final void onStop() {
        super.onStop();
        try {
            framework.stop();
            framework.waitForStop(WAIT_TIMEOUT);
        } catch (Exception ex) {
            LOGGER.error("Could not stop framework", ex);
        }
    }

}
