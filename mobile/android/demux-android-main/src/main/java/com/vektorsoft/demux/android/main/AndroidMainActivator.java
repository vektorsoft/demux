/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.main;

import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import java.util.Dictionary;
import java.util.Hashtable;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Activator for Android main bundle. This activator will register service for accessing 
 * main activity for application.
 *
 * @author Vladimir Djurovic
 */
public class AndroidMainActivator implements BundleActivator {
    
    /** Main activity for this application. */
    private final DMXMainActivityService mainActivityService;
    
    /**
     * Creates new instance.
     * @param service 
     */
    public AndroidMainActivator(DMXMainActivityService service){
        this.mainActivityService = service;
    }

    /**
     * Starts this bundle and registers {@link DMXMainActivityService}.
     * 
     * @param bc bundle context
     * @throws Exception  if an error occurs
     */
    @Override
    public void start(BundleContext bc) throws Exception {
        // register activity service
        Dictionary<String, Object> dict = new Hashtable<String, Object>();
        dict.put("dmx.android.activity", mainActivityService.getMainActivity());
        bc.registerService(DMXMainActivityService.class, mainActivityService, dict);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
        
    }

}
