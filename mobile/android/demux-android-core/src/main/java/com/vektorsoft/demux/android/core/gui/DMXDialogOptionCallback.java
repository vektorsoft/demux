/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.gui;

import android.support.v4.app.DialogFragment;

/**
 * This interface defines callback method which allow hooking into Android dialogs from controllers and
 * views. Method are defined for positive, negative and neutral buttons.
 *
 * @author Vladimir Djurovic
 */
public interface DMXDialogOptionCallback {

    /**
     * Invoked when user closes dialog by pressing positive feedback button.
     * 
     * @param fragment dialog fragment
     */
    void onPositiveButton(DialogFragment fragment);
    
    /**
     * Invoked when user closes dialog by pressing negative feedback button.
     * 
     * @param fragment dialog fragment
     */
    void onNegativeButton(DialogFragment fragment);
    
    /**
     * Invoked when user closes dialog by pressing neutral feedback button.
     * 
     * @param fragment dialog frgament
     */
    void onNeutralButton(DialogFragment fragment);
}
