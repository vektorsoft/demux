/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.bundle;

import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import com.vektorsoft.demux.android.core.resources.DMXAndroidLocaleManager;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 * Service customizer for {@link DMXMainActivityService}. 
 *
 * @author Vladimir Djurovic
 */
public class ActivityServiceCustomizer implements ServiceTrackerCustomizer<DMXMainActivityService, DMXMainActivityService> {
    
    /** Bundle context. */
    private BundleContext context;
    
    /**
     * Creates new instance.
     * 
     * @param context bundle context
     */
    public ActivityServiceCustomizer(BundleContext context){
        this.context = context;
    }

    /**
     * Invoked when service is being added. This method will create adapter to be used with application.
     * 
     * @param sr service reference
     * @return added service
     */
    @Override
    public DMXMainActivityService addingService(ServiceReference<DMXMainActivityService> sr) {
        
        DMXMainActivityService svc = (DMXMainActivityService)context.getService(sr);
//        
//        DMXResourceManager manager = new DMXAndroidLocaleManager(svc.getMainActivity());
//        DMXDefaultAdapter adapter = new DMXDefaultAdapter();
//        manager.getResourceHandler(ResourceHandlerType.ANDROID).addResourceListener(adapter);
//        DMXAndroidViewManager viewManager = new DMXAndroidViewManager(svc.getMainActivity(), adapter);
//        adapter.setViewManager(viewManager);
//        
//        context.registerService(DMXAdapter.class, adapter, null);
        
        return svc;
    }

    @Override
    public void modifiedService(ServiceReference<DMXMainActivityService> sr, DMXMainActivityService t) {
        
    }

    @Override
    public void removedService(ServiceReference<DMXMainActivityService> sr, DMXMainActivityService t) {
        
    }

}
