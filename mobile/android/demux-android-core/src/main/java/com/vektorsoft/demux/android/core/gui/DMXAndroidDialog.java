/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.gui;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;


/**
 * Implementation of {@link DMXDialog} for Android. This dialog can be used to display 
 * custom view and custom controls. It is asynchronous, meaning that it will not block main
 * UI thread when displayed.
 * 
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidDialog extends Dialog implements DMXDialog {
    
    /** View which will be displayed in this dialog. */
    private DMXDialogView view;
    
    /**
     * Creates new instance.
     * 
     * @param activity parent activity
     * @param view view to display
     */
    public DMXAndroidDialog(Activity activity, DMXDialogView view){
        super(activity);
        this.view = view;
        setTitle(view.getDialogTitle());
        setOwnerActivity(activity);
        setContentView((View)view.getViewUI());
        view.setDialog(this);
    }
    
    public DMXAndroidDialog(Activity activity, DMXDialogView view, String altTitle){
        this(activity, view);
        setTitle(altTitle);
    }

    @Override
    public int showDialog() {
        show();
        return DMXDialog.DLG_OPTION_CLOSED;
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    @Override
    public DMXDialogView getDialogView() {
        return view;
    }

}
