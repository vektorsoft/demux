/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.bundle;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import java.util.Locale;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class LocaleRegistrationCustomizer implements ServiceTrackerCustomizer<Locale, Locale>{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(LocaleRegistrationCustomizer.class);
    
    private final DMXLocaleManager resourceManager;
    private final BundleContext bundleContext;
    
    public LocaleRegistrationCustomizer(DMXLocaleManager resourceManager, BundleContext bc){
        this.resourceManager = resourceManager;
        this.bundleContext = bc;
    }

    @Override
    public Locale addingService(ServiceReference<Locale> sr) {
        Locale locale = bundleContext.getService(sr);
        if(sr.getProperty(DMXCoreConstants.PROP_LOCALE_ADDITIONAL) != null){
            Locale[] additional = (Locale[])sr.getProperty(DMXCoreConstants.PROP_LOCALE_ADDITIONAL);
            Locale[] allLoc = new Locale[additional.length + 1];
            allLoc[0] = locale;
            System.arraycopy(additional, 0, allLoc, 1, additional.length);
            resourceManager.setSupportedLocales(allLoc);
            resourceManager.setCurrentLocale(locale);
        } else {
            resourceManager.addSupportedLocale(locale);
        }
        
        if("true".equals(sr.getProperty(DMXCoreConstants.PROP_LOCALE_CURRENT))){
            resourceManager.setCurrentLocale(locale);
        }
        LOGGER.debug("Registered locale " + locale);
        return locale;
    }

    @Override
    public void modifiedService(ServiceReference<Locale> sr, Locale t) {
        
    }

    @Override
    public void removedService(ServiceReference<Locale> sr, Locale t) {
        
    }

}
