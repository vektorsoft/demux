/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.resources;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.vektorsoft.demux.core.resources.DMXResourceHandler;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Implementation of {@link DMXResourceHandler} interface specific for Android. This handler loads 
 * resource from Android resource system.
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidResourceHandler implements DMXResourceHandler{
    
    /** Logging tag. */
    private static final String LOG_TAG = "DMXAndroidResourceHandler";
    
    /** Android application context. */
    private Context context;
    
    /** Currently set locale. */
    private Locale currentLocale;
    
    /** 
     * Map of resource types. Each mapping contains of resource type, followed
     * by resource name and it's ID mapping.
     * Resource type is a string representing R class name of one of the valid 
     * Android resource types.
     */
    private final Map<String, Map<String, Integer>> resourceMap;
    
    
    /** R class object. */
    private Class rClazz;
    
    /** Classes for Android resource types (string,layouts etc.). */
    private Class[] resourceClasses;
    
    
    /**
     * Creates new handler instance.
     * 
     * @param context app context
     */
    public DMXAndroidResourceHandler(Context context){
        this.context = context;
        resourceMap = Collections.synchronizedMap(new HashMap<String, Map<String, Integer>>());
        StringBuilder sb =  new StringBuilder(context.getPackageName());
        sb.append(".R");
        Log.d(LOG_TAG, sb.toString());
        try {
            rClazz = Class.forName(sb.toString());
            resourceClasses = rClazz.getDeclaredClasses();
        } catch(ClassNotFoundException ex){
            Log.e(LOG_TAG, "Could not find class" + sb.toString(), ex);
        }
        
        initResources(sb.toString());
        
        
    }
    
    /**
     * Initializes resources by determining ID for each declared resource and caching
     * them for later retrieval.
     * 
     * @param rClassName R class name
     */
    private void initResources(final String rClassName) {
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if(rClazz == null){
                        rClazz = Class.forName(rClassName);
                    }
                    
                    if(resourceClasses == null){
                        resourceClasses = rClazz.getDeclaredClasses();
                    }
                    
                    for (Class cl : resourceClasses) {
                        String name = cl.getSimpleName();
                        Map<String, Integer> map = new HashMap<String, Integer>();
                        Field[] fields = cl.getFields();
                        for (Field f : fields) {
                            Integer val = f.getInt(cl);
                            map.put(f.getName(), val);
                        }
                        resourceMap.put(name, map);
                    }

                } catch (ClassNotFoundException ex) {
                    Log.e(LOG_TAG, "Could not find class" + rClassName, ex);
                } catch (IllegalAccessException ex) {
                    Log.e(LOG_TAG, "Could not access field", ex);
                }
            }
        });
        th.start();
    }

    @Override
    public void setCurrentLocale(Locale locale) {
        this.currentLocale = locale;
    }

    /**
     * Returns string resource with specified name.
     * 
     * @param name resource name
     * @return string value of the resource
     */
    @Override
    public String getString(String name) {
        Integer id = getResourceId("string", name);
        return context.getString(id);
    }
    
    /**
     * Returns string defined in a resource with specified name. If this resource does not exist,
     * returns the string specified as {@code fallback} parameter.
     * 
     * @param name resource name
     * @param fallback default string to return if resource is not found
     * @return resource string
     */
    public String getString(String name, String fallback){
        Integer id = getResourceId("string", name);
        if(id != 0){
            return context.getString(id);
        } else {
            return fallback;
        }
        
    }
    
    
    /**
     * Returns string array with a given name.
     * 
     * @param name resource name
     * @return string array
     */
    public String[] getStringArray(String name){
        Integer id = getResourceId("array", name);
        return context.getResources().getStringArray(id);
    }
    
    /**
     * Returns a quantity string with a given name. Caller must also provide desired 
     * {@code quantity}, and optional formatting arguments.
     * 
     * @param name resource name
     * @param quantity quantity
     * @param args formatting arguments
     * @return plural string, optionally formatted
     */
    public String getQuantityString(String name, int quantity, Object... args){
        Integer id = getResourceId("plurals", name);
        return context.getResources().getQuantityString(id, quantity, args);
    }

    /**
     * Formats string with specified parameters.
     * 
     * @param name resource name
     * @param args arguments for formatting
     * @return  formatted string
     */
    @Override
    public String formatMessage(String name, Object... args) {
        Integer id = getResourceId("string", name);
        return context.getString(id, args);
    }
    
    /**
     * Returns Android animation resource, based on it's type and name. Parameter {@code type}
     * should be one of the following values:
     * 
     * <ul>
     *  <li>
     * {@link AndroidResourceTypeEnum#PROPERTY_ANIMATION} - look for resource 
     * with name {@code name} in folder <code>res/animator</code>
     * </li>
     * <li>
     *  {@link AndroidResourceTypeEnum#TWEEN_ANIMATION} - look for resource with name {@code name} 
     * in folder <code>res/anim</code>
     * </li>
     * <li>
     * {@link AndroidResourceTypeEnum#FRAME_ANIMATION} - look for resource with name {@code name} 
     * in folder <code>res/drawable</code>
     * </li>
     * </ul>
     * 
     * @param type type if animation
     * @param name resource name
     * @return animation resource
     */
    public XmlResourceParser getAnimation(AndroidResourceTypeEnum type, String name){
        Integer id = getResourceId(type.getResourceClassName(), name);
        return context.getResources().getAnimation(id);
    }
    
    /**
     * Returns Android boolean resource.
     * 
     * @param name resource name
     * @return boolean resource
     */
    public boolean getBoolean(String name){
        Integer id = getResourceId("bool", name);
        return context.getResources().getBoolean(id);
    }
    
    /**
     * Returns a color with a given name.
     * 
     * @param name color name
     * @return integer representing color
     */
    public int getColor(String name){
        Integer id = getResourceId("color", name);
        return context.getResources().getColor(id);
    }
    
    /**
     * Returns a color state list with a given name.
     * 
     * @param name resource name
     * @return color state list
     */
    public ColorStateList getColorStateList(String name){
         Integer id = getResourceId("color", name);
        return context.getResources().getColorStateList(id);
    }
    
    /**
     * Returns a drawable with a given name.
     * 
     * @param name drawable name
     * @return  drawable instance
     */
    public Drawable getDrawable(String name){
        Integer id = getResourceId("drawable", name);
        return context.getResources().getDrawable(id);
    }
    
    
    /**
     * Returns a layout with a given name.
     * 
     * @param name layout file name
     * @return layout resource
     */
    public XmlResourceParser getLayout(String name){
        Integer id = getResourceId("layout", name);
        return context.getResources().getLayout(id);
    }
    
    /**
     * Returns ID of the menu with a given name.
     * 
     * @param menu manu name
     * @return menu ID
     */
    public int getMenuId(String menu){
         Integer id = getResourceId("menu", menu);
        return id;
    }
    
    /**
     * Returns a value for dimension with name {@code name}.
     * 
     * @param name dimension name
     * @return dimension value
     */
    public float getDimension(String name){
        Integer id = getResourceId("dimen", name);
        return context.getResources().getDimension(id);
    }
    
    /**
     * Returns identifier for a given {@code name}.
     * 
     * @param name identifier resoruce name
     * @return identifier value
     */
    public int getId(String name){
        Integer id = getResourceId("id", name);
        return id;
    }
    
    /**
     * Returns value of integer resource with a given name.
     * 
     * @param name resource name
     * @return integer value
     */
    public int getInteger(String name){
        Integer id = getResourceId("integer", name);
        return context.getResources().getInteger(id);
    }
    
    /**
     * Returns an array of integer values for resource with name {@code name}.
     * 
     * @param name resource name
     * @return integer array
     */
    public int[] getIntegerArray(String name){
         Integer id = getResourceId("integer", name);
        return context.getResources().getIntArray(id);
    }
    
    /**
     * Returns typed array for a given name.
     * 
     * @param name resource name
     * @return typed array
     */
    public TypedArray getTypedArray(String name){
        Integer id = getResourceId("array", name);
        return context.getResources().obtainTypedArray(id);
    }
    
    /**
     * Find resource ID based on resource type and name.
     * 
     * @param type resource type (string, drawable, layout etc.)
     * @param name resource name
     * @return  resource ID
     */
    private int getResourceId(String type, String name) {
        int val = 0;
        if (resourceMap.containsKey(type) && resourceMap.get(type).containsKey(name)) {
            val = resourceMap.get(type).get(name);
        } else {
            synchronized (resourceMap) {
                for (Class clazz : resourceClasses) {
                    if (type.equals(clazz.getSimpleName())) {
                        try {
                            Field field = clazz.getField(name);
                            val = field.getInt(clazz);
                        } catch (NoSuchFieldException ex) {
                            Log.e(LOG_TAG, "Non existent field " + name, ex);
                        } catch (IllegalAccessException iex) {
                            Log.e(LOG_TAG, "Could not access field", iex);
                        }

                    }
                    break;
                }
            }
        }

        return val;
    }
    
}
