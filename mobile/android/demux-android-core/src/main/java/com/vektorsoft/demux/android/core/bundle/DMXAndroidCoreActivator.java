/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.bundle;

import android.support.v4.app.FragmentActivity;
import com.vektorsoft.demux.android.core.app.DMXAndroidApplication;
import com.vektorsoft.demux.android.core.app.DMXAndroidViewManager;
import com.vektorsoft.demux.android.core.app.DMXMainActivityService;
import com.vektorsoft.demux.android.core.exception.AndroidExceptionHandlerService;
import com.vektorsoft.demux.android.core.exception.DMXAndroidExceptionHandler;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.android.core.resources.DMXAndroidLocaleManager;
import com.vektorsoft.demux.core.app.DMXApplication;
import com.vektorsoft.demux.core.exception.DMXExceptionHandlerService;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXAdapter;
import com.vektorsoft.demux.core.mva.DMXController;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.eb.BasicEventBus;
import com.vektorsoft.demux.eb.DMXEventBus;
import java.util.Locale;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Filter;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Activator for Android Core bundle. This activator will register Android implementations of {@link DMXApplication} and 
 * {@link DMXAdapter}.
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidCoreActivator implements BundleActivator, ServiceListener {
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger("DMXAndroidCoreActivator");
    
    private static final String FILTER_CTRL_OBJECT = "(objectClass=" + DMXController.class.getName() + ")";
    private static final String FILTER_VIEW_OBJECT = "(objectClass=" + DMXView.class.getName() + ")";
    private static final String FILTER_LOCALE_OBJECT = "(objectClass=" + Locale.class.getName() + ")";
    private static final String FILTER_EXTENSION_CALLBACK_OBJECT = "(objectClass=" + DMXExtensionCallback.class.getName() + ")";
    
    private ServiceTracker<DMXController, DMXController> controllerTracker;
    private ServiceTracker<DMXView, DMXView> viewServiceTracker;
    private ServiceTracker<Locale, Locale> localeTracker;
    private ServiceTracker<DMXExtensionCallback, DMXExtensionCallback> callbackTracker;
    
    private BundleContext context;
    private DMXDefaultAdapter adapter;
    private DMXEventBus eventBus;
    private DMXAndroidLocaleManager localeManager;
    private DMXAndroidResourceHandler resourceHandler;
    private DMXAndroidViewManager viewManager;

    @Override
    public void start(BundleContext bc) throws Exception {
        this.context = bc;
        // register application service
        DMXApplication application = new DMXAndroidApplication();
        bc.registerService(DMXApplication.class, application, null);
        
        adapter = new DMXDefaultAdapter();
        eventBus = new BasicEventBus();
        adapter.setEventBus(eventBus);
        
        
        
        
        //set up view manager
        ServiceReference<DMXMainActivityService> ref = context.getServiceReference(DMXMainActivityService.class);
        if(ref != null && ref.getProperty("dmx.android.activity") != null){
            setUpTracker((FragmentActivity)ref.getProperty("dmx.android.activity"));
        } else {
            context.addServiceListener(this);
        }
        
        
        // register exception handler service
        DMXAndroidExceptionHandler exceptionHandler = new DMXAndroidExceptionHandler();
        AndroidExceptionHandlerService exceptionService = new AndroidExceptionHandlerService(exceptionHandler);
        bc.registerService(DMXExceptionHandlerService.class, exceptionService, null);
    }

    @Override
    public void stop(BundleContext bc) throws Exception {
//        activityServiceTracker.open();
        if(controllerTracker != null){
            controllerTracker.open();
        }
        if(viewServiceTracker != null){
            viewServiceTracker.close();
        }
        if(localeTracker != null){
            localeTracker.close();
        }
        if(callbackTracker != null){
            callbackTracker.close();
        }
    }

    @Override
    public void serviceChanged(ServiceEvent se) {
         if (se.getType() == ServiceEvent.REGISTERED && se.getServiceReference().getProperty("dmx.android.activity") != null) {
            LOGGER.debug("DMXMainActivityService  registered");
            setUpTracker((FragmentActivity)se.getServiceReference().getProperty("dmx.android.activity"));
        }
    }
    
    private void setUpTracker(FragmentActivity activity) {
        localeManager = new DMXAndroidLocaleManager(activity);
        localeManager.setEventBus(eventBus);
        resourceHandler = new DMXAndroidResourceHandler(activity);
        resourceHandler.setCurrentLocale(localeManager.getCurrentLocale());
        viewManager = new DMXAndroidViewManager(activity, resourceHandler);
        viewManager.setEventBus(eventBus);
        adapter.setViewManager(viewManager);
        // start controlers tracker
        try {
            Filter ctrlFilter = context.createFilter(FILTER_CTRL_OBJECT);
            controllerTracker = new ServiceTracker<DMXController, DMXController>(context, ctrlFilter, new ControllerRegistrationCustomizer(context, adapter, localeManager));
            controllerTracker.open();
        } catch(InvalidSyntaxException ex){
            LOGGER.error("Could not set up controller tracker", ex);
        }
        try {
            Filter viewFilter = context.createFilter(FILTER_VIEW_OBJECT);
            viewServiceTracker = new ServiceTracker(context, viewFilter, new ViewRegistrationCustomizer(viewManager, context, adapter, resourceHandler));
            viewServiceTracker.open();
        } catch (InvalidSyntaxException ex) {
            LOGGER.error("Could not register view service tracker", ex);
        }
        // start locale tracker
        try {
            Filter localeFilter = context.createFilter(FILTER_LOCALE_OBJECT);
            localeTracker = new ServiceTracker<Locale, Locale>(context, localeFilter, new LocaleRegistrationCustomizer(localeManager, context));
            localeTracker.open();
        } catch(InvalidSyntaxException ex){
            LOGGER.error("Could not set up locale tracker");
        }
         // start extensions tracker
        try {
            Filter extFilter = context.createFilter(FILTER_EXTENSION_CALLBACK_OBJECT);
            callbackTracker = new ServiceTracker<DMXExtensionCallback, DMXExtensionCallback>(context, extFilter, new CallbackRegistrationCustomizer(context, adapter));
            callbackTracker.open();
        } catch (InvalidSyntaxException ex) {
            LOGGER.error("Could not set up callback tracker");
        }
    }

}
