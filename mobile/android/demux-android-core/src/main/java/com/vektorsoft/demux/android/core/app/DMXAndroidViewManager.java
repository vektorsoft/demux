/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.app;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.LinearLayout;
import com.vektorsoft.demux.android.core.gui.DMXAndroidDialogFactory;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.mva.AbstractViewManager;
import com.vektorsoft.demux.core.extension.DMXExtendable;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.mva.DMXViewManager;
import com.vektorsoft.demux.core.task.GUIBlockingScope;
import static com.vektorsoft.demux.core.task.GUIBlockingScope.COMPONENTS;
import static com.vektorsoft.demux.core.task.GUIBlockingScope.TOP_LEVEL;
import static com.vektorsoft.demux.core.task.GUIBlockingScope.VIEW;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.EventBusAware;
import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import com.vektorsoft.demux.eb.event.ViewRegisteredEvent;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of {@link com.vektorsoft.demux.core.mva.DMXViewManager} for
 * Android platform. This implementation should be able to handle registering
 * views with activities in a manner consistent with the rest of the framework.
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidViewManager extends AbstractViewManager implements EventBusAware{
    
    /**
     * Logger for this class.
     */
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger("DMXAndroidViewManager");
    
     /**
     * Name of the root view.
     */
    public static final String DMX_ANDROID_ROOT_VIEW = "com.vektorsoft.demux.android.core.DMXAndroidViewManager.root";
    
     /** Regular expression pattern for matching integer numbers. */
    private static final Pattern NUM_PATTERN;

    /**
     * Application main activity.
     */
    private final FragmentActivity mainActivity;
    
    /**
     * Mapping of event registration class to methods.
     */
    private final Map<String, Method> registrationMethods;
    
    /** Dialog factory. */
    private final DMXAndroidDialogFactory dlgFactory;
    
    private final DMXAndroidResourceHandler resourceHandler;
    
    private DMXEventBus eventBus;
    
    /** Currently shown dialog, if any. */
    private DMXDialog curDialog;
    
    static {
        NUM_PATTERN = Pattern.compile("[0-9]+");
    }

    /**
     * Creates new instance.
     *
     * @param activity main activity
     * @param resourceHandler  resource manager
     */
    public DMXAndroidViewManager(FragmentActivity activity, DMXAndroidResourceHandler resourceHandler) {
        this.mainActivity = activity;
        this.resourceHandler = resourceHandler;
        registrationMethods = new HashMap<String, Method>();
        DMXAndroidRootView rootView = new DMXAndroidRootView(mainActivity);
        registerView(rootView);
        
        dlgFactory = new DMXAndroidDialogFactory(this, activity);
        dlgFactory.setResourceHandler(resourceHandler);
    }

    @Override
    protected void processDeferredViews(final String parentId, Set<String> childrenIds) {
        if (childrenIds != null && !childrenIds.isEmpty()) {
            for (final String id : childrenIds) {
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        DMXView view = getView(id);
                        getView(parentId).addChildView(view);
                    }
                });
            }
        }
    }

    /**
     * <p>
     *  Selects active view for display. If view is a dialog, it will be displayed within popup window.
     * </p>
     * <p>
     *  Parameter {@code params} controls the way view will be displayed. The following values are taken into account
     * by this implementation:
     * </p>
     * <ul>
     *  <li>key {@link DMXViewManager#PARAM_PROGRESS_DIALOG}, with values {@link DMXViewManager#VAL_DLG_SHOW} or {@link DMXViewManager#VAL_DLG_CLOSE}. 
     * If <code>VAL_DLG_SHOW</code> is specified, dialog will be displayed. Otherwise, it will be closed.</li>
     * </ul>
     * 
     * @see DMXViewManager#selectView(java.lang.String, java.util.Map) 
     * 
     * @param viewId view ID
     * @param params optional parameters
     */
    @Override
    public void selectView(final String viewId, final Map<String, Object> params) {
        mainActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (viewId != null) {
                     if (params != null && params.containsKey(DMXViewManager.PARAM_PROGRESS_DIALOG)) {
                         int val = (Integer) params.get(DMXViewManager.PARAM_PROGRESS_DIALOG);
                         if (val == DMXViewManager.VAL_DLG_SHOW) {
                             curDialog = dlgFactory.createDialog(viewId, null);
                             curDialog.showDialog();
                         } else {
                             curDialog.closeDialog();
                             curDialog = null;
                         }
                     } else {
                         DMXView view = registeredViews.get(viewId);
                         if(view == null){
                             return;
                         }
                         String parentId = view.getParentViewId();
                         // check if this view is dialog view
                         boolean isdialog = true;
                         try {
                             view.getClass().getDeclaredMethod("isModal");
                         } catch(NoSuchMethodException ex){
                             isdialog = false;
                         }
                         if (isdialog) {
                             String title = null;
                             if(params != null && params.containsKey(DMXViewManager.PARAM_DIALOG_TITLE)){
                                 title = (String)params.get(DMXViewManager.PARAM_DIALOG_TITLE);
                             }
                             
                             curDialog = dlgFactory.createDialog(viewId, title);
                             curDialog.showDialog();

                         } else {
                             DMXView parent = registeredViews.get(parentId);
                             boolean state = params != null && params.containsKey(DMXViewManager.PARAM_VIEW_STATE)
                                     ? (Boolean)params.get(DMXViewManager.PARAM_VIEW_STATE) : true;
                             parent.setViewActive(viewId, state);
                         }

                     }
                 }

            }
        });
    }


    @Override
    public final void registerView(final DMXView view) {
        super.registerView(view);
        if (view instanceof AndroidContextAware) {
            ((AndroidContextAware) view).setContext(mainActivity);
        }

        // add this view to it's parent
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.constructUI();
                final String parentId = view.getParentViewId();
                if (parentId != null && !parentId.isEmpty()) {
                    DMXView parent = getView(parentId);
                    if (parent != null) {
                        parent.addChildView(view);
                    }
                }

                afterInitialViewRegistration(view);
                LOGGER.info("Registered view " + view.getViewId());
                if (eventBus != null) {
                     if(view instanceof DMXEventHandler){
                        LOGGER.debug("Register view " + view.getViewId() + " as event handler");
                        eventBus.subscribe(new PlatformThreadEventHandler((DMXEventHandler)view, mainActivity));
                    }
                    LOGGER.debug("Publising view registration event for view ID " + view.getViewId());
                    ViewRegisteredEvent event = new ViewRegisteredEvent(view.getViewDataIds());
                    eventBus.publish(event);
                }
            }
        });

    }



    @Override
    public void blockGui(final boolean state, final GUIBlockingScope scope, final String... ids) {
        mainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (scope) {
                    case TOP_LEVEL:
                        // not implemented
                        break;
                    case VIEW:
                        // not implemented
                        break;
                    case COMPONENTS:
                        for (String id : ids) {
                            View v = mainActivity.findViewById(Integer.parseInt(id));
                            if (v != null) {
                                v.setEnabled(!state);
                            }
                        }
                        break;
                    default:
                }
            }
        });

    }

    @Override
    public DMXAndroidDialogFactory getDialogFactory() {
        return dlgFactory;
    }


    @Override
    protected void removeFromParent(final String viewId) {
        mainActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if(!registeredViews.containsKey(viewId)){
                    return;
                }
                final String parentId = registeredViews.get(viewId).getParentViewId();
                if (parentId != null && !parentId.isEmpty()) {
                    DMXView parent = getView(parentId);
                    if (parent != null) {
                        parent.removeChildView(viewId);
                    }
                }
            }
        });
    }

   

    @Override
    public void setEventBus(DMXEventBus bus) {
        this.eventBus = bus;
    }

    @Override
    protected void applyViewExtension(final String viewId, final DMXExtensionCallback callback) {
        mainActivity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                DMXView view = getView(viewId);
                if (view != null && view instanceof DMXExtendable) {
                    ((DMXExtendable) view).setExtensionCallback(callback);
                    LOGGER.debug("Set extension callback for view " + viewId);
                }
            }
        });
    }


    /**
     * <p>
     * Root view of Android application. This view allows adding multiple views, which are shown
     * in Z-order, ie. only lastly added view is visible.
     * </p>
     * <p>
     *  Additionally, view position can be controlled by a placement constraint. This is integer which
     * represents view position in Z-order. View with highest constraint will be visible.
     * </p>
     */
    private static class DMXAndroidRootView implements DMXView {

        /**
         * Application main activity.
         */
        private final Activity mainActivity;
        
        /** Z-order list of child views. */
        private final List<DMXView> zOrder;
        
        /** Indicates whether view is initialized. */
        private boolean initialized = false;
        
        /** Root layout. */
        private final LinearLayout root;
        
        /**
         * View comparator.
         */
        private final ViewComparator comparator;
        
        private final Map<String, View> viewUiMap;

        /**
         * Creates new instance.
         *
         * @param activity application main activity
         */
        public DMXAndroidRootView(Activity activity) {
            this.mainActivity = activity;
            zOrder = new ArrayList<DMXView>();
            comparator = new ViewComparator();
            root = new LinearLayout(this.mainActivity);
            viewUiMap = new HashMap<String, View>();
        }
        
        /**
         * Initializes the view.
         */
        private void initView() {
            if (!initialized) {
                root.setOrientation(LinearLayout.VERTICAL);
                mainActivity.setContentView(root);
                initialized = true;
            }
        } 

        @Override
        public String getViewId() {
            return DMX_ANDROID_ROOT_VIEW;
        }

        @Override
        public String getParentViewId() {
            return null;
        }

        @Override
        public Object viewPlacementConstraint() {
            return null;
        }

        @Override
        public void addChildView(DMXView child) {
            if (!(child.getViewUI() instanceof View)) {
                throw new IllegalArgumentException("View UI must be instance of android.view.View");
            }
            View viewUi = (View) child.getViewUI();
            initView();
            Object constraint = child.viewPlacementConstraint();

            if (constraint != null && constraint instanceof Integer) {
                zOrder.add(child);
                // sort view in ascending order
                Collections.sort(zOrder, comparator);
                // reorder views in stack, only the last view should be visible
                root.removeAllViews();
                root.addView((View) zOrder.get(zOrder.size() - 1).getViewUI());

            } else {
                root.addView(viewUi);
            }
            viewUiMap.put(child.getViewId(), viewUi);

        }

        @Override
        public void removeChildView(String childId) {
            View view = viewUiMap.get(childId);
            if(view != null){
                root.removeView(view);
                viewUiMap.remove(childId);
            }
        }
        

        @Override
        public String[] getViewDataIds() {
            return new String[0];
        }

      

        @Override
        public Object getViewUI() {
            return mainActivity;
        }

        @Override
        public void constructUI() {
            
        }

        @Override
        public void setViewActive(String id, boolean state) {
            for(DMXView view : zOrder){
            if(id.equals(view.getViewId())){
                root.removeAllViews();
                root.addView((View)view.getViewUI());
                break;
            }
        }
        }
        
    }
    
     /**
     * Comparator for comparing view Z-order.
     */
    private static class ViewComparator implements Comparator<DMXView> {

        @Override
        public int compare(DMXView o1, DMXView o2) {
            Object c1 = o1.viewPlacementConstraint();
            Integer z1 = 0;
            if(c1 instanceof Integer){
                z1 = (Integer)c1;
            }
            Object c2 = o2.viewPlacementConstraint();
            Integer z2 = 0;
            if(c2 instanceof Integer){
                z2 = (Integer)c2;
            }
            
            return z1.compareTo(z2);
        }
        
    }
    
    private static class PlatformThreadEventHandler implements DMXEventHandler {
        
        private final DMXEventHandler handler;
        private final FragmentActivity activity;
        
        PlatformThreadEventHandler(DMXEventHandler handler, FragmentActivity activity){
            this.handler = handler;
            this.activity = activity;
        }

        @Override
        public String[] getTopics() {
            return handler.getTopics();
        }

        @Override
        public void handle(final DMXEvent event) {
            activity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    handler.handle(event);
                }
            });
        }

        @Override
        public DMXEventFilter getFilter(String topicName) {
            return handler.getFilter(topicName);
        }
        
    }
}
