/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.app;

import android.content.Context;
import com.vektorsoft.demux.core.mva.DMXAbstractView;
import com.vektorsoft.demux.core.mva.DMXView;

/**
 * An implementation of {@link DMXView} for use on Android devices. This class implements some 
 * common methods to facilitate view creation.
 *
 * @author Vladimir Djurovic
 */
public abstract  class DMXAndroidAbstractView extends DMXAbstractView implements AndroidContextAware {
    
    /** Android application context. */
    protected Context context;
    
    /**
     * Creates new instance.
     * 
     * @param ids optional IDs of view model data
     */
    public DMXAndroidAbstractView(String... ids){
        super(ids);
    }

    
    @Override
    public void setContext(Context context) {
        this.context = context;
        
    }
    

}
