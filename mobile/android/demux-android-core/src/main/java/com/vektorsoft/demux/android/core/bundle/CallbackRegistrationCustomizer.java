/******************************************************************************
 * 
 * Copyright 2012 - 2015 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.bundle;

import com.vektorsoft.demux.core.app.DMXCoreConstants;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.mva.DMXDefaultAdapter;
import java.util.Arrays;
import java.util.Map;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

/**
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class CallbackRegistrationCustomizer implements ServiceTrackerCustomizer<DMXExtensionCallback, DMXExtensionCallback>{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(CallbackRegistrationCustomizer.class);

    private final BundleContext bundleContext;
    private final DMXDefaultAdapter adapter;
    
    public CallbackRegistrationCustomizer(BundleContext context, DMXDefaultAdapter adapter){
        this.bundleContext = context;
        this.adapter = adapter;
    }
    
    
    @Override
    public DMXExtensionCallback addingService(ServiceReference<DMXExtensionCallback> sr) {
        DMXExtensionCallback callback = bundleContext.getService(sr);
        // get registration data, if any
        Map<String, Object> data = (Map<String, Object>)sr.getProperty(DMXCoreConstants.PROP_CTRL_DATA);
        if(data != null && !data.isEmpty()){
            for(Map.Entry<String, Object> entry : data.entrySet()){
                adapter.registerData(entry.getKey(), entry.getValue());
            }
        }
        adapter.registerExtensionCallback(callback);
        LOGGER.debug("Registered extension callback for " + Arrays.toString(callback.getMappings()));
        
        return callback;
    }

    @Override
    public void modifiedService(ServiceReference<DMXExtensionCallback> sr, DMXExtensionCallback t) {
        // do nothing
    }

    @Override
    public void removedService(ServiceReference<DMXExtensionCallback> sr, DMXExtensionCallback t) {
        String[] dataIds = t.getCallbackData();
        for(String id : dataIds){
            adapter.unregisterData(id);
        }
        adapter.removeExtensionCallback(t);
        LOGGER.debug("Removed extension callback for " + Arrays.toString(t.getMappings()));
    }

}
