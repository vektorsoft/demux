/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.resources;

import android.content.Context;
import android.content.res.Configuration;
import com.vektorsoft.demux.core.extension.DMXExtensionCallback;
import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerFactory;
import com.vektorsoft.demux.core.resources.DMXLocaleEvent;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.eb.DMXEventBus;
import com.vektorsoft.demux.eb.DMXEventConstants;
import com.vektorsoft.demux.eb.EventBusAware;
import com.vektorsoft.demux.eb.event.DMXEvent;
import com.vektorsoft.demux.eb.event.DMXEventFilter;
import com.vektorsoft.demux.eb.event.DMXEventHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidLocaleManager implements DMXLocaleManager, EventBusAware, DMXEventHandler{
    
    private static final DMXLogger LOGGER = DMXLoggerFactory.getLogger(DMXAndroidLocaleManager.class);
    
    /** List of locales supported by application. */
    protected List<Locale> supportedLocales;
    
    /** Application's current locale. */
    protected Locale currentLocale;
    
    
    /**
     * App context.
     */
    protected Context context;
    
    private DMXEventBus eventBus;
    
    /**
     * Creates new instance.
     * 
     * @param context app context
     */
    public DMXAndroidLocaleManager(Context context){
        this.context = context;
        currentLocale = Locale.getDefault();
        supportedLocales = new ArrayList<Locale>();
       
    }

    @Override
    public void setCurrentLocale(Locale locale) {
         if(locale == null){
            throw new IllegalArgumentException("Locale cannot be null");
        }
        this.currentLocale = locale;
        // change application locale
        Configuration config = new Configuration();
        config.locale = this.currentLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        publishLocaleEVent();
    }

    @Override
    public Locale getCurrentLocale() {
        return currentLocale;
    }

    @Override
    public void setSupportedLocales(Locale[] locales) {
         if(locales == null){
            throw new IllegalArgumentException("Supported locales cannot be null");
        }
        supportedLocales.clear();
        supportedLocales.addAll(Arrays.asList(locales));
        publishLocaleEVent();
    }

    @Override
    public Locale[] getSupportedLocales() {
        return supportedLocales.toArray(new Locale[supportedLocales.size()]);
    }

    @Override
    public void addSupportedLocale(Locale locale) {
         if(locale == null){
            throw new IllegalArgumentException("Supported locale cannot be null");
        }
        supportedLocales.add(locale);
        publishLocaleEVent();
    }

    @Override
    public void clearSupportedLocales() {
        supportedLocales.clear();
        currentLocale = Locale.getDefault();
        publishLocaleEVent();
    }


    @Override
    public void registerCallbackResources(DMXExtensionCallback callback) {
//        resourceHandler.loadCallbackResources(callback);
    }
    
      private void publishLocaleEVent(){
         if(eventBus != null){
             LOGGER.debug("Publishing locale event");
            eventBus.publish(new DMXLocaleEvent(supportedLocales, currentLocale));
        }
    }

    @Override
    public void setEventBus(DMXEventBus bus) {
        this.eventBus = bus;
        this.eventBus.subscribe(this);
    }

    @Override
    public String[] getTopics() {
        return new String[]{DMXEventConstants.TOPIC_VIEW_REGISTERED};
    }

    @Override
    public void handle(DMXEvent event) {
         LOGGER.debug("Received view registration event");
        publishLocaleEVent();
    }

    @Override
    public DMXEventFilter getFilter(String topicName) {
        return null;
    }

}
