/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.exception;

import com.vektorsoft.demux.core.exception.DMXExceptionHandler;

/**
 * Uncaught exception handler for Android platform.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class DMXAndroidExceptionHandler implements DMXExceptionHandler{
    
    private boolean enabled = true;
    private Thread.UncaughtExceptionHandler original;

    @Override
    public String getHandlerID() {
        return "DMXAndroidExceptionHandler";
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String[] overrideIds() {
        return null;
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        if(original != null){
            original.uncaughtException(t, e);
        }
        System.exit(2);
    }

    @Override
    public void setOriginalSystemHandler(Thread.UncaughtExceptionHandler systemHandler) {
        this.original = systemHandler;
    }
    
    

}
