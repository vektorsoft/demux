/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
 
package com.vektorsoft.demux.android.core.gui;

import android.support.v4.app.FragmentActivity;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.dlg.DialogFactory;
import com.vektorsoft.demux.core.mva.DMXView;
import com.vektorsoft.demux.core.mva.DMXViewManager;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Factory for creating dialogs on Android.
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidDialogFactory implements DialogFactory {

    /** Map of previously created dialogs. */
    private final Map<String, DMXDialog> dialogs;
    
    /** View manager. */
    private final DMXViewManager viewManager;
    
    /** Parent activity. */
    private final FragmentActivity parent;
    
    /** Resource manager. */
    private DMXAndroidResourceHandler resourceHandler;

    /**
     * Creates new instance.
     * 
     * @param viewManager view manager
     * @param parent  parent activity
     */
    public DMXAndroidDialogFactory(DMXViewManager viewManager, FragmentActivity parent) {
        this.viewManager = viewManager;
        this.parent = parent;
        dialogs = Collections.synchronizedMap(new HashMap<String, DMXDialog>());
    }

    /**
     * Set resource manager for this factory.
     * 
     * @param resourceHandler resource manager
     */
    public void setResourceHandler(DMXAndroidResourceHandler resourceHandler) {
        this.resourceHandler = resourceHandler;
    }

    
    /**
     * Android-specific implementation
     * 
     * @param viewId ID of view to display
     * @param title alternate dialog title
     * @return dialog instance
     */
    @Override
    public DMXAndroidDialog createDialog(String viewId, String title) {
        DMXAndroidDialog dlg = null;
        DMXView view = viewManager.getView(viewId);
        if (view instanceof DMXDialogView) {
            synchronized (dialogs) {
                if (dialogs.containsKey(viewId)) {
                    dlg = (DMXAndroidDialog)dialogs.get(viewId);
                } else {
                    dlg = new DMXAndroidDialog(parent, (DMXDialogView) view);
                    dialogs.put(viewId, dlg);
                }
            }

        }
        return dlg;
    }
    
    
    @Override
    public DMXAndroidOptionsDialog createOptionsDialog(String viewId, String title, int options){
        DMXAndroidOptionsDialog dlg = null;
        DMXView view = viewManager.getView(viewId);
        if(view instanceof DMXDialogView){
            dlg = new DMXAndroidOptionsDialog(parent, resourceHandler, options, (DMXDialogView)view);
        }
        return dlg;
    }
    
    
    @Override
    public DMXAndroidOptionsDialog createOptionsDialog(String viewId, String title, String[] options){
        DMXAndroidOptionsDialog dlg = null;
        DMXView view = viewManager.getView(viewId);
        if(view instanceof DMXDialogView){
            dlg = new DMXAndroidOptionsDialog(parent, resourceHandler, options, (DMXDialogView)view);
        }
        return dlg;
    }

    @Override
    public DMXDialog createMessageDialog(String title, String message, int options) {
        return new DMXAndroidOptionsDialog(parent, resourceHandler, title, options, message, DMXDialog.MessageType.MESSAGE_TYPE_PLAIN);
    }

    @Override
    public DMXDialog createMessageDialog(String title, String message, int options, DMXDialog.MessageType type) {
        return new DMXAndroidOptionsDialog(parent, resourceHandler, title, options, message, type);
    }

    @Override
    public DMXDialog createProgressDialog(String viewId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
