/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.android.core.log;

import com.vektorsoft.demux.core.log.DMXLogger;
import com.vektorsoft.demux.core.log.DMXLoggerProducer;

/**
 * An implementation of {@link DMXLoggerProducer} based on Android {@code Log} class.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class AndroidLogProducer implements DMXLoggerProducer{

    @Override
    public DMXLogger createLogger(String name) {
        return new AndroidLogger(name);
    }

    @Override
    public DMXLogger createLogger(Class clazz) {
        return createLogger(clazz.getName());
    }
    
}
