/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/
package com.vektorsoft.demux.android.core.gui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.vektorsoft.demux.android.core.resources.AndroidResourceTypeEnum;
import com.vektorsoft.demux.android.core.resources.DMXAndroidResourceHandler;
import com.vektorsoft.demux.core.dlg.DMXDialog;
import com.vektorsoft.demux.core.dlg.DMXDialogView;
import com.vektorsoft.demux.core.resources.DMXLocaleManager;
import com.vektorsoft.demux.core.resources.ResourceHandlerType;

/**
 * Implementation of Android {@code AlertDialog} for use within the framework. This class provides common dialog types and some
 * additional method for easy integration with the framework. It is possible to display simple text messages or display {@code DMXDialogView}
 * instances within the dialog.
 *
 * @author Vladimir Djurovic
 */
public final class DMXAndroidOptionsDialog extends DialogFragment implements DMXDialog {

    /** Constant for negative button. */
    private static final int NEGATIVE = -1;
    
    /** Constant for positive button. */
    private static final int POSITIVE = 1;
    
    /** Constant for neutral button. */
    private static final int NEUTRAL = 0;
    
    /** Parent activity.  */
    private FragmentActivity parent;
    
    /** Dialog builder. */
    private AlertDialog.Builder builder;
    
    /** Resource manager. */
    private DMXAndroidResourceHandler resourceHandler;
    
    /** Dialog callback interface. */
    private DMXDialogOptionCallback callback;
    
    /** View to display. */
    private DMXDialogView view;
    
    /** Dialog tag. */
    private String tag;
    
    /**
     * Returns icon for specified message type.
     * 
     * @param type message type
     * @return  corresponding icon
     */
    private Drawable getMessageIcon(DMXDialog.MessageType type){
        Drawable out = null;
        switch(type){
            case MESSAGE_TYPE_ERROR:
                out = resourceHandler.getDrawable("demux_android_msg_icon_error");
                break;
            case MESSAGE_TYPE_WARNING:
                out = resourceHandler.getDrawable("demux_android_msg_icon_warning");
                break;
            case MESSAGE_TYPE_INFO:
                out = resourceHandler.getDrawable("demux_android_msg_icon_info");
                break;
            case MESSAGE_TYPE_QUESTION:
                out = resourceHandler.getDrawable("demux_android_msg_icon_question");
                break;
        }
        return out;
    }

    /**
     * Basic constructor for the class.
     * 
     * @param parent parent activity
     * @param resourceHandler resource manager
     */
    protected DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler) {
        this.parent = parent;
        this.resourceHandler = resourceHandler;
        builder = new AlertDialog.Builder(parent);
        callback = new DMXDefaultCallback();
    }

    /**
     * Create dialog with specified title.
     * 
     * @param parent parent activity
     * @param resourceHandler resource manager
     * @param title dialog title
     */
    protected DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler, String title) {
        this(parent, resourceHandler);
        builder.setTitle(title);
        // set tag as dialgo title, replacin spaces with underscores
        tag = title.replaceAll("\\s", "_");
    }

    /**
     * Creates new dialog with specified title, message and predefined options
     * set. Parameter {@code options} is defined in {@link DMXDialog}.
     *
     * @param parent parent activity
     * @param resourceHandler resource manager
     * @param title dialog title
     * @param options dialog options (OK, Cancel, etc.)
     * @param message message to display
     * @param type type of dialog to show
     */
    public DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler, String title, int options, String message, DMXDialog.MessageType type) {
        this(parent, resourceHandler, title);
        builder.setMessage(message);
        Drawable icon = getMessageIcon(type);
        if(icon != null){
            builder.setIcon(icon);
        }
        
        initButtons(options);
    }

    /**
     * Creates new dialog with specified title, message and option buttons.
     * Parameter {@code options} should be 1, 2 or 3 element array, which
     * contains text to be displayed on dialog positive, negative and neutral
     * buttons, respectively. Array elements can either be keys to resource
     * strings, or actual text.
     *
     * @param parent parent activity
     * @param resourceHandler resource manager
     * @param title dialog title
     * @param options text for positive, negative and neutral buttons
     * @param message message to display
     * @param type message type
     */
    public DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler, String title, String[] options, String message, DMXDialog.MessageType type) {
        this(parent, resourceHandler, title);
        builder.setMessage(message);
        Drawable icon = getMessageIcon(type);
        if(icon != null){
            builder.setIcon(icon);
        }
        initButtons(options);

    }

    /**
     * Create dialog with specified options which will display view specified as {@code dlgView} parameter. Dialog title is pulled from
     * view definition. Parameter {@code options} is an integer representing options defined in {@link DMXDialog}.
     * 
     * @param parent parent activity
     * @param resourceHandler resource manager
     * @param options options buttons
     * @param dlgView dialog view to display
     */
    public DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler, int options, DMXDialogView dlgView) {
        this(parent, resourceHandler, dlgView.getDialogTitle());
        builder.setView((View) dlgView.getViewUI());
        initButtons(options);
        this.view = dlgView;
    }

    /**
     * Create dialog with specified options which will display view specified as {@code dlgView} parameter. Dialog title is pulled from
     * view definition. Parameter {@code options} is an array whose elements represent a text to be displayed on dialog buttons. Thse can either
     * be keys for string resources, or actual text.
     * 
     * @param parent parent activity
     * @param resourceHandler resource manager
     * @param options options buttoms text
     * @param dlgView dialog view to display
     */
    public DMXAndroidOptionsDialog(FragmentActivity parent, DMXAndroidResourceHandler resourceHandler, String[] options, DMXDialogView dlgView) {
        this(parent, resourceHandler, dlgView.getDialogTitle());
        builder.setView((View) dlgView.getViewUI());
        initButtons(options);
        this.view = dlgView;
    }

    /**
     * Initialize dialog buttons from specified dialog option. Parameter
     * {@code options} should be one of the options defined in
     * {@link DMXDialog}.
     *
     * @param options dialog options
     */
    private void initButtons(int options) {
        switch (options) {
            case DMXDialog.OPTIONS_OK:
                builder.setPositiveButton(resourceHandler.getString("dialog_button_ok", "OK"), new DialogButtonListener(POSITIVE));
                break;
            case DMXDialog.OPTIONS_OK_CANCEL:
                builder.setPositiveButton(resourceHandler.getString("dialog_button_ok", "OK"), new DialogButtonListener(POSITIVE));
                builder.setNegativeButton(resourceHandler.getString("dialog_button_cancel", "Cancel"), new DialogButtonListener(NEGATIVE));
                break;
            case DMXDialog.OPTIONS_YES_NO:
                builder.setPositiveButton(resourceHandler.getString("dialog_button_yes", "Yes"), new DialogButtonListener(POSITIVE));
                builder.setNegativeButton(resourceHandler.getString("dialog_button_no", "No"), new DialogButtonListener(NEGATIVE));
                break;
            case DMXDialog.OPTIONS_YES_NO_CANCEL:
                builder.setPositiveButton(resourceHandler.getString("dialog_button_yes", "Yes"), new DialogButtonListener(POSITIVE));
                builder.setNegativeButton(resourceHandler.getString("dialog_button_no", "No"), new DialogButtonListener(NEGATIVE));
                builder.setNeutralButton(resourceHandler.getString("dialog_button_cancel", "Cancel"), new DialogButtonListener(NEUTRAL));
                break;
            default:
        }
    }

    /**
     * Initialize buttons from specified array of options. Array should be
     * maximum of 3 elements, which represent positive, negative and neutral
     * buttons, in that order. Array elements can be either resource string
     * keys, or actual button text.
     *
     * @param options option strings
     */
    private void initButtons(String[] options) {
        if (options.length > 0) {
            builder.setPositiveButton(resourceHandler.getString(options[0], options[0]), new DialogButtonListener(POSITIVE));
        }
        if (options.length > 1) {
            builder.setNegativeButton(resourceHandler.getString(options[1], options[1]), new DialogButtonListener(NEGATIVE));
        }
        if (options.length > 2) {
            builder.setNeutralButton(resourceHandler.getString(options[2], options[2]), new DialogButtonListener(NEUTRAL));
        }
    }

    /**
     * Set callback interface for this dialog.
     *
     * @param callback callback interface
     */
    public void setCallback(DMXDialogOptionCallback callback) {
        this.callback = callback;
    }

    /**
     * Sets tag for this dialog. Default tag is dialog title, with spaces replaced
     * by underscores. For example, if dialog title is <code>My dialog</code>, default tag
     * value will be <code>My_dialog</code>.
     *
     * @param tag dialog tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return builder.create();
    }

    @Override
    public int showDialog() {
        show(parent.getSupportFragmentManager(), tag);
        return DMXDialog.DLG_OPTION_CLOSED;
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    @Override
    public DMXDialogView getDialogView() {
        return view;
    }

    /**
     * Listener for dialog buttons. This listener will call appropriate callback method of {@link callback} field.
     */
    private class DialogButtonListener implements DialogInterface.OnClickListener {

        /** Button option value. */
        private final int option;

        /**
         * Create new instance.
         * 
         * @param option option value.
         */
        public DialogButtonListener(int option) {
            this.option = option;
        }

        @Override
        public void onClick(DialogInterface di, int i) {
            switch (option) {
                case POSITIVE:
                    callback.onPositiveButton(DMXAndroidOptionsDialog.this);
                    break;
                case NEGATIVE:
                    callback.onNegativeButton(DMXAndroidOptionsDialog.this);
                    break;
                case NEUTRAL:
                    callback.onNeutralButton(DMXAndroidOptionsDialog.this);
                    break;
                default:
            }
        }
    }
}
