/**
 * ****************************************************************************
 *
 * Copyright 2012 - 2014 Vektor Software.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 ****************************************************************************
 */
package com.vektorsoft.demux.android.core.log;

import android.util.Log;
import com.vektorsoft.demux.core.log.DMXLogger;

/**
 * Android-specific implementation of {@link DMXLogger}. This class is a wrapper for Android {@code Log}.
 *
 * @author Vladimir Djurovic <vdjurovic@vektorsoft.com>
 */
public class AndroidLogger implements DMXLogger {
    
    /**
     * Log tag.
     */
    private final String tag;
    
    /**
     * Creates new instance.
     * 
     * @param tag log tag
     */
    public AndroidLogger(String tag){
        this.tag = tag;
    }

    @Override
    public void debug(String message) {
        Log.d(tag, message);
    }

    @Override
    public void debug(String message, Throwable throwable) {
        Log.d(tag, message, throwable);
    }
    

    @Override
    public void error(String message) {
        Log.e(tag, message);
    }

    @Override
    public void error(String message, Throwable throwable) {
        Log.e(tag, message, throwable);
    }

    @Override
    public void info(String message) {
        Log.i(tag, message);
    }

    @Override
    public void info(String message, Throwable throwable) {
        Log.i(tag, message, throwable);
    }
    

    @Override
    public void warn(String message) {
        Log.w(tag, message);
    }

    @Override
    public void warn(String message, Throwable throwable) {
        Log.w(tag, message, throwable);
    }
    
    
}
