/******************************************************************************
 * 
 * Copyright 2012 - 2014 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/

package com.vektorsoft.demux.android.core.resources;

/**
 *  Enumerates different types of Android resources which can be accessed from 
 * application. Each type has a name of <code>R</code> resource class associated.
 *
 * @author Vladimir Djurovic
 */
public enum AndroidResourceTypeEnum {
    
    /** Represent Android property animation type, which contains 
     * resources in <code>res/animator</code> folder. */
    PROPERTY_ANIMATION("animator"),
    /** Represents Android Tween animation type, which contains resources in 
     * <code>res/anim</code> folder. */
    TWEEN_ANIMATION ("anim"),
    /** Represents ANdroid frame animation, which contains resources 
     * in <code>res/drawable</code> folder. */
    FRAME_ANIMATION ("drawable");

    /** Name of R resource class. */
    private String name;
    
    /**
     * Creates new instance.
     * 
     * @param name R resource class name
     */
    private AndroidResourceTypeEnum(String name){
        this.name = name;
    }
    
    /**
     * Returns R resource class name.
     * 
     * @return  resource class name
     */
    public String getResourceClassName(){
        return name;
    }
}
