/******************************************************************************
 * 
 * Copyright 2012 - 2013 Vektor Software.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 *****************************************************************************/


package com.vektorsoft.demux.android.core.app;

import com.vektorsoft.demux.core.app.DMXApplication;
import java.util.Map;

/**
 * Android implementation of {@link DMXApplication}. This class does not support methods {@link DMXApplication#getArguments() } 
 * and {@link DMXApplication#getConfigurationParameters() }, since they are not applicable on Android platform.
 *
 * @author Vladimir Djurovic
 */
public class DMXAndroidApplication implements DMXApplication {
    
    /** Application name. */
    private String name;
    
    /** Application version. */
    private String version;
    
    /** Application build number. */
    private String buildNumber;
    
    /**
     * Creates new instance with default values.
     */
    public DMXAndroidApplication(){
        name = "DEMUX Android Application";
        version = "1.0.0";
        buildNumber = "1";
    }

    /**
     * Set application name.
     * 
     * @param name app name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String getName() {
        return name;
    }

    /**
     * Set application version.
     * 
     * @param version app version
     */
    public void setVersion(String version) {
        this.version = version;
    }
    

    @Override
    public String getVersion() {
        return version;
    }

    /**
     * Set application build number.
     * 
     * @param buildNumber app build number
     */
    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    
    @Override
    public String getBuildNumber() {
        return buildNumber;
    }

    @Override
    public boolean hasGUI() {
        return true;
    }

    /**
     * Not supported on Android. This method will throw {@link UnsupportedOperationException}.
     * 
     * @return args array.
     */
    @Override
    public String[] getArguments() {
        throw new UnsupportedOperationException("Not supported on Android."); 
    }

    /**
     * Not supported on Android. This method will throw {@link UnsupportedOperationException}.
     * 
     * @return map
     */
    @Override
    public Map<String, String> getConfigurationParameters() {
        throw new UnsupportedOperationException("Not supported on Android.");
    }

}
